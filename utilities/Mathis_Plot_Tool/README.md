# Mathis_Plot_Tool

Mathis_Plot_Tool.exe is a 64-bit Matlab application for Windows developed by Xavier Faure from CEA Tech. It makes it easy to create graphical representations of MATHIS results.

## Install

1. Install the version 9.6 of [Matlab Runtime](https://fr.mathworks.com/products/compiler/matlab-runtime.html)
2. Run Mathis_Plot_Tool.exe
3. Select a Mathis datafile which you previously ran
4. Enjoy
	
## Version Note 

- 20/11/2019 - first public release
- 17/06/2020 - Enhancement of timescale
- 18/06/2020 - Bug correction for TETA0<>0 in MATHIS datafile
- 21/06/2021 - Enhancement of histogram feature and change to version 9.6 of Matlab runtime


