function varargout = Mathis_Plot_Tool(varargin)
% MATHIS_PLOT_TOOL M-file for Mathis_Plot_Tool.fig
%      MATHIS_PLOT_TOOL, by itself, creates a new MATHIS_PLOT_TOOL or raises the existing
%      singleton*.
%
%      H = MATHIS_PLOT_TOOL returns the handle to a new MATHIS_PLOT_TOOL or the handle to
%      the existing singleton*.
%
%      MATHIS_PLOT_TOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MATHIS_PLOT_TOOL.M with the given input arguments.
%
%      MATHIS_PLOT_TOOL('Property','Value',...) creates a new MATHIS_PLOT_TOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Mathis_Plot_Tool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Mathis_Plot_Tool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Mathis_Plot_Tool

% Last Modified by GUIDE v2.5 17-Jun-2021 14:30:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Mathis_Plot_Tool_OpeningFcn, ...
    'gui_OutputFcn',  @Mathis_Plot_Tool_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Mathis_Plot_Tool is made visible.
function Mathis_Plot_Tool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Mathis_Plot_Tool (see VARARGIN)

% Choose default command line output for Mathis_Plot_Tool
handles.output = hObject;
handles.chemin = cd;
handles.multivarx = [];
set(handles.edit4,'enable','off')
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Mathis_Plot_Tool wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Mathis_Plot_Tool_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.listbox1,'string','Data files')
set(handles.listbox2,'string','Variables')
set(handles.listbox1,'value',1)
set(handles.listbox2,'value',1)
set(handles.edit4,'enable','off')
set(handles.axes2,'visible','on')
axes(handles.axes2)
if get(handles.radiobutton5,'value')
    hold off
    plot(0,0)
end
[nom,chemin] = uigetfile(strcat(handles.chemin,'\*.data'));
handles.chemin = chemin;
handles.base_nom = [];
set(handles.text1,'string',strcat(chemin,nom))
liste = dir(chemin);
jj=1;
lg = length(nom);
fichier_a_afficher = [];ind_ext=[];
for ii = 1:length(liste)
    if length(liste(ii).name)>lg
        if strcmp(liste(ii).name(1:lg),nom)
            if strcmp(liste(ii).name(end-3:end),'.res')
                indice_nom = strfind(liste(ii).name,'data'); %identification de 'data' pour ne garder que la fin du nom
                base_nom =  liste(ii).name(1:indice_nom-1); %c''est toujours la m�me donc ca ne retient que le dernier
                fichier_a_afficher{jj} = liste(ii).name(indice_nom:end);
                if strfind(char(fichier_a_afficher{jj}),'_ext.res')
                    ind_ext = jj;
                end
                jj=jj+1;
            end
        end
    end
end
if isempty(ind_ext)
    simu_offset = 367;
else
    copyfile(strcat(chemin,base_nom,fichier_a_afficher{ind_ext}),strcat('fichier_tempo'),'f')
    don = importdata('fichier_tempo');
    delete(strcat('fichier_tempo'))
    simu_offset = 366+don.data(1,2);
end
handles.time_unit =  {'D','H','M','S'};
handles.time_unit_convert = [1 24 24*60 24*60*60]; %vecteur de conversion des unit�s en jour
if isempty(fichier_a_afficher)
    set(handles.listbox1,'string',nom)
else
    set(handles.listbox1,'string',fichier_a_afficher)
end
handles.simu_offset = simu_offset;
handles.nom = nom;
handles.liste_fichier = fichier_a_afficher;
handles.base_nom = base_nom;
set(handles.listbox1,'Value',1)
set(handles.listbox1,'enable','on')
guidata(hObject, handles);

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.listbox2,'Value',1)
set(handles.listbox2,'string','Variables')
indice = get(handles.listbox1,'Value');
fichier = strcat(handles.base_nom,handles.liste_fichier{indice});

if strfind(fichier,'data_ATEC')
    message = 'This file must be seen directly in an editor software';
    erreur = errordlg(message,'Erreur');
    uiwait(erreur)
    return
end
%pause(1);
msg=waitbar(0.5,'Please wait...');
set(gcf,'Pointer','watch');

if isempty(handles.liste_fichier)
    copyfile(strcat(handles.chemin,handles.nom),strcat('fichier_tempo'),'f')
    don = importdata('fichier_tempo');
    delete(strcat('fichier_tempo'))
    liste_variable = don.textdata(2:end);
    handles.branche = don.data;
    handles.unite = don.textdata;
else
    copyfile(strcat(handles.chemin,fichier),strcat('fichier_tempo'),'f')
    branche = importdata('fichier_tempo');
    delete(strcat('fichier_tempo'))
    indice_sep = strfind(fichier,'_');
    if strfind(fichier,'histo')
        entete = branche.textdata;
        variable = char(entete(1));
        ind_unity = strfind(variable,'_');
        unite{1} = variable(ind_unity+1:end);
        for ii = 1:length(entete)-1
            liste_variable{ii} = entete{ii+1};
        end
        if strfind(fichier,'texpo')
            for ii = 1:length(entete);
                unite{ii+1} = unite{1};
            end
        else
            for ii = 1:length(entete);
                unite{ii+1} = unite{1};
            end
        end
        branche = branche.data;
    else
        entete = strcat(fichier(1:(indice_sep(end)-1)),'.head');
        
        if strcmp(fichier(end-6:end),'ext.res')
            %        ordre = branche.colheaders;
            entete = strcat(fichier(1:end-3),'head');   %rajout�
            %    else
        end      %rajout�
        if exist(strcat(handles.chemin,entete))~=0
            copyfile(strcat(handles.chemin,entete),strcat('fichier_tempo'),'f')
            ordre = importdata('fichier_tempo');
            delete(strcat('fichier_tempo'))
        else
            ordre = branche.textdata;
        end
        %    end
        for jj = 1:(length(ordre)-1)
            liste_variable{jj} = ordre{jj+1};
        end
        if isstruct(branche)
            inter = branche.textdata;
            branche = branche.data;
            %        if ~isempty(strfind(fichier,'_ext'))
            %            for jj = 1:length(inter);
            %                unite{jj} = inter{jj};
            %            end
            %        else
            if length(inter)==1
                inter = inter{:};
                espace = find(inter==' ');
                unite{1} = inter(1:espace(2)-1);
                for jj = 1:(length(branche(1,:))-1)
                    unite{jj+1}=inter(espace(2)+1:end);
                end
            else
                inter_vrai = '';
                for jj = 1:(length(inter))
                    ss_inter = inter{jj};
                    inter_vrai = strcat(inter_vrai,'_',ss_inter);
                end
                espace = find(inter_vrai=='_');
                unite{1} = inter_vrai(2:espace(3)-1);
                for jj = 1:(length(branche(1,:))-1)
                    unite{jj+1}=inter_vrai(espace(3)+1:end);
                end
            end
            
            %        end
        end
    end
    handles.branche = branche;
    handles.unite = unite;
end
set(gcf,'Pointer','arrow');
close(msg)
set(handles.listbox2,'string',liste_variable)
set(handles.listbox2,'Value',1)
set(handles.listbox2,'enable','on')
handles.liste_variable = liste_variable;
%set(handles.radiobutton5,'value',1)
handles.legende = [];
guidata(hObject, handles);


% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
indice_var = get(handles.listbox2,'Value');
variable = handles.liste_variable{indice_var};
set(handles.axes2,'visible','on')
axes(handles.axes2)
if get(handles.radiobutton5,'value')
    hold off
    plot(0,0)
    handles.legende=[];
end
if get(handles.radiobutton6,'value') && isempty(handles.multivarx)
    hold off
    plot(0,0)
    handles.legende=[];
    handles.multivarx = handles.branche(:,indice_var+1);
    xlabel(strcat(handles.unite{indice_var+1},'-',variable),'interpreter','none');
    guidata(hObject, handles);
    hold on
    return
end
nb_crb = length(handles.legende)+1;
if nb_crb>1
    hold all
end
if get(handles.radiobutton3,'value')
    legende = get(legend(handles.axes2),'string');
    legende = [legende,variable];
    %leg_couleur = [leg_couleur,couleur(nb_crb)];
else
    legende = [variable];
    %leg_couleur = couleur(nb_crb);
end
%couleur = 'kbyrgkbyrgkbyrgkbyrg';
if get(handles.radiobutton6,'value') && ~isempty(handles.multivarx)
    if length(handles.multivarx)~=length(handles.branche(:,indice_var+1))
        message = 'The selected variable do not have the same length, Please select an other one.';
        erreur = errordlg(message,'Error');
        uiwait(erreur)
        handles.multivarx = [];
        guidata(hObject, handles);
        return
    else
        plot(handles.multivarx,handles.branche(:,indice_var+1),'.')
        ylabel(strcat(handles.unite{indice_var+1},'-',variable),'interpreter','none');
        handles.multivarx = [];
    end
else
    for ii= 1:length(handles.time_unit)
        tps_courant = handles.unite{1};
        if contains(tps_courant(5:end),handles.time_unit{ii})
            sim_unit = handles.time_unit_convert(ii);
        end
    end
    tps = datetime(datevec(handles.simu_offset+(handles.branche(:,1)-handles.branche(1,1))/sim_unit));
    plot(tps,handles.branche(:,indice_var+1))
    xlabel(handles.unite{1},'interpreter','none');
    ylabel(handles.unite{indice_var+1},'interpreter','none');
end
box on
grid on
hold all

leg=legend(legende);
set(leg,'Interpreter','none')
handles.legende = legende;
handles.variable = variable;
handles.indice_var = indice_var;
%handles.leg_couleur = leg_couleur;
guidata(hObject, handles);
% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
premier = 0;
h = findobj('Filename','Figure_export');
if isempty(h)
    handles.figure_export = figure;
    set(handles.figure_export,'Filename','Figure_export');
    guidata(hObject, handles);
    figure(handles.figure_export);
    premier = 1;
else
    message='Append an existing figure ?';
    button = questdlg(message,'??','Yes','No','Yes');
    satisfait=0;
    if strcmp(button,'Yes')
        while satisfait==0
            if length(h)==1
                figure(h);
                satisfait = 1;
            else
                prompt = {'Please indicate which figure to append :'};
                dlg_title = 'Figure''s nb';
                num_lines = 1;
                def = {'1'};
                answer = inputdlg(prompt,dlg_title,num_lines,def);
                if isempty(answer)
                    return
                else
                    if isempty(find(h==str2num(answer{1}))) || length(answer)~=1
                        errordlg('Sorry, I didn''t find the figure, please try again');
                        satisfait=0;
                        uiwait(gcf)
                    else
                        figure(h(find(h==str2num(answer{1}))));
                        satisfait=1;
                    end
                end
            end
        end
        col = 'b';
    else
        handles.figure_export = figure;
        set(handles.figure_export,'Filename','Figure_export');
        guidata(hObject, handles);
        figure(handles.figure_export);
    end
end
hold all, grid on, box on
a = get(handles.axes2,'children');
b = get(handles.axes2);
if premier == 0
    legende = get(legend(gca),'string');
end
%signe = 'kryg';
a = flipud(a);
for ii=1:length(a);
    liste = get(a(ii));
    if length(liste.XData)>1
        if get(handles.radiobutton7,'value')
            plot(liste.XData,liste.YData,'LineStyle',liste.LineStyle,'Marker',liste.Marker);%,'Color',liste.Color,'LineWidth',liste.LineWidth);
        else
            if exist('vary') == 0
                nbvar = 1;
            else
                nbvar = nbvar+1;
            end
            vary(nbvar,:) = liste.YData;
%             [vary,varx] = hist(liste.YData,50);
%             plot(varx,vary)
        end
        legende_g{ii} = liste.DisplayName;
    end
end
if get(handles.radiobutton7,'value')
    xlabel(get(b.XLabel,'String'),'interpreter','none')
    ylabel(get(b.YLabel,'String'),'interpreter','none')
else
    bins = linspace(min(vary,[],'all'),max(vary,[],'all'),str2num(get(handles.edit4,'string')));
    for ii=1:length(vary(:,1))
        [Newvary,varx] = hist(vary(ii,:),bins);
        plot(varx,Newvary/length(vary(ii,:))*100)
    end
    xlabel(get(b.YLabel,'String'),'interpreter','none')
    ylabel('% of Events ')
end
if premier == 0
    legende = [legende,legende_g];
else
    legende = legende_g;
end
leg=legend(legende);
set(leg,'Interpreter','none')


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hold off
plot(0,0)
handles.legende=[];
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton6



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    nbbins = str2num(get(handles.edit4,'string'));
    if isinteger(nbbins)==0
        set(handles.edit4,'string',num2str(round(nbbins(1))));
    end
    if nbbins<2
        set(handles.edit4,'string',num2str(2))
    end
catch
    set(handles.edit4,'string','50');
end
% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit4,'enable','on')
% Hint: get(hObject,'Value') returns toggle state of radiobutton8


% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit4,'enable','off')
% Hint: get(hObject,'Value') returns toggle state of radiobutton7
