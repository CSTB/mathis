!***********************************************************************
!Copyright 2018, Fran�ois Demouge
!
!Copying and distribution of this file, with or without modification,
!are permitted in any medium without royalty provided the copyright
!notice and this notice are preserved.  This file is offered as-is,
!without any warranty.
!***********************************************************************
MODULE TYPE_USR_MODULE

TYPE TYPE_USR
    INTEGER								:: IDN=0
    INTEGER								:: IDNMOD=0
!***********************************
!Definition du type*****************
!PARAMETRES
    CHARACTER(100)						:: ID='USR'
    CHARACTER(256)						:: MODTYPE='null'
    INTEGER                             :: NOUTPUTS=10
!***********************************
!Declarer ici les variables constituant les param�tres avec leurs valeurs par d�faut
!d�but******************************


!fin********************************
!***********************************
!CONSTANTES*************************
!Declarer ici les variables  qui seront �ventuellement initialis�es par la fonction INIT_CONS_USR
!d�but******************************
 
 
!fin********************************       
!***********************************        
!VARIABLES**************************
!D�clarer ici les variables  qui seront �ventuellement initialis�es par la fonction INIT_VAR_USR
!d�but******************************



!fin******************************** 
!***********************************       
!IMAGES*****************************
!Declarer ici les pointeurs  qui seront cr��s par la fonction INIT_IMAGE_USR
!d�but******************************

!fin********************************
END TYPE TYPE_USR

END MODULE TYPE_USR_MODULE


MODULE DATA_USR_MODULE

	USE TYPE_USR_MODULE

	IMPLICIT NONE

	TYPE(TYPE_USR),DIMENSION(:),ALLOCATABLE,TARGET	:: USRRT 
	INTEGER											:: N_USR=0,N_USR_SET=0

END MODULE DATA_USR_MODULE

MODULE READ_NML_USR_MODULE

	USE TYPE_MATHIS_MODULE
	USE TYPE_USR_MODULE
	USE DATA_USR_MODULE

	IMPLICIT NONE
!***********************************
	CHARACTER(100)			:: ID
    CHARACTER(256)			:: MODTYPE
    INTEGER                :: NOUTPUTS
!Declaration des variables de la namelist (doit correspondre aux param�tres du type)
!d�but******************************           
 
 
!fin********************************
!***********************************
!
!***********************************
!Declaration de la namelist contenant tous les param�tres de l'objet 
!d�but******************************           
	NAMELIST /MOD/ ID,MODTYPE,NOUTPUTS
!fin********************************
!***********************************
CONTAINS

SUBROUTINE DEFAULT_NML_USR

	TYPE(TYPE_USR)		:: OBJ
			 
	ID=OBJ%ID
	MODTYPE=OBJ%MODTYPE
    NOUTPUTS=OBJ%NOUTPUTS
!*******************************
!on met les valeurs par d�faut des param�tres  dans les variables de la namelist
!d�but******************************
 
 
!fin********************************  
END SUBROUTINE DEFAULT_NML_USR

SUBROUTINE SET_NML_USR(OBJ,IDN,IDNMOD)

	TYPE(TYPE_USR),INTENT(INOUT)	:: OBJ
	INTEGER,INTENT(IN)				:: IDN,IDNMOD

	OBJ%IDN=IDN			 
	OBJ%IDNMOD=IDNMOD			 
	OBJ%ID=ID
	OBJ%MODTYPE=MODTYPE
    OBJ%NOUTPUTS=NOUTPUTS
!*******************************
!on affecte les valeurs de la namelist saisie par l'utilisateur aux param�tres de l'objet
!d�but******************************
 
 
!fin********************************  
END SUBROUTINE SET_NML_USR

SUBROUTINE COUNT_NML_USR(INPUT_FILE,IDNMOD) 
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'COUNT_NML_USR' :: COUNT_NML_USR

	CHARACTER(256),INTENT(IN)						:: INPUT_FILE
	INTEGER,INTENT(IN)								:: IDNMOD
	CHARACTER(256)									:: MESSAGE
	   
	INTEGER											:: I,IMOD,IOS

	IF (ALLOCATED(USRRT)) THEN
        DEALLOCATE(USRRT) 
	    N_USR=0
        N_USR_SET=0
    ENDIF

	IMOD=0
	OPEN(LU3,FILE=INPUT_FILE,ACTION='READ')
	REWIND(LU3)
	COUNT_OBJ_LOOP: DO
		CALL CHECKREAD('MOD',LU3,IOS)
		IF (IOS==0) EXIT COUNT_OBJ_LOOP
		IMOD=IMOD+1
		READ(LU3,NML=MOD,END=209,ERR=210,IOSTAT=IOS)
		IF (IMOD==IDNMOD) THEN
			N_USR=N_USR+1			
		ENDIF
		CYCLE COUNT_OBJ_LOOP
		210 IF (IMOD==IDNMOD) THEN
			WRITE(0,NML=MOD)
			WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &MOD line number ',IDNMOD,': check parameters names and values'
			CALL SHUTDOWN(MESSAGE,0)
		ENDIF		
	ENDDO COUNT_OBJ_LOOP
	209 CLOSE(LU3)
	
END SUBROUTINE COUNT_NML_USR

SUBROUTINE READ_NML_USR(INPUT_FILE,IDNMOD,IDNUSR)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'READ_NML_USR' :: READ_NML_USR

	CHARACTER(256),INTENT(IN)						:: INPUT_FILE
	INTEGER,INTENT(IN)								:: IDNMOD
	INTEGER,INTENT(OUT)								:: IDNUSR
	INTEGER											:: IDN
	CHARACTER(256)									:: MESSAGE
	INTEGER											:: IMOD,IOS


	IF (.NOT.ALLOCATED(USRRT)) THEN
		ALLOCATE(USRRT(N_USR))
	ENDIF
	IDN=0
	IMOD=0
	OPEN(LU3,FILE=INPUT_FILE,ACTION='READ')
	IF (N_USR.NE.0) THEN
		REWIND(LU3)
		SET_OBJ_LOOP: DO 	
			CALL CHECKREAD('MOD',LU3,IOS) 
			IF (IOS==0) EXIT SET_OBJ_LOOP
			IMOD=IMOD+1
			CALL DEFAULT_NML_USR
			READ(LU3,NML=MOD,END=209,ERR=210,IOSTAT=IOS)
			IF (IMOD==IDNMOD) THEN
				N_USR_SET=N_USR_SET+1				
				CALL SET_NML_USR(USRRT(N_USR_SET),N_USR_SET,IMOD)
                IDNUSR=N_USR_SET
				EXIT SET_OBJ_LOOP
			ENDIF
		210 CONTINUE
		ENDDO SET_OBJ_LOOP
		209 CLOSE(LU3)
	ENDIF
		
END SUBROUTINE READ_NML_USR

SUBROUTINE ADD_OBJ_USR(USRBUILD,IDNUSR)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'ADD_OBJ_USR' :: ADD_OBJ_USR
    TYPE(TYPE_BUILDING),INTENT(OUT)                 :: USRBUILD
    INTEGER ,INTENT(IN)                             :: IDNUSR
    TYPE(TYPE_USR),pointer                          :: OBJ
    !***********************************
!d�clarations de variables internes � la subroutine
!d�but******************************
 
 
!fin******************************** 
    NULLIFY(OBJ);OBJ=>USRRT(IDNUSR)
    NULLIFY(USRBUILD%MISC)
    NULLIFY(USRBUILD%EXT)
    NULLIFY(USRBUILD%SPEC)
    NULLIFY(USRBUILD%BOUND)
    NULLIFY(USRBUILD%LOC)
    NULLIFY(USRBUILD%BRANCH)
    NULLIFY(USRBUILD%HSRC)
    NULLIFY(USRBUILD%PERSON)
    NULLIFY(USRBUILD%WALL)
    NULLIFY(USRBUILD%CTRL)
    NULLIFY(USRBUILD%MAT)
    NULLIFY(USRBUILD%SURF)
    NULLIFY(USRBUILD%MOD)
!***********************************
!cr�ation d'objet MATHIS :
!rappel : � ce stade, seuls les param�tres du MOD peuvent �tre exploit�s
!d�but******************************
 
 
!fin********************************    

END SUBROUTINE ADD_OBJ_USR

END MODULE READ_NML_USR_MODULE


MODULE PROC_USR_MODULE

	USE TYPE_MATHIS_MODULE
	USE DATA_USR_MODULE
    
	IMPLICIT NONE

CONTAINS

SUBROUTINE INIT_CONS_USR(IDNUSR,BUILDING)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'INIT_CONS_USR' :: INIT_CONS_USR
	INTEGER,INTENT(IN)			:: IDNUSR
    TYPE(TYPE_BUILDING),INTENT(INOUT)  :: BUILDING
    TYPE(TYPE_USR),pointer      :: OBJ				
!***********************************
!d�clarations de variables internes � la subroutine
!d�but******************************
 
 
!fin********************************  
    NULLIFY(OBJ);OBJ=>USRRT(IDNUSR)
	BUILDING%MOD(OBJ%IDNMOD)%NOUTPUTS=OBJ%NOUTPUTS
!***********************************
!op�rations r�alis�es lors de l'initialisation des constantes de l'objet OBJ
!d�but******************************
 
 
!fin********************************
END SUBROUTINE INIT_CONS_USR

SUBROUTINE INIT_VAR_USR(IDNUSR,BUILDING)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'INIT_VAR_USR' :: INIT_VAR_USR
	INTEGER,INTENT(IN)			:: IDNUSR
    TYPE(TYPE_BUILDING),INTENT(INOUT)  :: BUILDING
    TYPE(TYPE_USR),pointer      :: OBJ				
!**********************************
!d�clarations de variables internes � la subroutine
!d�but******************************

    
!fin********************************    
    NULLIFY(OBJ);OBJ=>USRRT(IDNUSR)
!***********************************
!op�rations r�alis�es lors de l'initialisation des variables  de l'objet OBJ
!d�but******************************

    
!fin********************************
END SUBROUTINE INIT_VAR_USR

SUBROUTINE INIT_IMAGE_USR(IDNUSR,BUILDING)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'INIT_IMAGE_USR' :: INIT_IMAGE_USR
	INTEGER,INTENT(IN)			:: IDNUSR
    TYPE(TYPE_BUILDING),INTENT(INOUT)  :: BUILDING
    TYPE(TYPE_USR),pointer      :: OBJ
!***********************************
!d�clarations de variables internes � la subroutine
!d�but******************************

    
!fin********************************  
    NULLIFY(OBJ);OBJ=>USRRT(IDNUSR)
!op�rations r�alis�es lors de la cr�ation des images de l'objet OBJ
!d�but******************************
  
  
!fin********************************
END SUBROUTINE INIT_IMAGE_USR

SUBROUTINE FLUX_ME_USR(IDNUSR,BUILDING,OUTPUTS,F)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'FLUX_ME_USR' :: FLUX_ME_USR
	INTEGER,INTENT(IN)			:: IDNUSR
    TYPE(TYPE_BUILDING),INTENT(INOUT)  :: BUILDING
    TYPE(FLUX_ME),INTENT(INOUT) :: F
    DOUBLE PRECISION,DIMENSION(:),INTENT(INOUT)  :: OUTPUTS
    TYPE(TYPE_USR),pointer      :: OBJ
!***********************************
!d�clarations de variables internes � la subroutine
!d�but******************************
    
    
!fin********************************  
    NULLIFY(OBJ);OBJ=>USRRT(IDNUSR)
!op�rations r�alis�es au cours des it�rations du solveur
!pour le calcul des flux de masse et d'energie ou d'information g�n�r�s par l'objet OBJ
!d�but******************************
  
  
!fin********************************
END SUBROUTINE FLUX_ME_USR

SUBROUTINE UPDATE_USR(IDNUSR,FLAG,BUILDING,OUTPUTS)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'UPDATE_USR' :: UPDATE_USR
	INTEGER,INTENT(IN)			:: IDNUSR,FLAG
    TYPE(TYPE_BUILDING),INTENT(INOUT)  :: BUILDING
    DOUBLE PRECISION,DIMENSION(:),INTENT(INOUT)  :: OUTPUTS
    TYPE(TYPE_USR),pointer      :: OBJ
!***********************************
!d�clarations de variables internes � la subroutine
!d�but******************************
    
    
!fin********************************  
    NULLIFY(OBJ);OBJ=>USRRT(IDNUSR)
!op�rations r�alis�es � la fin d'un pas de temps (apr�s les it�rations du solveur)
!d�but******************************

    
!fin********************************
END SUBROUTINE UPDATE_USR

SUBROUTINE REWIND_USR(IDNUSR,FLAG,BUILDING,OUTPUTS)
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'REWIND_USR' :: REWIND_USR
	INTEGER,INTENT(IN)			:: IDNUSR,FLAG
    TYPE(TYPE_BUILDING),INTENT(INOUT)  :: BUILDING
    DOUBLE PRECISION,DIMENSION(:),INTENT(INOUT)  :: OUTPUTS
    TYPE(TYPE_USR),pointer      :: OBJ
!***********************************
!d�clarations de variables internes � la subroutine
!d�but******************************
    
    
!fin********************************  
    NULLIFY(OBJ);OBJ=>USRRT(IDNUSR)
!op�rations r�alis�es lorsque l'on revient en arri�re apr�s r�duction du pas de temps suite � une mauvaise convergence
!d�but******************************

    
!fin********************************
END SUBROUTINE REWIND_USR

!***********************************
!ajouter ici des subroutines � sa convenance
!debut


!fin
END MODULE PROC_USR_MODULE
