# MATHIS Users Models Dll Extensions 

MATHIS users can developped their own models and use them through the keyword &MOD/ (see programmer's guide, not written yet...).

We provide here the fortran template usermod_template.f90 to be used along with module_type_mathis.f90 for creating a dynamic library including the user's model.
Copying and distribution of those two files, with or without modification, are permitted in any medium without royalty provided the copyright notice present at the beginning of the files are preserved.
Theses files are offered as-is, without any warranty.
