# Linux Compilation

## Intel Fortran installation:

```
wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB
sudo apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB
rm GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB
sudo add-apt-repository "deb https://apt.repos.intel.com/oneapi all main"
sudo apt update
sudo apt install intel-oneapi-compiler-fortran
```

## Set an alias in the .bashrc file to avoid possible conflicts between MPI Intel Fortran versions:

```
alias intelSetEnv='source /opt/intel/oneapi/setvars.sh'
```

## Retrieve MINPACK sources:

Get the hybrd1.f source code and dependancies from [netlib](https://www.netlib.org/cgi-bin/netlibfiles.pl?filename=/minpack/hybrd1.f).
Unzip and place all the .f files in ~/mathis/source/minpack/   (adapt the absolute path)

## Open a terminal in the current directory (~/mathis/utilities/linux/, adapt the absolute path) and type:

```
intelSetEnv
chmod +x make_Mathis.sh
./make_Mathis.sh
```

## Set an alias in the .bashrc file so that the mathis command is recognized (adapt the absolute path):
 
``` 
alias mathis=~/mathis/utilities/linux/mathis
```

## Note: 

For cases with a large number of nodes and branches, the following command may be required before running mathis:

```
ulimit -s unlimited
```



