#!/bin/bash
#source ~/.bashrc
#shopt -s expand_aliases


ifx \
../../source/minpack/*.f \
../../source/module_type_mathis.f90 \
../../source/module_flux_me.f90 \
../../source/data* \
../../source/module_global_var.f90 \
../../source/module_dynamic_load.f90 \
../../source/read_nml* \
../../source/debouvent.f \
../../source/proc* \
../../source/module_read_data.f90 \
../../source/module_radiation.f90 \
../../source/module_enviro.f90 \
../../source/module_wall_solver.f90 \
../../source/module_post_proc.f90 \
../../source/module_post_atec.f90 \
../../source/module_solver.f90 \
../../source/main.f90 \
-o mathis -ldl -fpic  -static-intel -fpp -Wl,-rpath,'$ORIGIN'

#for compilation in debug mode change the last line by the following:
#-o mathis -ldl -g -traceback -fpic -static-intel -fpp -Wl,-rpath,'$ORIGIN'

rm ./*mod
