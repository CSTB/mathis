# Contributing 

## Contact

You can contact us at: francois.demouge@cstb.fr  

## Documentation

We attempt to write clear documentation in both the Technical Guide and the User's Guide. 
However, we do not always explain well concepts that we consider "obvious". 
As a result, documentation may not be as clear or complete as it should be. If you have struggled with 
some MATHIS concept or had to make multiple tries to get an input to work correctly, then help us by 
writing changes for that portion of the Technical Guide or User's Guide.

We have to:

- [ ] update the User's Guide with lastly developped keywords;
- [ ] write a Programmer's Guide highlighting the code structure and explaining the *&MOD/* Functionality;
- [ ] collect user feedback and integrate them in the different guides; 

## Reporting bugs 

If you have a case that gives an error message, use a debug version of MATHIS
and attempt to locate at least the general source of the error before making an issue report. Also, before 
sending us a datafile reproducing a bug, please make it as simple as possible in order for us to spend as 
little time as possible to understand the problem origin.
 
## *&MOD/* Functionality

We believe that the principle of MATHIS extensions through users dynamic libraries 
is a great feature and should become the first way to improve the tool with new models. 
User feedback is hugely welcome. 

We have to:

- [ ] explain how *&MOD/* works;
- [ ] create some examples;
- [ ] edit/write relevant comments in *usermod_template.f90*;
- [ ] edit/write comments of all variables in *module_type_mathis.f90*;
- [ ] create a Documentation Template for *&MOD/* sharing; 

## Build Instructions

MATHIS is currently compiled using Intel(R) Visual Fortran Compiler (2024.0 or later) and Microsoft Visual Studio 2022, 
for Windows Platforms. We provide the correspondings Mathis.sln and Mathis.vfproj files. Instruction for linux compilation 
can be found in /utilities/linux directory

Any help is welcome to create and share makefiles and relevant supports for various compilers/OS.

## V&V, CI/CD

Everything has to be constructed here. While we perform from time to time verification tests 
as well as validation against some experiments or others numerical tools, 
no automated procedure has been set up for continuous V&V or continuous integration/deployement.



