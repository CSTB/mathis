## Code Documentation using Doxygen

This directory contains a configuration file, `Doxyfile`, for use with the code documentation program [Doxygen](http://www.doxygen.org).

If Doxygen and [GraphViz](https://graphviz.org/) are installed on your system, type:
```
doxygen Doxyfile
```
and a new directory called `html` will be created containing an HTML version of documentation for the MATHIS code.  The main page is `html/index.html`.

Otherwise, you can consult the documentation here: [https://cstb.gitlab.io/mathis](https://cstb.gitlab.io/mathis)