    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE DATA_MUR_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    TYPE(TYPE_MUR),DIMENSION(:),ALLOCATABLE,TARGET	:: MURRT

    ! SAM tableaux d'indices sur les murs "thermiques" ou "hygro"
    INTEGER,DIMENSION(:),ALLOCATABLE                :: WHIP,WTIP
    INTEGER											:: N_MUR,N_WTHERM,N_WHYGRO


    CONTAINS

    SUBROUTINE TRANSLATE_WALLTYPE(OBJ)

        TYPE(TYPE_MUR),INTENT(INOUT)	:: OBJ

        OBJ%enum_walltype = wall_DEFAULT

        ! definition du type enum
        SELECT CASE (OBJ%WALLTYPE)
            CASE ('THERMAL')
                OBJ%enum_walltype = wall_thermal

            CASE ('HYGROTHERMAL')
                OBJ%enum_walltype = wall_hygrothermal
        END SELECT

        if(OBJ%enum_walltype == wall_DEFAULT) then
            print*, "Be carreful : Wall Type ", OBJ%WALLTYPE, " is unknown !"
        endif

    END SUBROUTINE TRANSLATE_WALLTYPE


    END MODULE DATA_MUR_MODULE
