    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE WALL_SOLVER_MODULE

    USE DATA_MUR_MODULE,ONLY: TYPE_MUR,MURRT,N_MUR,N_WTHERM,N_WHYGRO,WHIP,WTIP
    USE PROC_MUR_MODULE,ONLY: FLUX_ME_MURRT,EVAL_MUR_BOUNDS,EVAL_MUR_HYGROVARS
    USE GLOBAL_VAR_MODULE,ONLY:	LUOUT,RADIATION,DEBUG,DEBUGPLUS,TIME,DTIME,TIMESCALE,TIMEUNIT,TREF,TWTOL,HRWTOL,HRWRELAX,NHTWSTATMAX,LV,CPW,SUPERSATURATION,HYBRIDTOL,RHOH2O
    USE RADIATION_MODULE,ONLY: CALC_RADIATION

    IMPLICIT NONE

    INTEGER													:: UPDATE,HYGROCALL,HRWFLAG
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	            :: YY0,YYTAMPON


    ! SAM : pour debug, comptabilise le nombre d'appels par le solveur
    integer                                                 :: G_SAM_Nb_Calls, G_SAM_Nb_Calls_Total=0

    CONTAINS

    SUBROUTINE SOLVE_THERMAL_WALLS(TEMPCALL)

    INTEGER,INTENT(IN)							:: TEMPCALL
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: RES,WA,X
    INTEGER										:: IWT,I,J,INFO,LWA,N,JFLAG

    N=2*N_WTHERM
    IF (N_WTHERM.NE.0) THEN
        ALLOCATE(X(N))
        LWA=(N*(3*N+13))/2
        ALLOCATE(WA(LWA))
        ALLOCATE(RES(N))
        J=0
        DO IWT=1,N_WTHERM
            I=WTIP(IWT)
            X(J+1)=MURRT(I)%TPIN
            X(J+2)=MURRT(I)%TPOUT
            J=J+2
        ENDDO

        IF (TEMPCALL==0) THEN
            UPDATE=2

            CALL HYBRD1(RES_FLUX_THERMAL_WALLS,N,X,RES,HYBRIDTOL,INFO,WA,LWA) !RESOLUTION DU CHAMP DE TEMPERATURE STATIONNAIRE
        ELSEIF (TEMPCALL==1) THEN
            UPDATE=0
            CALL HYBRD1(RES_FLUX_THERMAL_WALLS,N,X,RES,HYBRIDTOL,INFO,WA,LWA) !RESOLUTION DU CHAMP DE TEMPERATURE INSTATIONNAIRE
        ELSEIF (TEMPCALL==2) THEN
            UPDATE=1
            CALL RES_FLUX_THERMAL_WALLS(N,X,RES,JFLAG)				   !MISE A JOUR DU CHAMP DE TEMPERATURE
        ENDIF

        DEALLOCATE(RES)
        DEALLOCATE(WA)
        DEALLOCATE(X)
    ENDIF

    END SUBROUTINE SOLVE_THERMAL_WALLS

    SUBROUTINE RES_FLUX_THERMAL_WALLS(N,X,RES,JFLAG)

    INTEGER										:: N,JFLAG,K,J,I,IWT
    DOUBLE PRECISION,DIMENSION(N)				:: X,RES
    DOUBLE PRECISION,DIMENSION(2)				:: TGRAD

    J=0
    DO IWT=1,N_WTHERM
        I=WTIP(IWT)
        MURRT(I)%TPIN=MAX(200.D0,MIN(X(J+1),2500.D0))
        MURRT(I)%TPOUT=MAX(200.D0,MIN(X(J+2),2500.D0))
        J=J+2
    ENDDO

    IF (RADIATION.EQV..TRUE.) THEN
        CALL CALC_RADIATION
    ENDIF

    J=0
    DO IWT=1,N_WTHERM
        I=WTIP(IWT)
        CALL EVAL_MUR_BOUNDS(MURRT(I))
        IF (UPDATE==2) THEN
            CALL THERMAL_WALL_CONDUCTION(UPDATE,MURRT(I)%TPIN,MURRT(I)%TPOUT,5.D16,MURRT(I)%MAT%LAMBDA,MURRT(I)%MAT%CS,MURRT(I)%MAT%RHO,MURRT(I)%TEMP,&
                MURRT(I)%DX,MURRT(I)%NUMNODE,MURRT(I)%NSLAB,&
                MURRT(I)%NETQ(1),MURRT(I)%NETQ(2),1,TGRAD)
        ELSE
            CALL THERMAL_WALL_CONDUCTION(UPDATE,MURRT(I)%TPIN,MURRT(I)%TPOUT,DTIME,MURRT(I)%MAT%LAMBDA,MURRT(I)%MAT%CS,MURRT(I)%MAT%RHO,MURRT(I)%TEMP,&
                MURRT(I)%DX,MURRT(I)%NUMNODE,MURRT(I)%NSLAB,&
                MURRT(I)%NETQ(1),MURRT(I)%NETQ(2),1,TGRAD)
        ENDIF
        RES(J+1)=MURRT(I)%NETQ(1)+MURRT(I)%MAT(1)%LAMBDA*TGRAD(1)
        RES(J+2)=MURRT(I)%NETQ(2)+MURRT(I)%MAT(MURRT(I)%NSLAB)%LAMBDA*TGRAD(2)
        J=J+2
    ENDDO

    END SUBROUTINE RES_FLUX_THERMAL_WALLS

    SUBROUTINE THERMAL_WALL_CONDUCTION(UPDATE,TEMPIN,TEMPOUT,DT,WK,WSPEC,WRHO,WTEMP,WALLDX,NUMNODE,NSLAB,WFLUXIN,WFLUXOUT,IWBOUND,TGRAD)

    IMPLICIT NONE

    INTEGER,PARAMETER             :: NN=100
    DOUBLEPRECISION,DIMENSION(NN) :: A, B, C, TNEW, TDERIV
    DOUBLEPRECISION,DIMENSION(3)  :: DDIF
    DOUBLEPRECISION,DIMENSION(2)  :: TGRAD
    DOUBLEPRECISION,DIMENSION(*)  :: WK, WSPEC, WRHO, WTEMP, WALLDX
    INTEGER,DIMENSION(*)          :: NUMNODE
    INTEGER                       :: UPDATE,NX,I,IWBOUND,IEND,ISLAB,NSLAB,NINTX,IBEG,IBREAK
    DOUBLE PRECISION              :: TEMPIN,WFLUXIN,XKRHOC,S,DT,HI,HIM1,TEMPOUT,WFLUXOUT

    NX = NUMNODE(1)

    DO I = 2, NX - 1
        TNEW(I) = WTEMP(I)
    ENDDO

    IF(IWBOUND.NE.4)THEN
        A(1) = 1.0D0
        B(1) = 0.0D0
        C(1) = 0.0D0
        TNEW(1) = TEMPIN
    ELSE
        A(1) = 1.0D0
        B(1) = 0.0D0
        C(1) = -1.0D0
        TNEW(1) = WALLDX(1) * WFLUXIN / WK(1)
    ENDIF

    IEND = 0
    DO ISLAB = 1, NSLAB
        NINTX = NUMNODE(1+ISLAB)
        XKRHOC = WK(ISLAB) / (WSPEC(ISLAB)*WRHO(ISLAB))
        S = 2.0D0 * DT * XKRHOC
        IBEG = IEND + 2
        IEND = IBEG + NINTX - 1
        DO I = IBEG, IEND
            HI = WALLDX(I)
            HIM1 = WALLDX(I-1)
            A(I) = 1.0D0 + S / (HI*HIM1)
            B(I) = -S / (HIM1*(HI+HIM1))
            C(I) = -S / (HI*(HI+HIM1))
        ENDDO
    ENDDO

    IBREAK = 1
    DO ISLAB = 2, NSLAB
        NINTX = NUMNODE(ISLAB)
        IBREAK = IBREAK + NINTX + 1
        B(IBREAK) = WK(ISLAB-1)  / WALLDX(IBREAK-1)
        C(IBREAK) = WK(ISLAB)  / WALLDX(IBREAK)
        A(IBREAK) = -(B(IBREAK)+C(IBREAK))
        TNEW(IBREAK) = 0.0D0
    ENDDO

    IF (IWBOUND.EQ.1) THEN
        A(NX) = 1.0D0
        B(NX) = 0.0D0
        C(NX) = 0.0D0
        TNEW(NX) = TEMPOUT
    ELSEIF (IWBOUND.EQ.2) THEN
        A(NX) = 1.0D0
        B(NX) = -1.0D0
        C(NX) = 0.0D0
        TNEW(NX) = 0.0D0
    ELSEIF (IWBOUND.EQ.3.OR.IWBOUND.EQ.4) THEN
        A(NX) = 1.0D0
        B(NX) = -1.0D0
        C(NX) = 0.0D0
        TNEW(NX) = WALLDX(NX-1) * WFLUXOUT / WK(NSLAB)
    ENDIF

    C(1) = C(1) / A(1)
    DO I = 2, NX - 1
        A(I) = A(I) - B(I) * C(I-1)
        C(I) = C(I) / A(I)
    ENDDO
    A(NX) = A(NX) - B(NX) * C(NX-1)
    DO I = 1, NX
        TDERIV(I) = 0.0D0
    ENDDO
    TDERIV(1) = 1.0D0

    TNEW(1) = TNEW(1) / A(1)
    TDERIV(1) = TDERIV(1)/A(1)
    DO I = 2, NX
        TNEW(I) = (TNEW(I)-B(I)*TNEW(I-1)) / A(I)
        TDERIV(I) = (TDERIV(I)-B(I)*TDERIV(I-1)) / A(I)
    ENDDO

    DO I = NX - 1, 1, -1
        TNEW(I) = TNEW(I) - C(I) * TNEW(I+1)
        TDERIV(I) = TDERIV(I) - C(I) * TDERIV(I+1)
    ENDDO

    IF (UPDATE.NE.0) THEN
        DO I = 1, NX
            WTEMP(I) = TNEW(I)
        ENDDO
    ENDIF

    DDIF(1) = (TNEW(2)-TNEW(1)) / WALLDX(1)
    DDIF(2) = (TNEW(3)-TNEW(2)) / WALLDX(2)
    DDIF(2) = (DDIF(2)-DDIF(1)) / (WALLDX(1)+WALLDX(2))
    TGRAD(1) = (DDIF(1)-DDIF(2)*WALLDX(1))

    DDIF(1) = (TNEW(NX-1)-TNEW(NX)) / WALLDX(NX-1)
    DDIF(2) = (TNEW(NX-2)-TNEW(NX-1)) / WALLDX(NX-2)
    DDIF(2) = (DDIF(2)-DDIF(1)) / (WALLDX(NX-1)+WALLDX(NX-2))
    TGRAD(2)= (DDIF(1)-DDIF(2)*WALLDX(NX-1))

    END SUBROUTINE THERMAL_WALL_CONDUCTION

    SUBROUTINE SOLVE_HYGRO_WALLS(TEMPCALL,INSTANT)

    INTEGER,INTENT(IN)							:: TEMPCALL
    !SAM ajout du parametre permettant de se reperer dans le temps
    DOUBLE PRECISION,INTENT(IN)	        		:: INSTANT
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: RES,WA,XX,YY,DYYDT,C,CTAMPON,ATOL,RTOL
    DOUBLE PRECISION							:: MAXRES,RESMAX1,RESMAX2,RESMAX3
    DOUBLE PRECISION							:: HRMAX,HRMAXRES,TMAX,TMAXRES
    INTEGER                                     :: HRMAXRESLOC,TMAXRESLOC
    INTEGER,DIMENSION(1)                        :: HRMAXRESLOC_TEST,TMAXRESLOC_TEST
    CHARACTER(100)                              :: WALLIDHRRES,WALLIDTRES
    INTEGER										:: IWT,IWH,IY,IX,I,J,K,JJJ,INFO,LWA,NHTW,N,JFLAG,ISTAT,IC,NBRELAX
    !SAM variable intermediaire pour limiter le nombre de calculs
    integer                                     :: Nb_Noeuds_courant, Ind_debut,Ind_fin1,Ind_fin2, ind_HR
    DOUBLE precision                            :: facteur_extrapolation, HR_extrapolee
    !SAM pour debug
    integer :: indice_noeud
    integer :: extrapolation_done


    IF (N_WHYGRO.NE.0) THEN
        NHTW=2*SUM(MURRT(WHIP)%NUMNODE(1))
        ALLOCATE(YY0(NHTW))
        ALLOCATE(YY(NHTW))
        ALLOCATE(YYTAMPON(NHTW))
        ALLOCATE(DYYDT(NHTW))
        ALLOCATE(C(NHTW))
        ALLOCATE(CTAMPON(NHTW))
        C=0.D0
        CTAMPON=0.D0
        ALLOCATE(ATOL(NHTW))
        ALLOCATE(RTOL(NHTW))
        IY=0
        DO IWH=1,N_WHYGRO
            I=WHIP(IWH)
            !SAM TODO : verifier pourquoi l'initialisation se fait avec les valeurs OLD alors qu'elles sont certainement identiques aux autres
            YY0(IY+1:IY+MURRT(I)%NUMNODE(1))=MURRT(I)%HR_OLD
            YY0(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1))=MURRT(I)%TEMP_OLD/TREF
            ATOL(IY+1:IY+MURRT(I)%NUMNODE(1))=HRWTOL
            ATOL(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1))=TWTOL
            RTOL=ATOL
            IY=IY+2*MURRT(I)%NUMNODE(1)
        ENDDO

        YY=YY0 ! a supprimer
        YYTAMPON=YY !a changer = YY0

        !SAM : on tente une initialisation du vecteur YY par une extrapolation depuis les valeurs associees aux 2 derniers pas de temps
        if(.FALSE.) then
        extrapolation_done=0
        Ind_debut=0
        DO IWH=1,N_WHYGRO
            I=WHIP(IWH)

            Nb_Noeuds_courant = MURRT(I)%NUMNODE(1)
            Ind_fin1=Ind_debut+Nb_Noeuds_courant
            Ind_fin2=Ind_fin1 +Nb_Noeuds_courant

            IF (MURRT(I)%TIME_OLD2 > 0) THEN                
                facteur_extrapolation=(INSTANT-MURRT(I)%TIME_OLD2) / (MURRT(I)%TIME_OLD - MURRT(I)%TIME_OLD2)

                ! extrapolation des valeurs de HR tout en s'assurant que HR est dans une gamme correcte
                Do ind_HR=ind_debut+1 , Ind_fin1

                    HR_extrapolee  = (MURRT(I)%HR_OLD2(ind_HR)   * (1.0D0 - facteur_extrapolation) + MURRT(I)%HR_OLD(ind_HR)   * facteur_extrapolation)

                    if( ( (SUPERSATURATION .EQV. .FALSE.)  .AND. ((HR_extrapolee > 1.00D0 ) .OR. (HR_extrapolee < 0.0D0 )) ) .OR.\
                        ( (SUPERSATURATION .EQV. .TRUE.)   .AND. ((HR_extrapolee > 1.01D0 ) .OR. (HR_extrapolee < 0.0D0 )) )  ) then
                    !if( (HR_extrapolee > 1.00D0 ) .OR. (HR_extrapolee < 0.0D0 ) ) then
                        YY(ind_HR) = MURRT(I)%HR_OLD(ind_HR)
                    else
                        YY(ind_HR) = HR_extrapolee
                    endif
                EndDo

                ! idem pour les temperatures
                YY(Ind_fin1+1  : Ind_fin2)  = MURRT(I)%TEMP_OLD2 * (1.0D0 - facteur_extrapolation)/TREF + MURRT(I)%TEMP_OLD * facteur_extrapolation/TREF

                !print*, "facteur=" , facteur_extrapolation, MURRT(I)%HR_OLD2(1), MURRT(I)%HR_OLD(1), YY(ind_debut+1)
                !print*, "facteur=" , facteur_extrapolation, MURRT(I)%TEMP_OLD2(1), MURRT(I)%TEMP_OLD(1), YY(ind_fin1+1)

                extrapolation_done=extrapolation_done+1
            ELSE
                !initialisation normale
                YY(ind_debut+1 : Ind_fin1) = MURRT(I)%HR_OLD
                YY(ind_fin1+1  : ind_fin2) = MURRT(I)%TEMP_OLD/TREF
            ENDIF
         
            IY=ind_fin2
        ENDDO
        endif

        !print*, "Nygro=", N_WHYGRO, "extrapolation=", extrapolation_done
        !print*, "YY0", YY0
        !print*, "YY", YY



        ISTAT=0
        MAXRES=0.D0

10      DO JJJ=1,3
            SELECT CASE (JJJ)
            CASE (1)
                HYGROCALL=2 !calcul temperatures
                N=NHTW/2
            CASE (2)
                HYGROCALL=1 !calcul humidite
                N=NHTW/2
            CASE (3)
                HYGROCALL=3 !calcul les 2
                N=NHTW
            ENDSELECT

            ALLOCATE(XX(N))
            LWA=(N*(3*N+13))/2 !utilise par le solveur
            ALLOCATE(WA(LWA))
            ALLOCATE(RES(N))

            IF (HYGROCALL<3) THEN
                IX=0
                IY=0
                DO IWH=1,N_WHYGRO
                    I=WHIP(IWH)
                    IF (HYGROCALL==1) THEN
                        XX(IX+1:IX+MURRT(I)%NUMNODE(1))=YY(IY+1:IY+MURRT(I)%NUMNODE(1))                         ! copie des HR pour les passer au solveur
                    ENDIF
                    IF (HYGROCALL==2) THEN
                        XX(IX+1:IX+MURRT(I)%NUMNODE(1))=YY(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1))   ! copie des temperatures pour les passer au solveur
                    ENDIF
                    IX=IX+MURRT(I)%NUMNODE(1)
                    IY=IY+2*MURRT(I)%NUMNODE(1)
                ENDDO
                CALL HYBRD1(RES_FLUX_HYGRO_WALLS,N,XX,RES,HYBRIDTOL,INFO,WA,LWA)
                IX=0
                IY=0
                DO IWH=1,N_WHYGRO
                    I=WHIP(IWH)
                    IF (HYGROCALL==1) THEN
                        YY(IY+1:IY+MURRT(I)%NUMNODE(1))=XX(IX+1:IX+MURRT(I)%NUMNODE(1))   ! recuperation des HR modifies par le solveur
                    ELSEIF (HYGROCALL==2) THEN
                        YY(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1))=XX(IX+1:IX+MURRT(I)%NUMNODE(1))   ! recuperation des temperatures modifies par le solveur
                    ENDIF
                    IX=IX+MURRT(I)%NUMNODE(1)
                    IY=IY+2*MURRT(I)%NUMNODE(1)
                ENDDO
            ELSEIF (HYGROCALL.EQ.3) THEN 
                XX=YY
                CALL HYBRD1(RES_FLUX_HYGRO_WALLS,N,XX,RES,HYBRIDTOL,INFO,WA,LWA)
                YY=XX
                !comptage du nombre d'itérations
                ISTAT=ISTAT+1
                !calcul des residus
                HYGROCALL=4
                CALL RES_FLUX_HYGRO_WALLS(N,YY,RES,JFLAG)
                !calcul des criteres de convergence
                RESMAX1=MINVAL(ATOL-DABS(RES)*DTIME)
                RESMAX2=0.D0
                RESMAX3=0.D0
                DO K=1,SIZE(YY)
                    IF (DABS(YY(K)).NE.0.D0) THEN
                        RESMAX2=MIN(RESMAX2,RTOL(K)-(DABS((RES(K)*DTIME/YY(K)))))
                        RESMAX3=MIN(RESMAX3,RTOL(K)-(DABS((YY(K)-YYTAMPON(K))/YY(K))))
                    ELSE
                        RESMAX2=MIN(RESMAX2,ATOL(K)-(DABS((RES(K)*DTIME))))
                        RESMAX3=MIN(RESMAX3,ATOL(K)-(DABS((YY(K)-YYTAMPON(K)))))
                    ENDIF
                ENDDO
                MAXRES=MAX(RESMAX1,RESMAX2,RESMAX3)
                
                IF ((DEBUGPLUS==.TRUE.).OR.((DEBUG==.TRUE.).AND.(MAXRES>=0.D0)).OR.((ISTAT==NHTWSTATMAX).AND.(MAXRES<0.D0))) THEN
                    IX=0
                    IY=0
                    HRMAXRES=0.D0
                    TMAXRES=0.D0
                    WALLIDHRRES='null'
                    WALLIDTRES='null'
                    DO IWH=1,N_WHYGRO
                        I=WHIP(IWH)
                        HRMAXRESLOC_TEST=MAXLOC(DABS(RES(IY+1:IY+MURRT(I)%NUMNODE(1)))*DTIME)
                        IF (ABS(RES(HRMAXRESLOC_TEST(1)))>=HRMAXRES) THEN
                            HRMAXRES=ABS(RES(IY+HRMAXRESLOC_TEST(1)))*DTIME
                            HRMAXRESLOC=HRMAXRESLOC_TEST(1)
                            HRMAX=YY(IY+HRMAXRESLOC)*100.D0
                            WALLIDHRRES=MURRT(I)%ID
                        ENDIF
                        TMAXRESLOC_TEST=MAXLOC(DABS(RES(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1)))*DTIME)
                        IF (ABS(RES(TMAXRESLOC_TEST(1)))>=TMAXRES) THEN
                            TMAXRES=ABS(RES(IY+MURRT(I)%NUMNODE(1)+TMAXRESLOC_TEST(1)))*DTIME
                            TMAXRESLOC=TMAXRESLOC_TEST(1)
                            TMAX=YY(IY+MURRT(I)%NUMNODE(1)+TMAXRESLOC)*TREF-273.15D0
                            WALLIDTRES=MURRT(I)%ID
                        ENDIF
                        IX=IX+MURRT(I)%NUMNODE(1)
                        IY=IY+2*MURRT(I)%NUMNODE(1)
                    ENDDO    
                    WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                    IF ((MAXRES<0.D0).AND.(ISTAT==NHTWSTATMAX)) THEN
                        WRITE(LUOUT,'(A)') 'WARNING: Hygrothermal Wall Solver reached max iteration number'
                    ENDIF
                    WRITE(LUOUT,'(A,I4)') '              HygroWalls Solver - Iter = ',ISTAT
                    WRITE(LUOUT,'(A,I4,A,E8.1,A,F8.2,A)') '              Max HRres('//TRIM(WALLIDHRRES)//')(',HRMAXRESLOC,')=',HRMAXRES,' HR= ',HRMAX,' %'
                    WRITE(LUOUT,'(A,I4,A,E8.1,A,F8.2,A)') '              Max  Tres('//TRIM(WALLIDTRES)//')(',TMAXRESLOC,')=',TMAXRES,' T= ',TMAX,' C'
                    WRITE(LUOUT,'(A)') ''
                ENDIF
                
            ENDIF

            
            IF (HRWFLAG.NE.-2) THEN
                IF  ((MAXRES<0.D0).AND.(ISTAT<=NHTWSTATMAX).AND.(HYGROCALL.NE.2)) THEN
                    !relaxation dynamique sur l'humidite
                    IY=0
                    NBRELAX=0
                    DO IWH=1,N_WHYGRO
                        I=WHIP(IWH)
                        DO IC=IY+1,IY+MURRT(I)%NUMNODE(1)
                            C(IC)=YYTAMPON(IC)-YY(IC)
                            IF (CTAMPON(IC).NE.0.D0) THEN
                                IF ((C(IC)/CTAMPON(IC).LT.-0.5D0).AND.(1.D0-C(IC)/CTAMPON(IC).NE.0.D0)) THEN
                                YY(IC)=YYTAMPON(IC)-C(IC)/(1.D0-C(IC)/CTAMPON(IC))
                                NBRELAX=NBRELAX+1
                                ENDIF
                            ENDIF
                        ENDDO
                        IY=IY+2*MURRT(I)%NUMNODE(1)
                    ENDDO
                    IF ((NBRELAX>0).AND.(DEBUG.EQV..TRUE.)) THEN
                        write(LUOUT,'(A,I1,A,I3,A)') '              Hygrocall= ',HYGROCALL,' - HR relaxation for ',NBRELAX,' nodes'
                        write(LUOUT,'(A)') ''
                    ENDIF
                    CTAMPON=YYTAMPON-YY
                    !mise a jour des variables deduites avec les valeurs de HR "sous-relaxees"
                    CALL HYGRO_WALL_MODEL(NHTW,TIME,YY,DYYDT)
                ENDIF
                YYTAMPON=YY          
            ENDIF
            DEALLOCATE(RES)
            DEALLOCATE(WA)
            DEALLOCATE(XX)
        ENDDO

        IF ((MAXRES<0.D0).AND.(ISTAT<NHTWSTATMAX)) GOTO 10
        

        DEALLOCATE(YY0)
        DEALLOCATE(YY)
        DEALLOCATE(YYTAMPON)
        DEALLOCATE(C)
        DEALLOCATE(CTAMPON)
        DEALLOCATE(ATOL)
        DEALLOCATE(RTOL)

    ENDIF

    END  SUBROUTINE SOLVE_HYGRO_WALLS

    SUBROUTINE RES_FLUX_HYGRO_WALLS(N,X,RES,JFLAG)

    INTEGER										:: N,JFLAG,K,J,I,NHTW,IY,IX,IWH
    DOUBLE PRECISION,DIMENSION(N)				:: X,RES
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE   :: Y,DYDT,RESTOT

    integer :: debug_output
    character(256) ::fname,fmt
    integer :: file_debug

    G_SAM_Nb_Calls=G_SAM_Nb_Calls+1

    debug_output=0

    if(debug_output) then
        file_debug=60
        FNAME='debug_hygroX.txt'
        OPEN(file_debug,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')

        write(file_debug, '(I6)') HYGROCALL

        WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (N)
        write(file_debug,trim(fmt)) X(1:N)
    endif

    IF ((HYGROCALL==3).OR.(HYGROCALL==4)) THEN
        NHTW=N
    ELSE
        NHTW=2*N
    ENDIF

    ALLOCATE(Y(NHTW))
    ALLOCATE(DYDT(NHTW))
    ALLOCATE(RESTOT(NHTW))
    IF ((HYGROCALL==3).OR.(HYGROCALL==4)) THEN
        Y=X
    ELSE
        IX=0;IY=0
        DO IWH=1,N_WHYGRO
            I=WHIP(IWH)
            IF (HYGROCALL==1) THEN
                Y(IY+1:IY+MURRT(I)%NUMNODE(1))                      =X(IX+1:IX+MURRT(I)%NUMNODE(1)) ! recuperation des HR sauvegardees (puis modifiees par le solveur a partir du deuxieme appel ?)
                Y(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1))=MURRT(I)%TEMP/TREF             ! copie des temperatures @todo : verifier si on a vraiment besoin de le faire a chaque appel
            ENDIF
            IF (HYGROCALL==2) THEN
                Y(IY+1:IY+MURRT(I)%NUMNODE(1))                      =MURRT(I)%HR                    ! copie des HR  @todo : verifier si on a vraiment besoin de le faire a chaque appel
                Y(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1))=X(IX+1:IX+MURRT(I)%NUMNODE(1)) ! recuperation des temperatures sauvegardees (puis modifiees par le solveur a partir du deuxieme appel ?)
            ENDIF
            IY=IY+2*MURRT(I)%NUMNODE(1)
            IX=IX+  MURRT(I)%NUMNODE(1)
        ENDDO
    ENDIF

    CALL HYGRO_WALL_MODEL(NHTW,TIME,Y,DYDT)
    RESTOT=DYDT-(Y-YY0)/DTIME

    IF ((HYGROCALL==3).OR.(HYGROCALL==4)) THEN
        RES=RESTOT
    ELSE
        IX=0;IY=0
        DO IWH=1,N_WHYGRO
            I=WHIP(IWH)
            IF (HYGROCALL==1) THEN
                RES(IX+1:IX+MURRT(I)%NUMNODE(1))=RESTOT(IY+1:IY+MURRT(I)%NUMNODE(1))                        ! copie des residus sur la base des HR
            ENDIF
            IF (HYGROCALL==2) THEN
                RES(IX+1:IX+MURRT(I)%NUMNODE(1))=RESTOT(IY+MURRT(I)%NUMNODE(1)+1:IY+2*MURRT(I)%NUMNODE(1))  ! copie des residus sur la base des temperatures
            ENDIF
            IY=IY+2*MURRT(I)%NUMNODE(1)
            IX=IX+MURRT(I)%NUMNODE(1)
        ENDDO
    ENDIF


    if(debug_output) then
        write(file_debug,trim(fmt)) DYDT(1:N)
        IF ((HYGROCALL.NE.3)) THEN
            write(file_debug,trim(fmt)) DYDT(N+1:2*N)
        endif
        write(file_debug,trim(fmt)) RES(1:N)

        close(file_debug)
    endif



    DEALLOCATE(Y)
    DEALLOCATE(DYDT)
    DEALLOCATE(RESTOT)

    END SUBROUTINE RES_FLUX_HYGRO_WALLS

    SUBROUTINE HYGRO_WALL_MODEL(NEQ,T,Y,DYDT)

    INTEGER,INTENT(IN)							:: NEQ
    DOUBLE PRECISION,INTENT(IN)					:: T
    DOUBLE PRECISION,DIMENSION(NEQ),INTENT(IN)	:: Y
    DOUBLE PRECISION,DIMENSION(NEQ),INTENT(OUT)	:: DYDT
    DOUBLE PRECISION,DIMENSION(NEQ)             :: YBAD

    INTEGER                                     :: IWH,I,J,K,IY,NBN,INODE,ISLAB
    DOUBLE PRECISION,DIMENSION(2)               :: PV,NETM
    DOUBLE PRECISION                            :: A,B,C,D,INERTIE,DXPOS,DXNEG
    DOUBLE PRECISION                            :: FLUXMIN,FLUXMOUT,FLUXQIN,FLUXQOUT
    DOUBLE PRECISION,DIMENSION(1)               :: MH2OCURRENT,MH2OMAX

    integer :: debug_output
    character(256) ::fname,fmt
    integer :: file_debug
    double precision :: pente_moyenne

    debug_output=0

    if(debug_output) then
        file_debug=0
        FNAME='debug_hygro2.txt'
        OPEN(file_debug,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
        WRITE(FMT,'("(",i4,"(ES15.7E3,:,""  ""))")') (10)

        FLUXMIN=-1.1234567D-100
        write(file_debug,trim(fmt)) FLUXMIN,2.0D0,3.0D0,4.0D0,5.0D0,6.0D0,7.0D0,8.0D0,9.0D0,10.0D0
        write(file_debug, '(I6)') HYGROCALL
    endif
    

    HRWFLAG=0
    DYDT=0.D0
    IY=0
    YBAD=0.0D0
    DO IWH=1,N_WHYGRO !boucle sur les murs
        I=WHIP(IWH)
        NBN=MURRT(I)%NUMNODE(1)

        !mise a jour des temperatures et des humidites interieures
        MURRT(I)%HR=Y(IY+1:IY+NBN)

        if(debug_output) then
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (NBN)
            write(file_debug,trim(fmt)) MURRT(I)%HR(1:NBN)
        endif

        IF (SUPERSATURATION.EQV..FALSE.) THEN
            IF ((MINVAL(MURRT(I)%HR)<1.D-9).OR.(MAXVAL(MURRT(I)%HR)>1.0D0)) THEN
                HRWFLAG=-2
                DO K=1,NBN
                    !IF ((MURRT(I)%HR(K)<1.D-9).OR.(MURRT(I)%HR(K)>1.0D0)) THEN
                    IF ((MURRT(I)%HR(K)<1.D-9).OR.(MURRT(I)%HR(K)>1.0D0)) THEN
                        YBAD(IY+K)=1.D0
                        MURRT(I)%HR(K)=YYTAMPON(IY+K)
                    ENDIF
                ENDDO
            ENDIF
            MURRT(I)%HR=MAX(1.D-9,MIN(1.0D0,MURRT(I)%HR))
            !MURRT(I)%HR=MAX(1.D-9,MIN(0.999D0,MURRT(I)%HR))
        ELSE
            IF ((MINVAL(MURRT(I)%HR)<1.D-9).OR.(MAXVAL(MURRT(I)%HR)>1.01D0)) THEN
                HRWFLAG=-2
                DO K=1,NBN
                    IF ((MURRT(I)%HR(K)<1.D-9).OR.(MURRT(I)%HR(K)>1.01D0)) THEN
                        YBAD(IY+K)=1.D0
                        MURRT(I)%HR(K)=YYTAMPON(IY+K)
                    ENDIF
                ENDDO
            ENDIF
            MURRT(I)%HR=MAX(1.D-9,MIN(1.01D0,MURRT(I)%HR))
        ENDIF

        ! partie mise a jour des temperatures
        MURRT(I)%TEMP=Y(IY+NBN+1:IY+2*NBN)*TREF

        if(debug_output) then
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (NBN)
            write(file_debug,trim(fmt)) MURRT(I)%TEMP(1:NBN)
        endif


        IF ((MINVAL(MURRT(I)%TEMP)<200.D0).OR.(MAXVAL(MURRT(I)%TEMP)>2500.D0)) THEN
            HRWFLAG=-2
            DO K=1,NBN
                IF ((MURRT(I)%TEMP(K)<200.D0).OR.(MURRT(I)%TEMP(K)>2500.0D0)) THEN
                    YBAD(IY+NBN+K)=1.D0
                    MURRT(I)%TEMP(K)=YYTAMPON(IY+NBN+K)*TREF
                ENDIF
            ENDDO
        ENDIF
        MURRT(I)%TEMP=MAX(200.D0,MIN(2500.D0,MURRT(I)%TEMP))
        MURRT(I)%TPIN=MURRT(I)%TEMP(1); MURRT(I)%TPOUT=MURRT(I)%TEMP(NBN)





        !determination des grandeurs dependant de HR
        IF ((HYGROCALL.EQ.1).OR.(HYGROCALL.EQ.3)) THEN
            CALL EVAL_MUR_HYGROVARS(MURRT(I))
        ELSEIF (HYGROCALL.EQ.4) THEN
            CALL EVAL_MUR_HYGROVARS(MURRT(I),0)
        ENDIF

        !determination des flux de masses et de chaleurs aux C.L. NETM et NETQ
        CALL EVAL_MUR_BOUNDS(MURRT(I))

        INODE=0
        DO ISLAB=1,MURRT(I)%NSLAB
            !determination des flux de masse et de chaleur aux extremites du slab
            IF (MURRT(I)%NSLAB==1) THEN
                FLUXMIN=MURRT(I)%NETMTOT(1)
                FLUXMOUT=MURRT(I)%NETMTOT(2)
                FLUXQIN=MURRT(I)%NETQ(1)
                FLUXQOUT=MURRT(I)%NETQ(2)
            ELSE
                IF (ISLAB==1) THEN
                    FLUXMIN=MURRT(I)%NETMTOT(1)
                    FLUXMOUT=MURRT(I)%HMCT(ISLAB)*(MURRT(I)%PV(INODE+MURRT(I)%NODES(ISLAB)+1)-MURRT(I)%PV(INODE+MURRT(I)%NODES(ISLAB)))
                    FLUXQIN=MURRT(I)%NETQ(1)
                    FLUXQOUT=MURRT(I)%HCCT(ISLAB)*(MURRT(I)%TEMP(INODE+MURRT(I)%NODES(ISLAB)+1)-MURRT(I)%TEMP(INODE+MURRT(I)%NODES(ISLAB)))
                ENDIF
                IF ((ISLAB>1).AND.(ISLAB.NE.MURRT(I)%NSLAB)) THEN
                    FLUXMIN=-MURRT(I)%HMCT(ISLAB-1)*(MURRT(I)%PV(INODE+1)-MURRT(I)%PV(INODE))
                    FLUXMOUT=MURRT(I)%HMCT(ISLAB)*(MURRT(I)%PV(INODE+MURRT(I)%NODES(ISLAB)+1)-MURRT(I)%PV(INODE+MURRT(I)%NODES(ISLAB)))
                    FLUXQIN=-MURRT(I)%HCCT(ISLAB-1)*(MURRT(I)%TEMP(INODE+1)-MURRT(I)%TEMP(INODE))
                    FLUXQOUT=MURRT(I)%HCCT(ISLAB)*(MURRT(I)%TEMP(INODE+MURRT(I)%NODES(ISLAB)+1)-MURRT(I)%TEMP(INODE+MURRT(I)%NODES(ISLAB)))
                ENDIF
                IF (ISLAB==MURRT(I)%NSLAB) THEN
                    FLUXMIN=-MURRT(I)%HMCT(ISLAB-1)*(MURRT(I)%PV(INODE+1)-MURRT(I)%PV(INODE))
                    FLUXMOUT=MURRT(I)%NETMTOT(2)
                    FLUXQIN=-MURRT(I)%HCCT(ISLAB-1)*(MURRT(I)%TEMP(INODE+1)-MURRT(I)%TEMP(INODE))
                    FLUXQOUT=MURRT(I)%NETQ(2)
                ENDIF
            ENDIF


            if(MURRT(I)%NODES(ISLAB) < 3) then
                print*, "pas assez de noeuds=",MURRT(I)%NODES(ISLAB)
                stop -1
            endif

            !termes de bilan hydrique
            !premier noeud du slab
            J=INODE+1
            DXPOS=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J+1)/2.D0 !SAM : DX correspond aux epaisseurs et donc DXPOS a la distance entre 2 centres de noeuds
            A=DXPOS/(&
                MURRT(I)%DX(J)   / (2.D0* ( MURRT(I)%PERM(J)  *MURRT(I)%PSAT(J)       + MURRT(I)%DW(J)    *MURRT(I)%SORP(J)   ) ) + &
                MURRT(I)%DX(J+1) / (2.D0* ( MURRT(I)%PERM(J+1)*MURRT(I)%PSAT(J+1)     + MURRT(I)%DW(J+1)  *MURRT(I)%SORP(J+1) ) ) &
                )
            C=DXPOS/(&
                MURRT(I)%DX(J)  /(2.D0*(MURRT(I)%PERM(J)    *MURRT(I)%HR(J)     *MURRT(I)%DPSATDT(J)))+&
                MURRT(I)%DX(J+1)/(2.D0*(MURRT(I)%PERM(J+1)  *MURRT(I)%HR(J+1)   *MURRT(I)%DPSATDT(J+1)))&
                )                

            pente_moyenne=MURRT(I)%SORP(J)

            if(1) then
                DYDT(IY+J)=(2.D0/pente_moyenne)*(1.D0/MURRT(I)%DX(J))*&
                    (&
                    A/DXPOS *(MURRT(I)%HR(J+1)  -MURRT(I)%HR(J))&
                    +C/DXPOS*(MURRT(I)%TEMP(J+1)-MURRT(I)%TEMP(J))&
                    +FLUXMIN&
                    +MURRT(I)%MSOURCE(J)&
                    !+(MURRT(I)%K(J+1)-MURRT(I)%K(J))*RHOH2O*9.81*DCOS(MURRT(I)%SLOPE) &
                    )
            endif


            if(debug_output) then

                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (MURRT(I)%NODES(ISLAB))
                write(file_debug,trim(fmt)) MURRT(I)%HR  ( INODE+1 : INODE+MURRT(I)%NODES(ISLAB))
                write(file_debug,trim(fmt)) MURRT(I)%TEMP( INODE+1 : INODE+MURRT(I)%NODES(ISLAB))
                write(file_debug,trim(fmt)) MURRT(I)%PERM( INODE+1 : INODE+MURRT(I)%NODES(ISLAB))
                write(file_debug,trim(fmt)) MURRT(I)%PSAT( INODE+1 : INODE+MURRT(I)%NODES(ISLAB))
                write(file_debug,trim(fmt)) MURRT(I)%DW  ( INODE+1 : INODE+MURRT(I)%NODES(ISLAB))
                write(file_debug,trim(fmt)) MURRT(I)%SORP( INODE+1 : INODE+MURRT(I)%NODES(ISLAB))
                write(file_debug,trim(fmt)) MURRT(I)%DPSATDT( INODE+1 : INODE+MURRT(I)%NODES(ISLAB))

                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1)
                write(file_debug,FMT) DXPOS
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (7)
                
                write(file_debug,trim(fmt)) DXPOS, MURRT(I)%SORP(J), MURRT(I)%SORP(J+1), A, C, FLUXMIN, FLUXMOUT

                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (2)
                write(file_debug,trim(fmt)) MURRT(I)%PERM(J)*MURRT(I)%PSAT(J), MURRT(I)%DW(J)*MURRT(I)%SORP(J)
                write(file_debug,trim(fmt)) MURRT(I)%PERM(J+1)*MURRT(I)%PSAT(J+1), MURRT(I)%DW(J+1)*MURRT(I)%SORP(J+1)
                write(file_debug,trim(fmt)) (2.D0/MURRT(I)%SORP(J+1)), (1.D0/MURRT(I)%DX(J))
                write(file_debug,trim(fmt)) A/DXPOS, (MURRT(I)%HR(J+1)-MURRT(I)%HR(J))
                write(file_debug,trim(fmt)) MURRT(I)%HR(J), MURRT(I)%HR(J+1)

                write(file_debug,trim(fmt)) A/DXPOS*(MURRT(I)%HR(J+1)-MURRT(I)%HR(J)), DYDT(IY+J)

                write(file_debug,trim(fmt)) (MURRT(I)%TEMP(J+1)-MURRT(I)%TEMP(J)), DYDT(IY+J)
            
                !close(file_debug) non : plus bas
            endif

               
            !noeuds internes
            DO J=INODE+2,INODE+MURRT(I)%NODES(ISLAB)-1

                pente_moyenne=MURRT(I)%SORP(J)

                !INERTIE=MURRT(I)%SORP(J)
                INERTIE=pente_moyenne

                DXPOS=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J+1)/2.D0
                DXNEG=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J-1)/2.D0
                A=DXPOS/(&
                    MURRT(I)%DX(J)   / (2.D0* ( MURRT(I)%PERM(J)  *MURRT(I)%PSAT(J)     + MURRT(I)%DW(J)    *MURRT(I)%SORP(J)   )   )  +&
                    MURRT(I)%DX(J+1) / (2.D0* ( MURRT(I)%PERM(J+1)*MURRT(I)%PSAT(J+1)   + MURRT(I)%DW(J+1)  *MURRT(I)%SORP(J+1) )   )&
                    )
                B=DXNEG/(&
                    MURRT(I)%DX(J)  / (2.D0* ( MURRT(I)%PERM(J)*MURRT(I)%PSAT(J)        + MURRT(I)%DW(J)*MURRT(I)%SORP(J)))+&
                    MURRT(I)%DX(J-1)/ (2.D0* ( MURRT(I)%PERM(J-1)*MURRT(I)%PSAT(J-1)    + MURRT(I)%DW(J-1)*MURRT(I)%SORP(J-1)))&
                    )
                C=DXPOS/(&
                    MURRT(I)%DX(J)  /(2.D0*(MURRT(I)%PERM(J)*MURRT(I)%HR(J)*MURRT(I)%DPSATDT(J)))+&
                    MURRT(I)%DX(J+1)/(2.D0*(MURRT(I)%PERM(J+1)*MURRT(I)%HR(J+1)*MURRT(I)%DPSATDT(J+1)))&
                    )
                D=DXNEG/(&
                    MURRT(I)%DX(J)  /(2.D0*(MURRT(I)%PERM(J)*MURRT(I)%HR(J)*MURRT(I)%DPSATDT(J)))+&
                    MURRT(I)%DX(J-1)/(2.D0*(MURRT(I)%PERM(J-1)*MURRT(I)%HR(J-1)*MURRT(I)%DPSATDT(J-1)))&
                    )

                !if(MURRT(I)%SORP_LISSEE(J) > 1.0D-6) then
                if(1) then
                    DYDT(IY+J)=(1.D0/INERTIE)*(1.D0/MURRT(I)%DX(J))*&
                        (&
                        A/DXPOS*(MURRT(I)%HR(J+1)-MURRT(I)%HR(J))&
                        +B/DXNEG*(MURRT(I)%HR(J-1)-MURRT(I)%HR(J))&
                        +C/DXPOS*(MURRT(I)%TEMP(J+1)-MURRT(I)%TEMP(J))&
                        +D/DXNEG*(MURRT(I)%TEMP(J-1)-MURRT(I)%TEMP(J))&
                        +MURRT(I)%MSOURCE(J)&
                        !+(MURRT(I)%K(J+1)-MURRT(I)%K(J-1))*RHOH2O*9.81*DCOS(MURRT(I)%SLOPE) &
                        )
                endif
            ENDDO


            !dernier noeud
            J=INODE+MURRT(I)%NODES(ISLAB)
            DXNEG=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J-1)/2.D0
            B=DXNEG/(&
                MURRT(I)%DX(J)/(2.D0*(MURRT(I)%PERM(J)*MURRT(I)%PSAT(J)         +MURRT(I)%DW(J)     *MURRT(I)%SORP(J)))+&
                MURRT(I)%DX(J-1)/(2.D0*(MURRT(I)%PERM(J-1)*MURRT(I)%PSAT(J-1)   +MURRT(I)%DW(J-1)   *MURRT(I)%SORP(J-1)))&
                )
            D=DXNEG/(&
                MURRT(I)%DX(J)/(2.D0*(MURRT(I)%PERM(J)*MURRT(I)%HR(J)*MURRT(I)%DPSATDT(J)))+&
                MURRT(I)%DX(J-1)/(2.D0*(MURRT(I)%PERM(J-1)*MURRT(I)%HR(J-1)*MURRT(I)%DPSATDT(J-1)))&
                )

	        !SAM attention l'avant derniere ligne n'etait pas presente lors des essais precedents
            ! test en remplacant SORP(J) par SORP(J-1) fonctionne mieux mais sans justification

            !pente_moyenne=0.5D0*(MURRT(I)%SORP(J-1)+MURRT(I)%SORP(J))
            pente_moyenne=MURRT(I)%SORP(J)

            if(1) then
                DYDT(IY+J)=(1.D0/pente_moyenne)*(2.D0/MURRT(I)%DX(J))*&
                    (&
                    B/DXNEG*(MURRT(I)%HR(J-1)   -MURRT(I)%HR(J))&
                    +D/DXNEG*(MURRT(I)%TEMP(J-1)-MURRT(I)%TEMP(J))&
                    +FLUXMOUT &
                    +MURRT(I)%MSOURCE(J)&
                    !+(MURRT(I)%K(J-1)-MURRT(I)%K(J))*RHOH2O*9.81*DCOS(MURRT(I)%SLOPE) &
                    )
            endif



            !termes de bilan thermique
            !premier noeud du slab
            J=INODE+1
            DXPOS=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J+1)/2.D0
            A=DXPOS/(&
                MURRT(I)%DX(J)/2.D0/(MURRT(I)%LAMBDA(J)     +LV*MURRT(I)%PERM(J)*MURRT(I)%HR(J)*MURRT(I)%DPSATDT(J))+&
                MURRT(I)%DX(J+1)/2.D0/(MURRT(I)%LAMBDA(J+1) +LV*MURRT(I)%PERM(J+1)*MURRT(I)%HR(J+1)*MURRT(I)%DPSATDT(J+1))&
                )
            C=DXPOS/(&
                MURRT(I)%DX(J)/2.D0/(LV*MURRT(I)%PERM(J)*MURRT(I)%PSAT(J))+&
                MURRT(I)%DX(J+1)/2.D0/(LV*MURRT(I)%PERM(J+1)*MURRT(I)%PSAT(J+1))&
                )

		!SAM attention la derniere ligne n'etait pas presente lors des essais precedents
            DYDT(IY+NBN+J)=(1.D0/TREF/(MURRT(I)%CP(J)*MURRT(I)%RHO(J)+MURRT(I)%YW(J)*CPW))*(2.D0/MURRT(I)%DX(J))*(&
                A/DXPOS*(MURRT(I)%TEMP(J+1)-MURRT(I)%TEMP(J))+&
                C/DXPOS*(MURRT(I)%HR(J+1)-MURRT(I)%HR(J))+&
                !CPW*MURRT(I)%TEMP(J)*(MURRT(I)%K(J+1)-MURRT(I)%K(J))*RHOH2O*9.81*DCOS(MURRT(I)%SLOPE)+ &
                FLUXQIN&
                +MURRT(I)%QSOURCE(J)&
                )
            !noeuds internes
            DO J=INODE+2,INODE+MURRT(I)%NODES(ISLAB)-1
                INERTIE=MURRT(I)%CP(J)*MURRT(I)%RHO(J)+MURRT(I)%YW(J)*CPW
                DXPOS=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J+1)/2.D0
                DXNEG=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J-1)/2.D0
                A=DXPOS/(&
                    MURRT(I)%DX(J)/2.D0     /(MURRT(I)%LAMBDA(J)    + LV*MURRT(I)%PERM(J)*MURRT(I)%HR(J)*MURRT(I)%DPSATDT(J))+&
                    MURRT(I)%DX(J+1)/2.D0   /(MURRT(I)%LAMBDA(J+1)  + LV*MURRT(I)%PERM(J+1)*MURRT(I)%HR(J+1)*MURRT(I)%DPSATDT(J+1))&
                    )
                B=DXNEG/(&
                    MURRT(I)%DX(J)/2.D0/(MURRT(I)%LAMBDA(J)+LV*MURRT(I)%PERM(J)*MURRT(I)%HR(J)*MURRT(I)%DPSATDT(J))+&
                    MURRT(I)%DX(J-1)/2.D0/(MURRT(I)%LAMBDA(J-1)+LV*MURRT(I)%PERM(J-1)*MURRT(I)%HR(J-1)*MURRT(I)%DPSATDT(J-1))&
                    )
                C=DXPOS/(&
                    MURRT(I)%DX(J)/2.D0/(LV*MURRT(I)%PERM(J)*MURRT(I)%PSAT(J))+&
                    MURRT(I)%DX(J+1)/2.D0/(LV*MURRT(I)%PERM(J+1)*MURRT(I)%PSAT(J+1))&
                    )
                D=DXNEG/(&
                    MURRT(I)%DX(J)/2.D0/(LV*MURRT(I)%PERM(J)*MURRT(I)%PSAT(J))+&
                    MURRT(I)%DX(J-1)/2.D0/(LV*MURRT(I)%PERM(J-1)*MURRT(I)%PSAT(J-1))&
                    )
                DYDT(IY+NBN+J)=(1.D0/TREF/(INERTIE))*(1.D0/MURRT(I)%DX(J))*(&
                    A/DXPOS*(MURRT(I)%TEMP(J+1)-MURRT(I)%TEMP(J))+&
                    B/DXNEG*(MURRT(I)%TEMP(J-1)-MURRT(I)%TEMP(J))+&
                    C/DXPOS*(MURRT(I)%HR(J+1)-MURRT(I)%HR(J))+&
                    D/DXNEG*(MURRT(I)%HR(J-1)-MURRT(I)%HR(J))+&
                    !CPW*MURRT(I)%TEMP(J)*(MURRT(I)%K(J+1)-MURRT(I)%K(J-1))*RHOH2O*9.81*DCOS(MURRT(I)%SLOPE)+ &
                    MURRT(I)%QSOURCE(J)&
                    )
            ENDDO
            !dernier noeud
            J=INODE+MURRT(I)%NODES(ISLAB)
            DXNEG=MURRT(I)%DX(J)/2.D0+MURRT(I)%DX(J-1)/2.D0
            B=DXNEG/(&
                MURRT(I)%DX(J)/2.D0/(MURRT(I)%LAMBDA(J)+LV*MURRT(I)%PERM(J)*MURRT(I)%HR(J)*MURRT(I)%DPSATDT(J))+&
                MURRT(I)%DX(J-1)/2.D0/(MURRT(I)%LAMBDA(J-1)+LV*MURRT(I)%PERM(J-1)*MURRT(I)%HR(J-1)*MURRT(I)%DPSATDT(J-1))&
                )
            D=DXNEG/(&
                MURRT(I)%DX(J)/2.D0/(LV*MURRT(I)%PERM(J)*MURRT(I)%PSAT(J))+&
                MURRT(I)%DX(J-1)/2.D0/(LV*MURRT(I)%PERM(J-1)*MURRT(I)%PSAT(J-1))&
                )

		!SAM attention la derniere ligne n'etait pas presente lors des essais precedents
            DYDT(IY+NBN+J)=(1.D0/TREF/(MURRT(I)%CP(J)*MURRT(I)%RHO(J)+MURRT(I)%YW(J)*CPW))*(2.D0/MURRT(I)%DX(J))*(&
                B/DXNEG*(MURRT(I)%TEMP(J-1)-MURRT(I)%TEMP(J))+&
                D/DXNEG*(MURRT(I)%HR(J-1)-MURRT(I)%HR(J))+&
                !CPW*MURRT(I)%TEMP(J)*(MURRT(I)%K(J-1)-MURRT(I)%K(J))*RHOH2O*9.81*DCOS(MURRT(I)%SLOPE)+&
                FLUXQOUT&
                +MURRT(I)%QSOURCE(J)&
                )

            if(debug_output) then
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (NBN)
                write(file_debug,trim(fmt)) DYDT(IY+1 : IY+NBN)
                write(file_debug,trim(fmt)) DYDT(IY+NBN+1 : IY+2*NBN)
                
                close(file_debug)
            endif

            INODE=INODE+MURRT(I)%NODES(ISLAB)

        ENDDO
        IY=IY+2*NBN
    ENDDO

    IF (HRWFLAG==-2) THEN
        DO I=1,NEQ
            IF (YBAD(I)==1.D0) THEN
               DYDT(I)=100000.D0
            ENDIF
        ENDDO
    ENDIF


    END SUBROUTINE HYGRO_WALL_MODEL

    SUBROUTINE DUMMY_JAC()

    IMPLICIT NONE

    END SUBROUTINE DUMMY_JAC

    END MODULE WALL_SOLVER_MODULE
