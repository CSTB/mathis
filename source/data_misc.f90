    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE DATA_MISC_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    TYPE(TYPE_MISC),DIMENSION(:),ALLOCATABLE,TARGET      :: MISCRT
    INTEGER                     :: N_MISC
    
    !Pointeurs utilises pour lecture de la namelist &MISC et comme variables globales dans le reste du code
    CHARACTER(256),POINTER				:: JOBNAME
    DOUBLE PRECISION,POINTER            :: TETA0,TETAEND,WEEKDAY,YEARDAY
    DOUBLE PRECISION,POINTER        	:: HOUR
    DOUBLE PRECISION,POINTER		    :: DTETA,DTETASTAT
    CHARACTER(1),POINTER                :: TIMEUNIT
    DOUBLE PRECISION,POINTER         	:: LV
    INTEGER,POINTER						:: CONVCRIT,NSAVE,NSTATMAX,NHTWSTATMAX,BUFFERSIZE
    DOUBLE PRECISION,POINTER            :: HYBRIDTOL,PTOL,TTOL,YKTOL,QMTOL,QMRTOL,STATTOL,HRWTOL,TWTOL,QCRIT,DPCRIT,RECRIT,PRELAX,HRWRELAX
    DOUBLE PRECISION ,POINTER           :: ATEC_BEGIN,ATEC_END,ROTATION
    LOGICAL,POINTER						:: GUESS_POWER,QUASISTAT,RADIATION,BOUSSINESQ,COMPRESSIBLE,SUMMERTIME,UNCOUPLED,UNCOUPLED_THERMAL_WALLS,GROUPED_RESULTS,ATEC,EXPLICIT,DEBUG,DEBUGPLUS,ISOTHERMAL,CONSTANT_SPECIFIC_HEAT,ISOTHERMALNODE,OUTPUT_MESSAGE_ONLY,MOISTURE_NODES,SUPERSATURATION,OUTPUT_HSRC_SPECPROD,ALL_SPEC_ARE_TRACE
    CHARACTER(100),POINTER              :: BACKGROUND_SPECIE,VA_LOCID,OUTPUT
    CHARACTER(100),POINTER				:: PSATMODEL
    integer                             :: enum_PSATMODEL

    END MODULE DATA_MISC_MODULE    
