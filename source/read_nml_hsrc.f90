    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE READ_NML_HSRC_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    CHARACTER(100)						:: ID,LOCID,CTRLID,HSRCTYPE,WALLID,ORIENTATION
    CHARACTER(100),DIMENSION(NSPECMAX)	:: SPECIDS
    CHARACTER(100),DIMENSION(NSLABMAX)	:: MATIDS
    DOUBLE PRECISION					:: MFLUX,HFLUX,T,AREA,XRAD,COEF,MH2O0
    DOUBLE PRECISION,DIMENSION(NSPECMAX)::	YKS

    NAMELIST /HSRC/ ID,LOCID,MFLUX,HFLUX,T,YKS,SPECIDS,CTRLID,HSRCTYPE,WALLID,AREA,XRAD,MATIDS,ORIENTATION,COEF,MH2O0

    CONTAINS

    SUBROUTINE DEFAULT_NML_HSRC

    TYPE(TYPE_HSRC)		:: OBJ

    ID=OBJ%ID
    LOCID=OBJ%LOCID
    MFLUX=OBJ%MFLUX
    HFLUX=OBJ%HFLUX
    T=OBJ%T
    YKS=OBJ%YKS
    SPECIDS=OBJ%SPECIDS
    CTRLID=OBJ%CTRLID
    HSRCTYPE=OBJ%HSRCTYPE
    WALLID=OBJ%WALLID
    AREA=OBJ%AREA
    XRAD=OBJ%XRAD
    MATIDS=OBJ%MATIDS
    ORIENTATION=OBJ%ORIENTATION
    COEF=OBJ%COEF
    MH2O0=OBJ%MH2O0

    END SUBROUTINE DEFAULT_NML_HSRC
    
    SUBROUTINE PRINT_DEFAULT_NML_HSRC(LUOUT)
    
    INTEGER,INTENT(IN)			:: LUOUT
    
    CALL DEFAULT_NML_HSRC
    WRITE(LUOUT,NML=HSRC,DELIM='APOSTROPHE ')
    
    END SUBROUTINE PRINT_DEFAULT_NML_HSRC

    SUBROUTINE SET_NML_HSRC(OBJ)

    TYPE(TYPE_HSRC),INTENT(INOUT)	:: OBJ

    OBJ%ID=ID
    OBJ%LOCID=LOCID
    OBJ%MFLUX=MFLUX
    OBJ%HFLUX=HFLUX
    OBJ%T=T
    OBJ%YKS=YKS
    OBJ%SPECIDS=SPECIDS
    OBJ%CTRLID=CTRLID
    OBJ%HSRCTYPE=HSRCTYPE
    OBJ%WALLID=WALLID
    OBJ%AREA=AREA
    OBJ%XRAD=XRAD
    OBJ%MATIDS=MATIDS
    OBJ%ORIENTATION=ORIENTATION
    OBJ%COEF=COEF
    OBJ%MH2O0=MH2O0
    END SUBROUTINE SET_NML_HSRC

    SUBROUTINE COUNT_NML_HSRC(LU1,LUOUT,N_OBJ)

    INTEGER,INTENT(IN)			:: LU1,LUOUT
    INTEGER,INTENT(OUT)			:: N_OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    N_OBJ=0
    REWIND(LU1)
    COUNT_OBJ_LOOP: DO
        CALL CHECKREAD('HSRC',LU1,IOS)
        IF (IOS==0) EXIT COUNT_OBJ_LOOP
        READ(LU1,NML=HSRC,END=209,ERR=210,IOSTAT=IOS)
        N_OBJ=N_OBJ+1
210     IF (IOS>0) THEN
            WRITE(LUOUT,NML=HSRC)
            WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &HSRC line number ',N_OBJ+1,': check parameters names and values'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDDO COUNT_OBJ_LOOP
209 REWIND(LU1)

    END SUBROUTINE COUNT_NML_HSRC


    SUBROUTINE READ_NML_HSRC(LU1,LUOUT,OBJRT)

    INTEGER,INTENT(IN)								:: LU1,LUOUT
    TYPE(TYPE_HSRC),DIMENSION(:),INTENT(INOUT)		:: OBJRT
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    REWIND(LU1)
    SET_OBJ_LOOP: DO I=1,SIZE(OBJRT)
        CALL CHECKREAD('HSRC',LU1,IOS)
        IF (IOS==0) EXIT SET_OBJ_LOOP
        CALL DEFAULT_NML_HSRC
        READ(LU1,HSRC)
        CALL SET_NML_HSRC(OBJRT(I))
    ENDDO SET_OBJ_LOOP
    REWIND(LU1)

    END SUBROUTINE READ_NML_HSRC

    END MODULE READ_NML_HSRC_MODULE
