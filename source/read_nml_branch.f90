    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE READ_NML_BRANCHE_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    CHARACTER(100)						:: ID,BRANCHTYPE,CTRLID
    CHARACTER(1000)						:: CDFILE
    CHARACTER(1000)						:: PQFILE
    CHARACTER(100),DIMENSION(2)			:: LOCIDS
    DOUBLE PRECISION					:: Z1,Z2
    DOUBLE PRECISION					:: LENGTH,DIAM,SECTION,COEF,RUGO,HEIGHT,IOR,kacc,Facc,Uacc,AV,Ar,Cx,Urame,pcrame,Densrame,Srame,friction,Lrame0,k31BrD,k31BrI,k11Br
    DOUBLE PRECISION,DIMENSION(2)		:: SINGU,ALPHA
    DOUBLE PRECISION					:: DPREF,RHOREF,DPV0,DPV1,DPV2,QM0,QV0,QV1,QV2,HR1,HR2,K,K1,K2,EXPO
    INTEGER                             :: NV
    CHARACTER(100)						:: ACCCTRLID,RAMECTRLID
    INTEGER                             :: NACC

    NAMELIST /BRANCH/ ID,LOCIDS,BRANCHTYPE,Z1,Z2,LENGTH,DIAM,SECTION,COEF,RUGO,SINGU,HEIGHT, &
                    DPREF,RHOREF,DPV0,DPV1,DPV2,QM0,QV0,QV1,QV2,HR1,HR2,K,K1,K2,EXPO,IOR,CDFILE,CTRLID,ALPHA,PQFILE,NV,  &
                    NACC,KACC,FACC,UACC,AV,AR,CX,URAME,PCRAME,DENSRAME,SRAME,ACCCTRLID,RAMECTRLID,FRICTION,LRAME0,K31BRD,K31BRI,K11BR

    CONTAINS

    SUBROUTINE DEFAULT_NML_BRANCHE

    TYPE(TYPE_BRANCHE)		:: OBJ

    ID=OBJ%ID
    BRANCHTYPE=OBJ%BRANCHTYPE
    LOCIDS=OBJ%LOCIDS
    Z1=OBJ%Z1
    Z2=OBJ%Z2
    LENGTH=OBJ%LENGTH
    DIAM=OBJ%DIAM
    SECTION=OBJ%SECTION
    HEIGHT=OBJ%HEIGHT
    COEF=OBJ%COEF
    RUGO=OBJ%RUGO
    SINGU=OBJ%SINGU
    DPV0=OBJ%DPV0
    DPV1=OBJ%DPV1
    DPV2=OBJ%DPV2
    QM0=OBJ%QM0
    QV0=OBJ%QV0
    QV1=OBJ%QV1
    QV2=OBJ%QV2
    HR1=OBJ%HR1
    HR2=OBJ%HR2
    ALPHA=OBJ%ALPHA
    K=OBJ%K0
    K1=OBJ%K1
    K2=OBJ%K2
    EXPO=OBJ%EXPO
    DPREF=OBJ%DPREF
    RHOREF=OBJ%RHOREF
    IOR=OBJ%IOR
    CDFILE=OBJ%CDFILE
    PQFILE=OBJ%PQFILE
    CTRLID=OBJ%CTRLID
    NV=OBJ%NV
    nacc=OBJ%nacc
    kacc=OBJ%kacc
    Facc=OBJ%Facc
    Uacc=OBJ%Uacc
    AV=OBJ%AV
    AR=OBJ%AR
    CX=OBJ%CX
    URAME=OBJ%URAME
    PCRAME=OBJ%PCRAME
    DENSRAME=OBJ%DENSRAME
    SRAME=OBJ%SRAME
    ACCCTRLID=OBJ%ACCCTRLID
    RAMECTRLID=OBJ%RAMECTRLID
    FRICTION=OBJ%FRICTION
    LRAME0=OBJ%LRAME0
    K31BRD=OBJ%K31BRD
    K31BRI=OBJ%K31BRI
    K11BR=OBJ%K11BR

    END SUBROUTINE DEFAULT_NML_BRANCHE
    
    SUBROUTINE PRINT_DEFAULT_NML_BRANCHE(LUOUT)
    
    INTEGER,INTENT(IN)			:: LUOUT
    
    CALL DEFAULT_NML_BRANCHE
    WRITE(LUOUT,NML=BRANCH,DELIM='APOSTROPHE ')
    
    END SUBROUTINE PRINT_DEFAULT_NML_BRANCHE




    SUBROUTINE SET_NML_BRANCHE(OBJ)

    TYPE(TYPE_BRANCHE),INTENT(INOUT)	:: OBJ

    OBJ%ID=ID
    OBJ%BRANCHTYPE=BRANCHTYPE
    OBJ%LOCIDS=LOCIDS
    OBJ%Z1=Z1
    OBJ%Z2=Z2
    OBJ%LENGTH=LENGTH
    OBJ%DIAM=DIAM
    OBJ%SECTION=SECTION
    OBJ%HEIGHT=HEIGHT
    OBJ%COEF=COEF
    OBJ%RUGO=RUGO
    OBJ%SINGU=SINGU
    OBJ%DPV0=DPV0
    OBJ%DPV1=DPV1
    OBJ%DPV2=DPV2
    OBJ%QM0=QM0
    OBJ%QV0=QV0
    OBJ%QV1=QV1
    OBJ%QV2=QV2
    OBJ%HR1=HR1
    OBJ%HR2=HR2
    OBJ%ALPHA=ALPHA
    OBJ%K0=K
    OBJ%K1=K1
    OBJ%K2=K2
    OBJ%EXPO=EXPO
    OBJ%DPREF=DPREF
    OBJ%RHOREF=RHOREF
    OBJ%IOR=IOR
    OBJ%CDFILE=CDFILE
    OBJ%PQFILE=PQFILE
    OBJ%CTRLID=CTRLID
    OBJ%NV=NV
    OBJ%nacc=nacc
    OBJ%kacc=kacc
    OBJ%Uacc=Uacc
    OBJ%Facc=Facc
    OBJ%Av=AV
    OBJ%Ar=Ar
    OBJ%Cx=Cx
    OBJ%Urame=Urame
    OBJ%pcrame=pcrame
    OBJ%Densrame=Densrame
    OBJ%Srame=Srame
    obj%accctrlid=accctrlid
    obj%ramectrlid=ramectrlid
    obj%friction=friction
    obj%Lrame0=Lrame0
    OBJ%k31BrD=k31BrD
    OBJ%k31BrI=k31BrI
    OBJ%k11Br=k11Br

    END SUBROUTINE SET_NML_BRANCHE

    SUBROUTINE COUNT_NML_BRANCHE(LU1,LUOUT,N_OBJ)

    INTEGER,INTENT(IN)			:: LU1,LUOUT
    INTEGER,INTENT(OUT)			:: N_OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    N_OBJ=0
    REWIND(LU1)
    COUNT_OBJ_LOOP: DO
        CALL CHECKREAD('BRANCH',LU1,IOS)
        IF (IOS==0) EXIT COUNT_OBJ_LOOP
        READ(LU1,NML=BRANCH,END=209,ERR=210,IOSTAT=IOS)
        N_OBJ=N_OBJ+1
210     IF (IOS>0) THEN
            WRITE(LUOUT,NML=BRANCH)
            WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &BRANCH line number ',N_OBJ+1,': check parameters names and values'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDDO COUNT_OBJ_LOOP
209 REWIND(LU1)

    END SUBROUTINE COUNT_NML_BRANCHE

    SUBROUTINE READ_NML_BRANCHE(LU1,LUOUT,OBJRT)

    INTEGER,INTENT(IN)								:: LU1,LUOUT
    TYPE(TYPE_BRANCHE),DIMENSION(:),INTENT(INOUT)	:: OBJRT
    CHARACTER(256)									:: MESSAGE
    INTEGER											:: I,IOS

    REWIND(LU1)
    SET_OBJ_LOOP: DO I=1,SIZE(OBJRT)
        CALL CHECKREAD('BRANCH',LU1,IOS)
        IF (IOS==0) EXIT SET_OBJ_LOOP
        CALL DEFAULT_NML_BRANCHE
        READ(LU1,BRANCH)
        CALL SET_NML_BRANCHE(OBJRT(I))
    ENDDO SET_OBJ_LOOP
    REWIND(LU1)

    END SUBROUTINE READ_NML_BRANCHE

    END MODULE READ_NML_BRANCHE_MODULE
