MODULE POST_ATEC_MODULE

    USE ENVIRO_MODULE
	
	IMPLICIT NONE
	
	DOUBLE PRECISION,PARAMETER						:: KSV=6.D0,KDV=3.D0
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE		:: CO2BASE,CO2TEXPO,HRBASE,HRTEXPO
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE		:: QEXTRAIT,QTRAVERS,QDEPER,QTRAVDEPER,EA_MODULE,HR75,CONDSV,CONDDV,CONDSVMAX,CONDDVMAX,H_INVERSE,H_INVERSE_OCC,QEAPERM,H_INVEAPERM,H_INVEAPERM_OCC,ICONE,IHR,RAMEAN,RAMEANOCC,DUREEOCC,H_SSAIRNEUF,H_SSAIRNEUF_OCC
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE		:: VA_HFONC,VA_HFONC_OCC,VA_QDOT,VA_QV
    DOUBLE PRECISION								:: TSV,TDV,YSATSV,YSATDV,DUREE,NSTEP
    DOUBLE PRECISION								:: VA_TMINI,VA_TMAXI,VA_TMOY
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE		:: TOPMIN,TOPMAX
    INTEGER,DIMENSION(:),ALLOCATABLE				:: ISINVERSE,ISAIRNEUF
	
	CONTAINS
	
	
	SUBROUTINE INIT_ATEC_RES(LU)
	
	INTEGER,INTENT(IN)							:: LU
	INTEGER                                     :: ILOC,IBOUND
	
	CALL CLEAN_ATEC_FILE(LU)
	
	IF ((ATEC.EQV..TRUE.).AND.(VA_LOCID.NE.'null')) THEN
        DO ILOC=1,N_LOC
            IF (VA_LOCID==LOCRT(ILOC)%ID) VA_IDN=ILOC
        ENDDO
        DO IBOUND=1,N_BOUND
            IF (VA_LOCID==BOUNDRT(IBOUND)%ID) VA_IDN=N_LOC+MDEXT(1)%N_ZPEXT+IBOUND
        ENDDO
        IF (VA_IDN==0) THEN
            WRITE(MESSAGE,'(A,A,A,I1,A)') 'ERROR - Problem with MISC : VA_LOCID (',VA_LOCID,') is unknown'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDIF
    
    IF (ALLOCATED(CO2BASE)) THEN
        DEALLOCATE(CO2BASE)
        DEALLOCATE(CO2TEXPO)
        DEALLOCATE(HRBASE)
        DEALLOCATE(HRTEXPO)
        DEALLOCATE(QDEPER)
        DEALLOCATE(QTRAVDEPER)
        DEALLOCATE(QEXTRAIT)
        DEALLOCATE(QTRAVERS)
        DEALLOCATE(RAMEAN)
        DEALLOCATE(RAMEANOCC)
        DEALLOCATE(EA_MODULE)
        DEALLOCATE(H_INVERSE)
        DEALLOCATE(H_INVERSE_OCC)
        DEALLOCATE(QEAPERM)
        DEALLOCATE(H_INVEAPERM)
        DEALLOCATE(H_INVEAPERM_OCC)
        DEALLOCATE(ISINVERSE)
        DEALLOCATE(H_SSAIRNEUF)
        DEALLOCATE(H_SSAIRNEUF_OCC)
        DEALLOCATE(ISAIRNEUF)
        DEALLOCATE(HR75)
        DEALLOCATE(CONDSV)
        DEALLOCATE(CONDDV)
        DEALLOCATE(CONDSVMAX)
        DEALLOCATE(CONDDVMAX)
        DEALLOCATE(ICONE)
        DEALLOCATE(IHR)
        DEALLOCATE(DUREEOCC)
        DEALLOCATE(VA_HFONC)
        DEALLOCATE(VA_HFONC_OCC)
        DEALLOCATE(VA_QDOT)
        DEALLOCATE(VA_QV)
        DEALLOCATE(TOPMAX)
        DEALLOCATE(TOPMIN)
    ENDIF
    IF (.NOT.ALLOCATED(CO2BASE)) THEN
        ALLOCATE(CO2BASE(N_LOC,201))
        CO2BASE=0.D0
        ALLOCATE(CO2TEXPO(N_LOC,201))
        CO2TEXPO=0.D0
        ALLOCATE(HRBASE(N_LOC,101))
        HRBASE=0.D0
        ALLOCATE(HRTEXPO(N_LOC,101))
        HRTEXPO=0.D0
        ALLOCATE(QDEPER(N_LOC))
        QDEPER=0.D0
        ALLOCATE(QTRAVDEPER(N_LOC))
        QTRAVDEPER=0.D0
        ALLOCATE(QEXTRAIT(N_LOC))
        QEXTRAIT=0.D0
        ALLOCATE(QTRAVERS(N_LOC))
        QTRAVERS=0.D0
        ALLOCATE(RAMEAN(N_LOC))
        RAMEAN=0.D0
        ALLOCATE(RAMEANOCC(N_LOC))
        RAMEANOCC=0.D0
        ALLOCATE(EA_MODULE(N_LOC))
        EA_MODULE=0.D0
        ALLOCATE(H_INVERSE(N_LOC))
        H_INVERSE=0.D0
        ALLOCATE(H_INVERSE_OCC(N_LOC))
        H_INVERSE_OCC=0.D0
        ALLOCATE(QEAPERM(N_LOC))
        QEAPERM=0.D0
        ALLOCATE(H_INVEAPERM(N_LOC))
        H_INVEAPERM=0.D0
        ALLOCATE(H_INVEAPERM_OCC(N_LOC))
        H_INVEAPERM_OCC=0.D0
        ALLOCATE(ISINVERSE(N_LOC))
        ISINVERSE=0
        ALLOCATE(H_SSAIRNEUF(N_LOC))
        H_SSAIRNEUF=0.D0
        ALLOCATE(H_SSAIRNEUF_OCC(N_LOC))
        H_SSAIRNEUF_OCC=0.D0
        ALLOCATE(ISAIRNEUF(N_LOC))
        ISAIRNEUF=0
        DUREE=0.D0
        ALLOCATE(HR75(N_LOC))
        HR75=0.D0
        ALLOCATE(CONDSV(N_LOC))
        CONDSV=0.D0
        ALLOCATE(CONDDV(N_LOC))
        CONDDV=0.D0
        ALLOCATE(CONDSVMAX(N_LOC))
        CONDSVMAX=0.D0
        ALLOCATE(CONDDVMAX(N_LOC))
        CONDDVMAX=0.D0
        ALLOCATE(ICONE(N_LOC))
        ICONE=0.D0
        ALLOCATE(IHR(N_LOC))
        IHR=0.D0
        ALLOCATE(DUREEOCC(N_LOC))
        DUREEOCC=0.D0

        ALLOCATE(VA_HFONC(N_LOC))
        VA_HFONC=0
        ALLOCATE(VA_HFONC_OCC(N_LOC))
        VA_HFONC_OCC=0
        ALLOCATE(VA_QDOT(N_LOC))
        VA_QDOT=0
        ALLOCATE(VA_QV(N_LOC))
        VA_QV=0
        ALLOCATE(TOPMAX(N_LOC))
        TOPMAX=-9999.D0
        ALLOCATE(TOPMIN(N_LOC))
        TOPMIN=9999.D0
        VA_TMINI=9999.D0
        VA_TMAXI=-9999.D0
        VA_TMOY=0.D0
    ENDIF
    END SUBROUTINE INIT_ATEC_RES

    SUBROUTINE CALC_ATEC_RES

    INTEGER								:: ICLASSECO2,ICLASSEHR,ILOC,IHSRC,IBRANCHE,ILOC1,ILOC2,IBOUND,ISVAFONC
    DOUBLE PRECISION						:: CLASSE
    DOUBLE PRECISION,DIMENSION(N_LOC)		:: OCC
    CHARACTER(100)						:: FMT

    DUREE=DUREE+DTETA
    NSTEP=NSTEP+1.D0
    OCC=LOCRT(:)%OCC
    DO ILOC=1,N_LOC
        IF (OCC(ILOC)>=1.D0) DUREEOCC(ILOC)=DUREEOCC(ILOC)+DTETA
        RAMEAN(ILOC)=RAMEAN(ILOC)+LOCRT(ILOC)%RA*DTETA
        IF (OCC(ILOC)>=1) RAMEANOCC(ILOC)=RAMEANOCC(ILOC)+LOCRT(ILOC)%RA*DTETA
        ICLASSECO2=INT(YK15(ILOC,IDNCO2)*1.D6*29.D0/44.D0/50)+1
        IF (ICLASSECO2<1) ICLASSECO2=1
        IF (ICLASSECO2>201) ICLASSECO2=201
        CO2BASE(ILOC,1:ICLASSECO2)=CO2BASE(ILOC,1:ICLASSECO2)+MIN(1.D0,OCC(ILOC))*YK15(ILOC,IDNCO2)*1.D6*29.D0/44.D0*DTETA/3600.D0
        CO2TEXPO(ILOC,1:ICLASSECO2)=CO2TEXPO(ILOC,1:ICLASSECO2)+MIN(1.D0,OCC(ILOC))*DTETA/3600.D0
        IF (HUM15(ILOC)>75.D0) THEN
            HR75(ILOC)=HR75(ILOC)+DTETA/3600.D0
        ENDIF
        ICLASSEHR=INT(HUM15(ILOC))+1
        IF (ICLASSEHR<1) ICLASSEHR=1
        IF (ICLASSEHR>101) ICLASSEHR=101
        HRBASE(ILOC,1:ICLASSEHR)=HRBASE(ILOC,1:ICLASSEHR)+HUM15(ILOC)*DTETA/3600.D0
        HRTEXPO(ILOC,1:ICLASSEHR)=HRTEXPO(ILOC,1:ICLASSEHR)+DTETA/3600.D0

        IF (MDEXT(1)%TEXT<=290.15D0) THEN
            TSV=T15(ILOC)-KSV/9.D0*(T15(ILOC)-MDEXT(1)%TEXT)
            CALL YK_HUM_EVAL(TSV,100.D0,YSATSV,1)
            IF (YSATSV<=YK15(ILOC,IDNH2O)) THEN
                CONDSV(ILOC)=CONDSV(ILOC)+DTETA/3600.D0
                IF (CONDSV(ILOC)>CONDSVMAX(ILOC)) THEN
                    CONDSVMAX(ILOC)=CONDSV(ILOC)
                ENDIF
            ELSE
                CONDSV(ILOC)=0.D0
            ENDIF
            TDV=T15(ILOC)-KDV/9.D0*(T15(ILOC)-MDEXT(1)%TEXT)
            CALL YK_HUM_EVAL(TDV,100.D0,YSATDV,1)
            IF (YSATDV<=YK15(ILOC,IDNH2O)) THEN
                CONDDV(ILOC)=CONDDV(ILOC)+DTETA/3600.D0
                IF (CONDDV(ILOC)>CONDDVMAX(ILOC)) THEN
                    CONDDVMAX(ILOC)=CONDDV(ILOC)
                ENDIF
            ELSE
                CONDDV(ILOC)=0.D0
            ENDIF
        ENDIF
    ENDDO

    ISAIRNEUF=0
    ISINVERSE=0
    QEAPERM=0.D0
    ISVAFONC=0

    DO IBRANCHE=1,N_BRANCHE

        ILOC1=BRANCHERT(IBRANCHE)%TAB(1)
        ILOC2=BRANCHERT(IBRANCHE)%TAB(2)
        IF (ILOC1<=N_LOC) THEN
            IF (LOCRT(ILOC1)%LOCTYPE=='ROOM') THEN
                IF ((MDEXT(1)%TEXT<=288.15D0).AND.(ILOC2>N_LOC)) THEN
                    BRANCHERT(IBRANCHE)%ODEPER(ILOC1)=BRANCHERT(IBRANCHE)%ODEPER(ILOC1)+1005.D0*(BRANCHERT(IBRANCHE)%T151-MDEXT(1)%TEXT)*DTETA
                ELSEIF (MDEXT(1)%TEXT<=288.15D0) THEN
                    IF (LOCRT(ILOC2)%LOCTYPE=='NODE') THEN
                        BRANCHERT(IBRANCHE)%ODEPER(ILOC1)=BRANCHERT(IBRANCHE)%ODEPER(ILOC1)+1005.D0*(BRANCHERT(IBRANCHE)%T151-MDEXT(1)%TEXT)*DTETA
                    ENDIF
                ENDIF
                IF ((ILOC2>N_LOC).AND.(ILOC2<N_LOC+MDEXT(1)%N_ZPEXT+1)) THEN
                    IF (BRANCHERT(IBRANCHE)%QM>=0.D0) THEN
                        IF ((BRANCHERT(IBRANCHE)%ATECTYPE=='BE')) THEN
                            IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                BRANCHERT(IBRANCHE)%QDEPER(ILOC1)=BRANCHERT(IBRANCHE)%QDEPER(ILOC1)+BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T151-MDEXT(1)%TEXT)*DTETA
                            ENDIF
                            QEXTRAIT(ILOC1)=QEXTRAIT(ILOC1)+BRANCHERT(IBRANCHE)%QM*DTETA
                        ELSE
                            IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                BRANCHERT(IBRANCHE)%QTRAVDEPER(ILOC1)=BRANCHERT(IBRANCHE)%QTRAVDEPER(ILOC1)+BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T151-MDEXT(1)%TEXT)*DTETA
                            ENDIF
                            QTRAVERS(ILOC1)=QTRAVERS(ILOC1)+BRANCHERT(IBRANCHE)%QM*DTETA
                        ENDIF
                    ENDIF
                ELSEIF (LOCRT(ILOC2)%LOCTYPE=='NODE') THEN
                    IF (BRANCHERT(IBRANCHE)%QM>=0.D0) THEN
                        IF ((BRANCHERT(IBRANCHE)%ATECTYPE=='BE')) THEN
                            IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                BRANCHERT(IBRANCHE)%QDEPER(ILOC1)=BRANCHERT(IBRANCHE)%QDEPER(ILOC1)+BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T151-MDEXT(1)%TEXT)*DTETA
                            ENDIF
                            QEXTRAIT(ILOC1)=QEXTRAIT(ILOC1)+BRANCHERT(IBRANCHE)%QM*DTETA
                        ENDIF
                    ENDIF
                ELSEIF (ILOC2>N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    IF ((BOUNDRT(ILOC2-N_LOC-MDEXT(1)%N_ZPEXT)%ID=='Bound_'//LOCRT(ILOC1)%ID).OR.(INDEX(BOUNDRT(ILOC2-N_LOC-MDEXT(1)%N_ZPEXT)%ID,'NP').NE.0)) THEN
                        IF (BRANCHERT(IBRANCHE)%QM>=0.D0) THEN
                            IF ((BRANCHERT(IBRANCHE)%ATECTYPE=='BE')) THEN
                                IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                    BRANCHERT(IBRANCHE)%QDEPER(ILOC1)=BRANCHERT(IBRANCHE)%QDEPER(ILOC1)+BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T151-MDEXT(1)%TEXT)*DTETA
                                ENDIF
                                QEXTRAIT(ILOC1)=QEXTRAIT(ILOC1)+BRANCHERT(IBRANCHE)%QM*DTETA
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
        IF (ILOC2<=N_LOC) THEN
            IF (LOCRT(ILOC2)%LOCTYPE=='ROOM') THEN
                IF ((MDEXT(1)%TEXT<=288.15D0).AND.(ILOC1>N_LOC)) THEN
                    BRANCHERT(IBRANCHE)%ODEPER(ILOC2)=BRANCHERT(IBRANCHE)%ODEPER(ILOC2)+1005.D0*(BRANCHERT(IBRANCHE)%T152-MDEXT(1)%TEXT)*DTETA
                ELSEIF (MDEXT(1)%TEXT<=288.15D0) THEN
                    IF (LOCRT(ILOC1)%LOCTYPE=='NODE') THEN
                        BRANCHERT(IBRANCHE)%ODEPER(ILOC2)=BRANCHERT(IBRANCHE)%ODEPER(ILOC2)+1005.D0*(BRANCHERT(IBRANCHE)%T152-MDEXT(1)%TEXT)*DTETA
                    ENDIF
                ENDIF
                IF ((ILOC1>N_LOC).AND.(ILOC1<N_LOC+MDEXT(1)%N_ZPEXT+1))  THEN
                    IF (BRANCHERT(IBRANCHE)%QM<=0.D0) THEN
                        IF ((BRANCHERT(IBRANCHE)%ATECTYPE=='BE')) THEN
                            IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                BRANCHERT(IBRANCHE)%QDEPER(ILOC2)=BRANCHERT(IBRANCHE)%QDEPER(ILOC2)-BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T152-MDEXT(1)%TEXT)*DTETA
                            ENDIF
                            QEXTRAIT(ILOC2)=QEXTRAIT(ILOC2)-BRANCHERT(IBRANCHE)%QM*DTETA
                        ELSE
                            IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                BRANCHERT(IBRANCHE)%QTRAVDEPER(ILOC2)=BRANCHERT(IBRANCHE)%QTRAVDEPER(ILOC2)-BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T152-MDEXT(1)%TEXT)*DTETA
                            ENDIF
                            QTRAVERS(ILOC2)=QTRAVERS(ILOC2)-BRANCHERT(IBRANCHE)%QM*DTETA
                        ENDIF
                    ENDIF
                ELSEIF (LOCRT(ILOC1)%LOCTYPE=='NODE') THEN
                    IF (BRANCHERT(IBRANCHE)%QM<=0.D0) THEN
                        IF ((BRANCHERT(IBRANCHE)%ATECTYPE=='BE')) THEN
                            IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                BRANCHERT(IBRANCHE)%QDEPER(ILOC2)=BRANCHERT(IBRANCHE)%QDEPER(ILOC2)-BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T152-MDEXT(1)%TEXT)*DTETA
                            ENDIF
                            QEXTRAIT(ILOC2)=QEXTRAIT(ILOC2)-BRANCHERT(IBRANCHE)%QM*DTETA
                        ENDIF
                    ENDIF
                ELSEIF (ILOC1>N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    IF ((BOUNDRT(ILOC1-N_LOC-MDEXT(1)%N_ZPEXT)%ID=='Bound_'//LOCRT(ILOC2)%ID).OR.(INDEX(BOUNDRT(ILOC1-N_LOC-MDEXT(1)%N_ZPEXT)%ID,'NP').NE.0)) THEN
                        IF (BRANCHERT(IBRANCHE)%QM<=0.D0) THEN
                            IF ((BRANCHERT(IBRANCHE)%ATECTYPE=='BE')) THEN
                                IF (MDEXT(1)%TEXT<=288.15D0) THEN
                                    BRANCHERT(IBRANCHE)%QDEPER(ILOC2)=BRANCHERT(IBRANCHE)%QDEPER(ILOC2)-BRANCHERT(IBRANCHE)%QM*1005.D0*(BRANCHERT(IBRANCHE)%T152-MDEXT(1)%TEXT)*DTETA
                                ENDIF
                                QEXTRAIT(ILOC2)=QEXTRAIT(ILOC2)-BRANCHERT(IBRANCHE)%QM*DTETA
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF


        IF (BRANCHERT(IBRANCHE)%ATECTYPE=='EA') THEN
            ILOC1=BRANCHERT(IBRANCHE)%TAB(1)
            ILOC2=BRANCHERT(IBRANCHE)%TAB(2)
            IF (ILOC1<=N_LOC) THEN
                EA_MODULE(ILOC1)=EA_MODULE(ILOC1)+3600.D0*BRANCHERT(IBRANCHE)%EA_MODULE*DTETA
                IF (BRANCHERT(IBRANCHE)%QM>0.D0) THEN
                    ISINVERSE(ILOC1)=1
                ENDIF
            ENDIF
            IF (ILOC2<=N_LOC) THEN
                EA_MODULE(ILOC2)=EA_MODULE(ILOC2)+3600.D0*BRANCHERT(IBRANCHE)%EA_MODULE*DTETA
                IF (BRANCHERT(IBRANCHE)%QM<0.D0) THEN
                    ISINVERSE(ILOC2)=1
                ENDIF
            ENDIF
        ENDIF
        !IF (ALL((BRANCHERT(IBRANCHE)%YK15AM<=MDEXT(1)%YK+YKTOL*SPECRT%YKREF).AND.(BRANCHERT(IBRANCHE)%YK15AM>=MDEXT(1)%YK-YKTOL*SPECRT%YKREF)))  THEN
        ILOC1=BRANCHERT(IBRANCHE)%TAB(1)
        ILOC2=BRANCHERT(IBRANCHE)%TAB(2)
        IF ((ILOC1<=N_LOC).AND.(BRANCHERT(IBRANCHE)%QM<0.D0)) THEN
            ISAIRNEUF(ILOC1)=1
        ENDIF
        IF ((ILOC2<=N_LOC).AND.(BRANCHERT(IBRANCHE)%QM>0.D0)) THEN
            ISAIRNEUF(ILOC2)=1
        ENDIF
        !ENDIF
        IF ((BRANCHERT(IBRANCHE)%ATECTYPE=='EA').OR.(BRANCHERT(IBRANCHE)%ATECTYPE=='PE')) THEN
            ILOC1=BRANCHERT(IBRANCHE)%TAB(1)
            ILOC2=BRANCHERT(IBRANCHE)%TAB(2)
            IF (ILOC1<=N_LOC) THEN
                QEAPERM(ILOC1)=QEAPERM(ILOC1)-BRANCHERT(IBRANCHE)%QM
            ENDIF
            IF (ILOC2<=N_LOC) THEN
                QEAPERM(ILOC2)=QEAPERM(ILOC2)+BRANCHERT(IBRANCHE)%QM
            ENDIF
        ENDIF
        !gestion des sorties specifiques aux systemes Vecteur Air
        IF (VA_IDN.NE.0) THEN
            ILOC1=BRANCHERT(IBRANCHE)%TAB(1)
            ILOC2=BRANCHERT(IBRANCHE)%TAB(2)
            IF (BRANCHERT(IBRANCHE)%QM.NE.0.D0) THEN
                IF ((ILOC1==VA_IDN).AND.(ILOC2<=N_LOC)) THEN
                    ISVAFONC=1
                    VA_HFONC(ILOC2)=VA_HFONC(ILOC2)+DTETA
                    IF (OCC(ILOC2)>=1) VA_HFONC_OCC(ILOC2)=VA_HFONC_OCC(ILOC2)+DTETA
                    VA_QV(ILOC2)=VA_QV(ILOC2)+BRANCHERT(IBRANCHE)%QV*DTETA
                    VA_QDOT(ILOC2)=VA_QDOT(ILOC2)+BRANCHERT(IBRANCHE)%QM*1000.D0*(BRANCHERT(IBRANCHE)%T151-BRANCHERT(IBRANCHE)%T152)*DTETA
                ELSEIF ((ILOC2==VA_IDN).AND.(ILOC1<=N_LOC)) THEN
                    ISVAFONC=1
                    VA_HFONC(ILOC1)=VA_HFONC(ILOC1)+DTETA
                    IF (OCC(ILOC1)>=1) VA_HFONC_OCC(ILOC1)=VA_HFONC_OCC(ILOC1)+DTETA
                    VA_QV(ILOC1)=VA_QV(ILOC1)-BRANCHERT(IBRANCHE)%QV*DTETA
                    VA_QDOT(ILOC1)=VA_QDOT(ILOC1)-BRANCHERT(IBRANCHE)%QM*1000.D0*(BRANCHERT(IBRANCHE)%T152-BRANCHERT(IBRANCHE)%T151)*DTETA
                ENDIF
            ENDIF
        ENDIF
    ENDDO

    DO ILOC=1,N_LOC
        IF (ISAIRNEUF(ILOC)==0) THEN
            H_SSAIRNEUF(ILOC)=H_SSAIRNEUF(ILOC)+DTETA/3600.D0
            IF (OCC(ILOC)>=1) H_SSAIRNEUF_OCC(ILOC)=H_SSAIRNEUF_OCC(ILOC)+DTETA/3600.D0
        ENDIF
        IF (ISINVERSE(ILOC)==1) THEN
            H_INVERSE(ILOC)=H_INVERSE(ILOC)+DTETA/3600.D0
            IF (OCC(ILOC)>=1.D0) H_INVERSE_OCC(ILOC)=H_INVERSE_OCC(ILOC)+DTETA/3600.D0
        ENDIF
        IF (QEAPERM(ILOC)<0.D0) THEN
            H_INVEAPERM(ILOC)=H_INVEAPERM(ILOC)+DTETA/3600.D0
            IF (OCC(ILOC)>=1.D0) H_INVEAPERM_OCC(ILOC)=H_INVEAPERM_OCC(ILOC)+DTETA/3600.D0
        ENDIF
        IF ((VA_IDN.NE.0).AND.(LOCRT(ILOC)%ID==VA_LOCID).AND.(ISVAFONC==1)) THEN
            IF (LOCRT(ILOC)%T15<VA_TMINI) VA_TMINI=LOCRT(ILOC)%T15
            IF (LOCRT(ILOC)%T15>VA_TMAXI) VA_TMAXI=LOCRT(ILOC)%T15
            VA_TMOY=VA_TMOY+LOCRT(ILOC)%T15*DTETA
        ENDIF
        IF (VA_IDN.NE.0) THEN
            IF (LOCRT(ILOC)%TOPER<TOPMIN(ILOC)) TOPMIN(ILOC)=LOCRT(ILOC)%TOPER
            IF (LOCRT(ILOC)%TOPER>TOPMAX(ILOC)) TOPMAX(ILOC)=LOCRT(ILOC)%TOPER
        ENDIF
    ENDDO

    DO IBOUND=1,N_BOUND
        IF ((VA_IDN.NE.0).AND.(BOUNDRT(IBOUND)%ID==VA_LOCID).AND.(ISVAFONC==1)) THEN
            IF (BOUNDRT(IBOUND)%TBOUND<VA_TMINI) VA_TMINI=BOUNDRT(IBOUND)%TBOUND
            IF (BOUNDRT(IBOUND)%TBOUND>VA_TMAXI) VA_TMAXI=BOUNDRT(IBOUND)%TBOUND
            VA_TMOY=VA_TMOY+BOUNDRT(IBOUND)%TBOUND*DTETA
        ENDIF
    ENDDO

    END SUBROUTINE CALC_ATEC_RES


    SUBROUTINE WRITE_ATEC_RES(LU,NSC)

    INTEGER,INTENT(IN)							:: LU
    INTEGER,INTENT(IN),OPTIONAL					:: NSC
    CHARACTER(100000)								:: FNAME,FMT,FMTLOC,FMTVATEMP
    CHARACTER(256)								:: MESSAGEVATEMP
    INTEGER										:: ICLASSECO2,ICLASSEHR,CLASSE,ILOC,IBRANCHE,ERR
    CHARACTER TAB
    TAB=CHAR(9)

    IF (DUREE==0.D0) THEN
        WRITE(*,*) 'WARNING : Simulation time did not exceed the ATEC_BEGIN time'
    ELSE

        MESSAGE=TRIM(LOCRT(IDNROOM(1))%ID)
        DO ILOC=2,N_ROOM
            MESSAGE=TRIM(MESSAGE)//TAB//TRIM(LOCRT(IDNROOM(ILOC))%ID)
        ENDDO
        WRITE(FMTLOC,'("(A",i8,")")') LEN_TRIM(MESSAGE)

        MESSAGEVATEMP='Tmin'//TAB//'Tmax'//TAB//'Tmoy'
        WRITE(FMTVATEMP,'("(A",i8,")")') LEN_TRIM(MESSAGEVATEMP)

        FNAME=TRIM(JOBNAME)//'_histo_CO2base.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(A9,",i4,"(A17,:,"" ""))")') (N_ROOM)
        WRITE(LU,FMT) 'CO2_ppmv ',LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(i9,",i4,"(E17.9,:,"" ""))")') (N_ROOM)
        DO ICLASSECO2=1,201
            CLASSE=(ICLASSECO2-1)*50
            WRITE(LU,TRIM(FMT)) CLASSE,CO2BASE(IDNROOM,ICLASSECO2)
        ENDDO
        CLOSE(LU)

        FNAME=TRIM(JOBNAME)//'_histo_CO2texpo.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(A9,",i4,"(A17,:,"" ""))")') (N_ROOM)
        WRITE(LU,FMT) 'CO2_ppmv ',LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(i9,",i4,"(E17.9,:,"" ""))")') (N_ROOM)
        DO ICLASSECO2=1,201
            CLASSE=(ICLASSECO2-1)*50
            WRITE(LU,TRIM(FMT)) CLASSE,CO2TEXPO(IDNROOM,ICLASSECO2)
        ENDDO
        CLOSE(LU)

        FNAME=TRIM(JOBNAME)//'_histo_HRbase.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(A5,",i4,"(A17,:,"" ""))")') (N_ROOM)
        WRITE(LU,FMT) 'HR_% ',LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(i5,",i4,"(E17.9,:,"" ""))")') (N_ROOM)
        DO ICLASSEHR=1,101
            CLASSE=(ICLASSEHR-1)
            WRITE(LU,TRIM(FMT)) CLASSE,HRBASE(IDNROOM,ICLASSEHR)
        ENDDO
        CLOSE(LU)

        FNAME=TRIM(JOBNAME)//'_histo_HRtexpo.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(A5,",i4,"(A17,:,"" ""))")') (N_ROOM)
        WRITE(LU,FMT) 'HR_% ',LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(i5,",i4,"(E17.9,:,"" ""))")') (N_ROOM)
        DO ICLASSEHR=1,101
            CLASSE=(ICLASSEHR-1)
            WRITE(LU,TRIM(FMT)) CLASSE,HRTEXPO(IDNROOM,ICLASSEHR)
        ENDDO
        CLOSE(LU)


        FNAME=TRIM(JOBNAME)//'_ATEC.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')

        DO ILOC=1,N_LOC
            DO IBRANCHE=1,N_BRANCHE
                IF (BRANCHERT(IBRANCHE)%ODEPER(ILOC).NE.0.D0) THEN
                    QDEPER(ILOC)=QDEPER(ILOC)+BRANCHERT(IBRANCHE)%QDEPER(ILOC)*3600.D0/(1.204785775D0*BRANCHERT(IBRANCHE)%ODEPER(ILOC))
                    QTRAVDEPER(ILOC)=QTRAVDEPER(ILOC)+BRANCHERT(IBRANCHE)%QTRAVDEPER(ILOC)*3600.D0/(1.204785775D0*BRANCHERT(IBRANCHE)%ODEPER(ILOC))
                ENDIF
            ENDDO
        ENDDO
        WRITE(LU,'("Debit moyen extrait deperditif (m3/h) :")')
        WRITE(LU,'(F9.2)') SUM(QDEPER)

        WRITE(LU,'("Debit moyen extrait deperditif par piece (m3/h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.2,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) QDEPER(IDNROOM)
        WRITE(LU,'(" ")')

        WRITE(LU,'("Debit moyen extrait par piece (m3/h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) QEXTRAIT(IDNROOM)*3600.D0/(1.204785775D0*DUREE)
        WRITE(LU,'(" ")')

        WRITE(LU,'("Debit moyen traversant deperditif par piece (m3/h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.2,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) QTRAVDEPER(IDNROOM)
        WRITE(LU,'(" ")')

        WRITE(LU,'("Debit moyen traversant par piece (m3/h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) QTRAVERS(IDNROOM)*3600.D0/(1.204785775D0*DUREE)
        WRITE(LU,'(" ")')

        !	  WRITE(LU,'("Duree maximale de condensation sur un DV (heures) :")')
        !	  WRITE(LU,FMTLOC) TRIM(MESSAGE)
        !	  WRITE(FMT,'("(",i4,"(F8.1,:,""	""))")') (N_ROOM)
        !	  WRITE(LU,FMT) CONDDVMAX(IDNROOM)
        !	  WRITE(LU,'(" ")')

        WRITE(LU,'("Nombre d''heures avec HR > 75% :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) HR75(IDNROOM)
        WRITE(LU,'(" ")')

        IHR=(2.5D0/LOG10(2.D0))*LOG10(1.D0+(HRTEXPO(:,66)+2.D0*HRTEXPO(:,76))/MAX(1D-16,HRTEXPO(:,1)))

        WRITE(LU,'("Valeur brute de l''indice humidite IHR () :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) ANINT(IHR(IDNROOM)*10)/10
        WRITE(LU,'(" ")')


        WRITE(LU,'("ppm cumules de CO2 (10^3 ppm.heure base 2000) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) CO2BASE(IDNROOM,41)/1000.D0
        WRITE(LU,'(" ")')

        ICONE=(2.5D0/LOG10(2.D0))*LOG10(1.D0+(CO2TEXPO(:,21)+2.D0*CO2TEXPO(:,35))/MAX(1D-16,CO2TEXPO(:,1)))

        WRITE(LU,'("Valeur brute de l''indice de confinement ICONE en occupation () :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) ANINT(ICONE(IDNROOM)*10)/10
        WRITE(LU,'(" ")')


        WRITE(LU,'("Module moyen des entrees d''air (m3/h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) EA_MODULE(IDNROOM)/DUREE
        WRITE(LU,'(" ")')

        WRITE(LU,'("Nombre d''heures d''inversion aux entrees d''air (h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) H_INVERSE(IDNROOM)
        WRITE(LU,'(" ")')

        WRITE(LU,'("Nombre d''heures d''inversion aux entrees d''air en occupation (h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) H_INVERSE_OCC(IDNROOM)
        WRITE(LU,'(" ")')


        WRITE(LU,'("Taux de renouvellement d''air moyen (vol/h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.2,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) RAMEAN(IDNROOM)/DUREE
        WRITE(LU,'(" ")')

        WRITE(LU,'("Taux de renouvellement d''air moyen en occupation (vol/h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.2,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) RAMEANOCC(IDNROOM)/MAX(1.D-12,DUREEOCC(IDNROOM))
        WRITE(LU,'(" ")')

        !WRITE(LU,'("Nombre d''heures sans apport d''air neuf (h) :")')
        !WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        !WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) H_SSAIRNEUF(IDNROOM)
        !WRITE(LU,'(" ")')
        !
        !WRITE(LU,'("Nombre d''heures sans apport d''air neuf en occupation (h) :")')
        !WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        !WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) H_SSAIRNEUF_OCC(IDNROOM)
        !WRITE(LU,'(" ")')

        !WRITE(LU,'("Nombre d''heures ou la somme des debits aux entrees d''air et permeabilites est negative (h) :")')
        !WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        !WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) H_INVEAPERM(IDNROOM)
        !WRITE(LU,'(" ")')
        !
        !WRITE(LU,'("Nombre d''heures ou la somme des debits aux entrees d''air et permeabilites est negative en occupation (h) :")')
        !WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        !WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        !WRITE(LU,FMT) H_INVEAPERM_OCC(IDNROOM)
        !WRITE(LU,'(" ")')

        WRITE(LU,'("Volume des pieces (m3) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%VOLUME
        WRITE(LU,'(" ")')

        WRITE(LU,'("Duree d''occupation (h) :")')
        WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) LOCRT(IDNROOM)%ID
        WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_ROOM)
        WRITE(LU,FMT) DUREEOCC(IDNROOM)/3600.D0
        WRITE(LU,'(" ")')

        IF (VA_IDN.NE.0) THEN

            WRITE(LU,'("Duree de fonctionnement du systeme vecteur air (h) :")')
            WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) LOCRT(:)%ID
            WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) VA_HFONC(:)/3600.D0
            WRITE(LU,'(" ")')

            WRITE(LU,'("Duree de fonctionnement du systeme vecteur air en occupation (h) :")')
            WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) LOCRT(:)%ID
            WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) VA_HFONC_OCC(:)/3600.D0
            WRITE(LU,'(" ")')

            WRITE(LU,'("Debit moyen du systeme vecteur air (m3/h) :")')
            WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) LOCRT(:)%ID
            WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) VA_QV(:)*3600.D0/MAX(1.D-12,VA_HFONC(:))
            WRITE(LU,'(" ")')

            WRITE(LU,'("Puissance moyenne du systeme vecteur air (W) :")')
            WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) LOCRT(:)%ID
            WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) VA_QDOT(:)/MAX(1.D-12,VA_HFONC(:))
            WRITE(LU,'(" ")')

            WRITE(LU,'("Temperatures de fonctionnement du systeme vecteur air (C) :")')
            WRITE(LU,FMTVATEMP) TRIM(MESSAGEVATEMP)
            WRITE(FMT,'("(",i4,"(F8.1,:,""	""))")') (3)
            WRITE(LU,FMT) VA_TMINI-273.15D0,VA_TMAXI-273.15D0,VA_TMOY/MAX(1.D-12,MAXVAL(VA_HFONC))-273.15D0
            WRITE(LU,'(" ")')

            WRITE(LU,'("Temperature operative mini (C) :")')
            WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) LOCRT(:)%ID
            WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) TOPMIN(:)-273.15D0
            WRITE(LU,'(" ")')

            WRITE(LU,'("Temperature operative maxi (C) :")')
            WRITE(FMT,'("(",i4,"(A9,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) LOCRT(:)%ID
            WRITE(FMT,'("(",i4,"(F9.1,:,""	""))")') (N_LOC)
            WRITE(LU,FMT) TOPMAX(:)-273.15D0
            WRITE(LU,'(" ")')

        ENDIF

        CLOSE(LU)

        CALL WRITE_ATEC_XML(LU,NSC)
    ENDIF

    END SUBROUTINE WRITE_ATEC_RES

    SUBROUTINE WRITE_ATEC_XML(LU,NSC)

    INTEGER,INTENT(IN)							:: LU
    INTEGER,INTENT(IN),OPTIONAL					:: NSC
    CHARACTER(100000)								:: FNAME,FMT,FMTLOC,DESCRIPTION
    INTEGER										:: ICLASSECO2,CLASSE,ILOC,ERR,TESTMBCR
    CHARACTER TAB

    TAB=CHAR(9)

    FNAME=TRIM(JOBNAME)//'_ATEC.xml'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')

    WRITE(LU,'("<?xml version=""1.0""?>")')

    WRITE(LU,'("<Sorties>")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>QDeper</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Debit moyen extrait deperditif par piece</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>m3/h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') QDEPER(ILOC)
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>QExtrait</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Debit moyen extrait par piece</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>m3/h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') QEXTRAIT(ILOC)*3600.D0/(1.204785775D0*DUREE)
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>QTravDeper</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Debit moyen traversant deperditif par piece</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>m3/h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') QTRAVDEPER(ILOC)
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')


    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>QTraversant</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Debit moyen traversant par piece</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>m3/h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') QTRAVERS(ILOC)*3600.D0/(1.204785775D0*DUREE)
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')


    ! WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Id>CondDVMax</Id>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Description>Duree maximale de condensation sur un double vitrage</Description>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    ! DO ILOC=1,N_LOC
    !WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
    !WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
    !WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') CONDDVMAX(ILOC)
    !   WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ! ENDDO
    ! WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    ! WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>HR75</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Nombre d''heures avec HR > 75%</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') HR75(ILOC)
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>IHR</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Valeur brute de l''indice humidite</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') ANINT(IHR(ILOC)*10)/10
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')


    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>PPMBase2000</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>ppm cumules de CO2 </Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>10^3 ppm.heure base 2000</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') CO2BASE(ILOC,41)/1000.D0
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>ICONE</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Valeur brute de l''indice de confinement en occupation</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') ANINT(ICONE(ILOC)*10)/10
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>MEA</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Module moyen des entrees d''air</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>m3/h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') EA_MODULE(ILOC)/DUREE
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>HInverse</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Nombre d''heures d''inversion aux entrees d''air</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') H_INVERSE(ILOC)
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>HInverseOcc</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Nombre d''heures d''inversion aux entrees d''air en occupation</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') H_INVERSE_OCC(ILOC)
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')



    WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Id>TauxAirNeufOcc</Id>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Description>Taux de renouvellement d''air moyen en occupation</Description>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    DO ILOC=1,N_LOC
        WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
        WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.2)"</Value>'//'")') RAMEANOCC(ILOC)/MAX(1.D-12,DUREEOCC(ILOC))
        WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ENDDO
    WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    ! WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Id>HSansAirNeufOcc</Id>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Description>Nombre d''heures sans apport d''air neuf en occupation</Description>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    ! DO ILOC=1,N_LOC
    !WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
    !WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
    !WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') H_SSAIRNEUF_OCC(ILOC)
    !   WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ! ENDDO
    ! WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    ! WRITE(LU,'("'//TAB//'</Sortie>'//'")')
    !
    ! WRITE(LU,'("'//TAB//'<Sortie>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Id>HInvEAPermOcc</Id>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Description>Nombre d''heures d''inversion aux entrees d''air et aux permeabilites en occupation</Description>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Unit>h</Unit>'//'")')
    ! WRITE(LU,'("'//TAB//TAB//'<Locals>'//'")')
    ! DO ILOC=1,N_LOC
    !WRITE(LU,'("'//TAB//TAB//TAB//'<Local>'//'")')
    !WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Id>'//TRIM(LOCRT(ILOC)%ID)//'</Id>'//'")')
    !WRITE(LU,'("'//TAB//TAB//TAB//TAB//'<Value>"(F8.1)"</Value>'//'")') H_INVEAPERM_OCC(ILOC)
    !   WRITE(LU,'("'//TAB//TAB//TAB//'</Local>'//'")')
    ! ENDDO
    ! WRITE(LU,'("'//TAB//TAB//'</Locals>'//'")')
    ! WRITE(LU,'("'//TAB//'</Sortie>'//'")')

    WRITE(LU,'("</Sorties>")')

    CLOSE(LU)


    END SUBROUTINE WRITE_ATEC_XML
	
	SUBROUTINE CLEAN_ATEC_FILE(LU)
	
	INTEGER,INTENT(IN)							:: LU
    CHARACTER(256)								:: FNAME
    INTEGER										:: ERR
	
	!Effacement des fichiers ATEC
	FNAME=TRIM(JOBNAME)//'_histo_CO2base.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_histo_CO2texpo.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_histo_HRbase.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_histo_HRtexpo.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_ATEC.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_ATEC.xml'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    
	END SUBROUTINE CLEAN_ATEC_FILE
	
END MODULE POST_ATEC_MODULE
