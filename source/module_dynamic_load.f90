    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE DYNAMICLOAD_MODULE

    !DEC$ IF .NOT.DEFINED(linux)
    USE KERNEL32
    !DEC$ ENDIF
    USE, INTRINSIC :: ISO_C_BINDING

    IMPLICIT NONE

    !DEC$ IF DEFINED(linux)
    ! interface to linux API
    INTERFACE ! All we need is interfaces for the prototypes in <dlfcn.h>
        FUNCTION DLOpen(file,mode) RESULT(handle) BIND(C,NAME="dlopen")
        ! void *dlopen(const char *file, int mode);
        USE ISO_C_BINDING
        CHARACTER(C_CHAR), DIMENSION(*), INTENT(IN) :: file
        ! C strings should be declared as character arrays
        INTEGER(C_INT), VALUE :: mode
        INTEGER(C_INTPTR_T) :: handle
        END FUNCTION
        FUNCTION DLSym(handle,name) RESULT(funptr) BIND(C,NAME="dlsym")
        ! void *dlsym(void *handle, const char *name);
        USE ISO_C_BINDING
        INTEGER(C_INTPTR_T), VALUE :: handle
        CHARACTER(C_CHAR), DIMENSION(*), INTENT(IN) :: name
        INTEGER(C_INTPTR_T) :: funptr ! A function pointer
        END FUNCTION
        FUNCTION DLClose(handle) RESULT(status) BIND(C,NAME="dlclose")
        ! int dlclose(void *handle);
        USE ISO_C_BINDING
        INTEGER(C_INTPTR_T), VALUE :: handle
        INTEGER(C_INT) :: status
        END FUNCTION
        FUNCTION DLError() RESULT(error) BIND(C,NAME="dlerror")
        ! char *dlerror(void);
        USE ISO_C_BINDING
        TYPE(C_PTR) :: error
        END FUNCTION
    END INTERFACE
    !DEC$ ENDIF

    !DEC$ IF .NOT.DEFINED(linux)
    INTEGER(HANDLE)     :: PLIB
    INTEGER(BOOL)       :: FREE_STATUS
    !DEC$ ELSE
    INTEGER(C_INTPTR_T)  :: PLIB
    INTEGER(C_INT) :: FREE_STATUS
    INTEGER, PARAMETER :: RTLD_LAZY=1
    !DEC$ ENDIF

    INTEGER(C_INTPTR_T) :: QCOUNT
    INTEGER(C_INTPTR_T) :: QREAD
    INTEGER(C_INTPTR_T) :: QADD
    INTEGER(C_INTPTR_T) :: QCONS
    INTEGER(C_INTPTR_T) :: QVAR
    INTEGER(C_INTPTR_T) :: QIMAGE
    INTEGER(C_INTPTR_T) :: QFLUX
    INTEGER(C_INTPTR_T) :: QUPDATE
    INTEGER(C_INTPTR_T) :: QREWIND
    CHARACTER(C_CHAR), DIMENSION(1), SAVE, TARGET, PRIVATE :: dummy_string="?"

    CONTAINS

    FUNCTION C_F_STRING(CPTR) RESULT(FPTR)
    ! Convert a null-terminated C string into a Fortran character array pointer
    TYPE(C_PTR), INTENT(IN) :: CPTR ! The C address
    CHARACTER(KIND=C_CHAR), DIMENSION(:), POINTER :: FPTR

    INTERFACE ! strlen is a standard C function from <string.h>
    ! int strlen(char *string)
    FUNCTION strlen(string) RESULT(len) BIND(C,NAME="strlen")
    USE ISO_C_BINDING
    TYPE(C_PTR), VALUE :: string ! A C pointer
    END FUNCTION
    END INTERFACE

    IF(C_ASSOCIATED(CPTR)) THEN
        CALL C_F_POINTER(FPTR=FPTR, CPTR=CPTR, SHAPE=[strlen(CPTR)])
    ELSE
        ! To avoid segfaults, associate FPTR with a dummy target:
        FPTR=>dummy_string
    END IF

    END FUNCTION

    END MODULE DYNAMICLOAD_MODULE
