    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE PROC_SAVE_RES_MODULE

    USE ENVIRO_MODULE
    !SAM
    !USE GLOBAL_VAR_MODULE,ONLY: G_indice_buffer_write,G_Buffer_loc_DP

    IMPLICIT NONE

    !SAM : ici j'ajoute un tampon de chaines comme buffer d'ecriture fichier
    integer                                             :: G_size_buffer_write
    integer                                             :: G_indice_buffer_write

    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_loc_DP,G_Buffer_loc_T,G_Buffer_loc_RHO,G_Buffer_loc_SOU
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_loc_HR,G_Buffer_loc_TROSEE,G_Buffer_loc_MH2OLIQ,G_Buffer_loc_TW,G_Buffer_loc_TOPER ! SAM si IDNH20 (invariable durant une simulation ?)
    DOUBLE PRECISION,DIMENSION(:,:,:),ALLOCATABLE,TARGET:: G_Buffer_loc_Y
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_loc_RA

    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_hsrc_QM
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_hsrc_QDOT
    DOUBLE PRECISION,DIMENSION(:,:,:),ALLOCATABLE,TARGET:: G_Buffer_hsrc_QMSPEC

    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_branch_QV
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_branch_QM
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_branch_DPFLOW
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_branch_RHOFLOW
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_ouv_QV1,G_Buffer_ouv_QV2

    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_EXT

    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_TP1, G_Buffer_wall_TP2
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_QC1, G_Buffer_wall_QC2
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_FRAD1, G_Buffer_wall_FRAD2
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_FSUN1, G_Buffer_wall_FSUN2
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_QM1, G_Buffer_wall_QM2, G_Buffer_wall_MWTOT
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_T, G_Buffer_wall_HR, G_Buffer_wall_YW
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_SLAB
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_wall_PSAT, G_Buffer_wall_SORP, G_Buffer_wall_PERM, G_Buffer_wall_DW

    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET  :: G_Buffer_ctrl_probe

    CONTAINS

   SUBROUTINE WRITE_TITLES(LU)

    INTEGER,INTENT(IN)							:: LU
    CHARACTER(256)								:: FNAME,FMT,TITRE,KSTRING,NSCSTRING
    INTEGER										:: K,J,ERR,ILOC,IBOUND,IMUR,ISLAB,IM
    DOUBLE PRECISION                             :: X,XTOT
    CHARACTER(100)								:: XSTRING

    integer                                     :: indice_mur
    integer                                     :: nombre_total_de_noeuds_buffer
    integer                                     :: nombre_total_de_noeuds_hygro_buffer, nombre_total_de_slabs_hygro_buffer


    !Ecriture des en-tetes pour les locaux********************************
    
    IF (ALLOCATED(G_Buffer_loc_DP)) THEN
        DEALLOCATE(G_Buffer_loc_DP)
        DEALLOCATE(G_Buffer_loc_T)
        DEALLOCATE(G_Buffer_loc_RHO)
        DEALLOCATE(G_Buffer_loc_SOU)
        DEALLOCATE(G_Buffer_loc_HR)
        DEALLOCATE(G_Buffer_loc_TROSEE)
        DEALLOCATE(G_Buffer_loc_MH2OLIQ)
        DEALLOCATE(G_Buffer_loc_TW)
        DEALLOCATE(G_Buffer_loc_TOPER)
        IF (ALLOCATED(G_Buffer_loc_Y)) DEALLOCATE(G_Buffer_loc_Y)
        DEALLOCATE(G_Buffer_loc_RA)
        DEALLOCATE(G_Buffer_hsrc_QM)
        DEALLOCATE(G_Buffer_hsrc_QDOT)
        IF (ALLOCATED(G_Buffer_hsrc_QMSPEC)) DEALLOCATE(G_Buffer_hsrc_QMSPEC)
        DEALLOCATE(G_Buffer_branch_QV)
        DEALLOCATE(G_Buffer_branch_QM)
        DEALLOCATE(G_Buffer_branch_DPFLOW)
        DEALLOCATE(G_Buffer_branch_RHOFLOW)
        IF (ALLOCATED(G_Buffer_ouv_QV1)) DEALLOCATE(G_Buffer_ouv_QV1)
        IF (ALLOCATED(G_Buffer_ouv_QV2)) DEALLOCATE(G_Buffer_ouv_QV2)
        DEALLOCATE(G_Buffer_EXT)
        DEALLOCATE(G_Buffer_wall_TP1)
        DEALLOCATE(G_Buffer_wall_TP2)
        DEALLOCATE(G_Buffer_wall_QC1)
        DEALLOCATE(G_Buffer_wall_QC2)
        DEALLOCATE(G_Buffer_wall_FRAD1)
        DEALLOCATE(G_Buffer_wall_FRAD2)
        DEALLOCATE(G_Buffer_wall_FSUN1)
        DEALLOCATE(G_Buffer_wall_FSUN2)
        IF (ALLOCATED(G_Buffer_wall_QM1)) THEN
            DEALLOCATE(G_Buffer_wall_QM1)
            DEALLOCATE(G_Buffer_wall_QM2)
            DEALLOCATE(G_Buffer_wall_MWTOT)
        ENDIF
        IF (ALLOCATED(G_Buffer_wall_T)) DEALLOCATE(G_Buffer_wall_T)
        IF (ALLOCATED(G_Buffer_wall_HR)) THEN
            DEALLOCATE(G_Buffer_wall_HR)
            DEALLOCATE(G_Buffer_wall_YW)
            DEALLOCATE(G_Buffer_wall_SLAB)
            DEALLOCATE(G_Buffer_wall_PSAT)
            DEALLOCATE(G_Buffer_wall_SORP)
            DEALLOCATE(G_Buffer_wall_PERM)
            DEALLOCATE(G_Buffer_wall_DW)
            
        ENDIF
        IF (ALLOCATED(G_Buffer_ctrl_probe)) DEALLOCATE(G_Buffer_ctrl_probe)
    ENDIF
    
    G_size_buffer_write = BUFFERSIZE
    G_indice_buffer_write = 0

    ! partie LOC
    ALLOCATE(G_Buffer_loc_DP(G_size_buffer_write,1+N_LOC))     !time + N_LOC valeurs
    ALLOCATE(G_Buffer_loc_T(G_size_buffer_write,1+N_LOC))      !time + N_LOC valeurs
    ALLOCATE(G_Buffer_loc_RHO(G_size_buffer_write,1+N_LOC))    !time + N_LOC valeurs
    ALLOCATE(G_Buffer_loc_SOU(G_size_buffer_write,1+N_LOC))    !time + N_LOC valeurs

    ALLOCATE(G_Buffer_loc_HR(G_size_buffer_write,1+N_LOC))     !time + N_LOC valeurs
    ALLOCATE(G_Buffer_loc_TROSEE(G_size_buffer_write,1+N_LOC)) !time + N_LOC valeurs
    ALLOCATE(G_Buffer_loc_MH2OLIQ(G_size_buffer_write,1+N_LOC))  !time + N_LOC valeurs
    ALLOCATE(G_Buffer_loc_TW(G_size_buffer_write,1+N_LOC))     !time + N_LOC valeurs
    ALLOCATE(G_Buffer_loc_TOPER(G_size_buffer_write,1+N_LOC))  !time + N_LOC valeurs
    IF (N_SPEC > 0) then
        ALLOCATE(G_Buffer_loc_Y(G_size_buffer_write,N_CTRL,1+N_LOC))   ! (time + N_LOC valeurs) pour chaque SPEC
    endif
    ALLOCATE(G_Buffer_loc_RA(G_size_buffer_write,1+N_LOC))     !time + N_LOC valeurs

    ! partie HSRC
    ALLOCATE(G_Buffer_hsrc_QM(G_size_buffer_write,1+N_HSRC))     !time + N_HSRC valeurs
    ALLOCATE(G_Buffer_hsrc_QDOT(G_size_buffer_write,1+N_HSRC))   !time + N_HSRC valeurs
    if(N_SPEC > 0) then
        ALLOCATE(G_Buffer_hsrc_QMSPEC(G_size_buffer_write,N_SPEC,1+N_HSRC))   ! (time + N_HSRC valeurs) pour chaque CTRL
    endif

    ! partie BRANCH + OUV
    ALLOCATE(G_Buffer_branch_QV(G_size_buffer_write,1+N_BRANCHE))     !time + N_BRANCHE valeurs
    ALLOCATE(G_Buffer_branch_QM(G_size_buffer_write,1+N_BRANCHE))     !time + N_BRANCHE valeurs
    ALLOCATE(G_Buffer_branch_DPFLOW(G_size_buffer_write,1+N_BRANCHE))     !time + N_BRANCHE valeurs
    ALLOCATE(G_Buffer_branch_RHOFLOW(G_size_buffer_write,1+N_BRANCHE))    !time + N_BRANCHE valeurs
    if(N_OUV>0) then
        ALLOCATE(G_Buffer_ouv_QV1(G_size_buffer_write,1+N_OUV))     !time + N_OUV valeurs
        ALLOCATE(G_Buffer_ouv_QV2(G_size_buffer_write,1+N_OUV))     !time + N_OUV valeurs
    endif

    ! partie EXT
    ALLOCATE(G_Buffer_EXT(G_size_buffer_write,16))     !time + 15 valeurs ?

    ! partie WALL
    ALLOCATE(G_Buffer_wall_TP1(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    ALLOCATE(G_Buffer_wall_TP2(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    ALLOCATE(G_Buffer_wall_QC1(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    ALLOCATE(G_Buffer_wall_QC2(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    ALLOCATE(G_Buffer_wall_FRAD1(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    ALLOCATE(G_Buffer_wall_FRAD2(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    ALLOCATE(G_Buffer_wall_FSUN1(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    ALLOCATE(G_Buffer_wall_FSUN2(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    if(N_WHYGRO>0) then
        ALLOCATE(G_Buffer_wall_QM1(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
        ALLOCATE(G_Buffer_wall_QM2(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
        ALLOCATE(G_Buffer_wall_MWTOT(G_size_buffer_write,1+N_MUR))     !time + N_MUR valeurs
    endif
        
    ! pour les murs, seuls certains sont sauvegardes
    ! chaque mur a un nombre specifique de noeuds, on fait simple : on totabilise le nombre total de noeuds (pas de tableau de pointeurs)
    nombre_total_de_noeuds_buffer=0
    nombre_total_de_noeuds_hygro_buffer=0
    nombre_total_de_slabs_hygro_buffer=0
    DO indice_mur=1,SIZE(MURRT)
        IF (MURRT(indice_mur)%POST_INTERNAL_VAR==.TRUE.) THEN
            nombre_total_de_noeuds_buffer=nombre_total_de_noeuds_buffer+MURRT(indice_mur)%NUMNODE(1)

            !IF (MURRT(indice_mur)%WALLTYPE=='HYGROTHERMAL') THEN
            IF (MURRT(indice_mur)%enum_walltype==wall_hygrothermal) THEN
                nombre_total_de_noeuds_hygro_buffer=nombre_total_de_noeuds_hygro_buffer+MURRT(indice_mur)%NUMNODE(1)
                nombre_total_de_slabs_hygro_buffer =nombre_total_de_slabs_hygro_buffer +MURRT(indice_mur)%NSLAB
            endif
        endif
    enddo

    !print*, "nombre de noeuds=", nombre_total_de_noeuds_buffer , " hygro=" , nombre_total_de_noeuds_hygro_buffer , " slabs=" , nombre_total_de_slabs_hygro_buffer

    ALLOCATE(G_Buffer_wall_T(G_size_buffer_write   ,1+nombre_total_de_noeuds_buffer))      ! attention : time + nb_noeuds valeurs (pas hygro)
    ALLOCATE(G_Buffer_wall_HR(G_size_buffer_write   ,1+nombre_total_de_noeuds_hygro_buffer))   !time + nb_noeuds_hygro valeurs
    ALLOCATE(G_Buffer_wall_YW(G_size_buffer_write   ,1+nombre_total_de_noeuds_hygro_buffer))   !time + nb_noeuds_hygro valeurs
    ALLOCATE(G_Buffer_wall_SLAB(G_size_buffer_write ,1+nombre_total_de_slabs_hygro_buffer))             ! attention : time + nb_slabs_hygro valeurs
    ALLOCATE(G_Buffer_wall_PSAT(G_size_buffer_write ,1+nombre_total_de_noeuds_hygro_buffer))    !time + nb_noeuds_hygro valeurs
    ALLOCATE(G_Buffer_wall_SORP(G_size_buffer_write ,1+nombre_total_de_noeuds_hygro_buffer))    !time + nb_noeuds_hygro valeurs
    ALLOCATE(G_Buffer_wall_PERM(G_size_buffer_write ,1+nombre_total_de_noeuds_hygro_buffer))    !time + nb_noeuds_hygro valeurs
    ALLOCATE(G_Buffer_wall_DW(G_size_buffer_write   ,1+nombre_total_de_noeuds_hygro_buffer))    !time + nb_noeuds_hygro valeurs

    ! partie CTRL ou PROBE
    IF ((OUTPUT=='ALL').OR.(OUTPUT=='CTRL')) THEN
        ALLOCATE(G_Buffer_ctrl_probe(G_size_buffer_write   ,1+N_CTRL))
    else
        ALLOCATE(G_Buffer_ctrl_probe(G_size_buffer_write   ,1+N_PROBE))
    endif

    FNAME=TRIM(JOBNAME)//'_loc.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(LU,'(A)') 'time'
        DO K=1,N_LOC
            WRITE(LU,'(A)') TRIM(LOCRT(K)%ID)
        ENDDO
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_DP.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' DP Pa'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_T.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' T C'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_RHO.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' RHO kg/m3'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_SOU.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        !IF (GUESS_POWER.EQV..TRUE.) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' SOU W'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
        !ENDIF
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_HR.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        IF (IDNH2O.NE.0) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            TITRE='time '//TRIM(TIMEUNIT)//' HR %'
            WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
            WRITE(LU,FMT) TRIM(TITRE)
            CLOSE(LU)
        ENDIF
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_TROSEE.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        IF (IDNH2O.NE.0) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            TITRE='time '//TRIM(TIMEUNIT)//' TROSEE C'
            WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
            WRITE(LU,FMT) TRIM(TITRE)
            CLOSE(LU)
        ENDIF
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_MH2OLIQ.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        !IF ((IDNH2O.NE.0).AND.(MOISTURE_NODES==.TRUE.)) THEN
        IF ((IDNH2O.NE.0)) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            TITRE='time '//TRIM(TIMEUNIT)//' MH2OLIQ kg'
            WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
            WRITE(LU,FMT) TRIM(TITRE)
            CLOSE(LU)
        ENDIF
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_TW.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF ((N_MUR>0).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0))) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' TW C'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_TOPER.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF ((N_MUR>0).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0))) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' TOPER C'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    IF (N_SPEC>0) THEN
        DO J=1,N_SPEC
            FNAME=TRIM(JOBNAME)//'_loc_Y'//TRIM(SPECRT(J)%ID)//'.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
            IF (ERR==0) CLOSE(LU,STATUS='DELETE')
            IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)
                IF (SPECRT(J)%ID=='CO2') THEN
                    TITRE=TRIM(TITRE)//' Y'//TRIM(SPECRT(J)%ID)//' ppmv'
                ELSEIF ((SPECRT(J)%ID=='H2O').OR.(SPECRT(J)%TRACE.EQV..FALSE.)) THEN
                    TITRE=TRIM(TITRE)//' Y'//TRIM(SPECRT(J)%ID)//' g/kg'
                ELSEIF (SPECRT(J)%TRACE.EQV..TRUE.) THEN
                    TITRE=TRIM(TITRE)//' Y'//TRIM(SPECRT(J)%ID)//' [User Unit]/m3'
                ENDIF
                WRITE(FMT,'("(A",i6,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDDO
    ENDIF

    FNAME=TRIM(JOBNAME)//'_loc_RA.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='LOC')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' RA vol/h'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF
	
    !Ecriture des en-tetes pour les sources********************************

    FNAME=TRIM(JOBNAME)//'_hsrc.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='HSRC')).AND.(NSAVE>0).AND.(N_HSRC>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(LU,'(A)') 'time'
        DO K=1,N_HSRC
            WRITE(LU,'(A)') TRIM(HSRCRT(K)%ID)
        ENDDO
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_hsrc_QM.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='HSRC')).AND.(NSAVE>0).AND.(N_HSRC>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QM kg/s'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_hsrc_QDot.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='HSRC')).AND.(NSAVE>0).AND.(N_HSRC>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QDOT W'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    DO J=1,N_SPEC
        FNAME=TRIM(JOBNAME)//'_hsrc_QM'//TRIM(SPECRT(J)%ID)//'.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        IF (OUTPUT_HSRC_SPECPROD.EQV..TRUE.) THEN
            IF (((OUTPUT=='ALL').OR.(OUTPUT=='HSRC')).AND.(NSAVE>0).AND.(N_HSRC>0)) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' QM'//TRIM(SPECRT(J)%ID)//' kg/s'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDIF
    ENDDO
    
    !Ecriture des en-tetes pour les branches******************

    FNAME=TRIM(JOBNAME)//'_branch.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(LU,'(A)') 'time'
        DO K=1,N_BRANCHE
            WRITE(LU,'(A)') TRIM(BRANCHERT(K)%ID)
        ENDDO
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_branch_QV.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QV m3/h'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_branch_QM.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QM kg/s'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_branch_DPFLOW.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' DPFLOW Pa'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_branch_RHOFLOW.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' RHOFLOW kg/m3'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    !Ecriture des en-tetes pour les ouvertures verticales******************

    FNAME=TRIM(JOBNAME)//'_ouv.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_OUV>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(LU,'(A)') 'time'
        DO K=1,N_OUV
            WRITE(LU,'(A)') TRIM(BRANCHERT(IDNOUV(K))%ID)
        ENDDO
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_ouv_QV1.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_OUV>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QV m3/h'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_ouv_QV2.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(NSAVE>0).AND.(N_OUV>0).AND.(N_BRANCHE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QV m3/h'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    !Ecriture de l'en-tete pour ext******************
	
    FNAME=TRIM(JOBNAME)//'_ext.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='EXT')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(LU,'(A)') 'time'
        WRITE(LU,'(A)') 'YEARDAY_(D) '
        WRITE(LU,'(A)') 'WEEKDAY_(D) '
        WRITE(LU,'(A)') 'HOUR_(h) '
        WRITE(LU,'(A)') 'VREF_(m/s) '
        WRITE(LU,'(A)') 'WINC_() '
        WRITE(LU,'(A)') 'TEXT_(C) '
        WRITE(LU,'(A)') 'HR_(%) '
        WRITE(LU,'(A)') 'SUNRAD_(W/m2) '
        WRITE(LU,'(A)') 'DIFFRAD_(W/m2) '
        WRITE(LU,'(A)') 'TSKY_(C) '
        WRITE(LU,'(A)') 'RHOEXT_(kg/m3) '
        WRITE(LU,'(A)') 'TROSEE_(C) '
        WRITE(LU,'(A)') 'RA_(vol/h) '
        WRITE(LU,'(A)') 'AZ_ANGLE()'
        WRITE(LU,'(A)') 'H_ANGLE()'

        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_ext.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='EXT')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' value See ext.head file'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF
	
    !Ecriture des en-tetes pour les murs******************

    FNAME=TRIM(JOBNAME)//'_mur.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_wall.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(LU,'(A)') 'time'
        DO K=1,N_MUR
            WRITE(LU,'(A)') TRIM(MURRT(K)%ID)
        ENDDO
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_mur_TP1.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_wall_TP1.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' TP1 C'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_mur_TP2.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    FNAME=TRIM(JOBNAME)//'_wall_TP2.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' TP2 C'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_wall_QC1.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QC1 W'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF
    FNAME=TRIM(JOBNAME)//'_wall_QC2.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' QC2 W'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_wall_FRAD1.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' FRAD1 W/m2'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF
    FNAME=TRIM(JOBNAME)//'_wall_FRAD2.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' FRAD2 W/m2'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_wall_FSUN1.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' FSUN1 W/m2'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF
    FNAME=TRIM(JOBNAME)//'_wall_FSUN2.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' FSUN2 W/m2'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_wall_QM1.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (N_WHYGRO.NE.0) THEN
        IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            TITRE='time '//TRIM(TIMEUNIT)//' QM1 kg/s'
            WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
            WRITE(LU,FMT) TRIM(TITRE)
            CLOSE(LU)
        ENDIF
    ENDIF
    FNAME=TRIM(JOBNAME)//'_wall_QM2.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (N_WHYGRO.NE.0) THEN
        IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            TITRE='time '//TRIM(TIMEUNIT)//' QM2 kg/s'
            WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
            WRITE(LU,FMT) TRIM(TITRE)
            CLOSE(LU)
        ENDIF
    ENDIF

    FNAME=TRIM(JOBNAME)//'_wall_MWTOT.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (N_WHYGRO.NE.0) THEN
        IF (((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0).AND.(N_MUR>0)) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            TITRE='time '//TRIM(TIMEUNIT)//' MWTOT kg/m2'
            WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
            WRITE(LU,FMT) TRIM(TITRE)
            CLOSE(LU)
        ENDIF
    ENDIF


    DO IMUR=1,SIZE(MURRT)
        !creation des fichiers des temperatures et des humidites internes
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'.mesh'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            WRITE(LU,'(A)') 'x (m)'
            X=0.D0
            DO ISLAB=1,MURRT(IMUR)%NSLAB
                !IF ((ISLAB==1).OR.(MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL')) THEN
                IF ((ISLAB==1).OR.(MURRT(IMUR)%enum_walltype==wall_hygrothermal)) THEN
                    WRITE(XSTRING,'(F8.6)')  X
                    WRITE(LU,'(A)') TRIM(XSTRING)
                ENDIF
                DO K=1,SIZE(SURFRT(MURRT(IMUR)%ISURF)%MESH(ISLAB)%DXM)
                    X=X+SURFRT(MURRT(IMUR)%ISURF)%MESH(ISLAB)%DXM(K)
                    WRITE(XSTRING,'(F8.6)')  X
                    WRITE(LU,'(A)') TRIM(XSTRING)
                ENDDO
            ENDDO
            CLOSE(LU)
        ENDIF


        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'.head'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            WRITE(LU,'(A)') 'time'
            IM=0
            DO ISLAB=1,MURRT(IMUR)%NSLAB
                !IF ((ISLAB==1).OR.(MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL')) THEN
                IF ((ISLAB==1).OR.(MURRT(IMUR)%enum_walltype==wall_hygrothermal)) THEN
                    IM=IM+1
                    WRITE(XSTRING,"('',i,'')")  IM
                    WRITE(LU,'(A)') 'x'//TRIM(ADJUSTL(XSTRING))
                ENDIF
                DO K=1,SIZE(SURFRT(MURRT(IMUR)%ISURF)%MESH(ISLAB)%DXM)
                    IM=IM+1
                    WRITE(XSTRING,"('',i,'')")  IM
                    WRITE(LU,'(A)') 'x'//TRIM(ADJUSTL(XSTRING))
                ENDDO
            ENDDO
            CLOSE(LU)
        ENDIF
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'_T.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            TITRE='time '//TRIM(TIMEUNIT)//' T C'
            WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
            WRITE(LU,FMT) TRIM(TITRE)
            CLOSE(LU)
        ENDIF
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'_HR.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' HR %'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDIF
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'_YW.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' YW kg/kg'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF

        ENDIF

        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'slab.head'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                WRITE(LU,'(A)') 'time'
                DO ISLAB=1,MURRT(IMUR)%NSLAB
                    WRITE(XSTRING,"('',i,'')")  ISLAB
                    WRITE(LU,'(A)') TRIM(ADJUSTL(MURRT(IMUR)%ID))//'_slab'//TRIM(ADJUSTL(XSTRING))
                ENDDO
            ENDIF
        ENDIF
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'slab_YWMEAN.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' YWMEAN kg/kg'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDIF
        
        !ajout xf 15/11/2023
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'_PSAT.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' PSAT Pa'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDIF
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'_SORP.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' PSAT Pa'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDIF
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'_DW.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' PSAT Pa'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDIF
        FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(IMUR)%ID)//'_PERM.res'
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
        IF (ERR==0) CLOSE(LU,STATUS='DELETE')
        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
            IF ((MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.).AND.(((OUTPUT=='ALL').OR.(OUTPUT=='WALL')).AND.(NSAVE>0))) THEN
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                TITRE='time '//TRIM(TIMEUNIT)//' PSAT Pa'
                WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
                WRITE(LU,FMT) TRIM(TITRE)
                CLOSE(LU)
            ENDIF
        ENDIF
    ENDDO


    !Ecriture des en-tetes pour les controleurs*********

    FNAME=TRIM(JOBNAME)//'_ctrl.head'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='CTRL').OR.(OUTPUT=='PROBE')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(LU,'(A)') 'time'
        IF ((OUTPUT=='ALL').OR.(OUTPUT=='CTRL')) THEN
            DO K=1,N_CTRL
                WRITE(LU,'(A)') TRIM(CTRLRT(K)%ID)
            ENDDO
        ELSE
            DO K=1,N_PROBE
                WRITE(LU,'(A)') TRIM(CTRLRT(IDNPROBE(K))%ID)
            ENDDO
        ENDIF
        CLOSE(LU)
    ENDIF

    FNAME=TRIM(JOBNAME)//'_ctrl_value.res'
    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU,STATUS='DELETE')
    IF (((OUTPUT=='ALL').OR.(OUTPUT=='CTRL').OR.(OUTPUT=='PROBE')).AND.(NSAVE>0)) THEN
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        TITRE='time '//TRIM(TIMEUNIT)//' value CtrlTypeDependent'
        WRITE(FMT,'("(A",i3,")")') LEN_TRIM(TITRE)
        WRITE(LU,FMT) TRIM(TITRE)
        CLOSE(LU)
    ENDIF

    END	SUBROUTINE WRITE_TITLES


    !SAM vidage des buffers dans les fichiers associes
    SUBROUTINE WRITE_BUFFERS(LU)

        INTEGER,INTENT(IN)							:: LU
        integer                                     :: mode_buffer, ind_buffer, ind_spec
        CHARACTER(256)								:: chaine_tempo
        CHARACTER(256)								:: FNAME,FMT

        !ajouter ici les indices debut, fin pour noeuds et slabs
        integer                                     :: indice_mur
        integer                                     :: indice_debut_noeud_mur,indice_fin_noeud_mur,nombre_courant_de_noeuds
        integer                                     :: indice_debut_noeud_hygro_mur,indice_fin_noeud_hygro_mur
        integer                                     :: indice_debut_slab_hygro_mur,indice_fin_slab_hygro_mur,nombre_courant_de_slabs


        IF (NSAVE>0) THEN
        ! ecriture des buffers LOC
        IF ((OUTPUT=='ALL').OR.(OUTPUT=='LOC')) THEN

            FNAME=TRIM(JOBNAME)//'_loc_DP.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_loc_DP(ind_buffer,:)
            enddo
            CLOSE(LU)

            FNAME=TRIM(JOBNAME)//'_loc_T.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_loc_T(ind_buffer,:)
            enddo
            CLOSE(LU)

            FNAME=TRIM(JOBNAME)//'_loc_RHO.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_loc_RHO(ind_buffer,:)
            enddo
            CLOSE(LU)

            !  IF (GUESS_POWER.EQV..TRUE.) THEN
            FNAME=TRIM(JOBNAME)//'_loc_SOU.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_loc_SOU(ind_buffer,:)
            enddo
            CLOSE(LU)
            !  ENDIF

            IF (IDNH2O.NE.0) THEN
                FNAME=TRIM(JOBNAME)//'_loc_HR.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_loc_HR(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_loc_TROSEE.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_loc_TROSEE(ind_buffer,:)
                enddo
                CLOSE(LU)
                !IF (MOISTURE_NODES==.TRUE.) THEN
                    FNAME=TRIM(JOBNAME)//'_loc_MH2OLIQ.res'
                    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                    WRITE(FMT,'("(",i4,"(ES15.7E3,:,""  ""))")') (1+N_LOC)
                    DO ind_buffer=1,G_indice_buffer_write
                        WRITE(LU,TRIM(FMT)) G_Buffer_loc_MH2OLIQ(ind_buffer,:)
                    enddo
                    CLOSE(LU)
                !ENDIF
            ENDIF

            IF (N_MUR>0) THEN
                FNAME=TRIM(JOBNAME)//'_loc_TW.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_loc_TW(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_loc_TOPER.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_loc_TOPER(ind_buffer,:)
                enddo
                CLOSE(LU)
            ENDIF

            IF (N_SPEC>0) THEN
                DO ind_spec=1,N_SPEC
                    FNAME=TRIM(JOBNAME)//'_loc_Y'//TRIM(SPECRT(ind_spec)%ID)//'.res'
                    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                    !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
                    DO ind_buffer=1,G_indice_buffer_write
                        WRITE(LU,TRIM(FMT)) G_Buffer_loc_Y(ind_buffer,ind_spec,:)
                    enddo
                    CLOSE(LU)
                ENDDO
            ENDIF


            FNAME=TRIM(JOBNAME)//'_loc_RA.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_LOC)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_loc_RA(ind_buffer,:)
            enddo
            CLOSE(LU)
        ENDIF !fin partie _loc

        ! ecriture des buffers HSRC
        IF (((OUTPUT=='ALL').OR.(OUTPUT=='HSRC')).AND.(N_HSRC>0)) THEN

            !print*, "N_HSRC=" , N_HSRC

            FNAME=TRIM(JOBNAME)//'_hsrc_QM.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            WRITE(FMT,'("(",i4,"(ES15.7E3,:,""  ""))")') (1+N_HSRC)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_hsrc_QM(ind_buffer,:)
            enddo
            CLOSE(LU)
    
            FNAME=TRIM(JOBNAME)//'_hsrc_QDot.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_HSRC)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_hsrc_QDOT(ind_buffer,:)
            enddo
            CLOSE(LU)
            
            
            IF (OUTPUT_HSRC_SPECPROD.EQV..TRUE.) THEN
                DO ind_spec=1,N_SPEC
                    FNAME=TRIM(JOBNAME)//'_hsrc_QM'//TRIM(SPECRT(ind_spec)%ID)//'.res'
                    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                    !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_HSRC)
                    DO ind_buffer=1,G_indice_buffer_write
                        WRITE(LU,TRIM(FMT)) G_Buffer_hsrc_QMSPEC(ind_buffer,ind_spec,:)
                    enddo
                    CLOSE(LU)
                ENDDO
            ENDIF
        ENDIF
    
        ! ecriture des buffers BRANCH
        IF (((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')).AND.(N_BRANCHE>0)) THEN

            FNAME=TRIM(JOBNAME)//'_branch_QV.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_BRANCHE)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_branch_QV(ind_buffer,:)
            enddo
            CLOSE(LU)
    
            FNAME=TRIM(JOBNAME)//'_branch_QM.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_BRANCHE)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_branch_QM(ind_buffer,:)
            enddo
            CLOSE(LU)
    
            FNAME=TRIM(JOBNAME)//'_branch_DPFLOW.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_BRANCHE)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_branch_DPFLOW(ind_buffer,:)
            enddo
            CLOSE(LU)
    
            FNAME=TRIM(JOBNAME)//'_branch_RHOFLOW.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_BRANCHE)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_branch_RHOFLOW(ind_buffer,:)
            enddo
            CLOSE(LU)
    
            IF (N_OUV>0) THEN
                FNAME=TRIM(JOBNAME)//'_ouv_QV1.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_OUV)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_ouv_QV1(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_ouv_QV2.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_OUV)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_ouv_QV2(ind_buffer,:)
                enddo
                CLOSE(LU)
            ENDIF
    
        ENDIF

        ! ecriture du buffeur EXT
        IF ((OUTPUT=='ALL').OR.(OUTPUT=='EXT')) THEN
            FNAME=TRIM(JOBNAME)//'_ext.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            WRITE(FMT,'("(",i4,"(ES15.7E3,:,""  ""))")') (16)
            DO ind_buffer=1,G_indice_buffer_write
                WRITE(LU,TRIM(FMT)) G_Buffer_ext(ind_buffer,:)
            enddo
            CLOSE(LU)
        ENDIF

        !ecriture des buffers WALL
        IF ((OUTPUT=='ALL').OR.(OUTPUT=='WALL')) THEN

            IF (N_MUR>0) THEN

                FNAME=TRIM(JOBNAME)//'_wall_TP1.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_TP1(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_wall_TP2.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_TP2(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_wall_QC1.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_QC1(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_wall_QC2.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_QC2(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_wall_FRAD1.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_FRAD1(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_wall_FRAD2.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_FRAD2(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_wall_FSUN1.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_FSUN1(ind_buffer,:)
                enddo
                CLOSE(LU)
                FNAME=TRIM(JOBNAME)//'_wall_FSUN2.res'
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_wall_FSUN2(ind_buffer,:)
                enddo
                CLOSE(LU)
    
                IF (N_WHYGRO.NE.0) THEN
                    FNAME=TRIM(JOBNAME)//'_wall_QM1.res'
                    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                    !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                    DO ind_buffer=1,G_indice_buffer_write
                        WRITE(LU,TRIM(FMT)) G_Buffer_wall_QM1(ind_buffer,:)
                    enddo
                    CLOSE(LU)
                    FNAME=TRIM(JOBNAME)//'_wall_QM2.res'
                    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                    !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                    DO ind_buffer=1,G_indice_buffer_write
                        WRITE(LU,TRIM(FMT)) G_Buffer_wall_QM2(ind_buffer,:)
                    enddo
                    CLOSE(LU)
                    FNAME=TRIM(JOBNAME)//'_wall_MWTOT.res'
                    OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                    !WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_MUR)
                    DO ind_buffer=1,G_indice_buffer_write
                        WRITE(LU,TRIM(FMT)) G_Buffer_wall_MWTOT(ind_buffer,:)
                    enddo
                    CLOSE(LU)
                ENDIF
    

                ! les noeuds internes dans les murs
                indice_debut_noeud_mur=2
                indice_debut_noeud_hygro_mur=2
                indice_debut_slab_hygro_mur=2

                DO indice_mur=1,SIZE(MURRT)
                    IF (MURRT(indice_mur)%POST_INTERNAL_VAR==.TRUE.) THEN
                        !ecriture des temperatures et des humidites internes

                        nombre_courant_de_noeuds=MURRT(indice_mur)%NUMNODE(1)
                        if(nombre_courant_de_noeuds>0) then ! toujours vrai a ce niveau ?

                            WRITE(FMT,'("(",i4,"(E13.6,:,""  ""))")') (nombre_courant_de_noeuds+1) ! le nombre de noeuds + heure

                            indice_fin_noeud_mur=indice_debut_noeud_mur+nombre_courant_de_noeuds-1

                            FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'_T.res'
                            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                            DO ind_buffer=1,G_indice_buffer_write
                                WRITE(LU,TRIM(FMT))      G_Buffer_wall_T(ind_buffer,1) , G_Buffer_wall_T(ind_buffer,indice_debut_noeud_mur:indice_fin_noeud_mur)
                            ENDDO
                            CLOSE(LU)

                            indice_debut_noeud_mur=indice_fin_noeud_mur+1


                            !IF (MURRT(indice_mur)%WALLTYPE=='HYGROTHERMAL') THEN
                            IF (MURRT(indice_mur)%enum_walltype==wall_hygrothermal) THEN

                                indice_fin_noeud_hygro_mur=indice_debut_noeud_hygro_mur+nombre_courant_de_noeuds-1

                                FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'_HR.res'
                                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                                DO ind_buffer=1,G_indice_buffer_write
                                    WRITE(LU,TRIM(FMT))      G_Buffer_wall_HR(ind_buffer,1) , G_Buffer_wall_HR(ind_buffer,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)
                                ENDDO
                                CLOSE(LU)

                                FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'_YW.res'
                                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                                DO ind_buffer=1,G_indice_buffer_write
                                    WRITE(LU,TRIM(FMT))      G_Buffer_wall_YW(ind_buffer,1) , G_Buffer_wall_YW(ind_buffer,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)
                                ENDDO
                                CLOSE(LU)
                                
                                !ajout xf 15/11/2023
                                FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'_PSAT.res'
                                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                                DO ind_buffer=1,G_indice_buffer_write
                                    WRITE(LU,TRIM(FMT))      G_Buffer_wall_PSAT(ind_buffer,1) , G_Buffer_wall_PSAT(ind_buffer,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)
                                ENDDO
                                CLOSE(LU)
                                FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'_SORP.res'
                                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                                DO ind_buffer=1,G_indice_buffer_write
                                    WRITE(LU,TRIM(FMT))      G_Buffer_wall_SORP(ind_buffer,1) , G_Buffer_wall_SORP(ind_buffer,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)
                                ENDDO
                                CLOSE(LU)
                                FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'_PERM.res'
                                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                                DO ind_buffer=1,G_indice_buffer_write
                                    WRITE(LU,TRIM(FMT))      G_Buffer_wall_PERM(ind_buffer,1) , G_Buffer_wall_PERM(ind_buffer,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)
                                ENDDO
                                CLOSE(LU)
                                FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'_DW.res'
                                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                                DO ind_buffer=1,G_indice_buffer_write
                                    WRITE(LU,TRIM(FMT))      G_Buffer_wall_DW(ind_buffer,1) , G_Buffer_wall_DW(ind_buffer,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)
                                ENDDO
                                CLOSE(LU)

                                indice_debut_noeud_hygro_mur=indice_fin_noeud_hygro_mur+1

                            ENDIF
                        ENDIF !fin si noeuds internes

                        nombre_courant_de_slabs=MURRT(indice_mur)%NSLAB
                        if(nombre_courant_de_slabs>0) then ! toujours vrai a ce niveau ?
                            
                            !IF (MURRT(indice_mur)%WALLTYPE=='HYGROTHERMAL') THEN
                            IF (MURRT(indice_mur)%enum_walltype==wall_hygrothermal) THEN

                                WRITE(FMT,'("(",i4,"(E13.6,:,""  ""))")') (nombre_courant_de_slabs+1) ! le nombre de slab + heure

                                indice_fin_slab_hygro_mur=indice_debut_slab_hygro_mur+nombre_courant_de_slabs-1

                                FNAME=TRIM(JOBNAME)//'_wall'//TRIM(MURRT(indice_mur)%ID)//'slab_YWMEAN.res'
                                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
                                DO ind_buffer=1,G_indice_buffer_write
                                    WRITE(LU,TRIM(FMT))      G_Buffer_wall_SLAB(ind_buffer,1) , G_Buffer_wall_SLAB(ind_buffer,indice_debut_slab_hygro_mur:indice_fin_slab_hygro_mur)
                                ENDDO
                                CLOSE(LU)

                                indice_debut_slab_hygro_mur=indice_fin_slab_hygro_mur+1

                            endif

                        ENDIF !fin si slabs
                    endif !fin si POST_INTERNE_VAR
                ENDDO !fin boucle MURS
            ENDIF !fin si MURS
        ENDIF! fin cas WALL

        !ecriture des buffers CTRL / PROBE
        IF ((OUTPUT=='ALL').OR.(OUTPUT=='CTRL').OR.(OUTPUT=='PROBE')) THEN

            FNAME=TRIM(JOBNAME)//'_ctrl_value.res'
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='OLD',ACCESS='APPEND')
            IF ((OUTPUT=='ALL').OR.(OUTPUT=='CTRL')) THEN
                WRITE(FMT,'("(",i4,"(ES15.7E3,:,""  ""))")') (1+N_CTRL)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_ctrl_probe(ind_buffer,:)
                enddo
            ELSE
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (1+N_PROBE)
                DO ind_buffer=1,G_indice_buffer_write
                    WRITE(LU,TRIM(FMT)) G_Buffer_ctrl_probe(ind_buffer,:)
                enddo
            ENDIF
            CLOSE(LU)
     
        ENDIF

        G_indice_buffer_write=0
        
        ENDIF

    END	SUBROUTINE WRITE_BUFFERS



    SUBROUTINE WRITE_RES(LU)

    INTEGER,INTENT(IN)							:: LU
    INTEGER										:: I,J,K,IBRANCHE,ILOC,ILOC1,ILOC2,IMUR
    CHARACTER(256)								:: FNAME,FMT,TITRE,KSTRING,NSCSTRING
    DOUBLE PRECISION,DIMENSION(N_HSRC)          :: SPECPROD

    !SAM
    integer                                     :: ind_buffer
    CHARACTER(256)								:: chaine_tempo
    integer                                     :: indice_debut_noeud_mur,indice_fin_noeud_mur,nombre_courant_de_noeuds
    integer                                     :: indice_debut_noeud_hygro_mur,indice_fin_noeud_hygro_mur
    integer                                     :: indice_debut_slab_hygro_mur,indice_fin_slab_hygro_mur,nombre_courant_de_slabs

        
    if ((OUTPUT=='ALL').OR.(OUTPUT=='LOC').OR.(OUTPUT=='HSRC').OR.(OUTPUT=='BRANCH').OR.(OUTPUT=='EXT').OR.(OUTPUT=='WALL').or.(OUTPUT=='CTRL').OR.(OUTPUT=='PROBE')) THEN
        G_indice_buffer_write=G_indice_buffer_write+1
    endif


    ! la PARTIE LOC
    IF ((OUTPUT=='ALL').OR.(OUTPUT=='LOC')) Then

        G_Buffer_loc_DP(G_indice_buffer_write,1)          = TIME/TIMESCALE
        G_Buffer_loc_DP(G_indice_buffer_write,2:N_LOC+1)  = DP+MDEXT(1)%RHOBUOY*9.81D0*LOCRT%ALT

        G_Buffer_loc_T(G_indice_buffer_write,1)          = TIME/TIMESCALE
        G_Buffer_loc_T(G_indice_buffer_write,2:N_LOC+1)  = T15-273.15D0

        G_Buffer_loc_RHO(G_indice_buffer_write,1)          = TIME/TIMESCALE
        G_Buffer_loc_RHO(G_indice_buffer_write,2:N_LOC+1)  = RHO15

        G_Buffer_loc_SOU(G_indice_buffer_write,1)          = TIME/TIMESCALE
        G_Buffer_loc_SOU(G_indice_buffer_write,2:N_LOC+1)  = SOU15INI

        IF (IDNH2O.NE.0) THEN

            G_Buffer_loc_HR(G_indice_buffer_write,1)          = TIME/TIMESCALE
            G_Buffer_loc_HR(G_indice_buffer_write,2:N_LOC+1)  = HUM15

            G_Buffer_loc_TROSEE(G_indice_buffer_write,1)          = TIME/TIMESCALE
            G_Buffer_loc_TROSEE(G_indice_buffer_write,2:N_LOC+1)  = TROSEE-273.15D0

            G_Buffer_loc_MH2OLIQ(G_indice_buffer_write,1)          = TIME/TIMESCALE
            G_Buffer_loc_MH2OLIQ(G_indice_buffer_write,2:N_LOC+1)  = LOCRT(:)%MH2OLIQ

            IF (N_MUR>0) THEN !SAM : je suppose que ca ne change pas durant la simulation
                G_Buffer_loc_TW(G_indice_buffer_write,1)          = TIME/TIMESCALE
                G_Buffer_loc_TW(G_indice_buffer_write,2:N_LOC+1)  = LOCRT(:)%TWMEAN-273.15D0

                G_Buffer_loc_TOPER(G_indice_buffer_write,1)          = TIME/TIMESCALE
                G_Buffer_loc_TOPER(G_indice_buffer_write,2:N_LOC+1)  = LOCRT(:)%TOPER-273.15D0
            ENDIF
        ENDIF

        DO J=1,N_SPEC ! donc rien si N_SPEC<1
            G_Buffer_loc_Y(G_indice_buffer_write,J,1)              = TIME/TIMESCALE

            IF (SPECRT(J)%ID=='CO2') THEN
                G_Buffer_loc_Y(G_indice_buffer_write,J,2:N_LOC+1)  = YK15(:,J)*1.D6*29.D0/44.D0
            ELSEIF ((SPECRT(J)%ID=='H2O').OR.(SPECRT(J)%TRACE.EQV..FALSE.)) THEN
                G_Buffer_loc_Y(G_indice_buffer_write,J,2:N_LOC+1)  = YK15(:,J)*1.D3
            ELSEIF (SPECRT(J)%TRACE.EQV..TRUE.) THEN
                G_Buffer_loc_Y(G_indice_buffer_write,J,2:N_LOC+1)  = YK15(:,J)*RHO15(:)
            ENDIF
        ENDDO


        G_Buffer_loc_RA(G_indice_buffer_write,1)          = TIME/TIMESCALE
        G_Buffer_loc_RA(G_indice_buffer_write,2:N_LOC+1)  = LOCRT(:)%RA

    ENDIF !FIN groupe _LOC

    ! LA PARTIE HSRC : attention, ca traite aussi les cas avec N_HSRC=0 !
    IF ((OUTPUT=='ALL').OR.(OUTPUT=='HSRC')) THEN
        IF (N_HSRC>0) THEN
        G_Buffer_hsrc_QM(G_indice_buffer_write,1)          = TIME/TIMESCALE
        G_Buffer_hsrc_QM(G_indice_buffer_write,2:N_HSRC+1)  = HSRCRT%MPDOT
    
        G_Buffer_hsrc_QDOT(G_indice_buffer_write,1)          = TIME/TIMESCALE
        IF (RADIATION==.TRUE.) THEN
            G_Buffer_hsrc_QDOT(G_indice_buffer_write,2:N_HSRC+1)  = HSRCRT%QDOT+HSRCRT%QDOTRAD
        else
            G_Buffer_hsrc_QDOT(G_indice_buffer_write,2:N_HSRC+1)  = HSRCRT%QDOT+HSRCRT%QDOTRAD ! SAM : oui c'est 2 fois la meme chose mais confirme par Francois
        endif
            
        IF (OUTPUT_HSRC_SPECPROD.EQV..TRUE.) THEN
            DO J=1,N_SPEC
                G_Buffer_hsrc_QMSPEC(G_indice_buffer_write,J,1)   = TIME/TIMESCALE
                !renverser l'ordre
                DO K=1,N_HSRC
                    SPECPROD(K)=HSRCRT(K)%PRODYK(J)
                ENDDO
                G_Buffer_hsrc_QMSPEC(G_indice_buffer_write,J,2:N_HSRC+1) = SPECPROD
                CLOSE(LU)
            ENDDO
        ENDIF
        ENDIF
    ENDIF

    ! la partie BRANCH et OUV
    IF ((OUTPUT=='ALL').OR.(OUTPUT=='BRANCH')) THEN
        IF (N_BRANCHE>0) THEN
        G_Buffer_branch_QV(G_indice_buffer_write,1)             = TIME/TIMESCALE
        G_Buffer_branch_QV(G_indice_buffer_write,2:N_BRANCHE+1) =BRANCHERT%QV*3600.D0
    
        G_Buffer_branch_QM(G_indice_buffer_write,1)             = TIME/TIMESCALE
        G_Buffer_branch_QM(G_indice_buffer_write,2:N_BRANCHE+1) =BRANCHERT%QM
    
        G_Buffer_branch_DPFLOW(G_indice_buffer_write,1)             = TIME/TIMESCALE
        G_Buffer_branch_DPFLOW(G_indice_buffer_write,2:N_BRANCHE+1) =BRANCHERT%DP
    
        G_Buffer_branch_RHOFLOW(G_indice_buffer_write,1)            = TIME/TIMESCALE
        G_Buffer_branch_RHOFLOW(G_indice_buffer_write,2:N_BRANCHE+1)=BRANCHERT%RHO15AM
    
        IF (N_OUV>0) THEN
            G_Buffer_ouv_QV1(G_indice_buffer_write,1)        = TIME/TIMESCALE
            G_Buffer_ouv_QV1(G_indice_buffer_write,2:N_OUV+1)=BRANCHERT(IDNOUV)%QVOUV(1)*3600.D0

            G_Buffer_ouv_QV2(G_indice_buffer_write,1)        = TIME/TIMESCALE
            G_Buffer_ouv_QV2(G_indice_buffer_write,2:N_OUV+1)=BRANCHERT(IDNOUV)%QVOUV(2)*3600.D0
        ENDIF
        ENDIF
    ENDIF

    ! la partie EXT
    IF ((OUTPUT=='ALL').OR.(OUTPUT=='EXT')) THEN
        G_Buffer_EXT(G_indice_buffer_write,1)  = TIME/TIMESCALE
        G_Buffer_EXT(G_indice_buffer_write,2)  = QUANTIEME
        G_Buffer_EXT(G_indice_buffer_write,3)  = JOUR
        G_Buffer_EXT(G_indice_buffer_write,4)  = HOUR
        G_Buffer_EXT(G_indice_buffer_write,5)  = MDEXT(1)%VREF
        G_Buffer_EXT(G_indice_buffer_write,6)  = MDEXT(1)%WINC
        G_Buffer_EXT(G_indice_buffer_write,7)  = MDEXT(1)%TEXT-273.15D0
        G_Buffer_EXT(G_indice_buffer_write,8)  = MDEXT(1)%HUMIDITE
        G_Buffer_EXT(G_indice_buffer_write,9)  = MDEXT(1)%SUNRAD
        G_Buffer_EXT(G_indice_buffer_write,10) = MDEXT(1)%DIFFRAD
        G_Buffer_EXT(G_indice_buffer_write,11) = MDEXT(1)%TSKY-273.15D0
        G_Buffer_EXT(G_indice_buffer_write,12) = MDEXT(1)%RHOEXT
        G_Buffer_EXT(G_indice_buffer_write,13) = MDEXT(1)%TROSEE-273.15D0
        G_Buffer_EXT(G_indice_buffer_write,14) = MDEXT(1)%RA
        G_Buffer_EXT(G_indice_buffer_write,15) = (MDEXT(1)%AZ_ANGLE+PI)*180/PI
        G_Buffer_EXT(G_indice_buffer_write,16) = MDEXT(1)%H_ANGLE*180/PI
    ENDIF

    ! la partie WALL
    IF ((OUTPUT=='ALL').OR.(OUTPUT=='WALL')) THEN
        IF (N_MUR>0) THEN
            G_Buffer_wall_TP1(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_TP1(G_indice_buffer_write,2:N_MUR+1)     =MURRT%TPIN-273.15D0

            G_Buffer_wall_TP2(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_TP2(G_indice_buffer_write,2:N_MUR+1)     =MURRT%TPOUT-273.15D0

            G_Buffer_wall_QC1(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_QC1(G_indice_buffer_write,2:N_MUR+1)     =-MURRT%NETQ(1)*MURRT%AREA

            G_Buffer_wall_QC2(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_QC2(G_indice_buffer_write,2:N_MUR+1)     =-MURRT%NETQ(2)*MURRT%AREA

            G_Buffer_wall_FRAD1(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_FRAD1(G_indice_buffer_write,2:N_MUR+1)     =MURRT%FRADIN

            G_Buffer_wall_FRAD2(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_FRAD2(G_indice_buffer_write,2:N_MUR+1)     =MURRT%FRADOUT

            G_Buffer_wall_FSUN1(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_FSUN1(G_indice_buffer_write,2:N_MUR+1)     =MURRT%FSUNIN

            G_Buffer_wall_FSUN2(G_indice_buffer_write,1)             = TIME/TIMESCALE
            G_Buffer_wall_FSUN2(G_indice_buffer_write,2:N_MUR+1)     =MURRT%FSUNOUT
    
            IF (N_WHYGRO.NE.0) THEN
                G_Buffer_wall_QM1(G_indice_buffer_write,1)             = TIME/TIMESCALE
                G_Buffer_wall_QM1(G_indice_buffer_write,2:N_MUR+1)     =-MURRT%NETMTOT(1)*MURRT%AREA

                G_Buffer_wall_QM2(G_indice_buffer_write,1)             = TIME/TIMESCALE
                G_Buffer_wall_QM2(G_indice_buffer_write,2:N_MUR+1)     =-MURRT%NETMTOT(2)*MURRT%AREA

                G_Buffer_wall_MWTOT(G_indice_buffer_write,1)             = TIME/TIMESCALE
                G_Buffer_wall_MWTOT(G_indice_buffer_write,2:N_MUR+1)     =MURRT%MWTOT    
            ENDIF
    

            !pour la partie qui integre le temps + tous les noeuds de tous les murs, on commence par enregistrer le temps
            G_Buffer_wall_T(G_indice_buffer_write,1)    = TIME/TIMESCALE
            G_Buffer_wall_HR(G_indice_buffer_write,1)   = TIME/TIMESCALE
            G_Buffer_wall_YW(G_indice_buffer_write,1)   = TIME/TIMESCALE
            G_Buffer_wall_SLAB(G_indice_buffer_write,1) = TIME/TIMESCALE
            G_Buffer_wall_PSAT(G_indice_buffer_write,1) = TIME/TIMESCALE
            G_Buffer_wall_SORP(G_indice_buffer_write,1) = TIME/TIMESCALE
            G_Buffer_wall_PERM(G_indice_buffer_write,1) = TIME/TIMESCALE
            G_Buffer_wall_DW(G_indice_buffer_write,1)   = TIME/TIMESCALE

            indice_debut_noeud_mur=2
            indice_debut_noeud_hygro_mur=2
            indice_debut_slab_hygro_mur=2
            DO IMUR=1,SIZE(MURRT)
                IF (MURRT(IMUR)%POST_INTERNAL_VAR==.TRUE.) THEN

                    !ecriture des temperatures et des humidites internes
                    nombre_courant_de_noeuds=MURRT(IMUR)%NUMNODE(1)
                    if(nombre_courant_de_noeuds>0) then ! toujours vrai a ce niveau ?

                        indice_fin_noeud_mur=indice_debut_noeud_mur+nombre_courant_de_noeuds-1
                        G_Buffer_wall_T(G_indice_buffer_write,indice_debut_noeud_mur:indice_fin_noeud_mur)     =MURRT(IMUR)%TEMP-273.15D0
                        indice_debut_noeud_mur=indice_fin_noeud_mur+1

                        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
                        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN

                            indice_fin_noeud_hygro_mur=indice_debut_noeud_hygro_mur+nombre_courant_de_noeuds-1

                            G_Buffer_wall_HR    (G_indice_buffer_write,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)  =MURRT(IMUR)%HR*100.D0
                            G_Buffer_wall_YW    (G_indice_buffer_write,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)  =MURRT(IMUR)%YW/MURRT(IMUR)%RHO
                            G_Buffer_wall_PSAT  (G_indice_buffer_write,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)  =MURRT(IMUR)%PSAT
                            G_Buffer_wall_SORP  (G_indice_buffer_write,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)  =MURRT(IMUR)%SORP
                            G_Buffer_wall_PERM  (G_indice_buffer_write,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)  =MURRT(IMUR)%PERM                              
                            G_Buffer_wall_DW    (G_indice_buffer_write,indice_debut_noeud_hygro_mur:indice_fin_noeud_hygro_mur)  =MURRT(IMUR)%DW

                            indice_debut_noeud_hygro_mur=indice_fin_noeud_hygro_mur+1

                        endif
                    endif

                    nombre_courant_de_slabs=MURRT(IMUR)%NSLAB
                    if(nombre_courant_de_slabs>0) then
                        !IF (MURRT(IMUR)%WALLTYPE=='HYGROTHERMAL') THEN
                        IF (MURRT(IMUR)%enum_walltype==wall_hygrothermal) THEN
                            indice_fin_slab_hygro_mur=indice_debut_slab_hygro_mur+nombre_courant_de_slabs-1
                            G_Buffer_wall_SLAB(G_indice_buffer_write,indice_debut_slab_hygro_mur:indice_fin_slab_hygro_mur)    =MURRT(IMUR)%YWMEAN
                            indice_debut_slab_hygro_mur=indice_fin_slab_hygro_mur+1
                        endif
                    endif

                ENDIF
            ENDDO

                
        ENDIF
    ENDIF

    !la partie CTRL / PROBE
    IF ((OUTPUT=='ALL').OR.(OUTPUT=='CTRL').OR.(OUTPUT=='PROBE')) THEN
        IF ((OUTPUT=='ALL').OR.(OUTPUT=='CTRL')) THEN
            G_Buffer_ctrl_probe(G_indice_buffer_write,1)          = TIME/TIMESCALE
            G_Buffer_ctrl_probe(G_indice_buffer_write,2:N_CTRL+1) =CTRLRT%VALUE
        ELSE
            G_Buffer_ctrl_probe(G_indice_buffer_write,1)          = TIME/TIMESCALE
            G_Buffer_ctrl_probe(G_indice_buffer_write,2:N_PROBE+1)=CTRLRT(IDNPROBE)%VALUE
        ENDIF
    ENDIF



    if(G_indice_buffer_write == G_size_buffer_write) then
        CALL WRITE_BUFFERS(LU)
    endif

END	SUBROUTINE WRITE_RES
    
END MODULE PROC_SAVE_RES_MODULE





