!***********************************************************************
!Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
!
!This file is part of MATHIS.
!
!MATHIS is free software: you can redistribute it and/or modify
!it under the terms of the GNU Lesser General Public License as published by
!the Free Software Foundation, either version 3 of the License, or
!(at your option) any later version.
!
!MATHIS is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU Lesser General Public License for more details.
!
!You should have received a copy of the GNU Lesser General Public License
!along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
!***********************************************************************
      SUBROUTINE DEBOUVENT(PI0,PE0,ZI,ZE,RH,RHBUOY,BF,HF,ALF,CD,ELF,ILOI
     &,TABS,TM,ZN,COCO,DPCRIT)
C
C......CE SOUS-PROGRAMME PERMET DE CALCULER LES DEBITS MASSE A TRAVERS
C      UNE OUVERTURE PAR INTEGRATION DES FORMULES DE BERNOULLI,
C      CHAQUE COTE DE L'OUVERTURE EST CONSTITUE DE DEUX ZONES
C      GAZEUSES, COUCHES HORIZONTALES HOMOGENES SUPERPOSEES,
C      LES CHAMPS DE PRESSION DE PART ET D'AUTRE VARIANT SELON
C      LES LOIS DE L'HYDROSTATIQUE. LES # PARAMETRES SONT:
C
C
C              EN ENTREE
C
C      PI0 = PRESSION DU COTE I A UNE COTE DE REFERENCE
C      PE0 =  " " " " " " " " E   MEME COTE " " " " " "
C      ZI  = COTE DE LA ZONE DE DISCONTINUITE DU COTE I, S/REF
C      ZE  =  " " " " " " " " " " " " " " " " " " " " E, " " "
C      RH(1)= MASSE VOLUMIQUE DES GAZ ZONE HAUTE DE I
C      RH(3)= " " " " " " " " " " " " " " " " " " " E
C      RH(2)= " " " " " " " " " " " " " "  BASSE DE I
C      RH(4)= " " " " " " " " " " " " " " " " " " " E
C      BF = COTE DU BAS DE L'OUVERTURE PAR RAPPORT A LA REFERENCE
C      HF =  " " " HAUT " " " " " " " " " " " " " " " " " " " " "
C      ALF = LARGEUR (UNIFORME) DE L'OUVERTURE
C      CD = COEFF DE DECHARGE A L'OUVERTURE
C
C
C                EN SORTIE
C
C
C
C         ZN = TABLEAU DES ZONES NEUTRES, CALCULEES SI ELLES SE
C         TROUVENT DANS L'OUVERTURE: ATTENTION ! DEUX ZONES NEUTRES
C         PEUVENT ETRE EGALES, C'EST POURQUOI LE TABLEAU EST
C         DIMENSIONNE A 6 BIEN QUE 3 ZONES NEUTRES AU MAXIMUM
C         SONT SUSCEPTIBLES D'ETRE RENCONTREES. TOUTES LES VALEURS
C         DU TABLEAU SONT INITIALISEES A 100000.
C
C
C           TM = TABLEAU (4,4) DES DEBITS, CHAQUE ELEMENT TM(I,J)
C           REPRESENTANT LA VALEUR ABSOLUE DU DEBIT (S'IL EXISTE)
C            DE LA ZONE I VERS LA ZONE J (SINON TM(I,J)=0)
C            1 (RESP.2) EST LA ZONE HAUTE (RESP.BASSE) DE I, ET
C            3 (RESP.4)  " " " " " " " " " " " " " " " " "E.
C
C----------------------------------------------------------------------------
C                                                                           |
C......IMPORTANTE REMISE A JOUR (SEPTEMBRE 1987) POUR CALCUL GENERALISE     |
C      DES     E N T R A I N E M E N T S    DANS L'OUVERTURE                |
C                                                                           |
C......mise a jour mai 88 pour ecrire profil de vitesses , parfois...       |
c                                                                           |
c......enrichissement en mai 90 pour calcul des entrainements selon         |
c      une OPTION SUPPLEMENTAIRE : Zukoski source LINEAIRE                  |
c                                                                           |
C----------------------------------------------------------------------------
C
C      VARIABLES RAJOUTEES EN ENTREE :
C
C
C        ELF  = coefficient compris entre 0 et 1, qui permet de localiser
C               la cote a laquelle le debit du pseudo-panache est equivalent
C               au debit circulant a travers l'ouverture :
C
C                * lorsque ELF = 1 , c'est la cote du BAS de la zone de debit
C                  d'ouverture si l'entrainement inductible est vers le HAUT,
C                  mais c'est la cote du HAUT de la zone de debit lorsque
C                  l'entrainement inductible est vers le BAS !
C
C                * lorsque ELF = 0 , c'est juste le contraire
C
C                * lorsque ELF est entre 0 et 1, la cote consideree est
C                  intermediaire, dans un rapport defini par ELF ; si ELF
C                  egale 0,5 la cote est celle du milieu de la zone de debit
C                  (hypothese T. TANAKA). XB mettrait peut-etre ELF = 1
C
C
C        ILOI = 1 pour loi zoukovskyenne d'entrainement
C             = 2  " " " " maccaffreyenne " " " " " " "
c             = 3  " " " " ZUK-LIN        " " " " " " "
C
C
C        TABS = temperatures absolues (K) des 4 zones gazeuses, indispensables
C               pour le calcul des entrainements (RH's ne suffisent plus)
C
C
C
C      VARIABLES RAJOUTEES EN SORTIE :
C
C
C        COCO = tableau (4,4) de nombres compris entre 0 et 1 : lorsqu'un
C               debit d'ouverture tm(i,j) non nul a ete calcule, on ventilera
C               eventuellement les debits de la maniere suivante :
C
C                # tm(i,j) * ( 1 - coco(i,j) )   de i vers j
C
C                # tm(i,j) *       coco(i,j)     de i vers m(j)
C
C                # tm( n(i) , m(j) ) * coco(i,j) de j vers m(j)
C
C               le debit tm( n(i) , m(j) ), s'il est non nul, n'est donc
C               qu'un debit maximum d'entrainement possible, qu'il convient
C               de manipuler de facon adequate.
C
C         REMARQUE : la transformation m( ) echange tout indice de zone haute
C         ---------- avec celui de la zone basse du meme cote, et vice-versa ;
C
C                    la transformation n( ) echange tout indice de zone haute
C                    avec l'indice zone haute de l'autre cote, et vice-versa,
C                    et meme chose pour les zones basses ;
C
C                    m( ) et n( ) sont involutives et commutent ! ! !
C
C
C        TM     est completee de facon a inclure les entrainements, d'une
C               maniere tres logique :
C
C                * TM(I,I) est un entrainement (maximum) de m(i) vers i,
C                  provoque par le debit d'ouverture TM( N(I),M(I) ), dont
C                  une partie monte concomitamment en I;
C
C                * TM( M(I),I ) est un entrainement (maximum) de m(i) vers i,
C                  provoque par le debit d'ouverture TM( MN(I),M(I) ), dont
C                  une partie monte concomitamment en I ; mn( ) est la
C                  transformation produit de m( ) par n( ).
C
C        Le calcul comportera quelques tests, de facon que les hauteurs
C        d'entrainement, mesurees normalement par la difference de cote entre
C        le point defini par ELF et la zone de discontinuite du cote ou
C        arrive le debit d'ouverture entrainANT, ne se recouvrent pas lorsque
C        coexistent DEUX debits d'ouverture circulant vers le meme cote et
C        susceptibles d'entrainer, tous deux vers le haut, ou tous deux vers le
C        bas : par exemple, dans le cas de deux debits susceptibles d'entrainer
C        vers le haut, l'entrainement induit par le debit s'ecoulant en partie
C        basse d'ouverture s'effectuera sur une hauteur limitee par le bord
C        inferieur du debit d'ouverture s'ecoulant en partie haute.
C
C-----------------------------------------------------------+++++++++++++++++++
C
C
C     ...SOME EXPLANATIONS FOR OUR NON-FRENCH READERS !!!
C            ( SEE FRENCH TEXT FOR RELEASE )
C
C     THIS SUBROUTINE COMPUTES THE MASS FLOW RATES THROUGH A
C     RECTANGULAR OPENING, DUE TO PRESSURE DIFFERENCE FIELD
C     ACROSS IT. EACH SIDE OF THE OPENING IS ASSUMED TO CONSIST
C     OF TWO (GASEOUS) LAYERS, BEING CALM ENOUGH (DESPITE OF THE
C     EXISTENCE OF THE FLOWS THEMSELVES!!) TO DARE SATISFY THE
C     HYDROSTATICS....BERNOULLI FORMULAS ARE APPLIED.
C
C              INPUT PARAMETERS
C
C         PI0 = PRESSURE AT A CERTAIN REFERENCE LEVEL, SIDE I
C         PE0 =  " " " " "  THE SAME   " " " " " " " , SIDE E
C         ZI = DISCONTINUITY HEIGHT, SIDE I (OVER REF. LEVEL)
C         ZE =  " " " " " " " " " " " " " E   " " " " " " "
C         RH(1)= GAS DENSITY OF THE UPPER LAYER, SIDE I
C         RH(3)=  " " " " " " " " " " " " " " " " " " E
C         RH(2)=  " " " " " " " " " LOWER " " " " " " I
C         RH(4)=  " " " " " " " " " " " " " " " " " " E
C         BF = OPENING SILL HEIGHT (OVER REF. LEVEL)
C         HF =  " "  SOFFIT " " " " " " " " " " " "
C         ALF = " " "  WIDTH
C         CD = CONSTRICTION COEFFICIENT
C
C             OUTPUT PARAMETERS
C
C         ZN = TABLE OF THE NEUTRAL PLANE HEIGHTS, COMPUTED IF IN
C         THE OPENING: AS TWO OF THESE MAY BE EQUAL, THE DIMENSION
C         IS 6, BUT THERE ARE AT MOST THREE NEUTRAL PLANE HEIGHTS.
C         THE INITIAL VALUES ARE 100000. (METRES)
C
C
C
C        TM = TABLE(4,4) OF THE MASS FLOW RATES THRU OPENING, EACH
C        ELEMENT TM(I,J) REPRESENTING THE ABSOLUTE VALUE OF THE MASS
C        FLOW RATE (IF ANY) FROM I TO J (IF NOT: TM(I,J)= 0)
C         1 IS RELATIVE TO THE UPPER GASEOUS LAYER, SIDE I
C         2  " " " " " " " " " LOWER " " " " " " " " " " "
C         3  " " " " " " " " " UPPER " " " " " " ", SIDE E
C         4  " " " " " " " " " LOWER " " " " " " " " " " "
C
      implicit doubleprecision (a-h,o-z)
C      implicit double precision (a-z)
      DIMENSION RH(4),RHBUOY(4),TM(4,4),ZN(6)
      dimension bord(2,4,4),coa(4),coco(4,4),m(4),n(4),mn(4)
      dimension fact(3),expo1(3),expo2(3),tabs(4),expo3(3)
      DATA G/9.81D0/
      DATA ZERO/0.D0/
      data m/2,1,4,3/,n/3,4,1,2/,mn/4,3,2,1/
      data fact/5.223134762D0,3.D0,6.653319495D0/,expo1/-0.2D0,-0.1277D0
     &,-0.3333D0/
      data expo2/1.6667D0,1.895D0,1.D0/,expo3/0.4D0,0.4D0,0.6667D0/
c
c...lecture nom fichier pour profil (il y aura autant de fichiers que
c...d'ouvertures verticales, meme nom et no de version croissants avec le
c...no d'ordre du passage porte d'ou : marchera pas sou unix.....)
c
c
C
C....INITIALIZATION
C
c..bord(1,i,j) et bord(2,i,j) sont les plus petite et plus grande cotes entre
c   lesquelles s'etend le debit tm(i,j). BORD est initialise a 100000., et
c   bord(1 et 2,i,j) restent egaux a 100000. lorsque tm(i,j)=0
c..coa est le tableau des "lambda-i" (voir notes), qui sert notamment a evaluer
c   le tableau coco
c
      DO 1 K=1,4
          coa(k)=0.d0
          DO 1 L=1,4
              bord(1,k,l)=100000.d0
              bord(2,k,l)=100000.d0
              coco(k,l)=0.d0
1     TM(K,L)=0.d0
      DO 2 K=1,6
2     ZN(K)=100000.d0
      IZN=1
      I=2
      J=4
c...formule qui marche si bf,zi,ze sont au-dessus de la reference
c      DP=(PI0-PE0)+G*(MIN(ZE,BF)*RH(4)-MIN(ZI,BF)*RH(2)
c     & + MAX(ZERO,BF-ZE)*RH(3)-MAX(ZERO,BF-ZI)*RH(1))
c
c...voici une formule tres compliquee qui marche dans tous les cas
c
      dp=(pi0-pe0)+g*(
     &-RHBUOY(1)*(max(min(max(zero,bf-zi),bf),zero)
     &+min(max(bf,zi),zero))
     &-RHBUOY(2)*(min(max(min(zero,bf-zi),bf),zero)
     &+max(min(bf,zi),zero))
     &+RHBUOY(3)*(max(min(max(zero,bf-ze),bf),zero)
     &+min(max(bf,ze),zero))
     &+RHBUOY(4)*(min(max(min(zero,bf-ze),bf),zero)
     &+max(min(bf,ze),zero)))
      ZA=BF
C
C....MAIN LOOP: INTEGRATION OVER INTERVALS [ZA,ZB] FROM BF TO HF
C
10    ZB=HF
C...FLOW RATE FROM OR TO ZONE 1 IF ZI <= ZA
      IF(ZI.LE.ZA)THEN
          I=1
C...MAKE ZB=MIN(HF,ZI) IF ZA < ZI < ZB
      ELSE IF (ZI.LT.ZB)THEN
          ZB=ZI
      END IF
C...FLOW RATE TO OR FROM ZONE 3 IF ZE <= ZA
      IF(ZE.LE.ZA)THEN
          J=3
C...MAKE ZB=MIN(HF,ZI,ZE) IF ZA < ZE < MIN(HF,ZI)
      ELSE IF (ZE.LT.ZB)THEN
          ZB=ZE
      END IF
C
C...EXPRESSION OF PRESSURE DIFFERENCES at ZA AND ZB
C
      DPA=DP
      DPB=DPA+(RHBUOY(J)-RHBUOY(I))*G*(ZB-ZA)
C
C...tentative linearisation
C
      IF ((DPA>=0.D0).AND.(DPA<DPCRIT)) THEN
          DPA=DPA**2.D0/DPCRIT
      ELSEIF ((DPA<0.D0).AND.(DPA>-DPCRIT)) THEN
          DPA=-DPA**2.D0/DPCRIT
      ENDIF
      IF ((DPB>=0.D0).AND.(DPB<DPCRIT)) THEN
          DPB=DPB**2.D0/DPCRIT
      ELSEIF ((DPB<0.D0).AND.(DPB>-DPCRIT)) THEN
          DPB=-DPB**2.D0/DPCRIT
      ENDIF
C
C...NO NEUTRAL PLANE HEIGHT IN [ZA,ZB]
C
      IF (DPA*DPB.GT.ZERO)THEN
          IF (DPA.GT.ZERO)THEN !......FROM I TO J
              bord(1,i,j)=za
              bord(2,i,j)=zb
              X=DSQRT(DPA)
              Y=DSQRT(DPB)
              XX=MIN(X,Y)
              YY=MAX(X,Y)
              TM(I,J)=CD*ALF*2.D0*DSQRT(2.D0*RH(I))/3.D0*(ZB-ZA) *
     &        (XX+YY/(1.D0+XX/YY))
          ELSE !.............FROM J TO I
              bord(1,j,i)=za
              bord(2,j,i)=zb
              X=DSQRT(-DPA)
              Y=DSQRT(-DPB)
              XX=MIN(X,Y)
              YY=MAX(X,Y)
              TM(J,I)=CD*ALF*2.D0*DSQRT(2.D0*RH(J))/3.D0*(ZB-ZA) *
     &        (XX+YY/(1.D0+XX/YY))
          END IF
C
C...NEUTRAL PLANE HEIGHT IN [ZA,ZB]
C
      ELSE IF (DPB.EQ.ZERO.AND.DPA.EQ.ZERO)THEN
          ZN(IZN)=(ZA+ZB)/2.D0
          IZN=IZN+1
      ELSE
          ZN(IZN)=ZA-(DPA/(DPB-DPA))*(ZB-ZA)
          IF(DPA.GT.DPB)THEN !.......BOTTOM FLOW I TO J
              bord(1,i,j)=za
              bord(2,i,j)=zn(izn)
              bord(1,j,i)=zn(izn)
              bord(2,j,i)=zb
              TM(I,J)=2.D0*CD*ALF/3.D0*(ZN(IZN)-ZA) *
     &                DSQRT(2.D0*RH(I)*DPA)
              TM(J,I)=2.D0*CD*ALF/3.D0*(ZB-ZN(IZN)) *
     &                DSQRT(-2.D0*RH(J)*DPB)
          ELSE !.....................BOTTOM FLOW J TO I
              bord(1,j,i)=za
              bord(2,j,i)=zn(izn)
              bord(1,i,j)=zn(izn)
              bord(2,i,j)=zb
              TM(J,I)=2.D0*CD*ALF/3.D0*(ZN(IZN)-ZA) *
     &                DSQRT(-2.D0*RH(J)*DPA)
              TM(I,J)=2.D0*CD*ALF/3.D0*(ZB-ZN(IZN)) *
     &                DSQRT(2.D0*RH(I)*DPB)
          END IF
          IZN=IZN+1
      END IF
      IF (ZB.EQ.HF) go to 11
C
C...REASSIGN LOWER BOUND AND LOWER BOUND PRESSURE DIFFERENCE
C
      ZA=ZB
      DP=DPB
      GO TO 10
11    continue
c
c..calcul des coefficients COA s'il y a lieu
c
      do 12 i=1,2
          do 12 j=3,4
              ii=i
              jj=j
              do 12 k=1,2
                  if(tm(ii,jj).ne.0.d0) then
                      if(coa(ii).eq.0.d0) then
                          if(rhbuoy(n(ii)).eq.rhbuoy(mn(ii))) then
                              if(rhbuoy(ii).gt.rhbuoy(n(ii))) then
                                  coa(ii)=-1.d0
                              else if(rhbuoy(ii).lt.rhbuoy(mn(ii))) then
                                  coa(ii)=+1.d0
                              end if
                          else
                              coa(ii)=rhbuoy(n(ii))+rhbuoy(mn(ii))
     &                                -2.d0*rhbuoy(ii)
                              coa(ii)=coa(ii)/dabs(rhbuoy(n(ii))
     &                                -rhbuoy(mn(ii)))
                              coa(ii)=min(1.d0,max(-1.d0,coa(ii)))
                          end if
                      end if
                  end if
                  ii=n(i)
                  jj=n(j)
12    continue
c
c..examen et calcul des entrainements
c
      do 13 i=1,2
          do 13 j=3,4
              ii=i
              jj=j           ! nb : le nb d'instructions suivantes est a baisser !
              zzz=ze         !     ( rearrangement dans manip de boucles ? )     !
              mj2=mod(j,2)             ! remplacable par 1 - j/4
              m1j=1-2*mj2              ! +1 si j pair ; -1 si j impair
              mji2=mod(j-i,2)
              elfeq=elf*(1.D0-2.D0*mj2)+mj2  ! elf si j pair (panache vers le haut
              do 13 k=1,2    !     k=1 : on voit (i,j)   |   k=2 : on voit n(i),n(j)
                  if(tabs(ii).eq.tabs(jj))go to 14       ! RIEN a FAIRE......
                  if(tm(ii,jj)*m1j*coa(ii).gt.0.d0) then ! c'est BON pour entrainement
                      quant=100000.d0-bord(1,m(ii),jj)  ! frac=0 si tm(m(ii),jj)=0
                      frac=quant/max(1.d0,quant)        ! frac=1 sinon
                      hent=m1j * ( mji2*zzz + (1.D0-mji2) * 
     &                    ( frac*bord(m(i),m(ii),jj)
     &                + (1.d0-frac)*zzz ) - elfeq*bord(1,ii,jj)
     &                - (1.d0-elfeq)*bord(2,ii,jj) )   ! c'etait la hauteur d'entrainement
                      z0=tm(ii,jj)**expo3(iloi) * 
     &                    dabs(tabs(ii)-tabs(jj))**expo1(iloi)
                      z0=fact(iloi)*z0
                      if(iloi.eq.1) z0=z0/rh(jj)**0.6D0 ! z0=distance depuis pseudo-source !!
                      if(iloi.eq.3) z0=z0/rh(jj)/alf**0.6667D0
                      tm(n(ii),m(jj))=tm(ii,jj) * 
     &                    ( (1.d0+hent/z0)**expo2(iloi)-1.d0)
                      coco(ii,jj)=m1j*coa(ii) ! les COCOS sont des COAS, mais POSITIFS !!!
                  end if
14                ii=n(i)
                  jj=n(j)
                  zzz=zi
13    continue
      return
      END
