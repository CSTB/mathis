    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE RADIATION_MODULE

    USE	DATA_EXT_MODULE,ONLY: MDEXT
    USE DATA_LOC_MODULE,ONLY: LOCRT,N_LOC
    USE DATA_MUR_MODULE,ONLY: MURRT,N_MUR,NBREPTSMAX
    USE GLOBAL_VAR_MODULE,ONLY:	PI,QUANTIEME,T15
    USE TYPE_MATHIS_MODULE,ONLY: TYPE_RADIANT_CAVITY

    IMPLICIT NONE


    TYPE(TYPE_RADIANT_CAVITY),DIMENSION(:),ALLOCATABLE,TARGET		:: RAD_CAV

    DOUBLE PRECISION,PARAMETER								:: SIGMA=5.67D-8



    CONTAINS


    SUBROUTINE INIT_CAVITY(OBJ,ILOC)

    TYPE(TYPE_RADIANT_CAVITY),INTENT(INOUT)	:: OBJ
    INTEGER									:: I,K,ILOC,IMUR

    OBJ%NM=0
    DO IMUR=1,N_MUR
        DO I=1,2
            IF (MURRT(IMUR)%LOCIDS(I)==LOCRT(ILOC)%ID) OBJ%NM=OBJ%NM+1
        ENDDO
    ENDDO
    ALLOCATE(OBJ%SURF(OBJ%NM))

    K=0
    DO IMUR=1,N_MUR
        DO I=1,2
            IF (MURRT(IMUR)%LOCIDS(I)==LOCRT(ILOC)%ID) THEN
                K=K+1
                NULLIFY(OBJ%SURF(K)%TP)
                NULLIFY(OBJ%SURF(K)%FRAD)
                NULLIFY(OBJ%SURF(K)%FSUN)
                NULLIFY(OBJ%SURF(K)%COSI)
                NULLIFY(OBJ%SURF(K)%SLOPE)
                NULLIFY(OBJ%SURF(K)%TRSFRAC)

                IF (I==1) THEN
                    OBJ%SURF(K)%TP=>MURRT(IMUR)%TPIN
                    OBJ%SURF(K)%FRAD=>MURRT(IMUR)%FRADIN
                    OBJ%SURF(K)%FSUN=>MURRT(IMUR)%FSUNIN
                ELSE
                    OBJ%SURF(K)%TP=>MURRT(IMUR)%TPOUT
                    OBJ%SURF(K)%FRAD=>MURRT(IMUR)%FRADOUT
                    OBJ%SURF(K)%FSUN=>MURRT(IMUR)%FSUNOUT
                ENDIF
                OBJ%SURF(K)%COSI=>MURRT(IMUR)%COSI
                OBJ%SURF(K)%SLOPE=>MURRT(IMUR)%SLOPE
                OBJ%SURF(K)%INT_THO_DIFF=MURRT(IMUR)%INT_THO_DIFF
                OBJ%SURF(K)%ANGLE=MURRT(IMUR)%ANGLE
                OBJ%SURF(K)%THO=MURRT(IMUR)%THO
                OBJ%SURF(K)%NBREPTS=MURRT(IMUR)%NBREPTS
                OBJ%SURF(K)%AREA=MURRT(IMUR)%AREA
                OBJ%SURF(K)%EPS=MURRT(IMUR)%EPS(I)
                OBJ%SURF(K)%ABS=MURRT(IMUR)%ABS(I)
                OBJ%SURF(K)%TRSINT=MURRT(IMUR)%TRS
                OBJ%SURF(K)%TRSEXT=MURRT(IMUR)%TRS
                OBJ%SURF(K)%TRSFRAC=>MURRT(IMUR)%TRSFRAC
                ALLOCATE(OBJ%SURF(K)%F(OBJ%NM))
                ALLOCATE(OBJ%SURF(K)%H(OBJ%NM))
            ENDIF
        ENDDO
    ENDDO
    OBJ%STOT=SUM(OBJ%SURF(:)%AREA)
    OBJ%STOTSQUARED=SUM(OBJ%SURF(:)%AREA**2.D0)
    NULLIFY(OBJ%TWMEAN);OBJ%TWMEAN=>LOCRT(ILOC)%TWMEAN
    NULLIFY(OBJ%HRAD);OBJ%HRAD=>LOCRT(ILOC)%HRAD

    CALL HRAD_CALC(OBJ,0)

    END SUBROUTINE INIT_CAVITY

    SUBROUTINE INIT_RADIATION

    INTEGER		:: I

    IF (ALLOCATED(RAD_CAV)) DEALLOCATE(RAD_CAV);   ALLOCATE(RAD_CAV(N_LOC))
    DO I=1,N_LOC
        CALL INIT_CAVITY(RAD_CAV(I),I)
    ENDDO

    END SUBROUTINE INIT_RADIATION

    SUBROUTINE HRAD_CALC(OBJ,FLAG)

    TYPE(TYPE_RADIANT_CAVITY),INTENT(INOUT)	:: OBJ
    INTEGER,INTENT(IN)                      :: FLAG
    INTEGER									:: I,J,K,S
    DOUBLE PRECISION						:: STOT,STOTSQUARED
    OBJ%ABS_MOY=0.D0
    OBJ%TRS_MOY=0.D0
    DO S=1,OBJ%NM
        OBJ%ABS_MOY = OBJ%ABS_MOY + (OBJ%SURF(S)%AREA/OBJ%STOT)*OBJ%SURF(S)%ABS
        IF((OBJ%SURF(S)%TRSEXT.GT.0d0).AND.(OBJ%SURF(S)%INT_THO_DIFF.NE.1.D0)) THEN
            OBJ%TRS_MOY = OBJ%TRS_MOY + (OBJ%SURF(S)%AREA/OBJ%STOT)*OBJ%SURF(S)%TRSFRAC*OBJ%SURF(S)%TRSINT*OBJ%SURF(S)%INT_THO_DIFF
        ELSEIF ((OBJ%SURF(S)%TRSEXT.GT.0d0).AND.(OBJ%SURF(S)%INT_THO_DIFF.EQ.1.D0).AND.(OBJ%SURF(S)%NBREPTS>10)) THEN
            OBJ%SURF(S)%INT_THO_DIFF=((OBJ%SURF(S)%THO(2)*DCOS(OBJ%SURF(S)%ANGLE(2)*(PI/180.D0))+OBJ%SURF(S)%THO(1)*DCOS(OBJ%SURF(S)%ANGLE(1)*(PI/180.D0)))/2)*(OBJ%SURF(S)%ANGLE(2)-OBJ%SURF(S)%ANGLE(1))*(3.14/180)
            DO J=3,OBJ%SURF(S)%NBREPTS
                OBJ%SURF(S)%INT_THO_DIFF=OBJ%SURF(S)%INT_THO_DIFF+((OBJ%SURF(S)%THO(J)*DCOS(OBJ%SURF(S)%ANGLE(J)*(PI/180.D0))+OBJ%SURF(S)%THO(J-1)*DCOS(OBJ%SURF(S)%ANGLE(J-1)*(PI/180.D0)))/2)*(OBJ%SURF(S)%ANGLE(J)-OBJ%SURF(S)%ANGLE(J-1))*(3.14/180)
            ENDDO
            OBJ%TRS_MOY = OBJ%TRS_MOY + (OBJ%SURF(S)%AREA/OBJ%STOT)*OBJ%SURF(S)%TRSFRAC*OBJ%SURF(S)%TRSINT*OBJ%SURF(S)%INT_THO_DIFF
        ELSEIF ((OBJ%SURF(S)%TRSEXT.GT.0d0).AND.(OBJ%SURF(S)%INT_THO_DIFF.EQ.1.D0).AND.(OBJ%SURF(S)%NBREPTS<=10)) THEN
            OBJ%TRS_MOY = OBJ%TRS_MOY + (OBJ%SURF(S)%AREA/OBJ%STOT)*OBJ%SURF(S)%TRSFRAC*OBJ%SURF(S)%TRSINT*0.8891377d0
        ELSE
            OBJ%TRS_MOY = OBJ%TRS_MOY + (OBJ%SURF(S)%AREA/OBJ%STOT)*OBJ%SURF(S)%TRSFRAC*OBJ%SURF(S)%TRSINT
        ENDIF
    ENDDO
    IF (FLAG==0) THEN
        IF (OBJ%NM>=2) THEN
            DO I=1,OBJ%NM	!CALCUL DES FACTEURS DE FORME FIJ
                OBJ%SURF(I)%F=0.D0
                DO J=1,OBJ%NM
                    IF (J.NE.I) THEN
                        OBJ%SURF(I)%F(J)=OBJ%SURF(J)%AREA/OBJ%STOT+((OBJ%SURF(J)%AREA**2.D0)/OBJ%STOTSQUARED)*(OBJ%SURF(I)%AREA/OBJ%STOT)
                    ELSE
                        OBJ%SURF(I)%F(J)=((OBJ%SURF(J)%AREA**2.D0)/OBJ%STOTSQUARED)*(OBJ%SURF(I)%AREA/OBJ%STOT)
                    ENDIF
                ENDDO
            ENDDO
            DO S=1,OBJ%NM	!CALCUL DES HIS
                OBJ%SURF(S)%H=0.D0
                DO I=1,OBJ%NM
                    OBJ%SURF(S)%H(I)=SIGMA/((1.D0-OBJ%SURF(I)%EPS)/(OBJ%SURF(I)%AREA*OBJ%SURF(I)%EPS) &
                        +(1.D0-OBJ%SURF(S)%EPS)/(OBJ%SURF(S)%AREA*OBJ%SURF(S)%EPS) &
                        +1.D0/(OBJ%SURF(I)%AREA*OBJ%SURF(I)%F(S)))
                ENDDO
            ENDDO
        ENDIF
    ENDIF

    END SUBROUTINE HRAD_CALC

    SUBROUTINE NWALL_RADIATION(OBJ)

    TYPE(TYPE_RADIANT_CAVITY),INTENT(INOUT)				:: OBJ
    INTEGER												:: I,S,J,K
    DOUBLE PRECISION									:: agl_incidence, tho_cor,poly,Tache_solaire,SUNDIR,SUNDIFF,AI,F,RB,INT_THO_DIFF_INI

    CALL HRAD_CALC(OBJ,1)
    IF (OBJ%NM>=1) THEN
        DO S=1,OBJ%NM
            OBJ%SURF(S)%FRAD=0.D0
            DO I=1,OBJ%NM
                IF (I.NE.S) THEN
                    OBJ%SURF(S)%FRAD=OBJ%SURF(S)%FRAD+OBJ%SURF(I)%H(S)*(OBJ%SURF(I)%TP**4.D0-OBJ%SURF(S)%TP**4.D0)/OBJ%SURF(S)%AREA
                    !On ajoute ici le flux solaire transmis par i sur s au pro rata des rapports des surfaces :
                    !Fs= (Ss/Stot)*(Si*Trs*G/Ss)
                ENDIF
                !!modif xf : prise en compte de la dependance du coef de transmission sur le spectre energetique en fonction de l'angle d'incidence du soleil vis a vis de la normale a la surface
                if (OBJ%SURF(I)%TRSEXT.GT.0d0) then
                    INT_THO_DIFF_INI = OBJ%SURF(I)%INT_THO_DIFF
                    agl_incidence = DACOSD(OBJ%SURF(I)%COSI)
                    IF (OBJ%SURF(I)%NBREPTS <10.D0) THEN
                        tho_cor=-1.3d-16*agl_incidence**9d0+4.4d-14*agl_incidence**8d0-6.3d-12*agl_incidence**7d0+4.9d-10*agl_incidence**6d0-2.3d-8*agl_incidence**5d0+6.3d-7*agl_incidence**4d0-9.8d-6*agl_incidence**3d0+7.8d-5*agl_incidence**2d0-2.5d-4*agl_incidence+1d0
                        OBJ%SURF(I)%INT_THO_DIFF=0.8891377d0
                    ELSE
                        poly=1.D0
                        tho_cor=0.D0
                        J=1.D0
                        K=1.D0
                        IF (INT_THO_DIFF_INI.EQ.1.D0) THEN
                            OBJ%SURF(I)%INT_THO_DIFF=((OBJ%SURF(I)%THO(J+1)*DCOS(OBJ%SURF(I)%ANGLE(J+1)*(PI/180.D0))+OBJ%SURF(I)%THO(J)*DCOS(OBJ%SURF(I)%ANGLE(J)*(PI/180.D0)))/2)*(OBJ%SURF(I)%ANGLE(J+1)-OBJ%SURF(I)%ANGLE(J))*(3.14/180)
                        ENDIF
                        DO J=1,OBJ%SURF(I)%NBREPTS
                            K=1.D0
                            poly=1.D0
                            DO K=1,OBJ%SURF(I)%NBREPTS
                                IF (K.NE.J) THEN
                                    poly = poly * ((agl_incidence-OBJ%SURF(I)%ANGLE(K))/(OBJ%SURF(I)%ANGLE(J)-OBJ%SURF(I)%ANGLE(K)))
                                ENDIF
                            ENDDO
                            poly=OBJ%SURF(I)%THO(J)*poly
                            tho_cor=tho_cor+poly
                            IF((J>2).AND.(INT_THO_DIFF_INI.EQ.1.D0)) THEN
                                OBJ%SURF(I)%INT_THO_DIFF=OBJ%SURF(I)%INT_THO_DIFF+((OBJ%SURF(I)%THO(J)*DCOS(OBJ%SURF(I)%ANGLE(J)*(PI/180.D0))+OBJ%SURF(I)%THO(J-1)*DCOS(OBJ%SURF(I)%ANGLE(J-1)*(PI/180.D0)))/2)*(OBJ%SURF(I)%ANGLE(J)-OBJ%SURF(I)%ANGLE(J-1))*(3.14/180)
                            ENDIF
                        ENDDO
                    ENDIF

                    if (tho_cor>1.d0) then
                        tho_cor=1.d0
                    endif
                    if (tho_cor<0.d0) then
                        tho_cor=0.d0
                    endif
                    Tache_solaire = OBJ%SURF(I)%AREA
                    AI=MDEXT(1)%SUNRAD/(1367.D0*(1.D0+0.033D0*DCOSD(360.D0*QUANTIEME/365.D0)))
                    F=0.D0
                    IF (DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD>0.D0) F=(DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD/(DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD+MDEXT(1)%DIFFRAD))**0.5D0
                    IF (MDEXT(1)%H_ANGLE>=5.D0*PI/180.D0) THEN
                        RB=OBJ%SURF(I)%COSI/DSIN(MDEXT(1)%H_ANGLE)
                    ELSE
                        RB=OBJ%SURF(I)%COSI
                    ENDIF

                    SUNDIFF = (Tache_solaire/OBJ%STOT)*(OBJ%SURF(I)%TRSFRAC*OBJ%SURF(I)%TRSEXT/OBJ%SURF(I)%ABS)*OBJ%SURF(I)%INT_THO_DIFF*OBJ%SURF(I)%ABS*(AI*MDEXT(1)%DIFFRAD*RB+((1.D0+DCOS(OBJ%SURF(I)%SLOPE))/2.D0)*MDEXT(1)%DIFFRAD*(1.D0-AI)*(1.D0+F*(DSIN(OBJ%SURF(I)%SLOPE/2.D0))**3.D0)+((1.D0-DCOS(OBJ%SURF(I)%SLOPE))/2.D0)*MDEXT(1)%ALBEDO*(DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD+MDEXT(1)%DIFFRAD))
                    SUNDIR =  (Tache_solaire/OBJ%STOT)*(OBJ%SURF(I)%TRSFRAC*OBJ%SURF(I)%TRSEXT/OBJ%SURF(I)%ABS)*tho_cor*OBJ%SURF(I)%ABS*MDEXT(1)%SUNRAD*OBJ%SURF(I)%COSI
                    OBJ%SURF(S)%FSUN=OBJ%SURF(S)%ABS*(SUNDIR+SUNDIFF)*(1.d0/(OBJ%ABS_MOY+OBJ%TRS_MOY))

                    OBJ%SURF(S)%FRAD=OBJ%SURF(S)%FRAD+OBJ%SURF(S)%FSUN

                endif
            ENDDO
            !On ajoute le flux provenant d'une source ponctuelle radiative
            !Fsource=(Ss/Stot)*HRAD/Ss
            OBJ%SURF(S)%FRAD=OBJ%SURF(S)%FRAD+OBJ%HRAD/OBJ%STOT
        ENDDO
    ENDIF

    END SUBROUTINE NWALL_RADIATION

    SUBROUTINE CALC_TWMEAN

    INTEGER :: I,S

    !On calcule la temperature moyenne radiante
    DO I=1,N_LOC
        IF (RAD_CAV(I)%NM>=1) THEN
            RAD_CAV(I)%TWMEAN=0.D0
            DO S=1,RAD_CAV(I)%NM
                RAD_CAV(I)%TWMEAN=RAD_CAV(I)%TWMEAN+RAD_CAV(I)%SURF(S)%TP*RAD_CAV(I)%SURF(S)%AREA/RAD_CAV(I)%STOT
            ENDDO
        ELSE
            RAD_CAV(I)%TWMEAN=T15(I)
        ENDIF
    ENDDO

    END SUBROUTINE CALC_TWMEAN


    SUBROUTINE CALC_RADIATION

    INTEGER :: I

    DO I=1,N_LOC
        CALL NWALL_RADIATION(RAD_CAV(I))
    ENDDO

    END SUBROUTINE CALC_RADIATION





    END MODULE RADIATION_MODULE
