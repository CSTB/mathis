    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE PROC_SPEC_MODULE

    USE DATA_SPEC_MODULE
    USE GLOBAL_VAR_MODULE, ONLY:	   CHECKREAD,SHUTDOWN
    USE GLOBAL_VAR_MODULE, ONLY:   LU1,LUOUT,MESSAGE,CP15,CV15,RU,PREF,DP,BACKGROUND_SPECIE,ATEC,CONSTANT_SPECIFIC_HEAT,ALL_SPEC_ARE_TRACE

    IMPLICIT NONE

    CONTAINS

    SUBROUTINE BACKGROUND_SPEC(ID,MW,CV)

    CHARACTER(100),INTENT(OUT)			:: ID
    DOUBLE PRECISION,INTENT(OUT)        :: MW,CV

    ID=BACKGROUND_SPECIE
    IF (BACKGROUND_SPECIE=='AIR') THEN
        MW=29.D-3
        CV=710.D0
    ELSEIF (BACKGROUND_SPECIE=='N2') THEN
        MW=28.D-3
        CV=730.D0
    ELSEIF (BACKGROUND_SPECIE=='WATER') THEN
        MW=18.D-3
        CV=4119.D0
    ENDIF
    
    END SUBROUTINE BACKGROUND_SPEC

    SUBROUTINE INIT_CONS_SPEC(OBJ,IDN)

    TYPE(TYPE_SPEC),INTENT(INOUT)		:: OBJ
    INTEGER,INTENT(IN)					:: IDN
    DOUBLE PRECISION        			:: MW,CV
    CHARACTER(100)			            :: ID
    
    OBJ%IDN=IDN
    CALL BACKGROUND_SPEC(ID,MW,CV)
    IF (OBJ%IDN==N_SPEC+1) THEN
        OBJ%ID=ID
        OBJ%MW=MW
        OBJ%CV=CV
    ELSEIF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
        OBJ%MW=MW
        OBJ%CV=CV
    ENDIF
    IF (OBJ%ID=='H2O') THEN
        IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
            OBJ%MW=18.D-3
            OBJ%CV=1410.D0
        ENDIF
        IF (ALL_SPEC_ARE_TRACE==.FALSE.) OBJ%TRACE=.FALSE.
        IDNH2O=OBJ%IDN
    ELSEIF (OBJ%ID=='CO2') THEN
        IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
            OBJ%MW=44.D-3
            OBJ%CV=650.D0
        ENDIF
        OBJ%YKREF=0.000607
        IF (ALL_SPEC_ARE_TRACE==.FALSE.) OBJ%TRACE=.FALSE.
        IDNCO2=OBJ%IDN
    ELSEIF (OBJ%ID=='O2') THEN
        IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
            OBJ%MW=32.D-3
            OBJ%CV=650.D0
        ENDIF
        OBJ%TRACE=.FALSE.
    ELSEIF (OBJ%ID=='N2') THEN
        IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
            OBJ%MW=28.D-3
            OBJ%CV=730.D0
        ENDIF
        OBJ%TRACE=.FALSE.
    ELSEIF ((OBJ%ID=='HE').OR.(OBJ%ID=='HELIUM')) THEN
        IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
            OBJ%MW=4.D-3
            OBJ%CV=3125.D0
        ENDIF
        OBJ%TRACE=.FALSE.
    ELSEIF (OBJ%ID=='AIR') THEN
        IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
            OBJ%MW=29.D-3
            OBJ%CV=710.D0
        ENDIF
        OBJ%TRACE=.FALSE.
    ENDIF

    IF (BACKGROUND_SPECIE=='WATER') THEN
        OBJ%CP=4186.D0
    ELSE
        OBJ%CP=OBJ%CV+RU/OBJ%MW
    ENDIF

    END SUBROUTINE INIT_CONS_SPEC
    
    SUBROUTINE INIT_CONS_SPECRT

    CHARACTER(100)			:: ID
    DOUBLE PRECISION		:: MW,CV
    INTEGER					:: I,K,L

    IDNH2O=0
    IDNCO2=0
    N_TRACE=0
    DO I=1,SIZE(SPECRT)
        CALL INIT_CONS_SPEC(SPECRT(I),I)
        IF (SPECRT(I)%TRACE.EQV..TRUE.) N_TRACE=N_TRACE+1
    ENDDO 

    IF (ALLOCATED(IDNTRACE)) DEALLOCATE(IDNTRACE);ALLOCATE(IDNTRACE(N_TRACE))
    IF (ALLOCATED(IDNNONTRACE)) DEALLOCATE(IDNNONTRACE);ALLOCATE(IDNNONTRACE(N_SPEC-N_TRACE))
    K=0;L=0
    DO I=1,N_SPEC
        IF (SPECRT(I)%TRACE.EQV..TRUE.) THEN
            K=K+1
            IDNTRACE(K)=I
        ELSE
            L=L+1
            IDNNONTRACE(L)=I
        ENDIF
    ENDDO
    IF (ATEC==.TRUE.) THEN
        IF (IDNCO2==0) THEN
            WRITE(MESSAGE,'(A)') 'ERROR : you must define a CO2 SPEC line for an ATEC calculation'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
        IF (IDNH2O==0) THEN
            WRITE(MESSAGE,'(A)') 'ERROR : you must define a H2O SPEC line for an ATEC calculation'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDIF

    END SUBROUTINE INIT_CONS_SPECRT

    END MODULE PROC_SPEC_MODULE
