    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE PROC_LOC_MODULE

    USE DATA_LOC_MODULE
    USE DATA_BRANCHE_MODULE,ONLY: BRANCHERT,N_BRANCHE
    USE DATA_CTRL_MODULE,ONLY : CTRLRT,N_CTRL
    USE DATA_EXT_MODULE,ONLY:	MDEXT
    USE DATA_SPEC_MODULE,ONLY : SPECRT,N_SPEC,IDNH2O
    USE FLUX_ME_MODULE
    USE GLOBAL_VAR_MODULE, ONLY:	   CHECKREAD,SHUTDOWN,YK_HUM_EVAL,BILINEAR_INTERP
    USE GLOBAL_VAR_MODULE, ONLY:   LU1,LUOUT,MESSAGE,TETA0,TIME,DTIME,RHO15,T15,DP,CP15,CV15,YK15,HUM15,SOU15INI,VARDED,ISOTHERMAL,ISOTHERMALNODE,GUESS_POWER,PI,LV,MOISTURE_NODES,ALL_SPEC_ARE_TRACE 

    IMPLICIT NONE

    CONTAINS

    SUBROUTINE INIT_CONS_LOC(OBJ,IDN)

    TYPE(TYPE_LOC),INTENT(INOUT)				:: OBJ
    INTEGER,INTENT(IN)							:: IDN

    OBJ%IDN=IDN
    OBJ%VOLUME=1.D0
    IF (OBJ%AREA*OBJ%HEIGHT.NE.0D0) OBJ%VOLUME=OBJ%AREA*OBJ%HEIGHT
    OBJ%TINI=273.15D0+OBJ%TINI
    IF (OBJ%TUP.NE.-9999.D0) THEN
        OBJ%TUP=273.15D0+OBJ%TUP
    ELSE
        OBJ%TUP=OBJ%TINI
    ENDIF
    IF (OBJ%TDOWN.NE.-9999.D0) THEN
        OBJ%TDOWN=273.15D0+OBJ%TDOWN
    ELSE
        OBJ%TDOWN=OBJ%TINI
    ENDIF
    IF ((OBJ%ZINT>0.D0).AND.(OBJ%ZINT<OBJ%HEIGHT)) THEN
        OBJ%ZINT=OBJ%ZINT
    ELSEIF (OBJ%ZINT>=OBJ%HEIGHT) THEN
        OBJ%ZINT=OBJ%HEIGHT
    ELSEIF (OBJ%ZINT.LT.0.D0) THEN
        OBJ%ZINT=0.D0
    ENDIF
    IF (OBJ%HEIGHT.NE.0.D0) THEN
        OBJ%TINI=OBJ%ZINT/OBJ%HEIGHT*OBJ%TDOWN+(1.D0-OBJ%ZINT/OBJ%HEIGHT)*OBJ%TUP
    ENDIF

    END SUBROUTINE INIT_CONS_LOC

    SUBROUTINE INIT_CONS_LOCRT

    INTEGER										:: I,J,K,L

    N_NODE=0
    N_ROOM=0
    DO I=1,SIZE(LOCRT)
        CALL INIT_CONS_LOC(LOCRT(I),I)
        IF (LOCRT(I)%LOCTYPE=='NODE') N_NODE=N_NODE+1
    ENDDO
    N_ROOM=N_LOC-N_NODE
    IF (ALLOCATED(IDNNODE)) DEALLOCATE(IDNNODE);ALLOCATE(IDNNODE(N_NODE))
    IF (ALLOCATED(IDNROOM)) DEALLOCATE(IDNROOM);ALLOCATE(IDNROOM(N_ROOM))
    
    K=0
    L=0
    N_PZONE=0
    DO I=1,N_LOC
        IF (LOCRT(I)%LOCTYPE=='NODE') THEN
            L=L+1
            IDNNODE(L)=LOCRT(I)%IDN
        ELSE
            K=K+1
            IDNROOM(K)=LOCRT(I)%IDN
        ENDIF
        IF (LOCRT(I)%PZONEID=='null') LOCRT(I)%PZONEID=LOCRT(I)%ID
        IF (LOCRT(I)%PZONEID==LOCRT(I)%ID) N_PZONE=N_PZONE+1
        DO J=1,N_LOC
            IF (LOCRT(I)%PZONEID==LOCRT(J)%ID) LOCRT(I)%IDNPZONE=J
        ENDDO
    ENDDO
    IF (ALLOCATED(IDNPZONE)) DEALLOCATE(IDNPZONE);ALLOCATE(IDNPZONE(N_PZONE))
    IF (ALLOCATED(IDNIZONE)) DEALLOCATE(IDNIZONE);ALLOCATE(IDNIZONE(N_LOC-N_PZONE))
   
   
    K=0
    L=0
    DO I=1,N_LOC
        IF (LOCRT(I)%IDNPZONE==LOCRT(I)%IDN) THEN
            L=L+1
            IDNPZONE(L)=LOCRT(I)%IDN
        ELSE
            K=K+1
            IDNIZONE(K)=LOCRT(I)%IDN
        ENDIF
    ENDDO

    END SUBROUTINE INIT_CONS_LOCRT


    SUBROUTINE INIT_VAR_LOC(ILOC)

    INTEGER,INTENT(IN)			  :: ILOC
    TYPE(TYPE_LOC),POINTER		  :: OBJ
    INTEGER						  :: K,NLAT,NRECT,ILAT,IRECT

    OBJ=>LOCRT(ILOC)

    OBJ%DTUP=OBJ%TUP-OBJ%TINI
    OBJ%DTDOWN=OBJ%TDOWN-OBJ%TINI
    OBJ%T15UP=OBJ%TINI+OBJ%DTUP
    OBJ%T15DOWN=OBJ%TINI+OBJ%DTDOWN
    CALL VARDED(0.D0,OBJ%T15UP,MDEXT(1)%YK,MDEXT(1)%CPEXT,MDEXT(1)%CVEXT,OBJ%RHOUP,OBJ%RHOBUOYUP,OBJ%HRUP,OBJ%TROSEEUP)
    CALL VARDED(0.D0,OBJ%T15DOWN,MDEXT(1)%YK,MDEXT(1)%CPEXT,MDEXT(1)%CVEXT,OBJ%RHODOWN,OBJ%RHOBUOYDOWN,OBJ%HRDOWN,OBJ%TROSEEDOWN)
    OBJ%OCC=0.D0
    OBJ%MH2OLIQ=0.D0
    OBJ%MH2OLIQ0=0.D0
    OBJ%MH2OLIQ0_OLD=0.D0
    OBJ%HRAD=0.D0
    OBJ%TWMEAN=OBJ%TINI
    OBJ%TOPER=OBJ%TINI
    OBJ%RA=0.D0

    NLAT=0
    NRECT=0
    IF (OBJ%LOCTYPE=='NODE') THEN
        DO K=1,N_BRANCHE
            IF ((BRANCHERT(K)%LOCIDS(1)==OBJ%ID)) THEN
                IF (BRANCHERT(K)%ALPHA(1)>1.D0) THEN
                    NLAT=NLAT+1
                ELSE
                    NRECT=NRECT+1
                ENDIF
            ELSEIF (BRANCHERT(K)%LOCIDS(2)==OBJ%ID) THEN
                IF (BRANCHERT(K)%ALPHA(2)>1.D0) THEN
                    NLAT=NLAT+1
                ELSE
                    NRECT=NRECT+1
                ENDIF
            ENDIF
        ENDDO
        ALLOCATE(OBJ%LATERALIDNS(NLAT));ALLOCATE(OBJ%STRAIGHTIDNS(NRECT))
        ILAT=0
        IRECT=0
        DO K=1,N_BRANCHE
            IF (BRANCHERT(K)%LOCIDS(1)==OBJ%ID) THEN
                IF (BRANCHERT(K)%ALPHA(1)>1.D0) THEN
                    ILAT=ILAT+1
                    OBJ%LATERALIDNS(ILAT)=BRANCHERT(K)%IDN
                ELSE
                    IRECT=IRECT+1
                    OBJ%STRAIGHTIDNS(IRECT)=BRANCHERT(K)%IDN
                ENDIF
            ELSEIF (BRANCHERT(K)%LOCIDS(2)==OBJ%ID) THEN
                IF (BRANCHERT(K)%ALPHA(2)>1.D0) THEN
                    ILAT=ILAT+1
                    OBJ%LATERALIDNS(ILAT)=BRANCHERT(K)%IDN
                ELSE
                    IRECT=IRECT+1
                    OBJ%STRAIGHTIDNS(IRECT)=BRANCHERT(K)%IDN
                ENDIF
            ENDIF
        ENDDO
        IF ((NLAT.NE.0).AND.(NRECT.LT.1)) THEN
            WRITE(MESSAGE,'(A,A,A)') 'ERROR : Problem with &LOC ',TRIM(OBJ%ID),': No main duct while at least one lateral duct joins this node'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ELSEIF ((NLAT.NE.0).AND.(NRECT.GT.2)) THEN
            WRITE(MESSAGE,'(A,A,A)') 'ERROR : Problem with &LOC ',TRIM(OBJ%ID),': More than two main ducts while at least one lateral duct joins this node'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDIF

    END SUBROUTINE INIT_VAR_LOC

    SUBROUTINE INIT_VAR_LOCRT

    INTEGER										:: I

    DO I=1,SIZE(LOCRT)
        CALL INIT_VAR_LOC(I)
    ENDDO

    END SUBROUTINE INIT_VAR_LOCRT

    SUBROUTINE INIT_IMAGE_LOC(ILOC)

    INTEGER,INTENT(IN)			  :: ILOC
    TYPE(TYPE_LOC),POINTER		  :: OBJ
    INTEGER									:: ICTRL

    OBJ=>LOCRT(ILOC)

    NULLIFY(OBJ%RHO15);OBJ%RHO15=>RHO15(OBJ%IDN)
    NULLIFY(OBJ%T15);OBJ%T15=>T15(OBJ%IDN)
    NULLIFY(OBJ%DP);OBJ%DP=>DP(OBJ%IDN)
    NULLIFY(OBJ%CP);OBJ%CP=>CP15(OBJ%IDN)
    NULLIFY(OBJ%CV);OBJ%CV=>CV15(OBJ%IDN)
    NULLIFY(OBJ%SOU15);OBJ%SOU15=>SOU15INI(OBJ%IDN)
    NULLIFY(OBJ%YK); ALLOCATE(OBJ%YK(N_SPEC));OBJ%YK=>YK15(OBJ%IDN,:)
    NULLIFY(OBJ%HR); OBJ%HR=>HUM15(OBJ%IDN)

    NULLIFY(OBJ%FRAC)
    DO ICTRL=1,N_CTRL
        IF (OBJ%TCTRLID==CTRLRT(ICTRL)%ID) OBJ%FRAC=>CTRLRT(ICTRL)%VALUE
    ENDDO

    END SUBROUTINE INIT_IMAGE_LOC

    SUBROUTINE INIT_IMAGE_LOCRT

    INTEGER										:: I

    DO I=1,SIZE(LOCRT)
        CALL INIT_IMAGE_LOC(I)
    ENDDO

    END SUBROUTINE INIT_IMAGE_LOCRT

    SUBROUTINE FLUX_ME_LOC(ILOC)

    INTEGER,INTENT(IN)			  :: ILOC
    TYPE(TYPE_LOC),POINTER		  :: OBJ
    DOUBLE PRECISION			  :: YH2OSAT,aa,coef,Q1,D1,Q1conf,D1conf,Qconf,Dconf,rhoconf,Q1sep,D1sep,Qsep,Dsep,rhosep
    double precision              :: terme 
    INTEGER						  :: K,L

    OBJ=>LOCRT(ILOC)

    OBJ%OCC=0.D0
    IF ((((GUESS_POWER.EQV..TRUE.).AND.(TIME.EQ.TETA0)).OR.(ISOTHERMAL.EQV..TRUE.)).AND.((OBJ%LOCTYPE.NE.'NODE').OR.((OBJ%LOCTYPE.EQ.'NODE').AND.(ISOTHERMALNODE.EQV..TRUE.)))) THEN
        OBJ%T15=273.15D0+OBJ%FRAC*(OBJ%TINI-273.15D0)
    ENDIF
    !Calcul des conditions dans les couches haute et basse
    IF (OBJ%LOCTYPE.NE.'NODE') THEN
        OBJ%T15UP=OBJ%T15+OBJ%DTUP
        OBJ%T15DOWN=OBJ%T15+OBJ%DTDOWN
        CALL VARDED(OBJ%DP,OBJ%T15UP,OBJ%YK,OBJ%CP,OBJ%CV,OBJ%RHOUP,OBJ%RHOBUOYUP,OBJ%HRUP,OBJ%TROSEEUP)
        CALL VARDED(OBJ%DP,OBJ%T15DOWN,OBJ%YK,OBJ%CP,OBJ%CV,OBJ%RHODOWN,OBJ%RHOBUOYDOWN,OBJ%HRDOWN,OBJ%TROSEEDOWN)
    ENDIF

    !AJOUT ET SUPPRESSION DE L'EAU CONDENSEE :
    !Proposition d'un nouveau modele - XF et FD - 05/06/2014
    !
    !				mdot = hm x S x (rhoair x Yh2o - rhop x Ysat)
    ! avec
    ! hm : coefficient de transfert de masse local pris ici egal a 3.32D-3 (cf. rapport CSTB EN-CAPE 08.247 C-V0)
    ! S:  surface de la paroi lieu de la condensation
    ! Yh2o : humidite absolue de l'air ambiant
    ! rhoair : masse volumique de l'air ambiant
    ! Ysat : humidite absolue de l'air au contact de la paroi
    ! rhop : masse volumique de l'air au contact de la paroi
    !
    ! N.B. : on suppose ci-dessous que rhop=rhoair
    !       et que toute la surface des parois du local participe a la condensation
    !
    IF ((IDNH2O.NE.0)) THEN
        CALL YK_HUM_EVAL(OBJ%T15,100.D0,YH2OSAT,1)
        ! cette ligne est a utiliser si on veut prendre la temperature moyenne des parois :
        !CALL YK_HUM_EVAL(OBJ%TWMEAN,100.D0,YH2OSAT,1)
        IF (OBJ%YK(IDNH2O)>YH2OSAT) THEN
            IF ((OBJ%LOCTYPE=='NODE')) THEN
                !OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)=3.32D-3*(4.D0*DSQRT(OBJ%AREA)*OBJ%HEIGHT+2.D0*OBJ%AREA)*OBJ%RHO15*(OBJ%YK(IDNH2O)-YH2OSAT)
                OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)=(OBJ%YK(IDNH2O)-YH2OSAT)*OBJ%RA/3600.D0*1.204785775D0
            ELSE
                OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)=MAX(3.32D-3*(4.D0*DSQRT(OBJ%AREA)*OBJ%HEIGHT+2.D0*OBJ%AREA)*OBJ%RHO15*(OBJ%YK(IDNH2O)-YH2OSAT),(OBJ%YK(IDNH2O)-YH2OSAT)*OBJ%RHO15*(OBJ%VOLUME)/DTIME)
            ENDIF
            IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='NODE').AND.(MOISTURE_NODES==.FALSE.))) OBJ%MH2OLIQ=OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)*DTIME
            IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='ROOM').OR.(MOISTURE_NODES==.TRUE.))) OBJ%MH2OLIQ=OBJ%MH2OLIQ0+OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)*DTIME
            IF (ALL_SPEC_ARE_TRACE==.FALSE.)  OBJ%FLUXES%DMOUT15DT(OBJ%IDN)=OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)
        ELSE
            IF ((OBJ%LOCTYPE=='NODE')) THEN
                OBJ%FLUXES%DMKIN15DT(OBJ%IDN,IDNH2O)=MAX(0.D0,MIN(OBJ%MH2OLIQ0/DTIME,(YH2OSAT-OBJ%YK(IDNH2O))*OBJ%RA/3600.D0*1.204785775D0))
            ELSE
                OBJ%FLUXES%DMKIN15DT(OBJ%IDN,IDNH2O)=MAX(0.D0,MIN(OBJ%MH2OLIQ0/DTIME,3.32D-3*(4.D0*DSQRT(OBJ%AREA)*OBJ%HEIGHT+2.D0*OBJ%AREA)*OBJ%RHO15*(YH2OSAT-OBJ%YK(IDNH2O))))
            ENDIF
            IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='NODE').AND.(MOISTURE_NODES==.FALSE.))) OBJ%MH2OLIQ=0.D0
            IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='ROOM').OR.(MOISTURE_NODES==.TRUE.))) OBJ%MH2OLIQ=OBJ%MH2OLIQ0-OBJ%FLUXES%DMKIN15DT(OBJ%IDN,IDNH2O)*DTIME
            IF (ALL_SPEC_ARE_TRACE==.FALSE.)  OBJ%FLUXES%DMIN15DT(OBJ%IDN)=OBJ%FLUXES%DMKIN15DT(OBJ%IDN,IDNH2O)
        ENDIF

        OBJ%FLUXES%DE15DT(OBJ%IDN)=(OBJ%FLUXES%DMIN15DT(OBJ%IDN)-OBJ%FLUXES%DMOUT15DT(OBJ%IDN))*(SPECRT(IDNH2O)%CP*OBJ%T15)
        !print*, "calc ref", OBJ%IDN, "=", OBJ%FLUXES%DE15DT(OBJ%IDN) -298.146D0

        terme = LV*(OBJ%FLUXES%DMIN15DT(OBJ%IDN)-OBJ%FLUXES%DMOUT15DT(OBJ%IDN))
        IF (terme<=0.D0) THEN
            !ici on ajoute la chaleur latente de condensation a la phase gazeuse
            OBJ%FLUXES%DE15DT(OBJ%IDN)=OBJ%FLUXES%DE15DT(OBJ%IDN) - terme

            !print*, "ajust ref", OBJ%IDN, "=", OBJ%FLUXES%DE15DT(OBJ%IDN) - 1985.726D0
        ELSE
            !la on soustrait la chaleur latente de vaporisation aux parois sous la forme d'un flux radiatif
            OBJ%HRAD=OBJ%HRAD - terme
        ENDIF
    ENDIF

    !calcul des pertes de charge dans les jonctions
    IF ((OBJ%LOCTYPE=='NODE').AND.(SIZE(OBJ%LATERALIDNS).GT.0).AND.(SIZE(OBJ%STRAIGHTIDNS).GE.1)) THEN
        CALL EVAL_JUNCTION_SINGU(OBJ)
    ENDIF

    END SUBROUTINE FLUX_ME_LOC




    !SAM : meme chose que ci-dessu mais sans copie
    SUBROUTINE FLUX_ME_LOC_FAST(ILOC, F)

        INTEGER,INTENT(IN)			  :: ILOC
        TYPE(FLUX_ME),INTENT(INOUT)				:: F
        TYPE(TYPE_LOC),POINTER		  :: OBJ
        DOUBLE PRECISION			  :: YH2OSAT,aa,coef,Q1,D1,Q1conf,D1conf,Qconf,Dconf,rhoconf,Q1sep,D1sep,Qsep,Dsep,rhosep
        INTEGER						  :: K,L

        double precision                :: terme_DMKOUT15DT, terme_DMKIN15DT, terme

    
        OBJ=>LOCRT(ILOC)
    
        OBJ%OCC=0.D0
        IF ((((GUESS_POWER.EQV..TRUE.).AND.(TIME.EQ.TETA0)).OR.(ISOTHERMAL.EQV..TRUE.)).AND.((OBJ%LOCTYPE.NE.'NODE').OR.((OBJ%LOCTYPE.EQ.'NODE').AND.(ISOTHERMALNODE.EQV..TRUE.)))) THEN
            OBJ%T15=273.15D0+OBJ%FRAC*(OBJ%TINI-273.15D0)
        ENDIF
        !Calcul des conditions dans les couches haute et basse
        IF (OBJ%LOCTYPE.NE.'NODE') THEN
            OBJ%T15UP=OBJ%T15+OBJ%DTUP
            OBJ%T15DOWN=OBJ%T15+OBJ%DTDOWN
            CALL VARDED(OBJ%DP,OBJ%T15UP,OBJ%YK,OBJ%CP,OBJ%CV,OBJ%RHOUP,OBJ%RHOBUOYUP,OBJ%HRUP,OBJ%TROSEEUP)
            CALL VARDED(OBJ%DP,OBJ%T15DOWN,OBJ%YK,OBJ%CP,OBJ%CV,OBJ%RHODOWN,OBJ%RHOBUOYDOWN,OBJ%HRDOWN,OBJ%TROSEEDOWN)
        ENDIF
    
        !AJOUT ET SUPPRESSION DE L'EAU CONDENSEE :
        !Proposition d'un nouveau modele - XF et FD - 05/06/2014
        !
        !				mdot = hm x S x (rhoair x Yh2o - rhop x Ysat)
        ! avec
        ! hm : coefficient de transfert de masse local pris ici egal a 3.32D-3 (cf. rapport CSTB EN-CAPE 08.247 C-V0)
        ! S:  surface de la paroi lieu de la condensation
        ! Yh2o : humidite absolue de l'air ambiant
        ! rhoair : masse volumique de l'air ambiant
        ! Ysat : humidite absolue de l'air au contact de la paroi
        ! rhop : masse volumique de l'air au contact de la paroi
        !
        ! N.B. : on suppose ci-dessous que rhop=rhoair
        !       et que toute la surface des parois du local participe a la condensation
        !
        IF ((IDNH2O.NE.0)) THEN
            CALL YK_HUM_EVAL(OBJ%T15,100.D0,YH2OSAT,1)
            ! cette ligne est a utiliser si on veut prendre la temperature moyenne des parois :
            !CALL YK_HUM_EVAL(OBJ%TWMEAN,100.D0,YH2OSAT,1)

            IF (OBJ%YK(IDNH2O)>YH2OSAT) THEN
                !RAZ des parties non traitees du cas ELSE
                OBJ%FLUXES%DMKIN15DT(OBJ%IDN,IDNH2O)    = 0.0D0
                OBJ%FLUXES%DMIN15DT(OBJ%IDN)            = 0.0D0
                terme_DMKIN15DT                         = 0.0D0

                IF ((OBJ%LOCTYPE=='NODE')) THEN
                    !OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)=3.32D-3*(4.D0*DSQRT(OBJ%AREA)*OBJ%HEIGHT+2.D0*OBJ%AREA)*OBJ%RHO15*(OBJ%YK(IDNH2O)-YH2OSAT)
                    terme_DMKOUT15DT = (OBJ%YK(IDNH2O)-YH2OSAT)*OBJ%RA/3600.D0*1.204785775D0
                ELSE
                    terme_DMKOUT15DT = MAX(3.32D-3*(4.D0*DSQRT(OBJ%AREA)*OBJ%HEIGHT+2.D0*OBJ%AREA)*OBJ%RHO15*(OBJ%YK(IDNH2O)-YH2OSAT),(OBJ%YK(IDNH2O)-YH2OSAT)*OBJ%RHO15*(OBJ%VOLUME)/DTIME)
                ENDIF
                OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)   = terme_DMKOUT15DT
                         F%DMKOUT15DT(OBJ%IDN,IDNH2O)   = F%DMKOUT15DT(OBJ%IDN,IDNH2O) + terme_DMKOUT15DT

                IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='NODE').AND.(MOISTURE_NODES==.FALSE.)))  OBJ%MH2OLIQ = terme_DMKOUT15DT * DTIME
                IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='ROOM').OR.(MOISTURE_NODES==.TRUE.)))    OBJ%MH2OLIQ = OBJ%MH2OLIQ0 + terme_DMKOUT15DT * DTIME
                
                IF (ALL_SPEC_ARE_TRACE==.FALSE.) THEN
                    OBJ%FLUXES%DMOUT15DT(OBJ%IDN)           = terme_DMKOUT15DT
                            F%DMOUT15DT(OBJ%IDN)           = F%DMOUT15DT(OBJ%IDN) + terme_DMKOUT15DT
                ENDIF

            ELSE
                !RAZ des parties non traitees du cas THEN
                OBJ%FLUXES%DMKOUT15DT(OBJ%IDN,IDNH2O)   = 0.0D0
                OBJ%FLUXES%DMOUT15DT(OBJ%IDN)           = 0.0D0
                terme_DMKOUT15DT                        = 0.0D0

                IF ((OBJ%LOCTYPE=='NODE')) THEN
                    terme_DMKIN15DT=MAX(0.D0,MIN(OBJ%MH2OLIQ0/DTIME,(YH2OSAT-OBJ%YK(IDNH2O))*OBJ%RA/3600.D0*1.204785775D0))
                ELSE
                    terme_DMKIN15DT=MAX(0.D0,MIN(OBJ%MH2OLIQ0/DTIME,3.32D-3*(4.D0*DSQRT(OBJ%AREA)*OBJ%HEIGHT+2.D0*OBJ%AREA)*OBJ%RHO15*(YH2OSAT-OBJ%YK(IDNH2O))))
                ENDIF
                OBJ%FLUXES%DMKIN15DT(OBJ%IDN,IDNH2O)    = terme_DMKIN15DT
                         F%DMKIN15DT(OBJ%IDN,IDNH2O)    = F%DMKIN15DT(OBJ%IDN,IDNH2O) + terme_DMKIN15DT

                IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='NODE').AND.(MOISTURE_NODES==.FALSE.)))  OBJ%MH2OLIQ = 0.D0
                IF ((TIME.NE.TETA0).AND.((OBJ%LOCTYPE=='ROOM').OR.(MOISTURE_NODES==.TRUE.)))    OBJ%MH2OLIQ = OBJ%MH2OLIQ0 - terme_DMKIN15DT * DTIME

                IF (ALL_SPEC_ARE_TRACE==.FALSE.) THEN
                    OBJ%FLUXES%DMIN15DT(OBJ%IDN)            = terme_DMKIN15DT
                             F%DMIN15DT(OBJ%IDN)            = F%DMIN15DT(OBJ%IDN) + terme_DMKIN15DT
                ENDIF
            ENDIF

            terme = (terme_DMKIN15DT - terme_DMKOUT15DT) * (SPECRT(IDNH2O)%CP*OBJ%T15)
            OBJ%FLUXES%DE15DT(OBJ%IDN)  = terme
                     !F%DE15DT(OBJ%IDN)  = F%DE15DT(OBJ%IDN) + terme
            !print*, "calc fast", OBJ%IDN, "=", OBJ%FLUXES%DE15DT(OBJ%IDN)        -298.146D0

            terme = LV*(terme_DMKIN15DT-terme_DMKOUT15DT)
            IF (terme <= 0.D0) THEN
                !ici on ajoute la chaleur latente de condensation a la phase gazeuse
                OBJ%FLUXES%DE15DT(OBJ%IDN)  = OBJ%FLUXES%DE15DT(OBJ%IDN) - terme
                         !F%DE15DT(OBJ%IDN)  = F%DE15DT(OBJ%IDN) - terme
                         !F%DE15DT(OBJ%IDN)  = F%DE15DT(OBJ%IDN) + OBJ%FLUXES%DE15DT(OBJ%IDN)
                !print*, "ajust fast", OBJ%IDN, "=", OBJ%FLUXES%DE15DT(OBJ%IDN)        - 1985.726D0
            ELSE
                !la on soustrait la chaleur latente de vaporisation aux parois sous la forme d'un flux radiatif
                OBJ%HRAD = OBJ%HRAD - terme
            ENDIF

            !version plus simple et conforme a la version pas fast
            F%DE15DT(OBJ%IDN)  = F%DE15DT(OBJ%IDN) + OBJ%FLUXES%DE15DT(OBJ%IDN)

        ENDIF
    
        !calcul des pertes de charge dans les jonctions
        IF ((OBJ%LOCTYPE=='NODE').AND.(SIZE(OBJ%LATERALIDNS).GT.0).AND.(SIZE(OBJ%STRAIGHTIDNS).GE.1)) THEN
            CALL EVAL_JUNCTION_SINGU(OBJ)
        ENDIF
    
    END SUBROUTINE FLUX_ME_LOC_FAST

























    SUBROUTINE FLUX_ME_LOCRT(F)

    TYPE(FLUX_ME),INTENT(INOUT)				:: F
    INTEGER									:: I

    BRANCHERT(:)%JUNCTION_SINGU=0.D0
    LOCRT%HRAD=0.D0
    DO I=1,SIZE(LOCRT)
        CALL INIT_FLUX(LOCRT(I)%FLUXES,N_LOC,N_SPEC,1)
        CALL FLUX_ME_LOC(I)
        !CALL ADD_FLUX(F,F,LOCRT(I)%FLUXES)
        CALL INCREMENTE_FLUX(F,LOCRT(I)%FLUXES)
    ENDDO

    END SUBROUTINE FLUX_ME_LOCRT

    SUBROUTINE INIT_FLUX_LOCRT(N_LOC,N_SPEC,FLAG)

    INTEGER,INTENT(IN)							 :: N_LOC,N_SPEC,FLAG
    INTEGER									 :: I

    DO I=1,SIZE(LOCRT)
        CALL INIT_FLUX(LOCRT(I)%FLUXES,N_LOC,N_SPEC,FLAG)
    ENDDO

    END SUBROUTINE INIT_FLUX_LOCRT


    !SAM : meme chose que ci-dessus mais sans init ni copie
    SUBROUTINE FLUX_ME_LOCRT_FAST(F)

        TYPE(FLUX_ME),INTENT(INOUT)				:: F
        INTEGER									:: I
  
        BRANCHERT(:)%JUNCTION_SINGU=0.D0
        LOCRT%HRAD=0.D0
        DO I=1,SIZE(LOCRT)
            CALL FLUX_ME_LOC_FAST(I, F)
        ENDDO
   
    END SUBROUTINE FLUX_ME_LOCRT_FAST



    SUBROUTINE EVAL_JUNCTION_SINGU(OBJ)

    TYPE(TYPE_LOC),INTENT(IN)			:: OBJ
    DOUBLE PRECISION					:: QPCONF,FPCONF,RHOPCONF
    DOUBLE PRECISION					:: QPSEP,FPSEP,RHOPSEP
    DOUBLE PRECISION					:: QL,FL,RHOL,ALPHA,COSALPHA
    DOUBLE PRECISION					:: QLCONF,FLCONF,RHOLCONF,COSALPHACONF,FRCONF
    DOUBLE PRECISION					:: QLSEP,FLSEP,RHOLSEP,COSALPHASEP,FRSEP
    INTEGER								:: L,K,NCONF,NSEP

    DOUBLE PRECISION					:: ACOEF,KCOEF

    QPCONF=0.D0;FPCONF=0.D0
    QPSEP=0.D0;FPSEP=0.D0
    QL=0.D0;FL=0.D0
    QLCONF=0.D0;FLCONF=0.D0;RHOLCONF=0.D0;COSALPHACONF=0.D0;FRCONF=0.D0
    QLSEP=0.D0;FLSEP=0.D0;RHOLSEP=0.D0;COSALPHASEP=0.D0;FRSEP=0.D0

    NCONF=0.D0;NSEP=0.D0

    DO L=1,SIZE(OBJ%STRAIGHTIDNS(:))
        IF (BRANCHERT(OBJ%STRAIGHTIDNS(L))%FLUXES%DMOUT15DT(OBJ%IDN)>=QPCONF) THEN
            RHOPCONF=BRANCHERT(OBJ%STRAIGHTIDNS(L))%RHO15AM
            QPCONF=BRANCHERT(OBJ%STRAIGHTIDNS(L))%FLUXES%DMOUT15DT(OBJ%IDN)/RHOPCONF
            FPCONF=BRANCHERT(OBJ%STRAIGHTIDNS(L))%SECTION
        ENDIF
        IF (BRANCHERT(OBJ%STRAIGHTIDNS(L))%FLUXES%DMIN15DT(OBJ%IDN)>=QPSEP) THEN
            RHOPSEP=BRANCHERT(OBJ%STRAIGHTIDNS(L))%RHO15AM
            QPSEP=BRANCHERT(OBJ%STRAIGHTIDNS(L))%FLUXES%DMIN15DT(OBJ%IDN)/RHOPSEP
            FPSEP=BRANCHERT(OBJ%STRAIGHTIDNS(L))%SECTION
        ENDIF
    ENDDO
    FRCONF=FPSEP
    FRSEP=FPCONF

    DO K=1,SIZE(OBJ%LATERALIDNS(:))

        IF ((BRANCHERT(OBJ%LATERALIDNS(K))%FLUXES%DMIN15DT(OBJ%IDN)>0.D0).AND.(QPCONF>0.D0)) THEN !confluence
            RHOL=BRANCHERT(OBJ%LATERALIDNS(K))%RHO15AM
            QL=BRANCHERT(OBJ%LATERALIDNS(K))%FLUXES%DMIN15DT(OBJ%IDN)/RHOL
            FL=BRANCHERT(OBJ%LATERALIDNS(K))%SECTION
            IF (OBJ%ID==BRANCHERT(OBJ%LATERALIDNS(K))%LOCIDS(1)) THEN
                ALPHA=BRANCHERT(OBJ%LATERALIDNS(K))%ALPHA(1)
            ELSE
                ALPHA=BRANCHERT(OBJ%LATERALIDNS(K))%ALPHA(2)
            ENDIF
            COSALPHA=DCOSD(ALPHA)
            QLCONF=QLCONF+QL
            FLCONF=FLCONF+FL
            RHOLCONF=RHOLCONF+RHOL
            COSALPHACONF=COSALPHACONF+COSALPHA

            NCONF=NCONF+1

            IF (BRANCHERT(OBJ%LATERALIDNS(K))%BRANCHTYPE=='TUYAU_NFE51766') THEN
                BRANCHERT(OBJ%LATERALIDNS(K))%JUNCTION_SINGU=BRANCHERT(OBJ%LATERALIDNS(K))%JUNCTION_SINGU&
                    +MAX(-5.D0,MIN(10.D0,(1.1D-4*3600.D0*MAX(0.D0,(QPCONF-QL))+80.D-4)*3600.D0*QL/(0.5D0*RHOPCONF*(QPCONF/FPCONF)**2.D0)))*(0.5D0*RHOPCONF*(QPCONF/FPCONF)**2.D0)
            ELSE
                CALL GET_A_K_COEFS('LCONF',FL,FRCONF,FPCONF,ALPHA,QL,QPCONF,ACOEF,KCOEF)
                BRANCHERT(OBJ%LATERALIDNS(K))%JUNCTION_SINGU=BRANCHERT(OBJ%LATERALIDNS(K))%JUNCTION_SINGU&
                    +MAX(-5.D0,MIN(10.D0,ACOEF*(1.D0+(QL/QPCONF*FPCONF/FL)**2.D0-2.D0*(FPCONF/FRCONF)*(1.D0-QL/QPCONF)**2.D0-2.D0*COSALPHA*(FPCONF/FL)*(QL/QPCONF)**2.D0)+KCOEF))&
                    *0.5D0*RHOPCONF*(QPCONF/FPCONF)**2.D0
            ENDIF
        ELSEIF ((BRANCHERT(OBJ%LATERALIDNS(K))%FLUXES%DMOUT15DT(OBJ%IDN)>0.D0).AND.(QPSEP>0.D0)) THEN	!separation de courant

            RHOL=BRANCHERT(OBJ%LATERALIDNS(K))%RHO15AM
            QL=BRANCHERT(OBJ%LATERALIDNS(K))%FLUXES%DMOUT15DT(OBJ%IDN)/RHOL
            FL=BRANCHERT(OBJ%LATERALIDNS(K))%SECTION

            IF (OBJ%ID==BRANCHERT(OBJ%LATERALIDNS(K))%LOCIDS(1)) THEN
                ALPHA=BRANCHERT(OBJ%LATERALIDNS(K))%ALPHA(1)
            ELSE
                ALPHA=BRANCHERT(OBJ%LATERALIDNS(K))%ALPHA(2)
            ENDIF
            COSALPHA=DCOSD(ALPHA)

            QLSEP=QLSEP+QL
            FLSEP=FLSEP+FL
            RHOLSEP=RHOLSEP+RHOL
            COSALPHASEP=COSALPHASEP+COSALPHA

            NSEP=NSEP+1

            CALL GET_A_K_COEFS('LSEP',FL,FRSEP,FPSEP,ALPHA,QL,QPSEP,ACOEF,KCOEF)
            BRANCHERT(OBJ%LATERALIDNS(K))%JUNCTION_SINGU=BRANCHERT(OBJ%LATERALIDNS(K))%JUNCTION_SINGU&
                +MAX(-5.D0,MIN(10.D0,ACOEF*(1.D0+(QL/QPSEP*FL/FPSEP)**2.D0-2.D0*(QL/QPSEP)*(FPSEP/FL)*COSALPHA)-KCOEF*(QL/QPSEP*FPSEP/FL)**2.D0))&
                *0.5D0*RHOPSEP*(QPSEP/FPSEP)**2.D0
        ENDIF
    ENDDO

    IF (NCONF.NE.0) THEN
        RHOLCONF=RHOLCONF/NCONF
        COSALPHACONF=COSALPHACONF/NCONF
    ENDIF
    IF (NSEP.NE.0) THEN
        RHOLSEP=RHOLSEP/NSEP
        COSALPHASEP=COSALPHASEP/NSEP
    ENDIF

    DO L=1,SIZE(OBJ%STRAIGHTIDNS(:))
        IF ((BRANCHERT(OBJ%STRAIGHTIDNS(L))%FLUXES%DMIN15DT(OBJ%IDN)>0.D0).AND.(QLCONF>0.D0)) THEN !confluence
            IF (BRANCHERT(OBJ%STRAIGHTIDNS(L))%BRANCHTYPE=='TUYAU_NFE51766') THEN
                BRANCHERT(OBJ%STRAIGHTIDNS(L))%JUNCTION_SINGU=BRANCHERT(OBJ%STRAIGHTIDNS(L))%JUNCTION_SINGU&
                    +MAX(-5.D0,MIN(10.D0,(1.1D-4*3600.D0*MAX(0.D0,(QPCONF-QLCONF))-80.D-4)*3600.D0*QLCONF/(0.5D0*RHOPCONF*(QPCONF/FPCONF)**2.D0)))*(0.5D0*RHOPCONF*(QPCONF/FPCONF)**2.D0)
            ELSE
                CALL GET_A_K_COEFS('RCONF',FLCONF,FRCONF,FPCONF,DACOSD(COSALPHACONF),QLCONF,QPCONF,ACOEF,KCOEF)
                BRANCHERT(OBJ%STRAIGHTIDNS(L))%JUNCTION_SINGU=BRANCHERT(OBJ%STRAIGHTIDNS(L))%JUNCTION_SINGU&
                    +MAX(-5.D0,MIN(10.D0,1.D0+((FPCONF/FRCONF)*(1.D0-QLCONF/QPCONF))**2.D0-2.D0*(FPCONF/FRCONF)*(1.D0-QLCONF/QPCONF)**2.D0-2.D0*COSALPHACONF*(FPCONF/FLCONF)*(QLCONF/QPCONF)**2.D0+KCOEF))&
                    *0.5D0*RHOPCONF*(QPCONF/FPCONF)**2.D0
            ENDIF
        ELSEIF ((BRANCHERT(OBJ%STRAIGHTIDNS(L))%FLUXES%DMOUT15DT(OBJ%IDN)>0.D0).AND.(QLSEP>0.D0)) THEN !separation
            CALL GET_A_K_COEFS('RSEP',FLSEP,FRSEP,FPSEP,DACOSD(COSALPHASEP),QPSEP-QLSEP,QPSEP,ACOEF,KCOEF)
            BRANCHERT(OBJ%STRAIGHTIDNS(L))%JUNCTION_SINGU=BRANCHERT(OBJ%STRAIGHTIDNS(L))%JUNCTION_SINGU&
                +KCOEF*0.5D0*RHOPSEP*(QPSEP/FPSEP)**2.D0
        ENDIF
    ENDDO


    END SUBROUTINE EVAL_JUNCTION_SINGU

    SUBROUTINE GET_A_K_COEFS(BTYPE,FL,FR,FP,ALPHA,QB,QP,A,K)

    CHARACTER(*),INTENT(IN)						:: BTYPE
    DOUBLE PRECISION,INTENT(IN)					:: FL,FR,FP,ALPHA,QB,QP
    DOUBLE PRECISION,INTENT(OUT)				::	A,K
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE	:: ADATA,KDATA
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: XDATA,YDATA
    INTEGER										:: NX,NY

    A=0.D0
    K=0.D0

    SELECT CASE (BTYPE)

    CASE('LCONF')

        IF (FL+FR.GT.FP) THEN
            K=0.D0
            NX=2;NY=7
            ALLOCATE(ADATA(NX,NY))
            ALLOCATE(XDATA(NX))
            ALLOCATE(YDATA(NY))
            XDATA=(/60.D0,90.D0/)
            YDATA=(/0.D0,0.2D0,0.3D0,0.4D0,0.6D0,0.8D0,1.0D0/)
            ADATA(1,:)=1.D0
            ADATA(2,:)=(/1.D0,1.D0,0.75D0,0.75D0,0.7D0,0.65D0,0.60D0/)

            CALL BILINEAR_INTERP(NX,NY,ALPHA,FL/FP,XDATA,YDATA,ADATA,A)

            DEALLOCATE(ADATA)
            DEALLOCATE(XDATA)
            DEALLOCATE(YDATA)

        ELSE
            A=1.D0
            NX=4;NY=5
            ALLOCATE(KDATA(NX,NY))
            ALLOCATE(XDATA(NX))
            ALLOCATE(YDATA(NY))
            XDATA=(/0.1D0,0.2D0,0.33D0,0.5D0/)
            YDATA=(/15.D0,30D0,45D0,60.D0,90.D0/)
            KDATA(1,:)=0.D0
            KDATA(2,:)=0.D0;KDATA(2,5)=0.1D0
            KDATA(3,:)=0.D0;KDATA(3,5)=0.20D0
            KDATA(4,:)=(/0.D0,0.D0,0.D0,0.1D0,0.25D0/)

            CALL BILINEAR_INTERP(NX,NY,FL/FP,ALPHA,XDATA,YDATA,KDATA,K)

            DEALLOCATE(KDATA)
            DEALLOCATE(XDATA)
            DEALLOCATE(YDATA)

        ENDIF

    CASE('RCONF')

        IF (FL+FR.GT.FP) THEN
            A=0.D0 !non utilise par la suite
            K=0.D0
        ELSE
            A=0.D0 !non utilise par la suite
            NX=4;NY=5
            ALLOCATE(KDATA(NX,NY))
            ALLOCATE(XDATA(NX))
            ALLOCATE(YDATA(NY))
            XDATA=(/0.1D0,0.2D0,0.33D0,0.5D0/)
            YDATA=(/15.D0,30.D0,45.D0,60.D0,90.D0/)
            KDATA(1,:)=0.D0;KDATA(1,3)=0.05D0
            KDATA(2,:)=0.D0;KDATA(2,3)=0.14D0
            KDATA(3,:)=(/0.14D0,0.17D0,0.14D0,0.10D0,0.D0/)
            KDATA(4,:)=(/0.40D0,0.35D0,0.30D0,0.25D0,0.D0/)

            CALL BILINEAR_INTERP(NX,NY,FL/FP,ALPHA,XDATA,YDATA,KDATA,K)

            DEALLOCATE(KDATA)
            DEALLOCATE(XDATA)
            DEALLOCATE(YDATA)

        ENDIF

    CASE('LSEP')

        IF (FL+FR.GT.FP) THEN
            K=0.D0
            IF (QB/FL.LE.0.8D0*QP/FP) THEN
                A=1.D0
            ELSE
                A=0.9D0
            ENDIF

        ELSE
            A=1.D0
            NX=1;NY=5
            ALLOCATE(KDATA(NX,NY))
            ALLOCATE(XDATA(NX))
            ALLOCATE(YDATA(NY))
            XDATA=(/1.D0/)
            YDATA=(/15.D0,30.D0,45.D0,60.D0,90.D0/)
            KDATA(1,:)=(/0.04D0,0.16D0,0.36D0,0.64D0,1.D0/)

            CALL BILINEAR_INTERP(NX,NY,1.D0,ALPHA,XDATA,YDATA,KDATA,K)

            DEALLOCATE(KDATA)
            DEALLOCATE(XDATA)
            DEALLOCATE(YDATA)

        ENDIF


    CASE('RSEP')
        A=0.D0 !non utilise par la suite
        IF (FL+FR.GT.FP) THEN
            IF (QB/FR.LE.QP/FP) THEN
                K=0.4D0*(1.D0-QB*FP/(QP*FR))**2.D0
            ELSE
                NX=1;NY=9
                ALLOCATE(KDATA(NX,NY))
                ALLOCATE(XDATA(NX))
                ALLOCATE(YDATA(NY))
                XDATA=(/1.D0/)
                YDATA=(/0.D0,0.1D0,0.2D0,0.3D0,0.4D0,0.5D0,0.6D0,0.8D0,1.0D0/)
                KDATA(1,:)=(/0.40D0,0.32D0,0.26D0,0.20D0,0.15D0,0.10D0,0.06D0,0.02D0,0.00D0/)

                CALL BILINEAR_INTERP(NX,NY,1.D0,QB*FP/(QP*FR),XDATA,YDATA,KDATA,K)

                DEALLOCATE(KDATA)
                DEALLOCATE(XDATA)
                DEALLOCATE(YDATA)
            ENDIF
        ELSE
            IF (ALPHA<=60.D0) THEN
                NX=1;NY=14
                ALLOCATE(KDATA(NX,NY))
                ALLOCATE(XDATA(NX))
                ALLOCATE(YDATA(NY))
                XDATA=(/1.D0/)
                YDATA=(/0.D0,0.1D0,0.2D0,0.3D0,0.4D0,0.5D0,0.6D0,0.8D0,1.0D0,1.2D0,1.4D0,1.6D0,1.8D0,2.0D0/)
                KDATA(1,:)=(/1.00D0,0.81D0,0.64D0,0.50D0,0.36D0,0.25D0,0.16D0,0.04D0,0.00D0,0.07D0,0.39D0,0.90D0,1.78D0,3.20D0/)

                CALL BILINEAR_INTERP(NX,NY,1.D0,QB*FP/(QP*FR),XDATA,YDATA,KDATA,K)

                DEALLOCATE(KDATA)
                DEALLOCATE(XDATA)
                DEALLOCATE(YDATA)
            ELSE
                NX=5;NY=14
                ALLOCATE(KDATA(NX,NY))
                ALLOCATE(XDATA(NX))
                ALLOCATE(YDATA(NY))
                XDATA=(/0.4D0,0.5D0,0.6D0,0.7D0,0.8D0/)
                YDATA=(/0.D0,0.1D0,0.2D0,0.3D0,0.4D0,0.5D0,0.6D0,0.8D0,1.0D0,1.2D0,1.4D0,1.6D0,1.8D0,2.0D0/)
                KDATA(1,:)=(/1.00D0,0.81D0,0.64D0,0.50D0,0.36D0,0.25D0,0.16D0,0.04D0,0.00D0,0.07D0,0.39D0,0.90D0,1.78D0,3.20D0/)
                KDATA(2,:)=(/1.00D0,0.81D0,0.64D0,0.52D0,0.40D0,0.30D0,0.23D0,0.16D0,0.20D0,0.36D0,0.78D0,1.36D0,2.43D0,4.00D0/)
                KDATA(3,:)=(/1.00D0,0.81D0,0.64D0,0.52D0,0.38D0,0.28D0,0.20D0,0.12D0,0.10D0,0.21D0,0.59D0,1.15D0,1.15D0,1.15D0/)
                KDATA(4,:)=(/1.00D0,0.81D0,0.64D0,0.50D0,0.37D0,0.26D0,0.18D0,0.07D0,0.05D0,0.14D0,0.49D0,0.49D0,0.49D0,0.49D0/)
                KDATA(5,:)=(/1.00D0,0.81D0,0.64D0,0.50D0,0.36D0,0.25D0,0.16D0,0.04D0,0.00D0,0.07D0,0.07D0,0.07D0,0.07D0,0.07D0/)

                CALL BILINEAR_INTERP(NX,NY,FR/FP,QB*FP/(QP*FR),XDATA,YDATA,KDATA,K)

                DEALLOCATE(KDATA)
                DEALLOCATE(XDATA)
                DEALLOCATE(YDATA)
            ENDIF
        ENDIF

    END SELECT


    END SUBROUTINE GET_A_K_COEFS




    SUBROUTINE UPDATE_LOC(ILOC,FLAG)

    INTEGER,INTENT(IN)					:: ILOC,FLAG
    TYPE(TYPE_LOC),POINTER				:: OBJ

    OBJ=>LOCRT(ILOC)

    IF (FLAG==1) OBJ%MH2OLIQ0_OLD=OBJ%MH2OLIQ0
    OBJ%MH2OLIQ0=OBJ%MH2OLIQ


    END SUBROUTINE UPDATE_LOC

    SUBROUTINE UPDATE_LOCRT(FLAG)

    INTEGER,INTENT(IN)					:: FLAG
    INTEGER								:: I

    DO I=1,SIZE(LOCRT)
        CALL UPDATE_LOC(I,FLAG)
    ENDDO

    END SUBROUTINE UPDATE_LOCRT

    SUBROUTINE REWIND_LOC(ILOC,FLAG)

    INTEGER,INTENT(IN)					:: ILOC,FLAG
    TYPE(TYPE_LOC),POINTER				:: OBJ

    OBJ=>LOCRT(ILOC)

    OBJ%MH2OLIQ0=OBJ%MH2OLIQ0_OLD
    OBJ%MH2OLIQ=OBJ%MH2OLIQ0

    END SUBROUTINE REWIND_LOC

    SUBROUTINE REWIND_LOCRT(FLAG)

    INTEGER,INTENT(IN)					:: FLAG
    INTEGER								:: I

    DO I=1,SIZE(LOCRT)
        CALL REWIND_LOC(I,FLAG)
    ENDDO

    END SUBROUTINE REWIND_LOCRT

    END MODULE PROC_LOC_MODULE
