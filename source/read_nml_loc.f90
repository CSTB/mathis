    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE READ_NML_LOC_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    CHARACTER(100)					:: ID,LOCTYPE,TCTRLID,PZONEID
    DOUBLE PRECISION				:: ALT,AREA,HEIGHT,TUP,TDOWN,ZINT
    DOUBLE PRECISION				:: TINI

    NAMELIST /LOC/ ID,LOCTYPE,ALT,TINI,AREA,HEIGHT,TUP,TDOWN,ZINT,TCTRLID,PZONEID

    CONTAINS

    SUBROUTINE DEFAULT_NML_LOC

    TYPE(TYPE_LOC)		:: OBJ

    ID=OBJ%ID
    LOCTYPE=OBJ%LOCTYPE
    AREA=OBJ%AREA
    HEIGHT=OBJ%HEIGHT
    ALT=OBJ%ALT
    TINI=OBJ%TINI
    TUP=OBJ%TUP
    TDOWN=OBJ%TDOWN
    ZINT=OBJ%ZINT
    TCTRLID=OBJ%TCTRLID
    PZONEID=OBJ%PZONEID

    END SUBROUTINE DEFAULT_NML_LOC

    SUBROUTINE PRINT_DEFAULT_NML_LOC(LUOUT)
    
    INTEGER,INTENT(IN)			:: LUOUT
    
    CALL DEFAULT_NML_LOC
    WRITE(LUOUT,NML=LOC,DELIM='APOSTROPHE ')
    
    END SUBROUTINE PRINT_DEFAULT_NML_LOC
    
    SUBROUTINE SET_NML_LOC(OBJ)

    TYPE(TYPE_LOC),INTENT(INOUT)	:: OBJ

    OBJ%ID=ID
    OBJ%LOCTYPE=LOCTYPE
    OBJ%AREA=AREA
    OBJ%HEIGHT=HEIGHT
    OBJ%ALT=ALT
    OBJ%TINI=TINI
    OBJ%TUP=TUP
    OBJ%TDOWN=TDOWN
    OBJ%ZINT=ZINT
    OBJ%TCTRLID=TCTRLID
    OBJ%PZONEID=PZONEID

    END SUBROUTINE SET_NML_LOC

    SUBROUTINE COUNT_NML_LOC(LU1,LUOUT,N_OBJ)

    INTEGER,INTENT(IN)			:: LU1,LUOUT
    INTEGER,INTENT(OUT)			:: N_OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    N_OBJ=0
    REWIND(LU1)
    COUNT_OBJ_LOOP: DO
        CALL CHECKREAD('LOC',LU1,IOS)
        IF (IOS==0) EXIT COUNT_OBJ_LOOP
        READ(LU1,NML=LOC,END=209,ERR=210,IOSTAT=IOS)
        N_OBJ=N_OBJ+1
210     IF (IOS>0) THEN
            WRITE(LUOUT,NML=LOC)
            WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &LOC line number ',N_OBJ+1,': check parameters names and values'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDDO COUNT_OBJ_LOOP
209 REWIND(LU1)

    END SUBROUTINE COUNT_NML_LOC


    SUBROUTINE READ_NML_LOC(LU1,LUOUT,OBJRT)

    INTEGER,INTENT(IN)								:: LU1,LUOUT
    TYPE(TYPE_LOC),DIMENSION(:),INTENT(INOUT)		:: OBJRT
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    REWIND(LU1)
    SET_OBJ_LOOP: DO I=1,SIZE(OBJRT)
        CALL CHECKREAD('LOC',LU1,IOS)
        IF (IOS==0) EXIT SET_OBJ_LOOP
        CALL DEFAULT_NML_LOC
        READ(LU1,LOC)
        CALL SET_NML_LOC(OBJRT(I))
    ENDDO SET_OBJ_LOOP
    REWIND(LU1)

    END SUBROUTINE READ_NML_LOC

    END MODULE READ_NML_LOC_MODULE
