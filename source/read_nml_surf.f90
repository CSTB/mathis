    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE READ_NML_SURF_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    CHARACTER(100)							:: ID
    CHARACTER(100),DIMENSION(NSLABMAX)		:: MATIDS
    INTEGER									:: NSLAB,NODEPERSLAB
    INTEGER,DIMENSION(NSLABMAX)		        :: NODES
    DOUBLE PRECISION,DIMENSION(NSLABMAX)	:: E
    DOUBLE PRECISION,DIMENSION(NSLABMAX-1)	:: HCCT,HMCT
    DOUBLE PRECISION                        :: MESH_RATIO
    DOUBLE PRECISION,DIMENSION(NSLABMAX)    :: HRINI
    DOUBLE PRECISION,DIMENSION(NSLABMAX)    :: TINI

    NAMELIST /SURF/ ID,NSLAB,MATIDS,E,NODEPERSLAB,NODES,HCCT,HMCT,MESH_RATIO,HRINI,TINI

    CONTAINS

    SUBROUTINE DEFAULT_NML_SURF

    TYPE(TYPE_SURF)		:: OBJ

    ID=OBJ%ID
    MATIDS=OBJ%MATIDS
    NSLAB=OBJ%NSLAB
    E=OBJ%E
    NODEPERSLAB=OBJ%NODEPERSLAB
    NODES=OBJ%NODES
    HCCT=OBJ%HCCT
    HMCT=OBJ%HMCT
    MESH_RATIO=OBJ%MESH_RATIO
    HRINI=OBJ%HRINI
    TINI=OBJ%TINI

    END SUBROUTINE DEFAULT_NML_SURF
    
    SUBROUTINE PRINT_DEFAULT_NML_SURF(LUOUT)
    
    INTEGER,INTENT(IN)			:: LUOUT
    
    CALL DEFAULT_NML_SURF
    WRITE(LUOUT,NML=SURF,DELIM='APOSTROPHE ')
    
    END SUBROUTINE PRINT_DEFAULT_NML_SURF

    SUBROUTINE SET_NML_SURF(OBJ)

    TYPE(TYPE_SURF),INTENT(INOUT)	:: OBJ

    OBJ%ID=ID
    OBJ%MATIDS=MATIDS
    OBJ%NSLAB=NSLAB
    OBJ%E=E
    OBJ%NODEPERSLAB=NODEPERSLAB
    OBJ%NODES=NODES
    OBJ%HCCT=HCCT
    OBJ%HMCT=HMCT
    OBJ%MESH_RATIO=MESH_RATIO
    OBJ%HRINI=HRINI
    OBJ%TINI=TINI

    END SUBROUTINE SET_NML_SURF

    SUBROUTINE COUNT_NML_SURF(LU1,LUOUT,N_OBJ)

    INTEGER,INTENT(IN)			:: LU1,LUOUT
    INTEGER,INTENT(OUT)			:: N_OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    N_OBJ=0
    REWIND(LU1)
    COUNT_OBJ_LOOP: DO
        CALL CHECKREAD('SURF',LU1,IOS)
        IF (IOS==0) EXIT COUNT_OBJ_LOOP
        READ(LU1,NML=SURF,END=209,ERR=210,IOSTAT=IOS)
        N_OBJ=N_OBJ+1
210     IF (IOS>0) THEN
            WRITE(LUOUT,NML=SURF)
            WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &SURF line number ',N_OBJ+1,': check parameters names and values'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDDO COUNT_OBJ_LOOP
209 REWIND(LU1)

    END SUBROUTINE COUNT_NML_SURF

    SUBROUTINE READ_NML_SURF(LU1,LUOUT,OBJRT)

    INTEGER,INTENT(IN)								:: LU1,LUOUT
    TYPE(TYPE_SURF),DIMENSION(:),INTENT(INOUT)	:: OBJRT
    CHARACTER(256)									:: MESSAGE
    INTEGER											:: I,IOS

    REWIND(LU1)
    SET_OBJ_LOOP: DO I=1,SIZE(OBJRT)
        CALL CHECKREAD('SURF',LU1,IOS)
        IF (IOS==0) EXIT SET_OBJ_LOOP
        CALL DEFAULT_NML_SURF
        READ(LU1,SURF)
        CALL SET_NML_SURF(OBJRT(I))
    ENDDO SET_OBJ_LOOP
    REWIND(LU1)

    END SUBROUTINE READ_NML_SURF

    END MODULE READ_NML_SURF_MODULE
