    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************

    ! #include "definitions.h90" non, Francois ne trouve pas ca beau !

    MODULE PROC_MAT_MODULE

    USE DATA_MAT_MODULE
    USE GLOBAL_VAR_MODULE,ONLY: LUOUT,MESSAGE,SUPERSATURATION

    IMPLICIT NONE

    !integer :: use_dichotomie = 1
    integer :: use_double_quadratique = 1


    CONTAINS

    SUBROUTINE INIT_CONS_MAT(OBJ,IDN)

    TYPE(TYPE_MAT),INTENT(INOUT)		:: OBJ
    INTEGER,INTENT(IN)					:: IDN

    integer use_old_clean
    double precision, dimension(1)                  :: tab_HR
    double precision, dimension(1)                  :: val_ref,pente_ref
    double precision, dimension(1)                  :: val_fast,pente_fast
    double precision, dimension(4)                  :: tab_coeffs
    integer                                         :: nb_valeurs, Indice_tableau, Indice_interpolation, nb_decompositions, nb_decomposition_boucle
    integer                                         :: Indice_tabulation
    double precision                                :: borne_sup_HR, coeff_interp
    INTEGER  							            :: LU=60
    CHARACTER(256)								    :: FNAME,FMT

    use_old_clean = 0
    nb_decompositions=10

    OBJ%IDN=IDN

    IF ((OBJ%LAMBDAFILE=='null').AND.(OBJ%LAMBDATABLE(1,1)==9999.D0)) THEN
        OBJ%LAMBDATABLE(1,1)=0.D0;OBJ%LAMBDATABLE(1,2)=OBJ%LAMBDA
        OBJ%LAMBDATABLE(2,1)=1.D0;OBJ%LAMBDATABLE(2,2)=OBJ%LAMBDA
    ELSEIF (OBJ%LAMBDAFILE.NE.'null') THEN
        !SAM copies dans les 2 premieres colonnes
        OBJ%LAMBDATABLE(:,1:2)=GET_MAT_TABLE(OBJ,OBJ%LAMBDAFILE)
        OBJ%LAMBDA=OBJ%LAMBDATABLE(1,2)
    ELSEIF (OBJ%LAMBDATABLE(1,1).NE.9999.D0) THEN
        OBJ%LAMBDA=OBJ%LAMBDATABLE(1,2)
    ENDIF

    !SAM nettoyage + precalcul de la tabulation
    if(use_old_clean) then
        CALL CLEAN_MAT_TABLE(OBJ%LAMBDATABLE)
    else 
        CALL CLEAN_MAT_TABLE_BIS(OBJ%LAMBDATABLE,.false.,(SUPERSATURATION==.FALSE.),OBJ%USE_LINEAR_LAMBDA,OBJ%ID,"LAMBDA")
    endif


    IF ((OBJ%DWFILE=='null').AND.(OBJ%DWTABLE(1,1)==9999.D0)) THEN
        OBJ%DWTABLE(1,1)=0.D0;OBJ%DWTABLE(1,2)=OBJ%DW
        OBJ%DWTABLE(2,1)=1.D0;OBJ%DWTABLE(2,2)=OBJ%DW
    ELSEIF (OBJ%DWFILE.NE.'null') THEN
        !SAM copies dans les 2 premieres colonnes
        OBJ%DWTABLE(:,1:2)=GET_MAT_TABLE(OBJ,OBJ%DWFILE)
    ENDIF
    !SAM nettoyage + precalcul de la tabulation
    if(use_old_clean) then
        CALL CLEAN_MAT_TABLE(OBJ%DWTABLE)
    else
        CALL CLEAN_MAT_TABLE_BIS(OBJ%DWTABLE,.false.,(SUPERSATURATION==.FALSE.),OBJ%USE_LINEAR_DW,OBJ%ID,"DW")
    endif

    
    IF ((OBJ%PERMFILE=='null').AND.(OBJ%PERMTABLE(1,1)==9999.D0)) THEN
        OBJ%PERMTABLE(1,1)=0.D0;OBJ%PERMTABLE(1,2)=OBJ%PERM
        OBJ%PERMTABLE(2,1)=1.D0;OBJ%PERMTABLE(2,2)=OBJ%PERM
    ELSEIF (OBJ%PERMFILE.NE.'null') THEN
        !SAM copies dans les 2 premieres colonnes
        OBJ%PERMTABLE(:,1:2)=GET_MAT_TABLE(OBJ,OBJ%PERMFILE)
        OBJ%PERMTABLE(:,1:2)=MAX(OBJ%PERMTABLE(:,1:2),1.D-20)
    ENDIF
    !!SAM nettoyage + precalcul de la tabulation
    if(use_old_clean) then
        CALL CLEAN_MAT_TABLE(OBJ%PERMTABLE)
    else
        CALL CLEAN_MAT_TABLE_BIS(OBJ%PERMTABLE,.false.,(SUPERSATURATION==.FALSE.),OBJ%USE_LINEAR_PERM,OBJ%ID,"PERM")
    endif


    IF ((OBJ%SORPFILE=='null').AND.(OBJ%SORPTABLE(1,1)==9999.D0)) THEN
        OBJ%SORPTABLE(1,1)=0.D0;OBJ%SORPTABLE(1,2)=OBJ%SORP
        OBJ%SORPTABLE(2,1)=1.D0;OBJ%SORPTABLE(2,2)=OBJ%SORP

        !!SAM nettoyage + precalcul de la tabulation. pente nulle authorisee
        if(use_old_clean) then
            CALL CLEAN_MAT_TABLE(OBJ%SORPTABLE)
        else
            CALL CLEAN_MAT_TABLE_BIS(OBJ%SORPTABLE,.false.,(SUPERSATURATION==.FALSE.),OBJ%USE_LINEAR_SORP,OBJ%ID,"SORP")
        endif

    ELSEIF (OBJ%SORPFILE.NE.'null') THEN
        !SAM copies dans les 2 premieres colonnes
        OBJ%SORPTABLE(:,1:2)=GET_MAT_TABLE(OBJ,OBJ%SORPFILE)

        !!SAM nettoyage + precalcul de la tabulation. pente nulle interdite
        if(use_old_clean) then
            CALL CLEAN_MAT_TABLE(OBJ%SORPTABLE)
        else
            CALL CLEAN_MAT_TABLE_BIS(OBJ%SORPTABLE,.true.,(SUPERSATURATION==.FALSE.),OBJ%USE_LINEAR_SORP,OBJ%ID,"SORP")
        endif
    ELSE
        !!SAM nettoyage + precalcul de la tabulation. pente nulle interdite
        if(use_old_clean) then
            CALL CLEAN_MAT_TABLE(OBJ%SORPTABLE)
        else
            CALL CLEAN_MAT_TABLE_BIS(OBJ%SORPTABLE,.true.,(SUPERSATURATION==.FALSE.),OBJ%USE_LINEAR_SORP,OBJ%ID,"SORP")
        endif
    ENDIF
    
    
    ! export conditionne au flag "EXPORT_TABLES" pour ce materiau
    ! prevoir l'export des autres tables
    if(OBJ%EXPORT_TABLES > 0) then

        FNAME = "test_table_" // TRIM(ADJUSTL(OBJ%ID)) //  "_bilan_SORP.txt"
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (9)
        
        nb_valeurs=OBJ%SORPTABLE(SIZE_MAT_TABLE,3)
        nb_decompositions=10

        DO Indice_tableau=1,nb_valeurs
            if(Indice_tableau < nb_valeurs)  then
                borne_sup_HR=OBJ%SORPTABLE(Indice_tableau+1,1)
                nb_decomposition_boucle=nb_decompositions-1
            else                                  
                borne_sup_HR=1.01D0
                nb_decomposition_boucle=nb_decompositions
            endif

            Do Indice_interpolation=0,nb_decomposition_boucle
                coeff_interp = (real(Indice_interpolation) / real(nb_decompositions))
                tab_HR(1) = (1.0D0-coeff_interp)*OBJ%SORPTABLE(Indice_tableau,1) + (coeff_interp)*borne_sup_HR

                val_ref(1:1) = GET_MAT_VALUE(OBJ%SORPTABLE,tab_HR,1,pente_ref )
                val_fast= GET_MAT_VALUE_FAST(OBJ%SORPTABLE,tab_HR,1,pente_fast,tab_coeffs)
        
                WRITE(LU,TRIM(FMT)) tab_HR(1), val_ref(1), pente_ref(1), val_fast(1), pente_fast(1), tab_coeffs(1), tab_coeffs(2), tab_coeffs(3), tab_coeffs(4)
            ENDDO
        ENDDO
        CLOSE(LU)


        FNAME = "test_table_" // TRIM(ADJUSTL(OBJ%ID)) //  "_bilan_LAMBDA.txt"
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (5)
        
        nb_valeurs=OBJ%LAMBDATABLE(SIZE_MAT_TABLE,3)
        nb_decompositions=10

        DO Indice_tableau=1,nb_valeurs
            if(Indice_tableau < nb_valeurs)  then
                borne_sup_HR=OBJ%LAMBDATABLE(Indice_tableau+1,1)
                nb_decomposition_boucle=nb_decompositions-1
            else                                  
                borne_sup_HR=1.01D0
                nb_decomposition_boucle=nb_decompositions
            endif

            Do Indice_interpolation=0,nb_decomposition_boucle
                coeff_interp = (real(Indice_interpolation) / real(nb_decompositions))
                tab_HR(1) = (1.0D0-coeff_interp)*OBJ%LAMBDATABLE(Indice_tableau,1) + (coeff_interp)*borne_sup_HR

                val_ref(1:1) = GET_MAT_VALUE(OBJ%LAMBDATABLE,tab_HR,1,pente_ref )
                val_fast= GET_MAT_VALUE_FAST(OBJ%LAMBDATABLE,tab_HR,1,pente_fast,tab_coeffs)
        
                WRITE(LU,TRIM(FMT)) tab_HR(1), val_ref(1), pente_ref(1), val_fast(1), pente_fast(1)
            ENDDO
        ENDDO
        CLOSE(LU)


        FNAME = "test_table_" // TRIM(ADJUSTL(OBJ%ID)) //  "_bilan_PERM.txt"
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (5)
        
        DO Indice_tabulation=1,(1012)*100
    
            tab_HR(1) = real(Indice_tabulation-1) / (1000.0*100.0)
            val_ref(1:1) = GET_MAT_VALUE(OBJ%PERMTABLE,tab_HR,1,pente_ref )
            val_fast= GET_MAT_VALUE_FAST(OBJ%PERMTABLE,tab_HR,1,pente_fast)
    
            WRITE(LU,TRIM(FMT)) tab_HR(1), val_ref(1), pente_ref(1), val_fast(1), pente_fast(1)
        ENDDO
        CLOSE(LU)

        FNAME = "test_table_" // TRIM(ADJUSTL(OBJ%ID)) //  "_bilan_DW.txt"
        OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
        WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (5)
        
        DO Indice_tabulation=1,(1012)*100
    
            tab_HR(1) = real(Indice_tabulation-1) / (1000.0*100.0)
            val_ref(1:1) = GET_MAT_VALUE(OBJ%DWTABLE,tab_HR,1,pente_ref )
            val_fast= GET_MAT_VALUE_FAST(OBJ%DWTABLE,tab_HR,1,pente_fast)
    
            WRITE(LU,TRIM(FMT)) tab_HR(1), val_ref(1), pente_ref(1), val_fast(1), pente_fast(1)
        ENDDO
        CLOSE(LU)

    endif

    

    END SUBROUTINE INIT_CONS_MAT

    SUBROUTINE INIT_CONS_MATRT

    INTEGER								:: I

    DO I=0,SIZE(MATRT)-1
        CALL INIT_CONS_MAT(MATRT(I),I)
    ENDDO

    END SUBROUTINE INIT_CONS_MATRT

    FUNCTION GET_MAT_TABLE(OBJ,ARGFILE)

    CHARACTER(100),INTENT(IN)           :: ARGFILE
    TYPE(TYPE_MAT),INTENT(IN)		    :: OBJ
    DOUBLE PRECISION,DIMENSION(SIZE_MAT_TABLE,2)   :: GET_MAT_TABLE
    INTEGER                                        :: I
    LOGICAL								           :: EX

    INQUIRE(FILE=ARGFILE,EXIST=EX)
    IF (.NOT.EX) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with MAT ',TRIM(OBJ%ID),': File ',ARGFILE,' is unknown.'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF
    OPEN(UNIT=LU3,FILE=ARGFILE,FORM="FORMATTED",STATUS="OLD",ACCESS="sequential",ACTION="read")
    READLOOP: DO I=1,SIZE_MAT_TABLE
        READ(LU3,*,END=10) GET_MAT_TABLE(I,1:2)
    ENDDO READLOOP
10  IF (GET_MAT_TABLE(I-1,1)==1.01D0) THEN
        GET_MAT_TABLE(I:SIZE_MAT_TABLE,1)=1.01D0
    ELSE
        GET_MAT_TABLE(I:SIZE_MAT_TABLE,1)=1.00D0
    ENDIF
    GET_MAT_TABLE(I:SIZE_MAT_TABLE,2)=GET_MAT_TABLE(I-1,2)

    CLOSE(LU3)

    END FUNCTION GET_MAT_TABLE

    FUNCTION GET_MAT_VALUE(TABLE,HR,N,SLOPE)

    !SAM la table a maintenant 6 colonnes : X val_lue A,B,C,D du polynome
    DOUBLE PRECISION,DIMENSION(SIZE_MAT_TABLE,8),INTENT(IN)    :: TABLE
    INTEGER,INTENT(IN)                              :: N
    DOUBLE PRECISION,DIMENSION(N),INTENT(IN)        :: HR
    DOUBLE PRECISION,DIMENSION(N),INTENT(INOUT),OPTIONAL    :: SLOPE
    DOUBLE PRECISION,DIMENSION(N)                   :: GET_MAT_VALUE
    INTEGER								            :: I,K

    DO K=1,N
        DO I=1,SIZE_MAT_TABLE-1
            IF ((TABLE(I+1,1)>HR(K))&
                .OR.(   (TABLE(I+1,1)==HR(K)).AND.(HR(K)==1.01D0)   .AND.   (SUPERSATURATION.EQV..TRUE. ))&
                .OR.(   (TABLE(I+1,1)==HR(K)).AND.(HR(K)==1.00D0)   .AND.   (SUPERSATURATION.EQV..FALSE.))          ) then
                    GOTO 200
            endif
        ENDDO
        I=SIZE_MAT_TABLE-1

200     IF (TABLE(I+1,1).NE.TABLE(I,1)) THEN
            GET_MAT_VALUE(K)=TABLE(I,2)+(TABLE(I+1,2)-TABLE(I,2))*(HR(K)-TABLE(I,1))/(TABLE(I+1,1)-TABLE(I,1))
        ELSE
            GET_MAT_VALUE(K)=TABLE(I,2)
        ENDIF

        IF (PRESENT(SLOPE)) THEN
            I=MAX(2,I)
            IF (TABLE(I+1,1).NE.TABLE(I-1,1)) THEN
                SLOPE(K)=(TABLE(I+1,2)-TABLE(I-1,2))/(TABLE(I+1,1)-TABLE(I-1,1))
            ELSE
                SLOPE(K)=0.D0
            ENDIF

        ENDIF
        
    ENDDO
    END FUNCTION GET_MAT_VALUE



    !SAM meme fonction, mais en version tabulee + interpolation quadratique
    FUNCTION GET_MAT_VALUE_FAST(TABLE,HR,N,SLOPE,tab_coefficients)

        !SAM la table a maintenant 8 colonnes ! x val_lue A,B,C,D du polynome + 2 pour un polynome de degre 1
        DOUBLE PRECISION,DIMENSION(SIZE_MAT_TABLE,8),INTENT(IN)    :: TABLE
        INTEGER,INTENT(IN)                              :: N
        DOUBLE PRECISION,DIMENSION(N),INTENT(IN)        :: HR
        DOUBLE PRECISION,DIMENSION(N),INTENT(INOUT),OPTIONAL    :: SLOPE
        DOUBLE PRECISION,DIMENSION(N,4),INTENT(INOUT),OPTIONAL    :: tab_coefficients

        DOUBLE PRECISION,DIMENSION(N)                   :: GET_MAT_VALUE_FAST
        double precision                                :: I_Reel, x
        INTEGER								            :: I,K
        integer                                         :: Imin,Imax,Imoy
       
        if(1) THEN

            DO K=1,N

                x = HR(K)

                Imin=1
                Imax=TABLE(SIZE_MAT_TABLE,3) ! indice de la derniere valeur exploitable

                !print*, "x=", x
                !print*, "maxi=", Imax

                !recherche dichotomique des bornes qui encadrent x
                do
                    if(Imax-Imin == 1) exit
                    Imoy=(Imin+Imax)/2

                    ! il est important de bien laisser >= afin que la valeur associee a HR=1.0 soit calculee sur la base de l'intervalle precedent
                    if(TABLE(Imoy,1) >= x) then 
                        Imax = Imoy
                    else
                        Imin = Imoy
                    endif

                end do
                !print*, "Imin=",Imin, " HRmin=",TABLE(Imin,1)
                !print*, "Imax=",Imax, " HRmax=",TABLE(Imax,1)
                I=Imin

                IF(PRESENT(tab_coefficients)) THEN
                    tab_coefficients(K,1) = TABLE(I,6)
                    tab_coefficients(K,2) = TABLE(I,5)
                    tab_coefficients(K,3) = TABLE(I,4)
                    tab_coefficients(K,4) = TABLE(I,3)
                ENDIF

                if(use_double_quadratique) then

                    ! recalage dans le repere local
                    x = x - TABLE(I,1)

                    GET_MAT_VALUE_FAST(K) = TABLE(I,6) + x *(TABLE(I,5) + x * TABLE(I,4)) ! polynome de degre 2

                    IF(PRESENT(SLOPE)) THEN
                        SLOPE(K) = TABLE(I,5) + x *(2.0D0*TABLE(I,4))
                    ENDIF

                else
                    GET_MAT_VALUE_FAST(K) = TABLE(I,6) + x *(TABLE(I,5) + x * (TABLE(I,4) + x*TABLE(I,3)))

                    IF(PRESENT(SLOPE)) THEN
                        SLOPE(K) = TABLE(I,5) + x *(2.0D0*TABLE(I,4) + x * 3.0D0*TABLE(I,3))
                    ENDIF
                endif
            ENDDO
        ELSE
            GET_MAT_VALUE_FAST = GET_MAT_VALUE(TABLE,HR,N,SLOPE)
            !GET_MAT_VALUE_FAST = GET_MAT_VALUE_TABULEE(TABLE,HR,N,SLOPE)
        ENDIF

    END FUNCTION GET_MAT_VALUE_FAST



    !SAM integration d'un precalcul a la suite du nettoyage de la table
    ! on part du principe que la table est triee dans l'ordre croissant des valeurs de la premiere colonne (et valeurs uniques)
    ! 6 colonnes : X val_lue A,B,C,D du polynome
    SUBROUTINE CLEAN_MAT_TABLE(TABLE)

    DOUBLE PRECISION,DIMENSION(SIZE_MAT_TABLE,8),INTENT(INOUT)    :: TABLE

    INTEGER								            :: I
    DOUBLE PRECISION                               :: MAXTABLE
    integer                                         :: Indice_tabulation
    double precision                                :: HR

    ! a desactiver si pas besoin de verifier
    integer                                         :: debug_table
    INTEGER  							            :: LU
    CHARACTER(256)								    :: FNAME,FMT
    double precision, dimension(1)                  :: tab_HR
    double precision, dimension(1)                  :: val_ref,pente_ref
    double precision, dimension(1)                  :: val_fast,pente_fast

    debug_table=0


    ! s'assurer qu'on retrouve bien HR=1.01 dans la table, associe la valeur max detectee (supposee etre la derniere lue visiblement)
    DO I=2,SIZE_MAT_TABLE
        IF (TABLE(I,1).NE.9999.D0) THEN
            MAXTABLE=TABLE(I,2)
        ELSE
            TABLE(I,1)=1.01D0
            TABLE(I,2)=MAXTABLE
        ENDIF
    ENDDO


    !SAM recopie de l'interpolation de Get_Map_Value (mais sans boucle sur K car uniquement pour la table)
    DO Indice_tabulation=1,SIZE_MAT_TABLE-1
        HR = real(Indice_tabulation-1) / MULT_MAT_TABLE

        DO I=1,SIZE_MAT_TABLE-1
            IF ((TABLE(I+1,1)>HR)&
                .OR.((TABLE(I+1,1)==HR).AND.(HR>=1.01D0).AND.(SUPERSATURATION.EQV..TRUE.))&
                .OR.((TABLE(I+1,1)==HR).AND.(HR>=1.00D0).AND.(SUPERSATURATION.EQV..FALSE.))) GOTO 20
        ENDDO

    20  IF (TABLE(I+1,1).NE.TABLE(I,1)) THEN
            TABLE(Indice_tabulation,3)=TABLE(I,2)+(TABLE(I+1,2)-TABLE(I,2))*(HR-TABLE(I,1))/(TABLE(I+1,1)-TABLE(I,1))
        ELSE
            TABLE(Indice_tabulation,3)=TABLE(I,2)
        ENDIF

        if(I>1)then
            !print*, "Indice=", Indice_tabulation, " HR=", HR, " I=", I, " VAL=", TABLE(Indice_tabulation,3)
        endif
    ENDDO
    TABLE(SIZE_MAT_TABLE,3)=TABLE(SIZE_MAT_TABLE-1,3) ! pour s'eviter des tests dans l'interpolation

    !Precalcul de la pente s'appuyant sur les valeurs qui entourent chaque abscisses
    !print*, "PENTE"
    TABLE(1,4)=(TABLE(2,3)-TABLE(1,3))*MULT_MAT_TABLE
    !print*, "Indice=1", " pente=", TABLE(1,4)
    DO Indice_tabulation=2,SIZE_MAT_TABLE-2
        TABLE(Indice_tabulation,4)=(TABLE(Indice_tabulation+1,3)-TABLE(Indice_tabulation-1,3))*(MULT_MAT_TABLE*0.5D0)
        !print*, "Indice=", Indice_tabulation, " pente=", TABLE(Indice_tabulation,4)
    ENDDO
    TABLE(SIZE_MAT_TABLE-1,4)=(TABLE(SIZE_MAT_TABLE-1,3)-TABLE(SIZE_MAT_TABLE-2,3))*MULT_MAT_TABLE
    !print*, "Indice=1001", " pente=", TABLE(1001,4)
    TABLE(SIZE_MAT_TABLE,4) = 0.0D0

    !Precalcul de la pente normalisee pour l'interpolation (a appliquer sur un coeff qui varie entre 0.0 et 1.0)
    DO Indice_tabulation=1,SIZE_MAT_TABLE-2
        TABLE(Indice_tabulation,5)=(TABLE(Indice_tabulation+1,3)-TABLE(Indice_tabulation,3))
    ENDDO
    TABLE(SIZE_MAT_TABLE-1,5) = 0.0D0
    TABLE(SIZE_MAT_TABLE,  5) = 0.0D0

    END SUBROUTINE CLEAN_MAT_TABLE


    subroutine merge(tableau, premier, milieu, dernier)
        double precision,dimension(SIZE_MAT_TABLE,8), intent(inout) :: tableau
        integer, intent(in) :: premier, dernier, milieu
        integer :: gauche, droite, destination

        !print*, "merge ", premier, " ", milieu , " " , dernier

        gauche=premier
        droite=milieu+1
        destination=premier

        ! avance de l'indice de gauche ou de droite en fonction de la valeur la plus petite dans la premiere colonne
        ! recopie dans les colonnes 3 et 4
        do while( (gauche <= milieu) .AND. (droite <= dernier) )
            if(tableau(gauche,1) <= tableau(droite,1)) then
                tableau(destination,3) = tableau(gauche,1)
                tableau(destination,4) = tableau(gauche,2)
                gauche = gauche + 1
            else
                tableau(destination,3) = tableau(droite,1)
                tableau(destination,4) = tableau(droite,2)
                droite = droite + 1
            endif
            destination = destination + 1
        enddo 

        ! ensuite, il en reste peut-etre a gauche ?
        do while (gauche <= milieu)
            tableau(destination,3) = tableau(gauche,1)
            tableau(destination,4) = tableau(gauche,2)
            gauche = gauche + 1
            destination = destination + 1
        enddo

        ! ou il en reste peut-etre a droite ?
        do while (droite <= dernier)
            tableau(destination,3) = tableau(droite,1)
            tableau(destination,4) = tableau(droite,2)
            droite = droite + 1
            destination = destination + 1
        enddo

        ! recopie dedans les 2 premieres colonnes
        !print*, "    merge:"
        do destination = premier, dernier
            tableau(destination,1) = tableau(destination,3)
            tableau(destination,2) = tableau(destination,4)

            !print*, "    " , tableau(destination,1)
        enddo
    endsubroutine merge

    recursive subroutine tri_fusion(tableau, premier, dernier)
        double precision,dimension(SIZE_MAT_TABLE,8), intent(inout) :: tableau
        integer, intent(in) :: premier, dernier
        integer :: milieu

        !print*, "trifusion ", premier, " ", dernier

        if(premier<dernier) then
            milieu = (premier + dernier) / 2
            call tri_fusion(tableau, premier, milieu)
            call tri_fusion(tableau, milieu+1, dernier)
            call merge(tableau, premier, milieu, dernier)
        endif
    endsubroutine tri_fusion

    



    ! SAM
    ! Nettoyage de la table lue (doublons, tri)
    ! puis determination de polynomes de degre 4 entre chaque valeurs
    ! 6 colonnes : X val_lue A,B,C,D du polynome
    SUBROUTINE CLEAN_MAT_TABLE_BIS(TABLE,pente_nulle_interdite,mode_rupture_apres_un, use_linear_interpolation, obj_name, type_name)

        DOUBLE PRECISION,DIMENSION(SIZE_MAT_TABLE,8),INTENT(INOUT)  :: TABLE
        logical, intent(in)                                         :: pente_nulle_interdite, mode_rupture_apres_un
        integer,intent(in)                                          :: use_linear_interpolation
        character(100),intent(in)                                   :: obj_name
        character(len=*),intent(in)                                 :: type_name
        
    
        INTEGER								            :: I,Indice_couple
        DOUBLE PRECISION                                :: last_val_table, max_HR, indice_max_HR, abscisse_courant
        double precision                                :: A,B,C,D,x0,x1,x2,V0,V1,V2,Vprim0,Vprim1,Xprim, deltaX
        double precision                                :: A1,B1,C1,A2,B2,C2,K
        double precision, dimension(SIZE_MAT_TABLE,6)   :: sauve_tab_coeffs
        integer                                         :: Indice_tabulation, nombre_abscisses, tri_necessaire, precedent_nombre_abscisses
        integer                                         :: nombre_abscisses_ajoutes, Indice_ajoutes
        double precision                                :: HR

        double precision                                :: delta, solution1, solution2, minsolution ! pour determination risque tengeante =0
    
        ! a desactiver si pas besoin de verifier
        integer                                         :: debug_table, surveillance
        INTEGER  							            :: LU
        CHARACTER(256)								    :: FNAME,FMT
        double precision, dimension(1)                  :: tab_HR
        double precision, dimension(1)                  :: val_ref,pente_ref
        double precision, dimension(1)                  :: val_fast,pente_fast
    
        debug_table=0
        surveillance=0
        !mode_rupture_apres_un=1


        if(debug_table) then

            ! commencer par reperer le nombre d'abscisses utilisables si tout va bien
            nombre_abscisses=1
            abscisse_courant=TABLE(1,1)
            DO I=2,SIZE_MAT_TABLE
                IF(TABLE(I,1) > abscisse_courant) THEN
                    nombre_abscisses=nombre_abscisses+1
                    abscisse_courant=TABLE(I,1)
                ELSE
                    print*, "dernier abscisse=",TABLE(I,1), " courante=" , abscisse_courant
                    exit
                ENDIF
            ENDDO
            print*, "TABLE:" // TRIM(ADJUSTL(obj_name)) // " " // TRIM(ADJUSTL(type_name)) // " estimation du nombre d'abscisses=", nombre_abscisses

            write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_init.txt"
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (2)

            DO I=1,SIZE_MAT_TABLE
                WRITE(LU,TRIM(FMT)) TABLE(I,1),TABLE(I,2)
            ENDDO
            CLOSE(LU)
        endif


        ! reperage rapide : nombre d'abscisses, tri necessaire, doublons ...
        tri_necessaire=.FALSE.
        abscisse_courant=TABLE(1,1)
        nombre_abscisses=1
        do I=2, SIZE_MAT_TABLE
            IF(TABLE(I,1) > abscisse_courant) THEN

                if(TABLE(I,1) < 9999.D0) then ! pas d'enregistrement des zones non initialisees
                    nombre_abscisses=nombre_abscisses+1
                    abscisse_courant=TABLE(I,1)

                    ! gestion eventuelle de suppression de doublons
                    if(I > nombre_abscisses) then
                        Table(nombre_abscisses,1) = Table(I,1)
                        Table(nombre_abscisses,2) = Table(I,2)
                    endif
                endif

            else if (TABLE(I,1) == abscisse_courant) then
                ! doublon. Pour un meme abscisse, les valeurs suivantes sont ignorees : on pourrait choisir de conserver la derniere ?
            else
                tri_necessaire = .TRUE.
                abscisse_courant=TABLE(I,1)
                nombre_abscisses = nombre_abscisses + 1 ! il faudra le re-determiner apres un tri + nouvelle suppression de doublons eventuels

                ! gestion eventuelle de suppression de doublons
                if(I > nombre_abscisses) then
                    Table(nombre_abscisses,1) = Table(I,1)
                    Table(nombre_abscisses,2) = Table(I,2)
                endif

            endif
        enddo
   
 
        if(debug_table) then
            print*, " tri=", tri_necessaire, " #abscisses=" , nombre_abscisses
        endif

        if(tri_necessaire) then
            call tri_fusion(TABLE,1,nombre_abscisses) ! pas besoin d'aller plus loin que nombre_abscisses


            if(debug_table) then
                write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_tridoublons.txt"
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (2)
    
                DO I=1,SIZE_MAT_TABLE
                    WRITE(LU,TRIM(FMT)) TABLE(I,1),TABLE(I,2)
                ENDDO
                CLOSE(LU)
            endif


            ! recalcul du nombre d'abscisses et re-suppression de doublons eventuels
            tri_necessaire=.FALSE.
            abscisse_courant=TABLE(1,1)
            precedent_nombre_abscisses=nombre_abscisses
            nombre_abscisses=1
            do I=2, precedent_nombre_abscisses
                IF(TABLE(I,1) > abscisse_courant) THEN
                    abscisse_courant=TABLE(I,1)
                    nombre_abscisses=nombre_abscisses+1
    
                    ! gestion eventuelle de suppression de doublons
                    if(I > nombre_abscisses) then

                        if(debug_table) print*, " ajout apres doublon de I=" , I, " en ", nombre_abscisses, " abs=", abscisse_courant

                        Table(nombre_abscisses,1) = Table(I,1)
                        Table(nombre_abscisses,2) = Table(I,2)
                    endif
    
                else if (TABLE(I,1) == abscisse_courant) then
                    ! doublon. Pour un meme abscisse, les valeurs suivantes sont ignorees : on pourrait choisir de conserver la derniere ?
                    if(debug_table) print*, "doublon en:", I, " =", abscisse_courant, " #abs=" , nombre_abscisses
                else
                    abscisse_courant=TABLE(I,1)
                    tri_necessaire = .TRUE.
                    nombre_abscisses = nombre_abscisses + 1 ! il faudra le re-determiner apres un tri + nouvelle suppression de doublons eventuels
    
                    ! gestion eventuelle de suppression de doublons
                    if(I > nombre_abscisses) then
                        Table(nombre_abscisses,1) = Table(I,1)
                        Table(nombre_abscisses,2) = Table(I,2)
                    endif
    
                endif
            enddo

            if(debug_table) then
                if(tri_necessaire) print*, "probleme apres le tri" 

                if(nombre_abscisses < precedent_nombre_abscisses) then
                    print*, "suppression de ", (precedent_nombre_abscisses-nombre_abscisses), " doublons"
                endif
                print*, "apres le tri, #abscisses=" , nombre_abscisses
            endif
        endif

        if(debug_table) then
            write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_tri.txt"
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (2)

            DO I=1,SIZE_MAT_TABLE
                WRITE(LU,TRIM(FMT)) TABLE(I,1),TABLE(I,2)
            ENDDO
            CLOSE(LU)
        endif
  
        ! s'assurer qu'on retrouve bien HR=0.0 dans la table, sinon l'ajouter au debut (en recopiant la premirere valeur et en decalant tout)
        IF (TABLE(1,1).NE. 0.0D0) THEN
            DO I=nombre_abscisses+1, 2, -1
                TABLE(I,1)=TABLE(I-1,1)
                TABLE(I,2)=TABLE(I-1,2)
            ENDDO
            TABLE(1,1)=0.0D0
            TABLE(1,2)=TABLE(2,2)
            nombre_abscisses=nombre_abscisses+1
            if(debug_table) print*, "ajout du 0.0 avec la valeur:", TABLE(1,2)," nombre_abscisses=", nombre_abscisses
        ENDIF
    
        ! s'assurer qu'on a bien une valeur pour HR=1.01D0
        IF (TABLE(nombre_abscisses,1) < 1.01D0) THEN
            TABLE(nombre_abscisses+1,1)=1.01D0
            TABLE(nombre_abscisses+1,2)=TABLE(nombre_abscisses,2)
            nombre_abscisses=nombre_abscisses+1
            if(debug_table) print*, "ajout du 1.01, en I=", nombre_abscisses
        ENDIF

        if(debug_table) then
            precedent_nombre_abscisses=nombre_abscisses
            nombre_abscisses=1
            abscisse_courant=0.0D0
            DO I=2,SIZE_MAT_TABLE ! normalement meme pas besoin de recompter mais pour verifier
                if(table(I,1) >= 9999) exit
                IF(TABLE(I,1) > abscisse_courant) THEN
                    nombre_abscisses=nombre_abscisses+1
                    abscisse_courant=TABLE(I,1)
                ELSE
                    exit
                ENDIF
            ENDDO
            print*, "recompte nombre d'abscisses=", nombre_abscisses
            if(nombre_abscisses .ne. precedent_nombre_abscisses) then
                print*, "erreur"
                STOP -1
            endif
        endif

        ! s'il n'y a que 2 abscisses differents, on insere une valeur au milieu (il y a deja forcement 0.0 et 1.01)
        IF (nombre_abscisses < 3) THEN
            TABLE(3,1) = TABLE(2,1)
            TABLE(3,2) = TABLE(2,2)
            TABLE(2,1) = (TABLE(1,1)+TABLE(3,1)) * 0.5D0
            TABLE(2,2) = (TABLE(1,2)+TABLE(3,2)) * 0.5D0
            nombre_abscisses = 3

            if(debug_table) print*, "ajout d'une troisieme valeur"
        ENDIF


        if(debug_table) then
            write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_complete.txt"
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (2)

            DO I=1,nombre_abscisses
                WRITE(LU,TRIM(FMT)) TABLE(I,1),TABLE(I,2)
                
                IF (TABLE(I,1)==1.01D0) THEN
                    exit
                ENDIF
            ENDDO
            CLOSE(LU)
        endif

        ! preparation des tangentes associees a chaque point de la courbe d'entree
        ! ces tangentes sont determinees sur la base d'une evolution polynominale de degre 2
        ! les pentes sont deteminees sur la base de 3 sommets, chaque valeur ne depend donc pas des valeurs tengentes precedentes afin d'eviter des divergences
        ! attention : cas particuliers pour les 2 premiereres et 2 dernieres valeurs (si plateau(x))
        ! attention : cas particulier pour la premiere et derniere valeur sinon

        ! cas particulier : les 2 premieres valeurs
        IF(TABLE(1,2) == TABLE(2,2)) THEN ! cas d'un plateau
            TABLE(1,3) = 0.0D0
            TABLE(2,3) = 0.0D0
        ELSE
            !polynome de degre 2, conditions : passage par les 3 premiers sommets (x0=0.0)
            x1=TABLE(2,1)
            x2=TABLE(3,1)
            V0=TABLE(1,2)
            V1=TABLE(2,2)
            V2=TABLE(3,2)
            C = V0
            A = (x1*V2-x2*V1-C*(x1-x2)) / (x1*x2*x2 - x1*x1*x2)
            B = (V1 - A*x1*x1 - C)      / x1
            TABLE(1,3) = B
            TABLE(2,3) = 2.0D0*A*x1 + B

            if(pente_nulle_interdite) then
                if((table(1,1) <= 1.0D0) .AND. (table(1,3) == 0.0D0)) table(1,3)=1.0E-6
                if((table(2,1) <= 1.0D0) .AND. (table(2,3) == 0.0D0)) table(2,3)=1.0E-6
            endif

        ENDIF

        if(debug_table) then
            !if( (table(1,3) == 0.0D0) .AND. (table(2,3) == 0.0D0)) then
            !    x0=0.0D0
            !    x2=-1.0D0
            !endif

            write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_courbes.txt"
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (2)
            DO x0 = 0.0D0 , x2, 0.0001D0
                WRITE(LU,TRIM(FMT)) x0, A*x0*x0+B*x0+C
            ENDDO
        endif

        ! cas particulier: les 2 dernieres valeurs
        IF(TABLE(nombre_abscisses-1,2) == TABLE(nombre_abscisses,2)) THEN ! cas d'un plateau
            TABLE(nombre_abscisses-1,3) = 0.0D0
            TABLE(nombre_abscisses  ,3) = 0.0D0

            ! si on veut que ca ne soit pas C1 sur l'abscisse 1.0 (avant dernier abscisse pour l'instant, TODO : gerer cette difference ?)
            if(mode_rupture_apres_un) then
                if( nombre_abscisses == 3) then !cas particulier
                    TABLE(nombre_abscisses-1,3) = (TABLE(2,2) - TABLE(1,2)) / (TABLE(2,1) - TABLE(1,1))
                else
                    ! polynome de degre 2, conditions : passage par les 3 sommets (N-3 N-2 N-1)
                    x0=TABLE(nombre_abscisses-3,1)
                    x1=TABLE(nombre_abscisses-2,1)
                    x2=TABLE(nombre_abscisses-1,1)
                    V0=TABLE(nombre_abscisses-3,2)
                    V1=TABLE(nombre_abscisses-2,2)
                    V2=TABLE(nombre_abscisses-1,2)
                    A = ((x1-x0)*(V2-V1) - (x2-x1)*(V1-V0)) / ((x1-x0)*(x2*x2-x1*x1) - (x2-x1)*(x1*x1-x0*x0))
                    B = (V1 - V0 - A*(x1*x1-x0*x0)) / (x1-x0)
                    C = V0 - A*x0*x0 - B*x0

                    TABLE(nombre_abscisses-1,3) = 2.0D0*A*x2 + B
                endif
            endif

        ELSE
            ! pas de plateau sur [1.0 1.01] mais on veut une coupure quand meme apres un ?
            ! configuration non probable
            if(mode_rupture_apres_un) then
                if( nombre_abscisses == 3) then !cas particulier
                    TABLE(nombre_abscisses-1,3) = (TABLE(2,2) - TABLE(1,2)) / (TABLE(2,1) - TABLE(1,1))
                else
                    ! polynome de degre 2, conditions : passage par les 3 sommets (N-3 N-2 N-1)
                    x0=TABLE(nombre_abscisses-3,1)
                    x1=TABLE(nombre_abscisses-2,1)
                    x2=TABLE(nombre_abscisses-1,1)
                    V0=TABLE(nombre_abscisses-3,2)
                    V1=TABLE(nombre_abscisses-2,2)
                    V2=TABLE(nombre_abscisses-1,2)
                    A = ((x1-x0)*(V2-V1) - (x2-x1)*(V1-V0)) / ((x1-x0)*(x2*x2-x1*x1) - (x2-x1)*(x1*x1-x0*x0))
                    B = (V1 - V0 - A*(x1*x1-x0*x0)) / (x1-x0)
                    C = V0 - A*x0*x0 - B*x0

                    TABLE(nombre_abscisses-1,3) = 2.0D0*A*x2 + B
                    TABLE(nombre_abscisses  ,3) = 0.0D0
                endif
            else

                !polynome de degre 2, conditions : passage par les 3 derniers sommets
                x0=TABLE(nombre_abscisses-2,1)
                x1=TABLE(nombre_abscisses-1,1)
                x2=TABLE(nombre_abscisses  ,1)
                V0=TABLE(nombre_abscisses-2,2)
                V1=TABLE(nombre_abscisses-1,2)
                V2=TABLE(nombre_abscisses  ,2)
                A = ((x1-x0)*(V2-V1) - (x2-x1)*(V1-V0)) / ((x1-x0)*(x2*x2-x1*x1) - (x2-x1)*(x1*x1-x0*x0))
                B = (V1 - V0 - A*(x1*x1-x0*x0)) / (x1-x0)
                C = V0 - A*x0*x0 - B*x0

                TABLE(nombre_abscisses-1,3) = 2.0D0*A*x1 + B
                TABLE(nombre_abscisses  ,3) = 2.0D0*A*x2 + B

                if(pente_nulle_interdite) then
                    if((table(nombre_abscisses-1,1) <= 1.0D0) .AND. (table(nombre_abscisses-1,3) <= 0.0D0)) table(nombre_abscisses-1,3)=1.0E-6
                    if((table(nombre_abscisses  ,1) <= 1.0D0) .AND. (table(nombre_abscisses  ,3) <= 0.0D0)) table(nombre_abscisses  ,3)=1.0E-6
                endif
            endif
        ENDIF

        if(debug_table) then
             
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (2)
            DO x1 = x0 , x2, 0.0001D0
                WRITE(LU,TRIM(FMT)) x1, A*x1*x1+B*x1+C

            ENDDO
            CLOSE(LU)
        endif

        ! partie centrale, toujours un polynome de degre 2, passage par 3 points (I:point central)
        DO I=3,nombre_abscisses-2
            x0=TABLE(I-1,1)
            x1=TABLE(I  ,1)
            x2=TABLE(I+1,1)
            V0=TABLE(I-1,2)
            V1=TABLE(I  ,2)
            V2=TABLE(I+1,2)
            A = ((x1-x0)*(V2-V1) - (x2-x1)*(V1-V0)) / ((x1-x0)*(x2*x2-x1*x1) - (x2-x1)*(x1*x1-x0*x0))
            B = (V1 - V0 - A*(x1*x1-x0*x0)) / (x1-x0)
            C = V0 - A*x0*x0 - B*x0

            TABLE(I,3) = 2.0D0*A*x1 + B
        ENDDO


        if(debug_table) then
            write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_pentes.txt"
            OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
            
            WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (4)

            DO I=1,nombre_abscisses
                WRITE(LU,TRIM(FMT)) TABLE(I,1),TABLE(I,2), 0.01D0, 0.01D0*TABLE(I,3)
                IF (TABLE(I,1)==1.01D0) THEN
                    exit
                ENDIF
            ENDDO
            CLOSE(LU)
        endif


        if(use_linear_interpolation) then
            X0     = TABLE(1  ,1)
            V0     = TABLE(1  ,2)
            DO I=1,nombre_abscisses-1
                X1     = TABLE(I+1,1)
                V1     = TABLE(I+1,2)

                ! interpolation lineaire donc coeff degre 2 = 0
                TABLE(I,4) = 0
                TABLE(I,5) = (V1-V0) / (X1-X0)
                TABLE(I,6) = V0

                X0 = X1
                V0 = V1
            ENDDO

            nombre_abscisses = nombre_abscisses  - 1
            ! la taille utile du tableau est stockee dans le DERNIER element du tableau
            ! l'info stockee correspond donc a l'indice de la derniere valeur utile comme pour l'interpolation double quadratique
            TABLE(SIZE_MAT_TABLE,3) = nombre_abscisses

        else if(use_double_quadratique) then

            ! Determination des parametres d'une double interpolation polynomiale de degre 2 pour chaque demi-portion (entre 2 abscisses)
            ! un point de controle intermediaire est cree pour chaque intervale, la valeur associee et la pente sont definis par les valeurs et pentes des bornes

            ! pour la premiere partie : V1(x)=A1.x^2 + B1.x +C1
            ! pour la premiere partie : V2(x)=A2.x^2 + B2.x +C2

            ! Generalisation de l'equation avec : l'intervale [0,DX] et un point intermediaire pose entre les 2 (parametre K)
            ! Le premier intervalle est donc sur [0,K*DX], le second sur [0,(1-K)*DX] avec juste un decalage,
            !
            ! V1(0) = V0        => C1=V0
            ! V1'(0)= V0'       => B1=Vprim0
            ! V2(1) = V1        => A2*((1-K)*DX)^2 + B2*((1-K)*DX) + C2 = V1
            ! V2'(1)=Vprim1     => 2*A2*((1-K)*DX) + B2 = Vprim1
            ! V1(K.DX)=V2(0)    => A1*(K*DX)^2 + B1*(K*DX) + C1 = C2
            ! V1'(K.DX)=V2'(0)  => 2*A1*(K*DX) + B1 = B2
            ! 
            ! A1 = (v1 -v0 −vprim0((1+K)*DX/2) -vprim1*((1-K)*DX/2) ) / (K*DX*DX)
            ! B1 = Vprim0
            ! C1 = V0
            ! B2 = 2*(K*DX)*A1 + B1
            ! C2 = ((K*DX)^2)*A1 + (K*DX)*B1 + C1
            ! A2 = (V1 - C2 - ((1-K)*DX)*B2) / ((1-K)*DX)^2

            ! test avec K=1/2 pour l'instant
            ! K=0.5D0

            ! On peut imposer une recherche de K telle que la tangeante en K.DX soit = (Vprim0 + Vprim1)/2
            ! K = ( (2/DX)*(V1-V0) -(1/2)*Vprim0 -(3/2)*Vprim1) / (Vprim0 - Vprim1)
            ! a condition que Vprim0 != Vprim1

            X0     = TABLE(1  ,1)
            V0     = TABLE(1  ,2)
            Vprim0 = TABLE(1  ,3) ! les pentes
            DO I=1,nombre_abscisses-1
                X1     = TABLE(I+1,1)
                V1     = TABLE(I+1,2)
                Vprim1 = TABLE(I+1,3)

                ! test pour la portion au-dela de HR=1.0 (Attention a bien gerer la valeur HR=1.0 dans Get_Mat_Value_Fast !)
                if((X0==1.0D0) .and. (mode_rupture_apres_un) .and. (V0==V1)) then
                    Vprim0 = 0.0D0
                endif

                !deltaX = 0.5D0 * (X1-X0)
                deltaX = (X1-X0)


                !if((X0 > 0.9969) .AND. (X1 < 0.9981)) then
                !    print*, "TABLE:" // TRIM(ADJUSTL(obj_name)) // " " // TRIM(ADJUSTL(type_name)) // " entre les bornes"
                !    debug_table=1
                !endif
    
                if( abs( Vprim0 - Vprim1) > 0.001D0) then
                    K = ( (2.0D0/deltaX)*(V1-V0) -0.5D0*Vprim0 -1.5D0*Vprim1) / (Vprim0 - Vprim1)
                else
                    K=0.5D0 ! on imposera une interpolation lineaire plus tard dans ce cas particulier
                endif
    
                if ( ( K < 0.01D0) .OR. (K > 0.99D0)) then
                    !print*, "zut, K=",K
                    !print*, "Xs: ", X0, X1, deltaX
                    !print*, "Vs: ", V0, V1
                    !print*, "V's: ", Vprim0, Vprim1
                    K=0.5D0
                    !surveillance=1
                else
                    
                    !print*, "Yes K=", K, " X1X2=" , X0 , " " , X1 , " " , obj_name, " ", type_name

                    surveillance=0
                endif
    
                A1 = (v1 -v0 -vprim0*((1.0D0+K)*deltaX/2.0D0) -vprim1*((1.0D0-K)*deltaX/2.0D0) ) / (K*deltaX*deltaX)
                B1 = Vprim0
                C1 = V0

                TABLE(I,4) = A1 
                TABLE(I,5) = B1
                TABLE(I,6) = C1

                ! pour le deuxieme demi-intervalle

                B2 = 2*(K*deltaX)*A1 + B1
                C2 = ((K*deltaX)**2.0D0)*A1 + (K*deltaX)*B1 + C1
                A2 = (V1 - C2 - ((1.0D0-K)*deltaX)*B2) / ( ((1.0D0-K)*deltaX)**2.0D0 )

                sauve_tab_coeffs(I,1) = X0*(1.0D0-K)+ (X1*K)
                sauve_tab_coeffs(I,2) = C2 ! Val
                sauve_tab_coeffs(I,3) = B2 ! Pente
                sauve_tab_coeffs(I,4) = A2 
                sauve_tab_coeffs(I,5) = B2 
                sauve_tab_coeffs(I,6) = C2


                ! Risque d'une derivee nulle dans l'intervalle ? On passe alors en lineaire

                if( ((x0 < 1.0D0) .AND. (mode_rupture_apres_un) ) .OR. &
                    (mode_rupture_apres_un == .FALSE.)          ) then

                    if(A1 .NE. 0.0D0) then ! polynome de degre 2
                        solution1 = - (0.5D0 * (B1 / A1))
                        if( ( pente_nulle_interdite      .AND. ((solution1 >= 0.000D0)           .AND. (solution1 <= K*deltaX)) ) .OR. &
                            ( (pente_nulle_interdite==0) .AND. ((solution1 >= 0.001D0*K*deltaX)  .AND. (solution1 <= 0.999D0*K*deltaX)) ) ) then
                            
                            if(surveillance) then
                                print*, "DERIVEE NULLE, A TRAITER"
                                print*, "X0=", X0, " X1=", X1, "K=", K
                                print*, "V0=", V0, " Vprim0=", Vprim0
                                print*, "V1=", V1, " Vprim1=", Vprim1
                                print*, "A1=", A1, "B1=",B1, " C1=", C1
                                print*, "S=", solution1, "Int1=[0,1]"
                                print*, "RISQUE DE DERIVEE NULLE (degre1), A TRAITER"
                            endif

                            ! solution basique : on passe en lineaire sans assurer la continuite de la pente
                            ! l'interpolation lineaire est applique sur tout l'intervalle [X0 X1] afin d'assurer la monotonie sur cette section

                            ! premier sous intervalle
                            TABLE(I,4) = 0
                            TABLE(I,5) = (V1-V0) / (X1-X0)
                            !TABLE(I,6)= C1
                            
                            ! deuxieme demi-intervalle invalide'
                            sauve_tab_coeffs(I,1) = -9999
                            sauve_tab_coeffs(I,4) = 0
                            sauve_tab_coeffs(I,5) = TABLE(I,5)
                            sauve_tab_coeffs(I,6) = V0*(1.0D0-K)+ V1*(K) ! conformement a l'interpolation lineaire sur cette section
                            sauve_tab_coeffs(I,2) = sauve_tab_coeffs(I,6) !val
                            sauve_tab_coeffs(I,3) = sauve_tab_coeffs(I,5) !pente

                            if( (pente_nulle_interdite) .AND. (TABLE(I,5)==0.0D0)) then
                                print*, "DERIVEE NULLE, A TRAITER"
                                print*, "X0=", X0, " X1=", X1, "K=", K
                                print*, "V0=", V0, " Vprim0=", Vprim0
                                print*, "V1=", V1, " Vprim1=", Vprim1
                                print*, "A1=", A1, "B1=",B1, " C1=", C1
                                print*, "S=", solution1, "Int1=[0,1]"
                                print*, "RISQUE DE DERIVEE NULLE (degre1), A TRAITER"
                                stop -1
                            endif
                        endif
                    else if(B1 == 0.0D0) then ! polynome de degre 1 par deduction ET pente nulle
                        ! dans le cas : pente nulle interdite sur tout un intervalle (pour SORP par exemple)
                        if(pente_nulle_interdite) then
                            print*, "DERIVEE NULLE, A TRAITER"
                            print*, "V0=", V0, " Vprim0=", Vprim0
                            print*, "V1=", V1, " Vprim1=", Vprim1
                            STOP -2
                        endif
                    endif


                    if(A2 .NE. 0.0D0) then ! polynome de degre 2
                        solution1 = - (0.5D0 * (B2 / A2))
                        if( ( pente_nulle_interdite      .AND. ((solution1 >= 0.000D0)           .AND. (solution1 <= K*deltaX)) ) .OR. &
                            ( (pente_nulle_interdite==0) .AND. ((solution1 >= 0.001D0*K*deltaX)  .AND. (solution1 <= 0.999D0*K*deltaX)) ) ) then
                            
                            if(surveillance) then
                                print*, "DERIVEE NULLE, A TRAITER"
                                print*, "X0=", X0, " X1=", X1, "K=", K
                                print*, "V0=", V0, " Vprim0=", Vprim0
                                print*, "V1=", V1, " Vprim1=", Vprim1
                                print*, "A2=", A2, "B2=",B2, " C2=", C2
                                print*, "S=", solution1, "Int2=[0,1]"
                                print*, "RISQUE DE DERIVEE NULLE (degre1), A TRAITER"
                            endif

                            ! premier demi-intervalle
                            TABLE(I,4) = 0
                            TABLE(I,5) = (V1-V0) / (X1-X0)
                            !TABLE(I,6)= C1
                            
                            ! deuxieme demi-intervalle
                            sauve_tab_coeffs(I,1) = -9999
                            sauve_tab_coeffs(I,4) = 0
                            sauve_tab_coeffs(I,5) = TABLE(I,5)
                            sauve_tab_coeffs(I,6) = V0*(1.0D0-K)+ V1*(K) ! conformement a l'interpolation lineaire sur cette section

                            if( (pente_nulle_interdite) .AND. (TABLE(I,5)==0.0D0)) then
                                print*, "DERIVEE NULLE, A TRAITER"
                                print*, "X0=", X0, " X1=", X1, "K=", K
                                print*, "V0=", V0, " Vprim0=", Vprim0
                                print*, "V1=", V1, " Vprim1=", Vprim1
                                print*, "A2=", A2, "B2=",B2, " C2=", C2
                                print*, "S=", solution1, "Int2=[0,1]"
                                print*, "RISQUE DE DERIVEE NULLE (degre1), A TRAITER"
                                stop -3
                            endif
                        endif
                    else if(B2 == 0.0D0) then ! polynome de degre 1 par deduction ET pente nulle
                        ! dans le cas : pente nulle interdite sur tout un intervalle (pour SORP par exemple)
                        if(pente_nulle_interdite) then
                            print*, "DERIVEE NULLE, A TRAITER"
                            print*, "V0=", V0, " Vprim0=", Vprim0
                            print*, "V1=", V1, " Vprim1=", Vprim1
                            STOP -4
                        endif
                    endif

                endif

                X0 = X1
                V0 = V1
                Vprim0 = Vprim1
            ENDDO


            ! insertion des demi-intervalles valides

            nombre_abscisses_ajoutes=0
            DO I=1,nombre_abscisses
                if(sauve_tab_coeffs(I,1) >= 0.0D0) nombre_abscisses_ajoutes=nombre_abscisses_ajoutes+1
            ENDDO

            Indice_couple = nombre_abscisses+nombre_abscisses_ajoutes
            DO I=nombre_abscisses, 1 , -1
                if(sauve_tab_coeffs(I,1) >= 0.0D0) then
                    TABLE(Indice_couple,1)   = sauve_tab_coeffs(I,1) ! HR
                    TABLE(Indice_couple,2)   = sauve_tab_coeffs(I,2) ! Val
                    TABLE(Indice_couple,3)   = sauve_tab_coeffs(I,3) ! Pente    
                    TABLE(Indice_couple,4)   = sauve_tab_coeffs(I,4) ! A2
                    TABLE(Indice_couple,5)   = sauve_tab_coeffs(I,5) ! B2
                    TABLE(Indice_couple,6)   = sauve_tab_coeffs(I,6) ! C2*
                    Indice_couple=Indice_couple-1
                endif

                if(I == Indice_couple) exit !plus besoin de poursuivre la boucle

                TABLE(Indice_couple,1) = TABLE(I,1) ! HR
                TABLE(Indice_couple,2) = TABLE(I,2) ! Val
                TABLE(Indice_couple,3) = TABLE(I,3) ! Pente
                TABLE(Indice_couple,4) = TABLE(I,4) ! A1
                TABLE(Indice_couple,5) = TABLE(I,5) ! B1
                TABLE(Indice_couple,6) = TABLE(I,6) ! C1
                Indice_couple=Indice_couple-1
            ENDDO

            nombre_abscisses = nombre_abscisses + nombre_abscisses_ajoutes - 1
         

            ! la taille utile du tableau est stockee dans le DERNIER element du tableau
            ! l'info stockee correspond donc a l'indice de la derniere valeur utile
            TABLE(SIZE_MAT_TABLE,3) = nombre_abscisses

            if(debug_table) then
                write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_modele.txt"
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (6)
                
                DO Indice_tabulation=1,nombre_abscisses
                    WRITE(LU,TRIM(FMT)) table(Indice_tabulation,1), table(Indice_tabulation,2), table(Indice_tabulation,3), table(Indice_tabulation,4), table(Indice_tabulation,5), table(Indice_tabulation,6)
                ENDDO
                CLOSE(LU)
            endif


        else

            ! Determination des parametres d'une interpolation polynomiale de degre 3 pour chaque portion (entre 2 abscisses)
            x0     = TABLE(1  ,1)
            V0     = TABLE(1  ,2)
            Vprim0 = TABLE(1  ,3) ! les pentes
            DO I=1,nombre_abscisses-1
                X1     = TABLE(I+1,1)
                V1     = TABLE(I+1,2)
                Vprim1 = TABLE(I+1,3)

                ! test pour la portion au-dela de HR=1.0 (Attention a bien gerer la valeur HR=1.0 dans Get_Mat_Value_Fast !)
                if((I==nombre_abscisses-1) .and. (mode_rupture_apres_un) .and. (V0==V1)) then
                    Vprim0 = 0.0D0
                endif

                ! on applique un changement de variable pour simplifier la resolution en mettant l'origine en X0, mais cela implique de le faire a l'exploitation
                Xprim = (x1 - x0)
                A = ( 2*V0 - 2*V1 + Vprim0*Xprim   + Vprim1*Xprim) / (Xprim*Xprim*Xprim)
                B = (-3*V0 + 3*V1 - 2*Vprim0*Xprim - Vprim1*Xprim) / (Xprim*Xprim)
                C = Vprim0
                D = V0

                ! pour ne pas faire de decalage a l'exploitation, on developpe A*(x-x0)^3 + B*(x-x0)^2 + C*(x-x0) + D et on distribue les coeffs dans A,B,C,D
                TABLE(I,3) = A                                  ! A ne change pas
                TABLE(I,4) = B - 3*x0*A                         ! B recupere une partie de developpement de (x-x0)^3 : le multiplicateur de x^2
                TABLE(I,5) = C + 3*x0*x0*A  - 2*x0*B            ! C recupere une partie du developpement de (x-x0)^3 et de (x-x0)^2 : le multiplicateur de x
                TABLE(I,6) = D - x0*x0*x0*A + x0*x0*B - x0*C    ! meme principe pour D

                !print*, "X0 X1=", X0, X1
                !print*, "Xprim=", Xprim
                !print*, "ABCD=", A,B,C,D
                !print*, "tab=", TABLE(I,4), TABLE(I,5), TABLE(I,6)


                ! Risque d'une derivee nulle dans l'intervalle ? On passe alors en lineaire

                if( ((x0 < 1.0D0) .AND. (mode_rupture_apres_un) ) .OR. &
                    (mode_rupture_apres_un == .FALSE.)          ) then

                    if(A .NE. 0.0D0) then !polynome de degre 3
                        A=3.0D0*TABLE(I,3)
                        B=2.0D0*TABLE(I,4)
                        C=TABLE(I,5)
                        delta = (B*B) - 4.0D0 * A * C ! B^2 - 4*A*C de la derivee du polynome
                        if(delta >= 0.0D0) then
                            delta = sqrt(delta)
                            solution1 = (-B - delta) / (2.0D0*A)
                            solution2 = (-B + delta) / (2.0D0*A)
                            
                            if( (solution1 >= TABLE(I,1)) .AND. (solution1 <= TABLE(I+1,1)) ) then                               
                                !solution basique : on passe en lineaire sans assurer la continuite de la pente
                                TABLE(I,3)=0
                                TABLE(I,4)=0
                                TABLE(I,5)=(V1-V0)/(X1-X0)
                                TABLE(I,6)=V0-TABLE(I,5)*X0

                                if( (pente_nulle_interdite) .AND. (TABLE(I,5)==0.0D0)) then
                                    print*, "DERIVEE NULLE, A TRAITER"
                                    print*, "V0=", V0, " Vprim0=", Vprim0
                                    print*, "V1=", V1, " Vprim1=", Vprim1
                                    print*, "A=", A, " B=",B, " C=", C
                                    print*, "S1=", solution1, " S2=", solution2, "Int=[", TABLE(I,1), " , ", TABLE(I+1,1), "]"
                                    print*, "RISQUE DE DERIVEE NULLE (degre2), A TRAITER"
                                    stop -1
                                endif

                            endif

                            if( (solution2 >= TABLE(I,1)) .AND. (solution2 <= TABLE(I+1,1)) ) then
                                !solution basique : on passe en lineaire sans assurer la continuite de la pente
                                TABLE(I,3)=0
                                TABLE(I,4)=0
                                TABLE(I,5)=(V1-V0)/(X1-X0)
                                TABLE(I,6)=V0-TABLE(I,5)*X0

                                if( (pente_nulle_interdite) .AND. (TABLE(I,5)==0.0D0)) then
                                    print*, "DERIVEE NULLE, A TRAITER"
                                    print*, "V0=", V0, " Vprim0=", Vprim0
                                    print*, "V1=", V1, " Vprim1=", Vprim1
                                    print*, "A=", A, " B=",B, " C=", C
                                    print*, "S1=", solution1, " S2=", solution2, "Int=[", TABLE(I,1), " , ", TABLE(I+1,1), "]"
                                    print*, "RISQUE DE DERIVEE NULLE (degre2), A TRAITER"
                                    stop -2
                                endif

                            endif
                        endif

                    else if(B .NE. 0.0D0) THEN !polynome de degre 2
                        solution1 = - (0.5D0*(C / B))
                        if( (solution1 >= TABLE(I,1)) .AND. (solution1 <= TABLE(I+1,1)) ) then

                            !solution basique : on passe en lineaire sans assurer la continuite de la pente
                            TABLE(I,3)=0
                            TABLE(I,4)=0
                            TABLE(I,5)=(V1-V0)/(X1-X0)
                            TABLE(I,6)=V0-TABLE(I,5)*X0

                            if( (pente_nulle_interdite) .AND. (TABLE(I,5)==0.0D0)) then
                                print*, "DERIVEE NULLE, A TRAITER"
                                print*, "V0=", V0, " Vprim0=", Vprim0
                                print*, "V1=", V1, " Vprim1=", Vprim1
                                print*, " B=",B, " C=", C
                                print*, "S1=", solution1, "Int=[", TABLE(I,1), " , ", TABLE(I+1,1), "]"
                                print*, "RISQUE DE DERIVEE NULLE (degre1), A TRAITER"
                                stop -3
                            endif

                        endif

                    else if( C == 0.0D0) then ! polynome de degre 1 (par deduction) ET pente nulle
                        ! dans le cas : pente nulle interdite sur tout un intervalle (pour SORP par exemple)
                        if(pente_nulle_interdite) then
                            print*, "DERIVEE NULLE, A TRAITER (plateau)"
                            print*, "V0=", V0, " Vprim0=", Vprim0
                            print*, "V1=", V1, " Vprim1=", Vprim1
                            print*, "Int=[", TABLE(I,1), " , ", TABLE(I+1,1), "]"
                            STOP -4
                        endif
                    endif

                endif !fin test sur la pente


                x0 = x1
                V0 = V1
                Vprim0 = Vprim1
            ENDDO

            ! pour le dernier indice (donc pour HR=1.01), on reste sur un plateau ?
            ! section non utilisee par la recherche dichotomique
            TABLE(nombre_abscisses,3)=0
            TABLE(nombre_abscisses,4)=0
            TABLE(nombre_abscisses,5)=0
            TABLE(nombre_abscisses,6)=V0


            if(debug_table) then
                write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_quadra1.txt"
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (6)

                DO I=1,nombre_abscisses-1
                    DO X0=TABLE(I,1) , TABLE(I+1,1) , 0.0001D0

                        !X1 = X0 - TABLE(I,1) !s'il faut appliquer un offset
                        X1 = X0               ! sans application d'offset lorsque A,B,C,D sont corriges
                        V0 = TABLE(I,3)*X1*X1*X1 + TABLE(I,4)*X1*X1 + TABLE(I,5)*X1 + TABLE(I,6)

                        WRITE(LU,TRIM(FMT)) X0 , V0 , TABLE(I,3), TABLE(I,4), TABLE(I,5), TABLE(I,6)
                    ENDDO
                ENDDO
                CLOSE(LU)
            endif


            ! construction de la table pour un acces par dichotomie 
            ! la taille utile du tableau est stockee dans le DERNIER element du tableau
            ! l'info stockee correspond donc a l'indice de la derniere valeur utile
            TABLE(SIZE_MAT_TABLE,3) = nombre_abscisses   

            if(debug_table) then
                write(FNAME, "(A,I0,A)") "test_table_" // TRIM(ADJUSTL(obj_name)) // "_" // TRIM(ADJUSTL(type_name)) // "_modele.txt"
                OPEN(LU,FILE=FNAME,FORM='FORMATTED',STATUS='REPLACE')
                WRITE(FMT,'("(",i4,"(E13.7,:,""  ""))")') (5)
                
                DO Indice_tabulation=1,nombre_abscisses
                    WRITE(LU,TRIM(FMT)) table(Indice_tabulation,1), table(Indice_tabulation,3), table(Indice_tabulation,4), table(Indice_tabulation,5), table(Indice_tabulation,6)
                ENDDO
                CLOSE(LU)
            endif

        endif
   
    
        END SUBROUTINE CLEAN_MAT_TABLE_BIS















    END MODULE PROC_MAT_MODULE
