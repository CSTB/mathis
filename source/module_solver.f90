    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE SOLVER_MODULE

    USE TYPE_MATHIS_MODULE
    USE GLOBAL_VAR_MODULE
    USE ENVIRO_MODULE
    USE READ_DATA_MODULE,ONLY : READ_DATA,PRINT_DEFAULT
    USE WALL_SOLVER_MODULE,ONLY : SOLVE_THERMAL_WALLS,RES_FLUX_THERMAL_WALLS,SOLVE_HYGRO_WALLS,UPDATE
    USE PROC_SAVE_RES_MODULE,ONLY: WRITE_TITLES,WRITE_RES,WRITE_BUFFERS
	USE POST_ATEC_MODULE,ONLY: INIT_ATEC_RES,CLEAN_ATEC_FILE,CALC_ATEC_RES,WRITE_ATEC_RES 

    IMPLICIT NONE

    INTEGER												:: COUNTLOOP,COUNTREDUC,COUNTSTAT,COUNTTOL,ISUC
    DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE			:: QEXT,QSOURCE,HSOURCE,HSOURCERAD,QOUTGOOD,QNODE,HNODE
    DOUBLE PRECISION, DIMENSION(:,:),ALLOCATABLE		:: QLOC
    INTEGER,DIMENSION(:),ALLOCATABLE					:: IP
    INTEGER												:: NAIRNODE,NWARNING
    TYPE(FLUX_ME)										:: FMUR
    DOUBLE PRECISION									:: RESMAX,RELAX
    

    CONTAINS

    SUBROUTINE INIT_SOLVER(FILE,LU,ISATEC,TDEBUT,TFIN,PASDETEMPS,NL,NB)

    INTEGER						:: K
    DOUBLE PRECISION,INTENT(OUT)	:: TDEBUT,TFIN,PASDETEMPS
    CHARACTER(256),INTENT(IN)		:: FILE
    INTEGER,INTENT(IN)			:: LU
    INTEGER,INTENT(OUT)			:: ISATEC,NL,NB
    INTEGER(2)					:: SIX=6
    INTEGER                     :: ERR

    COUNTLOOP=0;COUNTREDUC=0;COUNTSTAT=1;COUNTTOL=0;ISUC=0
    NWARNING=0
    
    INPUT_FILE=FILE
    LUOUT=LU
    IF (TRIM(OUTPUT_FILE).EQ.'null') OUTPUT_FILE=TRIM(INPUT_FILE)//'.out'            
    OPEN(LU0,FILE=OUTPUT_FILE,FORM='FORMATTED',STATUS='OLD',IOSTAT=ERR)
    IF (ERR==0) CLOSE(LU0,STATUS='DELETE')    
    IF (LUOUT.NE.0) THEN
        OPEN(LUOUT,FILE=OUTPUT_FILE,FORM='FORMATTED',STATUS='REPLACE')
    ENDIF    
    CALL WRITE_CALC_HEADER
    IF (TRIM(INPUT_FILE)=='null') THEN
        WRITE(MESSAGE,'(A)') ' '
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ELSEIF (TRIM(INPUT_FILE)=='default') THEN
        CALL PRINT_DEFAULT
        WRITE(MESSAGE,'(A)') ' '
        CALL SHUTDOWN(MESSAGE,LUOUT) 
    ENDIF
    IF (LUOUT.NE.0) WRITE(0,'(/A)') 'Mathis is running...'    
    WRITE(LUOUT,'(/A)') 'Reading data...'     
    CALL READ_DATA ! LECTURE DES DONNEES
    WRITE(LUOUT,'(/A)') 'Data read completed'    
    CALL ADD_OBJ_MOD(MODRT)
    
    DO K=1,N_BRANCHE
        CALL TRANSLATE_BRANCHTYPE(BRANCHERT(K))
    ENDDO
    
    DO K=1,N_MUR
        CALL TRANSLATE_WALLTYPE(MURRT(K))
    ENDDO

    DO K=1,N_CTRL
        CALL TRANSLATE_CTRLTYPE(CTRLRT(K))
    ENDDO

    CALL TRANSLATE_PSATMODEL()


    IF (N_MISC>1) THEN
        CALL SHUTDOWN('ERROR : You must specify only one &MISC line',LUOUT)
    ELSEIF (N_MISC==0) THEN
        CALL SHUTDOWN('ERROR : You must specify at least one &MISC line',LUOUT)
    ENDIF
     IF (N_EXT>1) THEN
        CALL SHUTDOWN('ERROR : You must specify only one &EXT line',LUOUT)
    ELSEIF (N_EXT==0) THEN
        CALL SHUTDOWN('ERROR : You must specify at least one &EXT line',LUOUT)
    ENDIF
    IF (N_LOC==0) CALL SHUTDOWN('ERROR: you must specify at least one &LOC line',LUOUT)
    
    CALL INIT_SYSTEM !INITIALISATION DES VARIABLES
    CALL SOLVE_THERMAL_WALLS(0) !INITIALISATION DES TEMPERATURES DES MURS
    CALL CALC_TWMEAN ! Initialisation de la temperature moyenne radiante
    CALL WRITE_TITLES(LU2)	 ! INITIALISATION DES FICHIERS DE STOCKAGE DES RESULTATS

    
    IF (ALLOCATED(RES)) DEALLOCATE(RES)
    IF (ALLOCATED(Y)) DEALLOCATE(Y)
    IF (ALLOCATED(Y0)) DEALLOCATE(Y0)
    IF (ALLOCATED(Y0GOOD)) DEALLOCATE(Y0GOOD)
    IF (ALLOCATED(YTAMPON)) DEALLOCATE(YTAMPON)
    IF (ALLOCATED(CTAMPON)) DEALLOCATE(CTAMPON)
    IF (ALLOCATED(C)) DEALLOCATE(C)
    IF (ALLOCATED(DYDT)) DEALLOCATE(DYDT)
    IF (ALLOCATED(ATOL)) DEALLOCATE(ATOL)
    IF (ALLOCATED(RTOL)) DEALLOCATE(RTOL)
    
    ! FD => NEQ =  nombre total des équations de bilan dans les noeuds
    ! FD => NEQ + N_DUCT =>nombre total d'équations du système aéraulique (bilan dans les noeuds + bilan de quantite de mouvement dans les branche a loi pression vs debit
    NEQ=(2+N_SPEC)*N_LOC 
    IF (.NOT.ALLOCATED(RES)) ALLOCATE(RES(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(Y)) ALLOCATE(Y(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(Y0)) ALLOCATE(Y0(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(Y0GOOD)) ALLOCATE(Y0GOOD(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(YTAMPON)) ALLOCATE(YTAMPON(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(YTAMPONMAIN)) ALLOCATE(YTAMPONMAIN(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(CTAMPON)) ALLOCATE(CTAMPON(N_LOC))
    IF (.NOT.ALLOCATED(C)) ALLOCATE(C(N_LOC))
    IF (.NOT.ALLOCATED(DYDT)) ALLOCATE(DYDT(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(ATOL)) ALLOCATE(ATOL(NEQ+N_DUCT))
    IF (.NOT.ALLOCATED(RTOL)) ALLOCATE(RTOL(NEQ+N_DUCT))

    Y(1:N_LOC)=(PREF+DP)/PREF
    ATOL(1:N_LOC)=PTOL
    RTOL(1:N_LOC)=PTOL
    RELAX=PRELAX
    C=0.D0
    CTAMPON=0.D0  
    
    Y(N_LOC+1:2*N_LOC)=(T15)/(TREF)
    ATOL(N_LOC+1:2*N_LOC)=TTOL
    RTOL(N_LOC+1:2*N_LOC)=TTOL

    DO K=1,N_SPEC
        Y((1+K)*N_LOC+1:(2+K)*N_LOC)=(YK15(:,K))/SPECRT(K)%YKREF
        ATOL((1+K)*N_LOC+1:(2+K)*N_LOC)=YKTOL
        RTOL((1+K)*N_LOC+1:(2+K)*N_LOC)=YKTOL
    ENDDO

    Y(NEQ+1:NEQ+N_DUCT)=1.D0
    ATOL(NEQ+1:NEQ+N_DUCT)=QMTOL
    RTOL(NEQ+1:NEQ+N_DUCT)=QMRTOL

    Y0=Y
    Y0GOOD=Y0
    YTAMPON=Y0
    YTAMPONMAIN=Y0
    DYDT=0.D0

    IF (ATEC.EQV..TRUE.) THEN
        ISATEC=1
        CALL INIT_ATEC_RES(LU2)
    ELSE
        ISATEC=0
		CALL CLEAN_ATEC_FILE(LU2)
    ENDIF
    TDEBUT=TETA0
    TFIN=TETAEND
    PASDETEMPS=DTETA
    NL=N_LOC
    NB=N_BRANCHE

    CALL INIT_Q_UNCOUPLED

    END SUBROUTINE INIT_SOLVER


    !SAM : en priorite 1 , bien comprendre comment est organise le vecteur X (ou Y, YTEMPON ...)


    SUBROUTINE SOLVE_SYSTEM(N,LWA)

    INTEGER,INTENT(IN)						:: N,LWA
    DOUBLE PRECISION,DIMENSION(N)           :: LOCALRES,X
    DOUBLE PRECISION,DIMENSION(LWA)	        :: WA
    DOUBLE PRECISION                        :: RESMAX1,RESMAX2,RESMAX3
    INTEGER									:: INFO,I,J,K,M,L,OLDSTATCALL,NBRELAX

    Y=YTAMPON

    IF ((STATCALL==0)) THEN
        X(1:N_DUCT)=Y(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF ((STATCALL==1).OR.(STATCALL==8)) THEN
        X(1:N_PZONE)=Y(IDNPZONE)
        X(N_PZONE+1:N_PZONE+N_DUCT)=Y(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF (STATCALL==2) THEN
        X(1:NEQ-2*N_LOC)=Y(2*N_LOC+1:NEQ)
    ENDIF
    IF ((STATCALL==7).OR.(STATCALL==9)) THEN
        X(1:N_PZONE)=Y(IDNPZONE)
        X(N_PZONE+1:N_PZONE+N_SPEC*N_LOC)=Y(2*N_LOC+1:NEQ)
        X(N_PZONE+N_SPEC*N_LOC+1:N_PZONE+N_SPEC*N_LOC+N_DUCT)=Y(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF (STATCALL==3) THEN
        IF (NAIRNODE==N_LOC) THEN
            X(1:N_LOC)=Y(N_LOC+1:2*N_LOC)
        ELSE
            DO K=1,N_NODE
                X(K)=Y(N_LOC+IDNNODE(K))
            ENDDO
        ENDIF
        J=NAIRNODE
        DO I=1,N_MUR
            IF (MURRT(I)%enum_walltype==wall_thermal) THEN
                X(J+1)=MURRT(I)%TPIN
                X(J+2)=MURRT(I)%TPOUT
                J=J+2
            ENDIF
        ENDDO
        UPDATE=0
    ENDIF
    IF (STATCALL==4) THEN
        X(1:N_PZONE)=Y(IDNPZONE)
        IF (NAIRNODE==N_LOC) THEN
            X(N_PZONE+1:N_PZONE+N_LOC)=Y(N_LOC+1:2*N_LOC)
        ELSEIF (NAIRNODE==N_NODE) THEN
            DO K=1,N_NODE
                X(N_PZONE+K)=Y(N_LOC+IDNNODE(K))
            ENDDO
        ENDIF
        X(N_PZONE+NAIRNODE+1:N_PZONE+NAIRNODE+N_LOC*N_SPEC)=Y(2*N_LOC+1:NEQ)
        X(N_PZONE+NAIRNODE+N_LOC*N_SPEC+1:N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT)=Y(NEQ+1:NEQ+N_DUCT)
        J=N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT
        DO I=1,N_MUR
            IF (MURRT(I)%enum_walltype==wall_thermal) THEN
                X(J+1)=MURRT(I)%TPIN
                X(J+2)=MURRT(I)%TPOUT
                J=J+2
            ENDIF
        ENDDO
        UPDATE=0
    ENDIF
    IF (STATCALL==5) THEN
        X(1:N_PZONE)=Y(IDNPZONE)
        IF (NAIRNODE==N_LOC) THEN
            X(N_PZONE+1:N_PZONE+N_LOC)=Y(N_LOC+1:2*N_LOC)
        ELSE
            DO K=1,N_NODE
                X(N_PZONE+K)=Y(N_LOC+IDNNODE(K))
            ENDDO
        ENDIF
        X(N_PZONE+NAIRNODE+1:N_PZONE+NAIRNODE+N_DUCT)=Y(NEQ+1:NEQ+N_DUCT)
        J=N_PZONE+NAIRNODE+N_DUCT
        DO I=1,N_MUR
            IF (MURRT(I)%enum_walltype==wall_thermal) THEN
                X(J+1)=MURRT(I)%TPIN
                X(J+2)=MURRT(I)%TPOUT
                J=J+2
            ENDIF
        ENDDO
        UPDATE=0
    ENDIF

    CALL HYBRD1(RESIDUS,N,X,LOCALRES,HYBRIDTOL,INFO,WA,LWA) !RESOLUTION DU SYSTEME

    OLDSTATCALL=STATCALL
    STATCALL=6	!CALCUL DES RESIDUS
    CALL RESIDUS(NEQ+N_DUCT,Y,RES,0)

    RESMAX1=MINVAL(ATOL-DABS(RES)*DTIME)
    RESMAX2=0.D0
    RESMAX3=0.D0
    DO K=1,SIZE(Y)
        IF (DABS(Y(K)).NE.0.D0) THEN
            RESMAX2=MIN(RESMAX2,RTOL(K)-(DABS((RES(K)*DTIME/Y(K)))))
            RESMAX3=MIN(RESMAX3,RTOL(K)-(DABS((Y(K)-YTAMPONMAIN(K))/Y(K))))
        ELSE
            RESMAX2=MIN(RESMAX2,ATOL(K)-(DABS((RES(K)*DTIME))))
            RESMAX3=MIN(RESMAX3,ATOL(K)-(DABS(Y(K)-YTAMPONMAIN(K))))
        ENDIF
    ENDDO
    
    SELECT CASE (CONVCRIT)
        CASE DEFAULT
        RESMAX=MAX(RESMAX1,RESMAX2,RESMAX3)
    CASE (1)
        RESMAX=RESMAX1
    CASE (2)
        RESMAX=RESMAX2
    CASE (3)
        RESMAX=MIN(RESMAX1,RESMAX2)
    CASE (4)
        RESMAX=MINVAL(ATOL-DABS(RES))
    END SELECT

    IF (DEBUGPLUS.EQV..TRUE.)  THEN
        WRITE(LUOUT,'(A9,I2,A8,I2)')    'STATCALL=',OLDSTATCALL,' - FLAG=',FLAG
        CALL DISPLAY_RESMAX
    ENDIF

    !mise a jour de Ytampon si FLAG<>-2
    !et sous relaxation des pressions si RESMAX<0.D0
    IF (FLAG.NE.-2) THEN
        IF (RESMAX<0.D0) THEN
            IF ((OLDSTATCALL==1).OR.(OLDSTATCALL==4).OR.(OLDSTATCALL==7).OR.(OLDSTATCALL==9)) THEN
                !application du facteur de sous relaxation sur la pression
                Y(1:N_LOC)=RELAX*Y(1:N_LOC)+(1.D0-RELAX)*YTAMPON(1:N_LOC)
                IF (RELAX==1.D0) THEN
                    !application d'un facteur de sous relaxation dynamique sur la pression
                    C(1:N_LOC)=YTAMPON(1:N_LOC)-Y(1:N_LOC)
                    NBRELAX=0
                    DO I=1,N_LOC
                        IF (CTAMPON(I).NE.0.D0) THEN
                            IF ((C(I)/CTAMPON(I).LT.-0.5D0).AND.(1.D0-C(I)/CTAMPON(I).NE.0.D0)) THEN
                                Y(I)=YTAMPON(I)-C(I)/(1.D0-C(I)/CTAMPON(I))
                                NBRELAX=NBRELAX+1
                            ENDIF
                        ENDIF
                    ENDDO
                    IF ((NBRELAX>0).AND.(DEBUG.EQV..TRUE.)) THEN
                        write(LUOUT,'(A,I1,A,I3,A)') '              Statcall= ',OLDSTATCALL,' - Pressure relaxation for ',NBRELAX,' nodes'
                        write(LUOUT,'(A)') ''
                    ENDIF
                ENDIF
                CTAMPON(1:N_LOC)=YTAMPON(1:N_LOC)-Y(1:N_LOC)
                STATCALL=OLDSTATCALL
                CALL FCN(NEQ+N_DUCT,TIME,Y,DYDT)
            ENDIF
        ENDIF
        YTAMPON=Y
    ENDIF

    END SUBROUTINE SOLVE_SYSTEM

    !SAM : en priorite 2, bien comprendre comment est organise le vecteur X (ou Y, YTEMPON ...) a ce niveau

    SUBROUTINE RESIDUS(N,X,RES,JFLAG)
    !CALCULE LES RESIDUS
    INTEGER										:: M,N,JFLAG,KFLAG,K,I,J,L
    DOUBLE PRECISION,DIMENSION(N)				:: X,RES
    DOUBLE PRECISION,DIMENSION(NEQ+N_DUCT)      :: YRES

    RES=0.D0
    IF (STATCALL==0) THEN
        Y(NEQ+1:NEQ+N_DUCT)=X(1:N_DUCT)
    ENDIF
    IF ((STATCALL==1).OR.(STATCALL==8)) THEN
        Y(IDNPZONE)=X(1:N_PZONE)
        IF (N_PZONE<N_LOC) Y(IDNIZONE)=Y(LOCRT(IDNIZONE)%IDNPZONE)
        Y(NEQ+1:NEQ+N_DUCT)=X(N_PZONE+1:N_PZONE+N_DUCT)
    ENDIF
    IF (STATCALL==2) THEN
        Y(2*N_LOC+1:NEQ)=X(1:NEQ-2*N_LOC)
    ENDIF
    IF ((STATCALL==7).OR.(STATCALL==9)) THEN
        Y(IDNPZONE)=X(1:N_PZONE)
        IF (N_PZONE<N_LOC) Y(IDNIZONE)=Y(LOCRT(IDNIZONE)%IDNPZONE)
        Y(2*N_LOC+1:NEQ)=X(N_PZONE+1:N_PZONE+N_SPEC*N_LOC)
        Y(NEQ+1:NEQ+N_DUCT)=X(N_PZONE+N_SPEC*N_LOC+1:N_PZONE+N_SPEC*N_LOC+N_DUCT)
    ENDIF
    IF (STATCALL==3) THEN
        IF (NAIRNODE==N_LOC) THEN
            Y(N_LOC+1:2*N_LOC)=X(1:N_LOC)
        ELSE
            DO K=1,N_NODE
                Y(N_LOC+IDNNODE(K))=X(K)
            ENDDO
        ENDIF
        J=NAIRNODE
        DO I=1,N_MUR
            IF (MURRT(I)%enum_walltype==wall_thermal) THEN
                MURRT(I)%TPIN=X(J+1)
                MURRT(I)%TPOUT=X(J+2)
                J=J+2
            ENDIF
        ENDDO
    ENDIF
    IF (STATCALL==4) THEN
        Y(IDNPZONE)=X(1:N_PZONE)
        IF (N_PZONE<N_LOC) Y(IDNIZONE)=Y(LOCRT(IDNIZONE)%IDNPZONE)
        IF (NAIRNODE==N_LOC) THEN
            Y(N_LOC+1:2*N_LOC)=X(N_PZONE+1:N_PZONE+N_LOC)
        ELSEIF (NAIRNODE==N_NODE) THEN
            DO K=1,N_NODE
                Y(N_LOC+IDNNODE(K))=X(N_PZONE+K)
            ENDDO
        ENDIF
        Y(2*N_LOC+1:NEQ)=X(N_PZONE+NAIRNODE+1:N_PZONE+NAIRNODE+N_LOC*N_SPEC)
        Y(NEQ+1:NEQ+N_DUCT)=X(N_PZONE+NAIRNODE+N_LOC*N_SPEC+1:N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT)
        J=N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT
        DO I=1,N_MUR
            IF (MURRT(I)%enum_walltype==wall_thermal) THEN
                MURRT(I)%TPIN=X(J+1)
                MURRT(I)%TPOUT=X(J+2)
                J=J+2
            ENDIF
        ENDDO
    ENDIF
    IF (STATCALL==5) THEN
        Y(IDNPZONE)=X(1:N_PZONE)
        IF (N_PZONE<N_LOC) Y(IDNIZONE)=Y(LOCRT(IDNIZONE)%IDNPZONE)
        IF (NAIRNODE==N_LOC) THEN
            Y(N_LOC+1:2*N_LOC)=X(N_PZONE+1:N_PZONE+N_LOC)
        ELSE
            DO K=1,N_NODE
                Y(N_LOC+IDNNODE(K))=X(N_PZONE+K)
            ENDDO
        ENDIF
        Y(NEQ+1:NEQ+N_DUCT)=X(N_PZONE+NAIRNODE+1:N_PZONE+NAIRNODE+N_DUCT)
        J=N_PZONE+NAIRNODE+N_DUCT
        DO I=1,N_MUR
            IF (MURRT(I)%enum_walltype==wall_thermal) THEN
                MURRT(I)%TPIN=X(J+1)
                MURRT(I)%TPOUT=X(J+2)
                J=J+2
            ENDIF
        ENDDO
    ENDIF
    IF (STATCALL==6) THEN
        Y=X
    ENDIF
    !ON CALCULE LES DERIVEES
    CALL FCN(NEQ+N_DUCT,TIME,Y,DYDT)
    YRES=DYDT-(Y-Y0)/DTIME
    !traitement Boussinesq
    IF (BOUSSINESQ==.TRUE.) THEN
        YRES(1:2*N_LOC)=DYDT(1:2*N_LOC)
    ENDIF
    !Traitement des noeuds sans volume
    IF (N_NODE.NE.0) THEN
        YRES(IDNNODE)=DYDT(IDNNODE)
        YRES(N_LOC+IDNNODE)=DYDT(N_LOC+IDNNODE)
        DO K=1,N_SPEC
            YRES(2*N_LOC+(K-1)*N_LOC+IDNNODE)=DYDT(2*N_LOC+(K-1)*N_LOC+IDNNODE)
        ENDDO
    ENDIF
    !traitement du QUASISTAT==.TRUE.
    IF (QUASISTAT.EQV..TRUE.) THEN
        YRES(IDNROOM)=DYDT(IDNROOM)
        YRES(NEQ+1:NEQ+N_DUCT)=DYDT(NEQ+1:NEQ+N_DUCT)
    ENDIF

    !ON EN DEDUIT LES RESIDUS
    IF (STATCALL==0) THEN
        RES(1:N_DUCT)=YRES(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF (STATCALL==1) THEN
        IF ((QUASISTAT==.FALSE.).AND.((ISOTHERMAL.EQV..TRUE.).OR.((TIME==TETA0).AND.(GUESS_POWER.EQV..TRUE.)))) THEN
            RES(1:N_PZONE)=YRES(IDNPZONE)
        ELSE
            RES(1:N_PZONE)=YRES(N_LOC+IDNPZONE)
        ENDIF
        RES(N_PZONE+1:N_PZONE+N_DUCT)=YRES(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF (STATCALL==8) THEN
        RES(1:N_PZONE)=YRES(IDNPZONE)
        RES(N_PZONE+1:N_PZONE+N_DUCT)=YRES(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF (STATCALL==2) THEN
        RES(1:N_SPEC*N_LOC)=YRES(2*N_LOC+1:NEQ)
    ENDIF
    IF (STATCALL==7) THEN
        RES(1:N_PZONE)=YRES(N_LOC+IDNPZONE)
        RES(N_PZONE+1:N_PZONE+N_SPEC*N_LOC)=YRES(2*N_LOC+1:NEQ)
        RES(N_PZONE+N_SPEC*N_LOC+1:N_PZONE+N_SPEC*N_LOC+N_DUCT)=YRES(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF (STATCALL==9) THEN
        RES(1:N_PZONE)=YRES(IDNPZONE)
        RES(N_PZONE+1:N_PZONE+N_SPEC*N_LOC)=YRES(2*N_LOC+1:NEQ)
        RES(N_PZONE+N_SPEC*N_LOC+1:N_PZONE+N_SPEC*N_LOC+N_DUCT)=YRES(NEQ+1:NEQ+N_DUCT)
    ENDIF
    IF (STATCALL==3) THEN
        IF (NAIRNODE==N_LOC) THEN
            RES(1:NAIRNODE)=YRES(1:N_LOC)
        ELSE
            RES(1:NAIRNODE)=YRES(IDNNODE)
        ENDIF
        IF (N_WTHERM.NE.0) THEN
            CALL RES_FLUX_THERMAL_WALLS(2*N_WTHERM,X(NAIRNODE+1:N),RES(NAIRNODE+1:N),KFLAG)
        ENDIF
    ENDIF
    IF (STATCALL==4) THEN
        RES(1:N_PZONE)=YRES(IDNPZONE)
        IF (NAIRNODE==N_LOC) THEN
            RES(N_PZONE+1:N_PZONE+N_LOC)=YRES(N_LOC+1:2*N_LOC)
        ELSE
            DO K=1,N_NODE
                RES(N_PZONE+K)=YRES(N_LOC+IDNNODE(K))
            ENDDO
        ENDIF
        RES(N_PZONE+NAIRNODE+1:N_PZONE+NAIRNODE+N_LOC*N_SPEC)=YRES(2*N_LOC+1:NEQ)
        RES(N_PZONE+NAIRNODE+N_LOC*N_SPEC+1:N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT)=YRES(NEQ+1:NEQ+N_DUCT)
        IF (N_WTHERM.NE.0) THEN
            CALL RES_FLUX_THERMAL_WALLS(2*N_WTHERM,X(N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT+1:N),RES(N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT+1:N),KFLAG)
        ENDIF
    ENDIF
    IF (STATCALL==5) THEN
        RES(1:N_PZONE)=YRES(IDNPZONE)
        IF (NAIRNODE==N_LOC) THEN
            RES(N_PZONE+1:N_PZONE+N_LOC)=YRES(N_LOC+1:2*N_LOC)
        ELSE
            DO K=1,N_NODE
                RES(N_PZONE+K)=YRES(N_LOC+IDNNODE(K))
            ENDDO
        ENDIF
        RES(N_PZONE+NAIRNODE+1:N_PZONE+NAIRNODE+N_DUCT)=YRES(NEQ+1:NEQ+N_DUCT)
        IF (N_WTHERM.NE.0) THEN
            CALL RES_FLUX_THERMAL_WALLS(2*N_WTHERM,X(N_PZONE+NAIRNODE+N_DUCT+1:N),RES(N_PZONE+NAIRNODE+N_DUCT+1:N),KFLAG)
        ENDIF
    ENDIF
    IF (STATCALL==6) THEN
        RES=YRES
    ENDIF
    
    DO K=1,SIZE(DYDT)
        IF (DYDT(K).EQ.100000.D0) Y(K)=YTAMPON(K)
    ENDDO
    

    END SUBROUTINE RESIDUS

    SUBROUTINE SOLVE_TIMESTEP(TASKED,DTASKED,INFOOPT)
    DOUBLE PRECISION,INTENT(IN)		:: TASKED,DTASKED
    INTEGER,DIMENSION(13),INTENT(IN),OPTIONAL :: INFOOPT
    INTEGER,DIMENSION(13)			:: INFO
    DOUBLE PRECISION				:: T
    INTEGER							:: ITERLOOP,ISPEC,ITARGET=1
    INTEGER							:: N,LWA,K,I,OLDSTATCALL

    IF (PRESENT(INFOOPT)) THEN
        INFO=INFOOPT
    ELSE
        INFO=-9999
    ENDIF

    IF ((INFO(8)==1).OR.(INFO(8)==2)) THEN

    ELSEIF (INFO(8)==-1) THEN
        IF (ATEC.EQV..TRUE.) CALL WRITE_ATEC_RES(LU2)
        CALL WRITE_BUFFERS(LU2)
        CALL UPDATE_MODRT(2)
    ELSEIF (INFO(8)==3)THEN
        DTETA=DTASKED
        TETA0=TASKED+DTASKED
        TIME0=TETA0
        HOUR=MODULO(TETA0/3600.D0,24.D0)
        HOUR0=HOUR
        JOUR0=WEEKDAY+HOUR/24.D0
        JOUR=JOUR0
        QUANTIEME0=1.D0+TIME0/3600/24.D0
        QUANTIEME=QUANTIEME0
    ELSE
        IF ((INFO(7).NE.0).AND.(INFO(7).NE.-9999)) THEN
            CALL REWIND_SYSTEM(0)
            IF (COUNTSTAT>1) THEN
                DTETASTAT=DTETASTAT/(2.D0**(COUNTSTAT-1))
                COUNTSTAT=1
            ENDIF
        ENDIF
        IF ((INFO(7)==0).OR.(INFO(7)==-9999)) THEN
            Y0GOOD=Y
        ENDIF
        Y0=Y0GOOD

        T=TASKED-DTASKED+DTETA

100     ITERLOOP=0

        TIME=T
        IF (TIME==TETA0) THEN
            DTIME=DTETASTAT
        ELSE
            DTIME=DTETA
        ENDIF

        !EVALUATION DE L'ETAT DES CONTROLEURS FONCTIONS DU TEMPS UNIQUEMENT
        CALL EVAL_CTRLRT(0)
  
        YTAMPON=Y
        YTAMPONMAIN=YTAMPON
        
        STATCALL=1 !DETERMINATION DU CHAMP DE PRESSION
        N=N_PZONE+N_DUCT
        LWA=((N)*(3*(N)+13))/2
        CALL SOLVE_SYSTEM(N,LWA)

        DO WHILE (((RESMAX<0.D0).AND.(ITERLOOP<NSTATMAX)).OR.(ITERLOOP<1))

            IF ((ISOTHERMAL.EQV..TRUE.).OR.((GUESS_POWER.EQV..TRUE.).AND.(TIME==TETA0))) THEN
                Y(N_LOC+IDNROOM)=(T15(IDNROOM))/TREF
                YTAMPON=Y
            ENDIF

            IF (ISOTHERMALNODE.EQV..TRUE.) THEN
                Y(N_LOC+IDNNODE)=(T15(IDNNODE))/TREF
                YTAMPON=Y
            ENDIF

            IF (N_SPEC>0) THEN
                STATCALL=2 !DETERMINATION DES FRACTIONS MASSIQUES
                N=N_SPEC*N_LOC
                LWA=((N)*(3*(N)+13))/2
                CALL SOLVE_SYSTEM(N,LWA)
            ENDIF

            IF (UNCOUPLED.EQV..FALSE.) THEN
                STATCALL=3 !DETERMINATION DES TEMPERATURES
                NAIRNODE=0
                IF ((GUESS_POWER.EQV..FALSE.).OR.((ISOTHERMAL.EQV..FALSE.).AND.(TIME>TETA0))) THEN
                    NAIRNODE=N_LOC
                ELSEIF (ISOTHERMALNODE.EQV..FALSE.) THEN
                    NAIRNODE=N_NODE
                ENDIF
                IF (UNCOUPLED_THERMAL_WALLS.EQV..FALSE.) THEN
                    N=NAIRNODE+2*N_WTHERM
                ELSE
                    N=NAIRNODE
                ENDIF
                IF (N.NE.0) THEN
                    LWA=((N)*(3*(N)+13))/2
                    CALL SOLVE_SYSTEM(N,LWA)
                ENDIF
            ENDIF

            IF (UNCOUPLED.EQV..TRUE.)	THEN
                STATCALL=3 !DETERMINATION DES TEMPERATURES
                IF ((GUESS_POWER.EQV..FALSE.).OR.((ISOTHERMAL.EQV..FALSE.).AND.(TIME>TETA0))) THEN
                    N=N_LOC
                ELSEIF (ISOTHERMALNODE.EQV..FALSE.) THEN
                    N=N_NODE
                ELSE
                    N=0
                ENDIF
                IF (N.NE.0) THEN
                    CALL SOLVE_UNCOUPLED_TEMPERATURE(N)
                ENDIF
                IF (((N==0).OR.(UNCOUPLED_THERMAL_WALLS.EQV..TRUE.)).AND.(TIME==TETA0)) THEN
                    CALL SOLVE_THERMAL_WALLS(0)
                ENDIF
            ENDIF
 
            STATCALL=1 !DETERMINATION DU CHAMP DE PRESSION
            N=N_PZONE+N_DUCT
            LWA=((N)*(3*(N)+13))/2
            CALL SOLVE_SYSTEM(N,LWA)
            
            IF ((RESMAX<0.D0).OR.(FLAG==-2)) THEN

                IF (((RESMAX<0.D0).OR.(FLAG==-2)).AND.((UNCOUPLED.EQV..FALSE.))) THEN
                    STATCALL=4 !DETERMINATION DU SYSTEME GLOBAL
                    IF ((GUESS_POWER.EQV..FALSE.).OR.((ISOTHERMAL.EQV..FALSE.).AND.(TIME>TETA0))) THEN
                        NAIRNODE=N_LOC
                    ELSEIF ((N_NODE.NE.0).AND.(ISOTHERMALNODE.EQV..FALSE.)) THEN
                        NAIRNODE=N_NODE
                    ENDIF
                    N=N_PZONE+NAIRNODE+N_LOC*N_SPEC+N_DUCT+2*N_WTHERM
                    LWA=((N)*(3*(N)+13))/2
                    CALL SOLVE_SYSTEM(N,LWA)
                ELSEIF (((RESMAX<0.D0).OR.(FLAG==-2)).AND.(BACKGROUND_SPECIE.NE.'WATER')) THEN
                    IF ((GUESS_POWER.EQV..FALSE.).OR.((ISOTHERMAL.EQV..FALSE.).AND.(TIME>TETA0))) THEN
                        STATCALL=9 !DETERMINATION DES FRACTIONS MASSIQUES ET DE LA PRESSION EN CONSERVANT L'ENERGIE
                        N=NEQ+N_DUCT-2*N_LOC+N_PZONE
                        LWA=((N)*(3*(N)+13))/2
                        CALL SOLVE_SYSTEM(N,LWA)
                    ENDIF
                    IF ((ISOTHERMAL==.TRUE.).OR.((GUESS_POWER.EQV..TRUE.).AND.(TIME==TETA0)))  THEN
                        STATCALL=7 !DETERMINATION DES FRACTIONS MASSIQUES ET DE LA PRESSION EN CONSERVANT LA MASSE
                        N=NEQ+N_DUCT-2*N_LOC+N_PZONE
                        LWA=((N)*(3*(N)+13))/2
                        CALL SOLVE_SYSTEM(N,LWA)
                        IF (((RESMAX<0.D0).OR.(FLAG==-2))) THEN
                            STATCALL=9 !DETERMINATION DES FRACTIONS MASSIQUES ET DE LA PRESSION EN CONSERVANT L'ENERGIE
                            N=NEQ+N_DUCT-2*N_LOC+N_PZONE
                            LWA=((N)*(3*(N)+13))/2
                            CALL SOLVE_SYSTEM(N,LWA)
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            ITERLOOP=ITERLOOP+1
            YTAMPONMAIN=YTAMPON
        ENDDO
       

        IF (TIME==TETA0) THEN
            IF (RESMAX<0.D0) THEN
                IF ((ITERLOOP<NSTATMAX).AND.(COUNTREDUC>=1)) THEN
                    IF (DEBUG.EQV..FALSE.) THEN
                        WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                    ELSE
                        WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                        WRITE(LUOUT,'(A,A,I4,A,F8.2)') '              Flow Solver -',' Iter = ',ITERLOOP,' - CFL=',CFLNUMBER
                        CALL DISPLAY_RESMAX
                    ENDIF
                    Y0=Y
                    CALL SOLVE_THERMAL_WALLS(2)
                    CALL EVAL_CTRLRT(2)
                    COUNTSTAT=COUNTSTAT+1
                    DTETASTAT=DTETASTAT*2.D0
                    WRITE(MESSAGE,'(A,F9.2,A)') 'False Time step multiplied by 2: Dt=',DTETASTAT,' s'
                    WRITE(LUOUT,'(A)') TRIM(MESSAGE)
                    COUNTREDUC=COUNTREDUC-1
                    IF ((COUNTREDUC.NE.0).AND.(COUNTSTAT<3)) GOTO 100
                ELSEIF ((ITERLOOP==NSTATMAX).AND.(COUNTREDUC<=3)) THEN
                    IF (DEBUG.EQV..FALSE.) THEN
                        WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                    ELSE
                        WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                        WRITE(LUOUT,'(A,A,I4,A,F8.2)') '              Flow Solver -',' Iter = ',ITERLOOP,' - CFL=',CFLNUMBER
                        CALL DISPLAY_RESMAX
                    ENDIF
                    COUNTREDUC=COUNTREDUC+1
                    Y=Y0
                    TIME=TIME0
                    CALL EVAL_CTRLRT(0)
                    CALL FCN(NEQ+N_DUCT,TIME0,Y,DYDT)
                    DTETASTAT=DTETASTAT/2.D0
                    WRITE(MESSAGE,'(A,F9.2,A)') 'False Time step divided by 2: Dt=',DTETASTAT,' s'
                    WRITE(LUOUT,'(A)') TRIM(MESSAGE)
                    GOTO 100
                ELSE
                    COUNTREDUC=0
                ENDIF
            ENDIF
        ENDIF

        IF ((((ITERLOOP==NSTATMAX).OR.(FLAG==-2)).AND.(TIME.NE.TETA0).AND.(COUNTREDUC.LE.3))) THEN
            IF (OUTPUT_MESSAGE_ONLY==.FALSE.) THEN
                IF (DEBUG.EQV..FALSE.) THEN
                    WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                ELSE
                    WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                    WRITE(LUOUT,'(A,A,I4,A,F8.2)') '              Flow Solver -',' Iter = ',ITERLOOP,' - CFL=',CFLNUMBER
                    CALL DISPLAY_RESMAX
                ENDIF
            ENDIF
            Y=Y0
            TIME=TIME0
            CALL EVAL_CTRLRT(0)
            CALL FCN(NEQ+N_DUCT,TIME0,Y,DYDT)
            DTETA=DTETA/2.D0
            T=TIME0+DTETA
            IF ((INFO(13)==1).OR.(INFO(7)==-9999)) COUNTLOOP=COUNTLOOP*2
            NSAVE=NSAVE*2
            COUNTREDUC=COUNTREDUC+1
            WRITE(MESSAGE,'(A,F7.2,A)') 'Time step divided by 2: Dt=',DTETA,' s'
            WRITE(LUOUT,'(A)') TRIM(MESSAGE)
            ISUC=2*ISUC
            ITARGET=2*ITARGET
            GOTO 100
        ELSEIF (TIME.NE.TETA0) THEN
            IF ((INFO(13)==1).OR.(INFO(7)==-9999)) THEN
                IF (ATEC_BEGIN>ATEC_END) THEN
                    IF ((ATEC.EQV..TRUE.).AND.((QUANTIEME>=ATEC_BEGIN).OR.(QUANTIEME<=ATEC_END+1.D0)))   CALL CALC_ATEC_RES
                ELSE
                    IF ((ATEC.EQV..TRUE.).AND.((QUANTIEME>=ATEC_BEGIN).AND.(QUANTIEME<=ATEC_END+1.D0)))	CALL CALC_ATEC_RES
                ENDIF
            ENDIF
            IF ((INFO(13)==1).OR.(INFO(7)==-9999)) COUNTLOOP=COUNTLOOP+1
            IF ((NSAVE.NE.0).AND.((COUNTLOOP==NSAVE).OR.(TIME==TETA0))) THEN
                IF ((INFO(13)==1).OR.(INFO(8)==3).OR.(INFO(7)==-9999)) THEN
                    CALL WRITE_RES(LU2) ! STOCKAGE DES RESULTATS
                    COUNTLOOP=0
                ENDIF
            ENDIF
            IF ((INFO(13)==1).OR.(INFO(7)==-9999)) THEN
                TIME0=T ! MISE A JOUR DE L'INSTANT PRECEDENT
                CALL UPDATE_SYSTEM(1)
            ELSEIF (INFO(13)==0) THEN
                CALL UPDATE_SYSTEM(0)
            ENDIF
            IF (COUNTREDUC>=1) THEN
                ISUC=ISUC+1
                IF (ISUC==ITARGET) THEN
                    DTETA=DTETA*2.D0
                    NSAVE=NSAVE/2
                    IF ((INFO(13)==1).OR.(INFO(7)==-9999)) COUNTLOOP=COUNTLOOP/2
                    COUNTREDUC=COUNTREDUC-1
                    WRITE(MESSAGE,'(A,F7.2,A)') 'Time step multiplied by 2: Dt=',DTETA,' s'
                    WRITE(LUOUT,'(A)') TRIM(MESSAGE)
                    ISUC=0
                    ITARGET=ITARGET/2
                ENDIF
            ENDIF
        ELSEIF ((NSAVE.NE.0).AND.(TIME==TETA0)) THEN
            IF ((INFO(8)==3).OR.(INFO(7)==-9999)) CALL WRITE_RES(LU2) ! STOCKAGE DES RESULTATS
            COUNTLOOP=0
        ENDIF
        IF (ITERLOOP<NSTATMAX) THEN
            NWARNING=0
            IF (TIME==TETA0) THEN
                COUNTREDUC=0
                WRITE(LUOUT,'(A)') 'Initial steady state calculation done'
                IF ((INFO(13)==1).OR.(INFO(7)==-9999)) THEN
                    CALL UPDATE_SYSTEM(1)
                ELSEIF (INFO(13)==0) THEN
                    CALL UPDATE_SYSTEM(0)
                ENDIF
                WRITE(LUOUT,'(/A)') 'Transient simulation...'             
            ENDIF
            IF (OUTPUT_MESSAGE_ONLY==.FALSE.) THEN
                IF (DEBUG.EQV..FALSE.) THEN
                    WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                ELSE
                    WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
                    WRITE(LUOUT,'(A,A,I4,A,F8.2)') '              Flow Solver -',' Iter = ',ITERLOOP,' - CFL=',CFLNUMBER
                    CALL DISPLAY_RESMAX
                ENDIF
            ENDIF
        ELSE
            NWARNING=NWARNING+1
            WRITE(LUOUT,'(A2,F11.3,A1,A1)') 't=',TIME/TIMESCALE,' ',TIMEUNIT
            WRITE(LUOUT,'(A,A,I4,A,F8.2)') '              Flow Solver -',' Iter = ',ITERLOOP,' - CFL=',CFLNUMBER
            CALL DISPLAY_RESMAX
            IF (NWARNING<4) WRITE(LUOUT,'(A)') 'Continue anyway...'
        ENDIF
        IF (FLAG==-2) THEN
            WRITE(MESSAGE,'(A,F11.3,A,A,A)') 'Convergence problem at t=',TIME/TIMESCALE,' ',TIMEUNIT,' : the solution has reached a range of non-physical values'
            CALL SHUTDOWN(TRIM(MESSAGE),LUOUT)
        ENDIF
        IF (NWARNING>=4) THEN
            WRITE(MESSAGE,'(A,F11.3,A,A,A)') 'Convergence problem at t=',TIME/TIMESCALE,' ',TIMEUNIT,' : too many successive warnings'
            CALL SHUTDOWN(TRIM(MESSAGE),LUOUT)
        ENDIF
        IF (T<TASKED) THEN ! attention aux arrondis, a revoir pour etre certain du test d'arret
            T=T+DTETA
            Y0=Y
            GOTO 100
        ENDIF
    ENDIF

    END SUBROUTINE SOLVE_TIMESTEP

    SUBROUTINE DISPLAY_RESMAX
    !reminder : must be call after STATCALL=6 only
    INTEGER :: K
    INTEGER,DIMENSION(:),ALLOCATABLE                    :: IPRES,ITRES,IYKRES,IQMRES

    ALLOCATE(IPRES(N_LOC))
    IPRES=MAXLOC(DABS(RES(1:N_LOC)))
    WRITE(LUOUT,'(A,E8.1,A,F8.2,A)') '              Max Pres('//TRIM(LOCRT(IPRES(1))%ID)//')=',MAXVAL(DABS(RES(1:N_LOC)))*DTIME,' DP= ',LOCRT(IPRES(1))%DP+MDEXT(1)%RHOBUOY*9.81D0*LOCRT(IPRES(1))%ALT,' Pa'
    ALLOCATE(ITRES(N_LOC))
    ITRES=MAXLOC(DABS(RES(N_LOC+1:2*N_LOC)))
    WRITE(LUOUT,'(A,E8.1,A,F8.2,A)') '              Max Tres('//TRIM(LOCRT(ITRES(1))%ID)//')=',MAXVAL(DABS(RES(N_LOC+1:2*N_LOC)))*DTIME,'  T= ',LOCRT(ITRES(1))%T15-273.15,' C'
    IF (N_SPEC.NE.0) THEN
        ALLOCATE(IYKRES(N_LOC))
        DO K=1,N_SPEC
            IYKRES=MAXLOC(DABS(RES((1+K)*N_LOC+1:(2+K)*N_LOC)))
            IF (SPECRT(K)%ID=='CO2') THEN
                WRITE(LUOUT,'(A,E8.1,A,F8.2,A)') '              Max '//TRIM(SPECRT(K)%ID)//'res('//TRIM(LOCRT(IYKRES(1))%ID)//')=',MAXVAL(DABS(RES((1+K)*N_LOC+1:(2+K)*N_LOC)))*DTIME,' YK= ',LOCRT(IYKRES(1))%YK(K)*1.D6*29.D0/44.D0,' ppmv'    
            ELSEIF ((SPECRT(K)%TRACE.EQV..FALSE.).OR.(SPECRT(K)%ID=='H2O')) THEN
                WRITE(LUOUT,'(A,E8.1,A,F8.2,A)') '              Max '//TRIM(SPECRT(K)%ID)//'res('//TRIM(LOCRT(IYKRES(1))%ID)//')=',MAXVAL(DABS(RES((1+K)*N_LOC+1:(2+K)*N_LOC)))*DTIME,' YK= ',LOCRT(IYKRES(1))%YK(K)*1000.D0,' g/kg'
            ELSE
                WRITE(LUOUT,'(A,E8.1,A,F8.2,A)') '              Max '//TRIM(SPECRT(K)%ID)//'res('//TRIM(LOCRT(IYKRES(1))%ID)//')=',MAXVAL(DABS(RES((1+K)*N_LOC+1:(2+K)*N_LOC)))*DTIME,' YK= ',LOCRT(IYKRES(1))%YK(K)*LOCRT(IYKRES(1))%RHO15,' [user unit]/m3'
            ENDIF
        ENDDO
    ENDIF
    IF (N_DUCT.NE.0) THEN
        ALLOCATE(IQMRES(N_DUCT))
        IQMRES=MAXLOC(DABS(RES(NEQ+1:NEQ+N_DUCT)))
        WRITE(LUOUT,'(A,E8.1,A,F8.2,A)') '              Max QMres('//TRIM(BRANCHERT(IDNDUCT(IQMRES(1)))%ID)//')=',MAXVAL(DABS(RES(NEQ+1:NEQ+N_DUCT)))*DTIME,' QV= ',BRANCHERT(IDNDUCT(IQMRES(1)))%QM/BRANCHERT(IDNDUCT(IQMRES(1)))%RHO15AM*3600.D0,' m3/h'
    ENDIF
     WRITE(LUOUT,'(A,E8.1,A,F8.2,A)') ''
    IF (ALLOCATED(ITRES)) DEALLOCATE(ITRES)
    IF (ALLOCATED(IPRES)) DEALLOCATE(IPRES)
    IF (ALLOCATED(IYKRES)) DEALLOCATE(IYKRES)
    IF (ALLOCATED(IQMRES)) DEALLOCATE(IQMRES)
    
    END SUBROUTINE DISPLAY_RESMAX

    SUBROUTINE UPDATE_SYSTEM(FLAG)

    INTEGER,INTENT(IN)		::FLAG
    INTEGER					::IMUR

    !IF ((N_WHYGRO.NE.0).AND.(TIME>TETA0)) THEN
    IF ((N_WHYGRO.NE.0)) THEN
        !SAM on ajoute le parametre TIME utilise pour ameliorer l'initialisation des donnees pour le solveur
        CALL SOLVE_HYGRO_WALLS(0,TIME)
    ENDIF
    CALL UPDATE_MURRT(FLAG,TIME)
    IF (((ISOTHERMAL==.TRUE.).AND.((N_NODE==0).OR.(ISOTHERMALNODE==.TRUE.))).OR.(UNCOUPLED_THERMAL_WALLS==.TRUE.)) THEN
        CALL SOLVE_THERMAL_WALLS(1)
    ENDIF
    CALL SOLVE_THERMAL_WALLS(2)

    CALL UPDATE_HSRCRT(FLAG)
    CALL UPDATE_LOCRT(FLAG)
    CALL UPDATE_BRANCHRT(FLAG)
    CALL UPDATE_MODRT(FLAG)
    CALL UPDATE_CTRLRT(FLAG)

    END SUBROUTINE UPDATE_SYSTEM

    SUBROUTINE REWIND_SYSTEM(FLAG)

    INTEGER,INTENT(IN)		::FLAG
    INTEGER					::IMUR

    CALL REWIND_MURRT(FLAG)
    CALL REWIND_HSRCRT(FLAG)
    CALL REWIND_LOCRT(FLAG)
    CALL REWIND_MODRT(FLAG)
    CALL REWIND_CTRLRT(FLAG)

    END SUBROUTINE REWIND_SYSTEM


    SUBROUTINE SOLVE_UNCOUPLED_TEMPERATURE(NASKED)

    INTEGER,INTENT(IN)							:: NASKED
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: LOCALRES,WA,X
    DOUBLE PRECISION							:: PHI,RESMAX1,RESMAX2,RESMAX3
    INTEGER										:: I,J,K,INFO,LWA,N,JFLAG,OLDSTATCALL

    Y=YTAMPON

    NAIRNODE=NASKED
    IF (UNCOUPLED_THERMAL_WALLS==.FALSE.) THEN
        N=NAIRNODE+2*N_WTHERM
    ELSE
        N=NAIRNODE
    ENDIF
    ALLOCATE(X(N))
    LWA=(N*(3*N+13))/2
    ALLOCATE(WA(LWA))
    ALLOCATE(LOCALRES(N))
    IF (NAIRNODE==N_LOC) THEN
        ALLOCATE(IP(N_LOC))
        IP=LOCRT(:)%IDN
    ELSE
        ALLOCATE(IP(N_NODE))
        IP=LOCRT(IDNNODE)%IDN
    ENDIF

    X(1:NAIRNODE)=T15(IP)
    J=NAIRNODE
    IF (UNCOUPLED_THERMAL_WALLS==.FALSE.) THEN
    DO I=1,N_MUR
        !IF (MURRT(I)%WALLTYPE=='THERMAL') THEN
        IF (MURRT(I)%enum_walltype==wall_thermal) THEN
            X(J+1)=MURRT(I)%TPIN
            X(J+2)=MURRT(I)%TPOUT
            J=J+2
        ENDIF
    ENDDO
    ENDIF
    IF (TIME==TETA0) THEN
        UPDATE=2
    ELSE
        UPDATE=0
    ENDIF
    CALL HYBRD1(RES_TEMP_AIR_AND_WALLS,N,X,LOCALRES,HYBRIDTOL,INFO,WA,LWA)

    OLDSTATCALL=STATCALL
    STATCALL=6	!CALCUL DES RESIDUS
    CALL RESIDUS(NEQ+N_DUCT,Y,RES,0)
    RESMAX1=MINVAL(ATOL-DABS(RES)*DTIME)
    RESMAX2=0.D0
    RESMAX3=0.D0
    DO K=1,SIZE(Y)
        IF (DABS(Y(K)).NE.0.D0) THEN
            RESMAX2=MIN(RESMAX2,RTOL(K)-(DABS((RES(K)*DTIME/Y(K)))))
            RESMAX3=MIN(RESMAX3,RTOL(K)-(DABS((Y(K)-YTAMPONMAIN(K))/Y(K))))
        ELSE
            RESMAX2=MIN(RESMAX2,ATOL(K)-(DABS((RES(K)*DTIME))))
            RESMAX3=MIN(RESMAX3,ATOL(K)-(DABS(Y(K)-YTAMPONMAIN(K))))
        ENDIF
    ENDDO
    
    SELECT CASE (CONVCRIT)
        CASE DEFAULT
        RESMAX=MAX(RESMAX1,RESMAX2,RESMAX3)
    CASE (1)
        RESMAX=RESMAX1
    CASE (2)
        RESMAX=RESMAX2
    CASE (3)
        RESMAX=MIN(RESMAX1,RESMAX2)
    CASE (4)
        RESMAX=MINVAL(ATOL-DABS(RES))
    END SELECT

    IF (DEBUGPLUS.EQV..TRUE.)  THEN
        WRITE(LUOUT,'(A9,I2,A8,I2)')    'STATCALL=',OLDSTATCALL,' - FLAG=',FLAG
        CALL DISPLAY_RESMAX
    ENDIF

    IF (FLAG.NE.-2) THEN
        YTAMPON=Y
    ENDIF

    DEALLOCATE(LOCALRES)
    DEALLOCATE(WA)
    DEALLOCATE(X)
    DEALLOCATE(IP)

    END SUBROUTINE SOLVE_UNCOUPLED_TEMPERATURE

    SUBROUTINE RES_TEMP_AIR_AND_WALLS(N,X,LOCALRES,JFLAG)

    INTEGER										:: N,JFLAG,K,J,I,KFLAG
    DOUBLE PRECISION,DIMENSION(N)				:: X,LOCALRES
    DOUBLE PRECISION,DIMENSION(N_LOC)			:: BILAN

    LOCALRES=0.D0
    T15(IP)=X(1:NAIRNODE)
    Y(N_LOC+1:2*N_LOC)=(T15)/(TREF)
    IF (UNCOUPLED_THERMAL_WALLS==.FALSE.) THEN
    J=NAIRNODE
    DO I=1,N_MUR
        IF (MURRT(I)%enum_walltype==wall_thermal) THEN
            MURRT(I)%TPIN=MAX(200.D0,MIN(X(J+1),2500.D0))
            MURRT(I)%TPOUT=MAX(200.D0,MIN(X(J+2),2500.D0))
            J=J+2
        ENDIF
    ENDDO
    ENDIF

    CALL FCN(NEQ+N_DUCT,TIME,Y,DYDT)

    CALL GET_Q_UNCOUPLED

    QOUTGOOD=QEXT+QSOURCE
    DO I=1,N_LOC
        DO J=1,N_LOC
            QOUTGOOD(I)=QOUTGOOD(I)+QLOC(I,J)
        ENDDO
    ENDDO
    
    IF (BOUSSINESQ==.FALSE.) THEN
        QOUTGOOD(IDNROOM)=QOUTGOOD(IDNROOM)+(RHO15(IDNROOM)*TREF*LOCRT(IDNROOM)%VOLUME/T15(IDNROOM))*(Y(N_LOC+IDNROOM)-Y0(N_LOC+IDNROOM))/DTIME
    ENDIF
    
    IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
        !ajout des termes lies aux variations des fractions massiques
        DO K=1,N_SPEC
            QOUTGOOD(IDNROOM)=QOUTGOOD(IDNROOM)+(RHO15(IDNROOM)*LOCRT(IDNROOM)%VOLUME/(CP15(IDNROOM)-CV15(IDNROOM)))*(SPECRT(K)%CP-SPECRT(K)%CV)*(Y((1+K)*N_LOC+(IDNROOM))-Y0((1+K)*N_LOC+(IDNROOM)))/DTIME
            QOUTGOOD(IDNROOM)=QOUTGOOD(IDNROOM)-(RHO15(IDNROOM)*LOCRT(IDNROOM)%VOLUME/(CP15(IDNROOM)-CV15(IDNROOM)))*(SPECRT(N_SPEC+1)%CP-SPECRT(N_SPEC+1)%CV)*(Y((1+K)*N_LOC+(IDNROOM))-Y0((1+K)*N_LOC+(IDNROOM)))/DTIME
        ENDDO
    ENDIF

    BILAN=0
    DO I=1,N_LOC
        BILAN(I)=BILAN(I)-QOUTGOOD(I)*CP15(I)*T15(I)+HSOURCE(I)+QEXT(I)*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
        DO J=1,N_LOC
            BILAN(I)=BILAN(I)+QLOC(I,J)*CP15(J)*T15(J)
        ENDDO
    ENDDO
    IF ((N_WTHERM.NE.0).AND.(UNCOUPLED_THERMAL_WALLS.EQV..FALSE.)) THEN
        CALL RES_FLUX_THERMAL_WALLS(2*N_WTHERM,X(NAIRNODE+1:N),LOCALRES(NAIRNODE+1:N),KFLAG)
    ENDIF
    CALL INIT_FLUX(FMUR,N_LOC,N_SPEC,1)
    
    CALL FLUX_ME_MURRT_FAST(FMUR)
    !CALL FLUX_ME_MURRT(FMUR)

    BILAN=BILAN+FMUR%DE15DT

    IF (CONSTANT_SPECIFIC_HEAT==.FALSE.) THEN
        !ajout des termes liees aux variations des fractions massiques
        DO K=1,N_SPEC
            BILAN(IDNROOM)=BILAN(IDNROOM)-(LOCRT(IDNROOM)%VOLUME)*(RHO15(IDNROOM)*CV15(IDNROOM)*T15(IDNROOM)/(CP15(IDNROOM)-CV15(IDNROOM)))*(SPECRT(K)%CP-CP15(IDNROOM)/CV15(IDNROOM)*SPECRT(K)%CV)*(Y((1+K)*N_LOC+(IDNROOM))-Y0((1+K)*N_LOC+(IDNROOM)))/DTIME
            BILAN(IDNROOM)=BILAN(IDNROOM)+(LOCRT(IDNROOM)%VOLUME)*(RHO15(IDNROOM)*CV15(IDNROOM)*T15(IDNROOM)/(CP15(IDNROOM)-CV15(IDNROOM)))*(SPECRT(N_SPEC+1)%CP-CP15(IDNROOM)/CV15(IDNROOM)*SPECRT(N_SPEC+1)%CV)*(Y((1+K)*N_LOC+(IDNROOM))-Y0((1+K)*N_LOC+(IDNROOM)))/DTIME
        ENDDO
    ENDIF

    IF (QUASISTAT==.FALSE.) BILAN(IDNROOM)=BILAN(IDNROOM)-(PREF*LOCRT(IDNROOM)%VOLUME/((CP15(IDNROOM)-CV15(IDNROOM))/CV15(IDNROOM)))*(Y(IDNROOM)-Y0(IDNROOM))/DTIME

    LOCALRES(1:NAIRNODE)=BILAN(IP)

    END SUBROUTINE RES_TEMP_AIR_AND_WALLS

    SUBROUTINE GET_Q_UNCOUPLED

    INTEGER			:: I,J
    INTEGER,POINTER :: TAB1,TAB2
    integer         :: indice_loc

    QEXT=0.D0
    QLOC=0.D0
    QSOURCE=0.D0
    HSOURCE=0.D0
    HSOURCERAD=0.D0
    QNODE=0.D0
    HNODE=0.D0

    IF (N_PZONE<N_LOC) THEN
        !    !traitement des debits intrazone
        QLOC=QINTRA
    ENDIF

    ! normalement, il n'y a que 2 LOC non nuls au maximum pour chaque branche non ?
    if(0) then
        DO I=1,N_BRANCHE
            ! on dit que ce qui rentre dans le local vient de l'exterieur :
            QEXT=QEXT+BRANCHERT(I)%FLUXES%DMIN15DT
            ! on corrige le flux d'energie pour que le bilan thermique de RES_TEMP_AIR_AND_WALLS ne retienne au final bien que DE15DT
            HSOURCE=HSOURCE+BRANCHERT(I)%FLUXES%DE15DT+BRANCHERT(I)%FLUXES%DMOUT15DT*CP15*T15-BRANCHERT(I)%FLUXES%DMIN15DT*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
        ENDDO
    else
        DO I=1,N_BRANCHE
            indice_loc=BRANCHERT(I)%TAB(1)
            IF (indice_loc<=N_LOC) THEN
                ! on dit que ce qui rentre dans le local vient de l'exterieur :
                QEXT(indice_loc)    =QEXT(indice_loc)+  BRANCHERT(I)%FLUXES%DMIN15DT(indice_loc)
                ! on corrige le flux d'energie pour que le bilan thermique de RES_TEMP_AIR_AND_WALLS ne retienne au final bien que DE15DT
                HSOURCE(indice_loc) =HSOURCE(indice_loc)+   BRANCHERT(I)%FLUXES%DE15DT(indice_loc)+    BRANCHERT(I)%FLUXES%DMOUT15DT(indice_loc)*CP15(indice_loc)*T15(indice_loc)- BRANCHERT(I)%FLUXES%DMIN15DT(indice_loc)*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
            ENDIF
            indice_loc=BRANCHERT(I)%TAB(2)
            IF (indice_loc<=N_LOC) THEN
                ! on dit que ce qui rentre dans le local vient de l'exterieur :
                QEXT(indice_loc)    =QEXT(indice_loc)+  BRANCHERT(I)%FLUXES%DMIN15DT(indice_loc)
                ! on corrige le flux d'energie pour que le bilan thermique de RES_TEMP_AIR_AND_WALLS ne retienne au final bien que DE15DT
                HSOURCE(indice_loc) =HSOURCE(indice_loc)+   BRANCHERT(I)%FLUXES%DE15DT(indice_loc)+    BRANCHERT(I)%FLUXES%DMOUT15DT(indice_loc)*CP15(indice_loc)*T15(indice_loc)- BRANCHERT(I)%FLUXES%DMIN15DT(indice_loc)*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
            ENDIF
        ENDDO
    endif

    IF (IDNH2O.NE.0) THEN
        DO I=1,N_LOC
            QLOC(I,I)=QLOC(I,I)+(LOCRT(I)%FLUXES%DMIN15DT(I))
            HSOURCE(I)=HSOURCE(I)+(LOCRT(I)%FLUXES%DMIN15DT(I)-LOCRT(I)%FLUXES%DMOUT15DT(I))*(SPECRT(IDNH2O)%CP-LOCRT(I)%CP)*LOCRT(I)%T15 ! flux enthalpique + correction liee a la chaleur specifique de l'eau
            HSOURCE(I)=HSOURCE(I)-MIN(0.D0,((LOCRT(I)%FLUXES%DMIN15DT(I))-LOCRT(I)%FLUXES%DMOUT15DT(I)))*LV !condensation
            HSOURCERAD(I)=HSOURCERAD(I)+LOCRT(I)%HRAD !sources radiatives+vaporisation
        ENDDO
    ENDIF

    DO I=1,N_HSRC
        IF (HSRCRT(I)%IP<=N_LOC) THEN
            IF (HSRCRT(I)%T==-9999.D0+273.15D0) THEN
                IF (HSRCRT(I)%MPDOT>0.D0) THEN
                    QLOC(HSRCRT(I)%IP,HSRCRT(I)%IP)=QLOC(HSRCRT(I)%IP,HSRCRT(I)%IP)+HSRCRT(I)%MPDOT
                ENDIF
                HSOURCE(HSRCRT(I)%IP)=HSOURCE(HSRCRT(I)%IP)+HSRCRT(I)%QDOT
            ELSEIF (HSRCRT(I)%MPDOT>0.D0) THEN
                QSOURCE(HSRCRT(I)%IP)=QSOURCE(HSRCRT(I)%IP)+HSRCRT(I)%MPDOT
                HSOURCE(HSRCRT(I)%IP)=HSOURCE(HSRCRT(I)%IP)+HSRCRT(I)%QDOT+HSRCRT(I)%MPDOT*HSRCRT(I)%CP15*HSRCRT(I)%T15
            ELSE
                HSOURCE(HSRCRT(I)%IP)=HSOURCE(HSRCRT(I)%IP)+HSRCRT(I)%QDOT+HSRCRT(I)%MPDOT*HSRCRT(I)%CP15*HSRCRT(I)%T15
            ENDIF
        ENDIF
    ENDDO

    DO I=1,N_PERSON
        IF (PERSONRT(I)%IP<=N_LOC) THEN
            QLOC(PERSONRT(I)%IP,PERSONRT(I)%IP)=QLOC(PERSONRT(I)%IP,PERSONRT(I)%IP)+PERSONRT(I)%MPDOT
            HSOURCE(PERSONRT(I)%IP)=HSOURCE(PERSONRT(I)%IP)+PERSONRT(I)%QDOT
        ENDIF
    ENDDO

    if(0) then
        DO I=1,N_MUR
            !prise en compte du flux enthalpique pour les murs hygro
            ! on dit que ce qui rentre dans le local vient de l'exterieur :
            QEXT=QEXT+MURRT(I)%FLUXES%DMIN15DT
            ! on corrige le flux d'energie pour que le bilan thermique de RES_TEMP_AIR_AND_WALLS ne retienne au final bien que DE15DT
            !N.B.:  DE15DT et ajoute plus tard par RES_TEMP_AIR_AND_WALLS
            HSOURCE=HSOURCE+MURRT(I)%FLUXES%DMOUT15DT*CP15*T15-MURRT(I)%FLUXES%DMIN15DT*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
        ENDDO
    else
        DO I=1,N_MUR
            indice_loc=MURRT(I)%TAB(1)
            IF (indice_loc<=N_LOC) THEN
                QEXT(indice_loc)    = QEXT(indice_loc)+     MURRT(I)%FLUXES%DMIN15DT(indice_loc)
                HSOURCE(indice_loc) = HSOURCE(indice_loc)+  MURRT(I)%FLUXES%DMOUT15DT(indice_loc)*CP15(indice_loc)*T15(indice_loc)- MURRT(I)%FLUXES%DMIN15DT(indice_loc)*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
            ENDIF
            indice_loc=MURRT(I)%TAB(2)
            IF (indice_loc<=N_LOC) THEN
                QEXT(indice_loc)    = QEXT(indice_loc)+     MURRT(I)%FLUXES%DMIN15DT(indice_loc)
                HSOURCE(indice_loc) = HSOURCE(indice_loc)+  MURRT(I)%FLUXES%DMOUT15DT(indice_loc)*CP15(indice_loc)*T15(indice_loc)- MURRT(I)%FLUXES%DMIN15DT(indice_loc)*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
            ENDIF
        ENDDO
    endif

    DO I=1,N_MOD
        ! on dit que ce qui rentre dans le local vient de l'exterieur :
        QEXT=QEXT+MODRT(I)%FLUXES%DMIN15DT
        ! on corrige le flux d'energie pour que le bilan thermique de RES_TEMP_AIR_AND_WALLS ne retienne au final bien que DE15DT
        HSOURCE=HSOURCE+MODRT(I)%FLUXES%DE15DT+MODRT(I)%FLUXES%DMOUT15DT*CP15*T15-MODRT(I)%FLUXES%DMIN15DT*MDEXT(1)%CPEXT*MDEXT(1)%TEXT
    ENDDO

    END SUBROUTINE GET_Q_UNCOUPLED

    SUBROUTINE INIT_Q_UNCOUPLED

    IF (ALLOCATED(QEXT)) DEALLOCATE(QEXT)
    IF (ALLOCATED(QOUTGOOD)) DEALLOCATE(QOUTGOOD)
    IF (ALLOCATED(QSOURCE)) DEALLOCATE(QSOURCE)
    IF (ALLOCATED(HSOURCE)) DEALLOCATE(HSOURCE)
    IF (ALLOCATED(HSOURCERAD)) DEALLOCATE(HSOURCERAD)
    IF (ALLOCATED(QNODE)) DEALLOCATE(QNODE)
    IF (ALLOCATED(HNODE)) DEALLOCATE(HNODE)
    IF (ALLOCATED(QLOC)) DEALLOCATE(QLOC)
    
    ALLOCATE(QEXT(N_LOC))
    ALLOCATE(QOUTGOOD(N_LOC))
    ALLOCATE(QSOURCE(N_LOC))
    ALLOCATE(HSOURCE(N_LOC))
    ALLOCATE(HSOURCERAD(N_LOC))
    ALLOCATE(QNODE(N_LOC))
    ALLOCATE(HNODE(N_LOC))
    ALLOCATE(QLOC(N_LOC,N_LOC))

    CALL DEALLOCATE_FLUX(FMUR)
    CALL INIT_FLUX(FMUR,N_LOC,N_SPEC,0)

    END SUBROUTINE INIT_Q_UNCOUPLED

    END MODULE SOLVER_MODULE
