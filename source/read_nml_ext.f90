    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE READ_NML_EXT_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE
    
    DOUBLE PRECISION					        :: TEXT,HR,VMETEO,HVMETEO,HVREF,WDIR,SUNRAD,DIFFRAD,TSKY,TGROUND
    DOUBLE PRECISION					        :: LATITUDE,LONGITUDE,STDMERIDIAN,ALBEDO,ALTREF
    DOUBLE PRECISION,DIMENSION(NZPEXTMAX)       :: CP
    LOGICAL								        :: INTERPOLATION,MOVING_SUN
    CHARACTER(1000)						        :: CPFILE,ATMOFILE
    CHARACTER(4)						        :: RUGO
    INTEGER								        :: N_ZPEXT
    CHARACTER(100),DIMENSION(NSPECMAX)			:: SPECIDS,YKCTRLIDS
    DOUBLE PRECISION,DIMENSION(NSPECMAX)		:: YKS
    CHARACTER(1)								:: TIMEUNIT
    
    NAMELIST /EXT/ TEXT,HR,VMETEO,HVMETEO,HVREF,RUGO,WDIR,CP,N_ZPEXT,CPFILE,ATMOFILE,SUNRAD,DIFFRAD,TSKY,TGROUND,LATITUDE,LONGITUDE,STDMERIDIAN,INTERPOLATION,YKS,SPECIDS,YKCTRLIDS,ALBEDO,TIMEUNIT,MOVING_SUN,ALTREF

    CONTAINS
    
    SUBROUTINE DEFAULT_NML_EXT

        TYPE(TYPE_EXT)		:: OBJ
        
        TEXT=OBJ%TEXT
        HR=OBJ%HR
        VMETEO=OBJ%VMETEO
        WDIR=OBJ%WDIR
        CP=OBJ%CP
        N_ZPEXT=OBJ%N_ZPEXT
        CPFILE=OBJ%CPFILE
        ATMOFILE=OBJ%ATMOFILE
        SUNRAD=OBJ%SUNRAD
        DIFFRAD=OBJ%DIFFRAD
        TSKY=OBJ%TSKY
        TGROUND=OBJ%TGROUND
        LATITUDE=OBJ%LATITUDE
        LONGITUDE=OBJ%LONGITUDE
        STDMERIDIAN=OBJ%STDMERIDIAN
        INTERPOLATION=OBJ%INTERPOLATION
        SPECIDS=OBJ%SPECIDS ; SPECIDS(1)='CO2'
        YKS=OBJ%YKS ; YKS(1)=607.D-6 !kg/kg
        YKCTRLIDS=OBJ%YKCTRLIDS
        HVREF=OBJ%HVREF
        RUGO=OBJ%RUGO
        HVMETEO=OBJ%HVMETEO
        ALBEDO=OBJ%ALBEDO
        ALTREF=OBJ%ALTREF
        TIMEUNIT=OBJ%TIMEUNIT
        MOVING_SUN=OBJ%MOVING_SUN
    
    END SUBROUTINE DEFAULT_NML_EXT
    
    SUBROUTINE PRINT_DEFAULT_NML_EXT(LUOUT)
    
    INTEGER,INTENT(IN)			:: LUOUT
    
    CALL DEFAULT_NML_EXT
    WRITE(LUOUT,NML=EXT,DELIM='APOSTROPHE ')
    
    END SUBROUTINE PRINT_DEFAULT_NML_EXT
    
    SUBROUTINE SET_NML_EXT(OBJ)
    
    TYPE(TYPE_EXT),INTENT(INOUT)		:: OBJ
    
        OBJ%TEXT=TEXT
        OBJ%HR=HR
        OBJ%VMETEO=VMETEO
        OBJ%WDIR=WDIR
        OBJ%CP=CP
        OBJ%N_ZPEXT=N_ZPEXT
        OBJ%CPFILE=CPFILE
        OBJ%ATMOFILE=ATMOFILE
        OBJ%SUNRAD=SUNRAD
        OBJ%DIFFRAD=DIFFRAD
        OBJ%TSKY=TSKY
        OBJ%TGROUND=TGROUND
        OBJ%LATITUDE=LATITUDE
        OBJ%LONGITUDE=LONGITUDE
        OBJ%STDMERIDIAN=STDMERIDIAN
        OBJ%INTERPOLATION=INTERPOLATION
        OBJ%SPECIDS=SPECIDS 
        OBJ%YKS=YKS 
        OBJ%YKCTRLIDS=YKCTRLIDS
        OBJ%HVREF=HVREF
        OBJ%RUGO=RUGO
        OBJ%HVMETEO=HVMETEO
        OBJ%ALBEDO=ALBEDO
        OBJ%ALTREF=ALTREF
        OBJ%TIMEUNIT=TIMEUNIT
        OBJ%MOVING_SUN=MOVING_SUN
      
    END SUBROUTINE SET_NML_EXT
    
    
    SUBROUTINE COUNT_NML_EXT(LU1,LUOUT,N_OBJ)

    INTEGER,INTENT(IN)			:: LU1,LUOUT
    INTEGER,INTENT(OUT)			:: N_OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    N_OBJ=0
    REWIND(LU1)
    COUNT_OBJ_LOOP: DO
        CALL CHECKREAD('EXT',LU1,IOS)
        IF (IOS==0) EXIT COUNT_OBJ_LOOP
        READ(LU1,NML=EXT,END=209,ERR=210,IOSTAT=IOS)
        N_OBJ=N_OBJ+1
210     IF (IOS>0) THEN
            WRITE(LUOUT,NML=EXT)
            WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &EXT line number ',N_OBJ+1,': check parameters names and values'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDDO COUNT_OBJ_LOOP
209 REWIND(LU1)

    END SUBROUTINE COUNT_NML_EXT
    

    SUBROUTINE READ_NML_EXT(LU1,LUOUT,OBJ)

    INTEGER,INTENT(IN)								:: LU1,LUOUT
    TYPE(TYPE_EXT),INTENT(INOUT)		:: OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    REWIND(LU1)
    CALL CHECKREAD('EXT',LU1,IOS)
    CALL DEFAULT_NML_EXT
    IF (IOS.NE.0) READ(LU1,EXT)
    CALL SET_NML_EXT(OBJ)
    REWIND(LU1)

    END SUBROUTINE READ_NML_EXT
    
    
    END MODULE READ_NML_EXT_MODULE
