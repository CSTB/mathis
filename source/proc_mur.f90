    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE PROC_MUR_MODULE

    USE DATA_MUR_MODULE
    USE DATA_LOC_MODULE,ONLY:	LOCRT,N_LOC
    USE DATA_MAT_MODULE,ONLY:	MATRT,N_MAT
    USE DATA_SURF_MODULE,ONLY:	SURFRT,N_SURF
    USE DATA_EXT_MODULE,ONLY:	MDEXT
    USE DATA_BOUND_MODULE,ONLY : BOUNDRT,N_BOUND
    USE DATA_SPEC_MODULE,ONLY : N_SPEC,IDNH2O,SPECRT
    USE DATA_CTRL_MODULE,ONLY : CTRLRT,N_CTRL
    USE PROC_MAT_MODULE,ONLY : GET_MAT_VALUE
    USE PROC_MAT_MODULE,ONLY : GET_MAT_VALUE_FAST

    USE FLUX_ME_MODULE

    USE GLOBAL_VAR_MODULE,ONLY: LU1,LUOUT,MESSAGE,GUESS_POWER,RADIATION,PI,T15,CP15,HUM15,TIME,TETA0,ROTATION,QUANTIEME,PREF,SUPERSATURATION
    USE GLOBAL_VAR_MODULE, ONLY: CHECKREAD,SHUTDOWN,JOBNAME,OUTPUT,NSAVE,TIMEUNIT,TIMESCALE
    USE GLOBAL_VAR_MODULE, ONLY: TREF,IDNH2O,MH2O,RU,RHOH2O,CPW,LV
    USE GLOBAL_VAR_MODULE, ONLY: NSAVE

    IMPLICIT NONE



    CONTAINS

    SUBROUTINE INIT_CONS_MUR(OBJ,IDN)

    TYPE(TYPE_MUR),INTENT(INOUT)			    :: OBJ
    INTEGER,INTENT(IN)							:: IDN

    INTEGER										:: ITAB,IEXT,ILOC,ISURF,ISLAB,IMAT,IBOUND,INODE,II
    CHARACTER(100)								:: IDEXT

    INTEGER                                     :: K,ERR

    OBJ%SLOPE=PI/180.D0*OBJ%SLOPE
    OBJ%ORIENTATION=PI/180.D0*OBJ%ORIENTATION

    DO ITAB=1,2
        OBJ%TAB(ITAB)=0
        DO ILOC=1,N_LOC
            IF (OBJ%LOCIDS(ITAB)==LOCRT(ILOC)%ID) OBJ%TAB(ITAB)=ILOC
        ENDDO
        DO IEXT=1,MDEXT(1)%N_ZPEXT
            WRITE(IDEXT,"('',i,'')") IEXT
            IF (OBJ%LOCIDS(ITAB)=='EXT'//TRIM(ADJUSTL(IDEXT))) OBJ%TAB(ITAB)=N_LOC+IEXT
        ENDDO
        IF (OBJ%LOCIDS(ITAB)=='EXT') OBJ%TAB(ITAB)=N_LOC+1
        DO IBOUND=1,N_BOUND
            IF (OBJ%LOCIDS(ITAB)==BOUNDRT(IBOUND)%ID) OBJ%TAB(ITAB)=N_LOC+MDEXT(1)%N_ZPEXT+IBOUND
        ENDDO
        IF (OBJ%LOCIDS(ITAB)=='GROUND') THEN
            OBJ%TAB(ITAB)=-1
        ENDIF
        IF (OBJ%TAB(ITAB)==0) THEN
            WRITE(MESSAGE,'(A,A,A,I1,A)') 'ERROR - Problem with WALL ',TRIM(OBJ%ID),': LOCIDS(',ITAB,') is unknown'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDDO

    OBJ%ISURF=-1
    DO ISURF=0,N_SURF
        IF (OBJ%SURFID==SURFRT(ISURF)%ID) OBJ%ISURF=ISURF
    ENDDO
    IF (OBJ%ISURF==-1) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with WALL ',TRIM(OBJ%ID),': SURFID (',TRIM(OBJ%SURFID),') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF
    
    OBJ%NSLAB=SURFRT(OBJ%ISURF)%NSLAB
    ALLOCATE(OBJ%MAT(OBJ%NSLAB))
    ALLOCATE(OBJ%NODES(OBJ%NSLAB))
    IF (OBJ%NSLAB>1) THEN
        ALLOCATE(OBJ%HCCT(OBJ%NSLAB-1));OBJ%HCCT=SURFRT(OBJ%ISURF)%HCCT(1:OBJ%NSLAB-1)
        ALLOCATE(OBJ%HMCT(OBJ%NSLAB-1));OBJ%HMCT=SURFRT(OBJ%ISURF)%HMCT(1:OBJ%NSLAB-1)
    ENDIF


    !SELECT CASE (OBJ%WALLTYPE)
    SELECT CASE (OBJ%enum_walltype)
    !CASE ('THERMAL')
    CASE(wall_thermal)
        OBJ%NODES=SURFRT(OBJ%ISURF)%NODES
        OBJ%NUMNODE(1)=1
        DO ISLAB=1,OBJ%NSLAB
            OBJ%NUMNODE(1)=OBJ%NUMNODE(1)+OBJ%NODES(ISLAB)+1
        ENDDO
        ALLOCATE(OBJ%DX(OBJ%NUMNODE(1)-1))
        INODE=0
        DO ISLAB=1,OBJ%NSLAB
            OBJ%NUMNODE(ISLAB+1)=OBJ%NODES(ISLAB)
            OBJ%DX(INODE+1:INODE+(OBJ%NODES(ISLAB)+1))=SURFRT(OBJ%ISURF)%MESH(ISLAB)%DXM
            INODE=INODE+OBJ%NODES(ISLAB)+1
            DO IMAT=0,N_MAT
                IF (MATRT(IMAT)%ID==SURFRT(OBJ%ISURF)%MATIDS(ISLAB)) THEN
                    OBJ%MAT(ISLAB)=MATRT(IMAT)
                ENDIF
            ENDDO
        ENDDO
    !CASE ('HYGROTHERMAL')
    case(wall_hygrothermal)
        OBJ%NODES=SURFRT(OBJ%ISURF)%NODES+2
        OBJ%MRATIO=SURFRT(OBJ%ISURF)%MESH_RATIO
        OBJ%NUMNODE(1)=0
        DO ISLAB=1,OBJ%NSLAB
            OBJ%NUMNODE(1)=OBJ%NUMNODE(1)+OBJ%NODES(ISLAB)
        ENDDO
        ALLOCATE(OBJ%DX(OBJ%NUMNODE(1)))
        INODE=0
        DO ISLAB=1,OBJ%NSLAB
            OBJ%NUMNODE(ISLAB+1)=OBJ%NODES(ISLAB)
            OBJ%DX(INODE+1)=SURFRT(OBJ%ISURF)%MESH(ISLAB)%DXM(1)
            OBJ%DX(INODE+OBJ%NODES(ISLAB))=SURFRT(OBJ%ISURF)%MESH(ISLAB)%DXM(OBJ%NODES(ISLAB)-1)
            DO II=2,OBJ%NODES(ISLAB)-1
                OBJ%DX(INODE+II)=0.5D0*(SURFRT(OBJ%ISURF)%MESH(ISLAB)%DXM(II-1)+SURFRT(OBJ%ISURF)%MESH(ISLAB)%DXM(II))
            ENDDO
            INODE=INODE+OBJ%NODES(ISLAB)
            DO IMAT=0,N_MAT
                IF (MATRT(IMAT)%ID==SURFRT(OBJ%ISURF)%MATIDS(ISLAB)) THEN
                    OBJ%MAT(ISLAB)=MATRT(IMAT)
                ENDIF
            ENDDO
        ENDDO
    END SELECT

    !IF ((OBJ%WALLTYPE=='HYGROTHERMAL').AND.(IDNH2O==0)) THEN
    IF ((OBJ%enum_walltype==wall_hygrothermal).AND.(IDNH2O==0)) THEN
        WRITE(MESSAGE,'(A)') 'ERROR : you must define a H2O SPEC line for a HYGROTHERMAL wall calculation'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF

    END SUBROUTINE INIT_CONS_MUR

    SUBROUTINE INIT_CONS_MURRT

    INTEGER								:: I,J,K

    N_WHYGRO=0
    N_WTHERM=0
    DO I=1,SIZE(MURRT)
        CALL INIT_CONS_MUR(MURRT(I),I)
        !IF (MURRT(I)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(I)%enum_walltype==wall_hygrothermal) THEN
            N_WHYGRO=N_WHYGRO+1
        ELSE
            N_WTHERM=N_WTHERM+1
        ENDIF
    ENDDO
    IF (ALLOCATED(WHIP)) DEALLOCATE(WHIP);ALLOCATE(WHIP(N_WHYGRO))
    IF (ALLOCATED(WTIP)) DEALLOCATE(WTIP);ALLOCATE(WTIP(N_WTHERM))
    J=0
    K=0
    DO I=1,SIZE(MURRT)
        !IF (MURRT(I)%WALLTYPE=='HYGROTHERMAL') THEN
        IF (MURRT(I)%enum_walltype==wall_hygrothermal) THEN
            J=J+1
            WHIP(J)=I
        ELSE
            K=K+1
            WTIP(K)=I
        ENDIF
    ENDDO

    END SUBROUTINE INIT_CONS_MURRT

    SUBROUTINE INIT_VAR_MUR(OBJ)

    TYPE(TYPE_MUR),INTENT(INOUT)			:: OBJ

    DOUBLE PRECISION				:: SURF1,SURF2
    INTEGER                         :: ISLAB,INODE


    ALLOCATE(OBJ%TEMP(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%TEMP_OLD(OBJ%NUMNODE(1)))
    !initialisation des temperatures
    INODE=0
    DO ISLAB=1,OBJ%NSLAB
        IF (SURFRT(OBJ%ISURF)%TINI(ISLAB).NE.9999.D0) THEN
            !SELECT CASE (OBJ%WALLTYPE)
            SELECT CASE (OBJ%enum_walltype)
            !CASE ('THERMAL')
            CASE (wall_thermal)
                OBJ%TEMP(INODE+1:INODE+OBJ%NODES(ISLAB)+1)=SURFRT(OBJ%ISURF)%TINI(ISLAB)+273.15D0
                INODE=INODE+OBJ%NODES(ISLAB)+1
                IF (ISLAB==OBJ%NSLAB) THEN
                    OBJ%TEMP(OBJ%NUMNODE(1))=SURFRT(OBJ%ISURF)%TINI(ISLAB)+273.15D0
                ENDIF
            !CASE ('HYGROTHERMAL')
            CASE (wall_hygrothermal)
                OBJ%TEMP(INODE+1:INODE+OBJ%NODES(ISLAB))=SURFRT(OBJ%ISURF)%TINI(ISLAB)+273.15D0
                INODE=INODE+OBJ%NODES(ISLAB)
            END SELECT
        ELSE
            OBJ%TEMP(1:OBJ%NUMNODE(1))=MDEXT(1)%TEXT
        ENDIF
    ENDDO


    OBJ%TEMP_OLD=OBJ%TEMP
    OBJ%TPIN=OBJ%TEMP(1)
    OBJ%TPOUT=OBJ%TEMP(OBJ%NUMNODE(1))
    OBJ%FRADIN=0.D0
    OBJ%FRADOUT=0.D0
    OBJ%FSUNIN=0.D0
    OBJ%FSUNOUT=0.D0
    OBJ%COSI=0.D0

    ALLOCATE(OBJ%HR(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%HR_OLD(OBJ%NUMNODE(1)))


    !SAM ajout d'anciennes valeurs complementaires et des instants correspondants
    ALLOCATE(OBJ%HR_OLD2(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%TEMP_OLD2(OBJ%NUMNODE(1)))
    OBJ%TIME_OLD=-1.0D0
    OBJ%TIME_OLD2=-1.0D0

    !initialisation des HR
    INODE=0
    DO ISLAB=1,OBJ%NSLAB
        IF (SURFRT(OBJ%ISURF)%HRINI(ISLAB).NE.9999.D0) THEN
            !SELECT CASE (OBJ%WALLTYPE)
            SELECT CASE (OBJ%enum_walltype)
            !CASE ('THERMAL')
            CASE (wall_thermal)
                OBJ%HR(INODE+1:INODE+OBJ%NODES(ISLAB)+1)=SURFRT(OBJ%ISURF)%HRINI(ISLAB)/100.D0
                INODE=INODE+OBJ%NODES(ISLAB)+1
            !CASE ('HYGROTHERMAL')
            CASE (wall_hygrothermal)
                OBJ%HR(INODE+1:INODE+OBJ%NODES(ISLAB))=SURFRT(OBJ%ISURF)%HRINI(ISLAB)/100.D0
                INODE=INODE+OBJ%NODES(ISLAB)
            END SELECT
        ELSE
            OBJ%HR(1:OBJ%NUMNODE(1))=MDEXT(1)%HUMIDITE/100.D0
        ENDIF
    ENDDO
    OBJ%HR_OLD=OBJ%HR
    OBJ%HRPIN=OBJ%HR(1)
    OBJ%HRPOUT=OBJ%HR(OBJ%NUMNODE(1))

    ALLOCATE(OBJ%MSOURCE(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%QSOURCE(OBJ%NUMNODE(1)))
    OBJ%MSOURCE=0.D0
    OBJ%QSOURCE=0.D0

    ALLOCATE(OBJ%LAMBDA(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%DW(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%PERM(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%SORP(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%PV(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%PSAT(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%DPSATDT(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%YW(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%K(OBJ%NUMNODE(1)))

    ALLOCATE(OBJ%RHO(OBJ%NUMNODE(1)))
    ALLOCATE(OBJ%CP(OBJ%NUMNODE(1)))
    !SELECT CASE (OBJ%WALLTYPE)
    SELECT CASE (OBJ%enum_walltype)
    !CASE ('THERMAL')
    CASE (wall_thermal)
        INODE=1
        DO ISLAB=1,OBJ%NSLAB !stockage de RHO et CP dans les noeuds internes de chaque slab
            ! a priori \E7a ne sert a rien...
            OBJ%RHO(INODE+1:INODE+OBJ%NODES(ISLAB))=OBJ%MAT(ISLAB)%RHO
            OBJ%CP(INODE+1:INODE+OBJ%NODES(ISLAB))=OBJ%MAT(ISLAB)%CS
            INODE=INODE+OBJ%NODES(ISLAB)+1
        ENDDO
    !CASE ('HYGROTHERMAL')
    CASE (wall_hygrothermal)
        INODE=0
        DO ISLAB=1,OBJ%NSLAB !stockage de RHO et CP dans tous les noeuds de chaque slab
            OBJ%RHO(INODE+1:INODE+OBJ%NODES(ISLAB))=OBJ%MAT(ISLAB)%RHO
            OBJ%CP(INODE+1:INODE+OBJ%NODES(ISLAB))=OBJ%MAT(ISLAB)%CS
            INODE=INODE+OBJ%NODES(ISLAB)
        ENDDO
    END SELECT

    ALLOCATE(OBJ%YWMEAN(OBJ%NSLAB))

    CALL EVAL_MUR_HYGROVARS(OBJ,0)


    END SUBROUTINE INIT_VAR_MUR

    SUBROUTINE INIT_VAR_MURRT

    INTEGER										:: I

    DO I=1,SIZE(MURRT)
        CALL INIT_VAR_MUR(MURRT(I))
    ENDDO

    END SUBROUTINE INIT_VAR_MURRT


    SUBROUTINE INIT_IMAGE_MUR(OBJ)

    TYPE(TYPE_MUR),INTENT(INOUT)				:: OBJ
    INTEGER                                     :: ITAB,ICTRL

    NULLIFY(OBJ%TIN);
    NULLIFY(OBJ%TOUT);
    NULLIFY(OBJ%HRIN);
    NULLIFY(OBJ%HROUT);
    IF (OBJ%TAB(1)==-1) THEN
        OBJ%TIN=>MDEXT(1)%TGROUND
        OBJ%HRIN=>MDEXT(1)%HUMIDITE ! a reprendre
    ELSE IF (OBJ%TAB(1)<=N_LOC) THEN
        OBJ%TIN=>T15(OBJ%TAB(1))
        OBJ%HRIN=>HUM15(OBJ%TAB(1))
    ELSE IF (OBJ%TAB(1)<=N_LOC+MDEXT(1)%N_ZPEXT) THEN
        OBJ%TIN=>MDEXT(1)%TEXT
        OBJ%HRIN=>MDEXT(1)%HUMIDITE
    ELSE
        OBJ%TIN=>BOUNDRT(OBJ%TAB(1)-N_LOC-MDEXT(1)%N_ZPEXT)%TBOUND
        OBJ%HRIN=>BOUNDRT(OBJ%TAB(1)-N_LOC-MDEXT(1)%N_ZPEXT)%HR
    ENDIF
    IF (OBJ%TAB(2)==-1) THEN
        OBJ%TOUT=>MDEXT(1)%TGROUND
        OBJ%HROUT=>MDEXT(1)%HUMIDITE ! a reprendre
    ELSE IF (OBJ%TAB(2)<=N_LOC) THEN
        OBJ%TOUT=>T15(OBJ%TAB(2))
        OBJ%HROUT=>HUM15(OBJ%TAB(2))
    ELSE IF (OBJ%TAB(2)<=N_LOC+MDEXT(1)%N_ZPEXT) THEN
        OBJ%TOUT=>MDEXT(1)%TEXT
        OBJ%HROUT=>MDEXT(1)%HUMIDITE
    ELSE
        OBJ%TOUT=>BOUNDRT(OBJ%TAB(2)-N_LOC-MDEXT(1)%N_ZPEXT)%TBOUND
        OBJ%HROUT=>BOUNDRT(OBJ%TAB(2)-N_LOC-MDEXT(1)%N_ZPEXT)%HR
    ENDIF

    NULLIFY(OBJ%QFRACIN);
    NULLIFY(OBJ%QFRACOUT);
    NULLIFY(OBJ%MFRACIN);
    NULLIFY(OBJ%MFRACOUT);

    DO ICTRL=1,N_CTRL
        DO ITAB=1,2
            IF (OBJ%QCTRLIDS(ITAB)==CTRLRT(ICTRL)%ID) THEN
                IF (ITAB==1) THEN
                    OBJ%QFRACIN=>CTRLRT(ICTRL)%VALUE
                ELSE
                    OBJ%QFRACOUT=>CTRLRT(ICTRL)%VALUE
                ENDIF
            ENDIF
            IF (OBJ%MCTRLIDS(ITAB)==CTRLRT(ICTRL)%ID) THEN
                IF (ITAB==1) THEN
                    OBJ%MFRACIN=>CTRLRT(ICTRL)%VALUE
                ELSE
                    OBJ%MFRACOUT=>CTRLRT(ICTRL)%VALUE
                ENDIF
            ENDIF
        ENDDO
        IF (OBJ%TRSCTRLID==CTRLRT(ICTRL)%ID) THEN
            OBJ%TRSFRAC=>CTRLRT(ICTRL)%VALUE
        ENDIF
    ENDDO
    IF (.NOT.ASSOCIATED(OBJ%QFRACIN)) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with WALL ',TRIM(OBJ%ID),': QCTRLIDS(',TRIM(OBJ%QCTRLIDS(1)),') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF
    IF (.NOT.ASSOCIATED(OBJ%QFRACOUT)) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with WALL ',TRIM(OBJ%ID),': QCTRLIDS(',TRIM(OBJ%QCTRLIDS(2)),') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF
    IF (.NOT.ASSOCIATED(OBJ%MFRACIN)) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with WALL ',TRIM(OBJ%ID),': MCTRLIDS(',TRIM(OBJ%MCTRLIDS(1)),') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF
    IF (.NOT.ASSOCIATED(OBJ%MFRACOUT)) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with WALL ',TRIM(OBJ%ID),': MCTRLIDS(',TRIM(OBJ%MCTRLIDS(2)),') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF
    IF (.NOT.ASSOCIATED(OBJ%TRSFRAC)) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with WALL ',TRIM(OBJ%ID),': TRSCTRLID(',TRIM(OBJ%TRSCTRLID),') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF

    END SUBROUTINE INIT_IMAGE_MUR

    SUBROUTINE INIT_IMAGE_MURRT

    INTEGER										:: I

    DO I=1,SIZE(MURRT)
        CALL INIT_IMAGE_MUR(MURRT(I))
    ENDDO

    END SUBROUTINE INIT_IMAGE_MURRT

    SUBROUTINE FLUX_ME_MUR(OBJ)

    TYPE(TYPE_MUR),INTENT(INOUT)			:: OBJ

    CALL EVAL_MUR_BOUNDS(OBJ)

    !CALCUL DES PERTES LIEES AUX ECHANGES CONVECTIF
    !(LES MURS NE RAYONNENT PAS AVEC LE GAZ)
    IF ((OBJ%TAB(1)<=N_LOC).AND.(OBJ%TAB(1)>0)) THEN
        OBJ%FLUXES%DE15DT(OBJ%TAB(1))=-OBJ%AREA*OBJ%HCONV(1)*(OBJ%TIN-OBJ%TPIN)
    ENDIF
    IF ((OBJ%TAB(2)<=N_LOC).AND.(OBJ%TAB(2)>0)) THEN
        OBJ%FLUXES%DE15DT(OBJ%TAB(2))=-OBJ%AREA*OBJ%HCONV(2)*(OBJ%TOUT-OBJ%TPOUT)
    ENDIF

    !IF (OBJ%WALLTYPE=='HYGROTHERMAL') THEN
    IF (OBJ%enum_walltype==wall_hygrothermal) THEN
        !ajouts des flux de masse de vapeur d'eau
        IF ((OBJ%TAB(1)<=N_LOC).AND.(OBJ%TAB(1)>0)) THEN
            IF (OBJ%NETM(1).GE.0.D0) THEN
                OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(1),IDNH2O)=OBJ%AREA*OBJ%NETM(1)
                OBJ%FLUXES%DMOUT15DT(OBJ%TAB(1))=OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(1),IDNH2O)
            ELSE
                OBJ%FLUXES%DMKIN15DT(OBJ%TAB(1),IDNH2O)=-OBJ%AREA*OBJ%NETM(1)
                OBJ%FLUXES%DMIN15DT(OBJ%TAB(1))=OBJ%FLUXES%DMKIN15DT(OBJ%TAB(1),IDNH2O)
            ENDIF
            !ajout du flux enthalpique
            OBJ%FLUXES%DE15DT(OBJ%TAB(1))=OBJ%FLUXES%DE15DT(OBJ%TAB(1))+(OBJ%FLUXES%DMIN15DT(OBJ%TAB(1))*OBJ%TPIN-OBJ%FLUXES%DMOUT15DT(OBJ%TAB(1))*OBJ%TIN)*(SPECRT(IDNH2O)%CP)
        ENDIF
        IF ((OBJ%TAB(2)<=N_LOC).AND.(OBJ%TAB(2)>0)) THEN
            IF (OBJ%NETM(2).GE.0.D0) THEN
                OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(2),IDNH2O)=OBJ%AREA*OBJ%NETM(2)
                OBJ%FLUXES%DMOUT15DT(OBJ%TAB(2))=OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(2),IDNH2O)
            ELSE
                OBJ%FLUXES%DMKIN15DT(OBJ%TAB(2),IDNH2O)=-OBJ%AREA*OBJ%NETM(2)
                OBJ%FLUXES%DMIN15DT(OBJ%TAB(2))=OBJ%FLUXES%DMKIN15DT(OBJ%TAB(2),IDNH2O)
            ENDIF
            !ajout du flux enthalpique
            OBJ%FLUXES%DE15DT(OBJ%TAB(2))=OBJ%FLUXES%DE15DT(OBJ%TAB(2))+(OBJ%FLUXES%DMIN15DT(OBJ%TAB(2))*OBJ%TPOUT-OBJ%FLUXES%DMOUT15DT(OBJ%TAB(2))*OBJ%TOUT)*(SPECRT(IDNH2O)%CP)
        ENDIF
    ENDIF

    END SUBROUTINE FLUX_ME_MUR





    ! SAM : idem fonction precedente, sauf que plutot d'initialiser un vecteur qu'il faut initialiser et incrementer, on fait tout directement dans la destination
    SUBROUTINE FLUX_ME_MUR_FAST(OBJ, F)

        TYPE(TYPE_MUR),INTENT(INOUT)			:: OBJ
        TYPE(FLUX_ME),INTENT(INOUT)				:: F

        DOUBLE PRECISION                        :: terme
    
        CALL EVAL_MUR_BOUNDS(OBJ)
    
        !CALCUL DES PERTES LIEES AUX ECHANGES CONVECTIF
        !(LES MURS NE RAYONNENT PAS AVEC LE GAZ)
        IF ((OBJ%TAB(1)<=N_LOC).AND.(OBJ%TAB(1)>0)) THEN
            terme = -OBJ%AREA * OBJ%HCONV(1) * (OBJ%TIN-OBJ%TPIN)
            OBJ%FLUXES%DE15DT(OBJ%TAB(1)) = terme

            F%DE15DT(OBJ%TAB(1)) = F%DE15DT(OBJ%TAB(1)) + terme
        ELSE
            ! OBJ%FLUXES%DE15DT(OBJ%TAB(1)) = 0.0D0
        ENDIF

        IF ((OBJ%TAB(2)<=N_LOC).AND.(OBJ%TAB(2)>0)) THEN
            terme = -OBJ%AREA * OBJ%HCONV(2) * (OBJ%TOUT-OBJ%TPOUT)
            OBJ%FLUXES%DE15DT(OBJ%TAB(2)) = terme

            F%DE15DT(OBJ%TAB(2)) = F%DE15DT(OBJ%TAB(2)) + terme
        ELSE
            ! OBJ%FLUXES%DE15DT(OBJ%TAB(2)) = 0.0D0
        ENDIF
    
        !IF (OBJ%WALLTYPE=='HYGROTHERMAL') THEN
        IF (OBJ%enum_walltype==wall_hygrothermal) THEN
            !ajouts des flux de masse de vapeur d'eau
            IF ((OBJ%TAB(1)<=N_LOC).AND.(OBJ%TAB(1)>0)) THEN
                IF (OBJ%NETM(1).GE.0.D0) THEN
                    ! RAZ des elements non traites dans ce cas
                    OBJ%FLUXES%DMKIN15DT(OBJ%TAB(1),IDNH2O) = 0.0D0
                    OBJ%FLUXES%DMIN15DT(OBJ%TAB(1))         = 0.0D0

                    terme = OBJ%AREA*OBJ%NETM(1)
                    OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(1),IDNH2O)= terme
                    F%DMKOUT15DT(OBJ%TAB(1),IDNH2O) = F%DMKOUT15DT(OBJ%TAB(1),IDNH2O) + terme

                    OBJ%FLUXES%DMOUT15DT(OBJ%TAB(1))= terme
                    F%DMOUT15DT(OBJ%TAB(1))         = F%DMOUT15DT(OBJ%TAB(1)) + terme

                    !ajout du flux enthalpique
                    terme = (-terme * OBJ%TIN)*(SPECRT(IDNH2O)%CP)
                    OBJ%FLUXES%DE15DT(OBJ%TAB(1))   = OBJ%FLUXES%DE15DT(OBJ%TAB(1)) + terme
                    F%DE15DT(OBJ%TAB(1))            = F%DE15DT(OBJ%TAB(1))          + terme
                ELSE
                    ! RAZ des elements non traites dans ce cas
                    OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(1),IDNH2O)= 0.0D0
                    OBJ%FLUXES%DMOUT15DT(OBJ%TAB(1))        = 0.0D0

                    terme = -OBJ%AREA*OBJ%NETM(1)
                    OBJ%FLUXES%DMKIN15DT(OBJ%TAB(1),IDNH2O) = terme
                    F%DMKIN15DT(OBJ%TAB(1),IDNH2O)          = F%DMKIN15DT(OBJ%TAB(1),IDNH2O) + terme

                    OBJ%FLUXES%DMIN15DT(OBJ%TAB(1)) = terme
                    F%DMIN15DT(OBJ%TAB(1))          = F%DMIN15DT(OBJ%TAB(1)) + terme

                    !ajout du flux enthalpique
                    terme = (terme * OBJ%TPIN)
                    OBJ%FLUXES%DE15DT(OBJ%TAB(1))   = OBJ%FLUXES%DE15DT(OBJ%TAB(1)) + terme
                    F%DE15DT(OBJ%TAB(1))            = F%DE15DT(OBJ%TAB(1))          + terme
                ENDIF
            ENDIF


            IF ((OBJ%TAB(2)<=N_LOC).AND.(OBJ%TAB(2)>0)) THEN
                IF (OBJ%NETM(2).GE.0.D0) THEN
                    ! RAZ des elements non traites dans ce cas
                    OBJ%FLUXES%DMKIN15DT(OBJ%TAB(2),IDNH2O)     = 0.0D0
                    OBJ%FLUXES%DMIN15DT(OBJ%TAB(2))             = 0.0D0

                    terme = OBJ%AREA*OBJ%NETM(2)
                    OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(2),IDNH2O)    = terme
                    F%DMKOUT15DT(OBJ%TAB(2),IDNH2O)             = F%DMKOUT15DT(OBJ%TAB(2),IDNH2O) + terme

                    OBJ%FLUXES%DMOUT15DT(OBJ%TAB(2))            = terme
                    F%DMOUT15DT(OBJ%TAB(2))                     = F%DMOUT15DT(OBJ%TAB(2)) + terme

                    !ajout du flux enthalpique
                    terme = (-terme*OBJ%TOUT)*(SPECRT(IDNH2O)%CP)
                    OBJ%FLUXES%DE15DT(OBJ%TAB(2))   = OBJ%FLUXES%DE15DT(OBJ%TAB(2)) + terme
                    F%DE15DT(OBJ%TAB(2))            = F%DE15DT(OBJ%TAB(2))          + terme
                ELSE
                    ! RAZ des elements non traites dans ce cas
                    OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(2),IDNH2O)    = 0.0D0
                    OBJ%FLUXES%DMKOUT15DT(OBJ%TAB(2),IDNH2O)    = 0.0D0

                    terme = -OBJ%AREA*OBJ%NETM(2)
                    OBJ%FLUXES%DMKIN15DT(OBJ%TAB(2),IDNH2O)     = terme
                    F%DMKIN15DT(OBJ%TAB(2),IDNH2O)              = F%DMKIN15DT(OBJ%TAB(2),IDNH2O) + terme

                    OBJ%FLUXES%DMIN15DT(OBJ%TAB(2))             = terme
                    F%DMIN15DT(OBJ%TAB(2))                      = F%DMIN15DT(OBJ%TAB(2)) + terme

                    !ajout du flux enthalpique
                    terme = (terme*OBJ%TPOUT)
                    OBJ%FLUXES%DE15DT(OBJ%TAB(2))   = OBJ%FLUXES%DE15DT(OBJ%TAB(2)) + terme
                    F%DE15DT(OBJ%TAB(2))            = F%DE15DT(OBJ%TAB(2))          + terme
                ENDIF
            ENDIF


        ENDIF
    
    END SUBROUTINE FLUX_ME_MUR_FAST



    SUBROUTINE FLUX_ME_MURRT(F)

    TYPE(FLUX_ME),INTENT(INOUT)				:: F
    INTEGER									:: I
    DO I=1,SIZE(MURRT)
        CALL INIT_FLUX(MURRT(I)%FLUXES,N_LOC,N_SPEC,1)
        CALL FLUX_ME_MUR(MURRT(I))
        !CALL ADD_FLUX(F,F,MURRT(I)%FLUXES)
        CALL INCREMENTE_FLUX(F,MURRT(I)%FLUXES)
    ENDDO
    END SUBROUTINE FLUX_ME_MURRT

    SUBROUTINE INIT_FLUX_MURRT(N_LOC,N_SPEC,FLAG)

    INTEGER,INTENT(IN)						  :: N_LOC,N_SPEC,FLAG
    INTEGER									  :: I

    DO I=1,SIZE(MURRT)
        CALL INIT_FLUX(MURRT(I)%FLUXES,N_LOC,N_SPEC,FLAG)
    ENDDO
    END SUBROUTINE INIT_FLUX_MURRT




    ! SAM : meme fonction que precedemment, mais sans l'initialisation et recopie
    SUBROUTINE FLUX_ME_MURRT_FAST(F)

        TYPE(FLUX_ME),INTENT(INOUT)				:: F
        INTEGER									:: I

        DO I=1,SIZE(MURRT)
            !CALL INIT_FLUX(MURRT(I)%FLUXES,N_LOC,N_SPEC,1) plus besoin
            CALL FLUX_ME_MUR_FAST(MURRT(I), F)
            !CALL INCREMENTE_FLUX(F,MURRT(I)%FLUXES) plus besoin non plus
        ENDDO
    END SUBROUTINE FLUX_ME_MURRT_FAST




    


    !SAM Ajout du parametre INSTANT
    SUBROUTINE UPDATE_MUR(IMUR,FLAG,INSTANT)

    INTEGER,INTENT(IN)					:: IMUR,FLAG
    DOUBLE PRECISION,INTENT(IN)         :: INSTANT
    TYPE(TYPE_MUR),POINTER				:: OBJ
    INTEGER                             :: INODE,ISLAB


    OBJ=>MURRT(IMUR)
    IF (FLAG==1) THEN
        !SAM ajout d'un instant precedent 2
        OBJ%HR_OLD2=OBJ%HR_OLD
        OBJ%TEMP_OLD2=OBJ%TEMP_OLD
        OBJ%TIME_OLD2=OBJ%TIME_OLD

        OBJ%TIME_OLD=INSTANT

        OBJ%TEMP_OLD=OBJ%TEMP
        OBJ%HR_OLD=OBJ%HR
    ENDIF

    END SUBROUTINE UPDATE_MUR


    !SAM Ajout du parametre INSTANT
    SUBROUTINE UPDATE_MURRT(FLAG,INSTANT)

    INTEGER,INTENT(IN)					:: FLAG
    DOUBLE PRECISION,INTENT(IN)         :: INSTANT
    INTEGER								:: I

    DO I=1,SIZE(MURRT)
        CALL UPDATE_MUR(I,FLAG,INSTANT)
    ENDDO

    END SUBROUTINE UPDATE_MURRT

    SUBROUTINE REWIND_MUR(IMUR,FLAG)

    INTEGER,INTENT(IN)					:: IMUR,FLAG
    TYPE(TYPE_MUR),POINTER				:: OBJ

    OBJ=>MURRT(IMUR)
    OBJ%TEMP=OBJ%TEMP_OLD
    OBJ%HR=OBJ%HR_OLD

    END SUBROUTINE REWIND_MUR

    SUBROUTINE REWIND_MURRT(FLAG)

    INTEGER,INTENT(IN)					:: FLAG
    INTEGER								:: I

    DO I=1,SIZE(MURRT)
        CALL REWIND_MUR(I,FLAG)
    ENDDO

    END SUBROUTINE REWIND_MURRT

    SUBROUTINE EVAL_MUR_BOUNDS(OBJ)

    TYPE(TYPE_MUR),INTENT(INOUT)			:: OBJ
    DOUBLE PRECISION                        :: AI,F,RB,FSUN
    DOUBLE PRECISION,PARAMETER              :: SIGMA=5.67D-8
    DOUBLE PRECISION           :: MH2OCURRENT,MH2OMAX
    DOUBLE PRECISION ,DIMENSION(1)          :: YWSAT


    OBJ%NETQ(1)=OBJ%HCONV(1)*(OBJ%TIN-OBJ%TPIN)+OBJ%FRADIN
    OBJ%NETQ(2)=OBJ%HCONV(2)*(OBJ%TOUT-OBJ%TPOUT)+OBJ%FRADOUT
    OBJ%NETM(1)=0.D0
    OBJ%NETM(2)=0.D0
    OBJ%NETMTOT(1)=0.D0
    OBJ%NETMTOT(2)=0.D0
    IF (TIME.NE.TETA0) THEN
        OBJ%NETQ(1)= OBJ%NETQ(1)+OBJ%QFRACIN*OBJ%QPRIME(1)
        OBJ%NETQ(2)= OBJ%NETQ(2)+OBJ%QFRACOUT*OBJ%QPRIME(2)
    ENDIF

    IF (RADIATION.EQV..TRUE.) THEN
        !calcul de l'angle du mur avec le soleil
        IF (ROTATION>=0.D0) THEN
            OBJ%COSI=DCOS(MDEXT(1)%H_ANGLE)*DSIN(OBJ%SLOPE)*DCOS(MDEXT(1)%AZ_ANGLE-MODULO(OBJ%ORIENTATION-PI+&
                MODULO(FLOOR(((TIME-TETA0)/(3600.D0*24.D0))/7.D0)*PI/180.D0*ROTATION,2.D0*PI),2.D0*PI))+DSIN(MDEXT(1)%H_ANGLE)*DCOS(OBJ%SLOPE)
        ELSE
            OBJ%COSI=DCOS(MDEXT(1)%H_ANGLE)*DSIN(OBJ%SLOPE)*DCOS(MDEXT(1)%AZ_ANGLE-MODULO(OBJ%ORIENTATION-PI+ABS(PI/180.D0*ROTATION),2.D0*PI))+DSIN(MDEXT(1)%H_ANGLE)*DCOS(OBJ%SLOPE)
        ENDIF
        OBJ%COSI=MAX(0.D0,OBJ%COSI)
        !modele de ciel anisotropisque de HDKR, cf. Duffie & Bechman, page 96
        AI=MDEXT(1)%SUNRAD/(1367.D0*(1.D0+0.033D0*DCOSD(360.D0*QUANTIEME/365.D0)))
        F=0.D0
        IF (DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD>0.D0) F=(DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD/(DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD+MDEXT(1)%DIFFRAD))**0.5D0
        IF (MDEXT(1)%H_ANGLE>=5.D0*PI/180.D0) THEN
            RB=OBJ%COSI/DSIN(MDEXT(1)%H_ANGLE)
        ELSE
            RB=OBJ%COSI
        ENDIF
        FSUN=MDEXT(1)%SUNRAD*OBJ%COSI+AI*MDEXT(1)%DIFFRAD*RB+(1.D0+DCOS(OBJ%SLOPE))/2.D0*MDEXT(1)%DIFFRAD*(1.D0-AI)*(1.D0+F*(DSIN(OBJ%SLOPE/2.D0))**3.D0)+(1.D0-DCOS(OBJ%SLOPE))/2.D0*MDEXT(1)%ALBEDO*(DSIN(MDEXT(1)%H_ANGLE)*MDEXT(1)%SUNRAD+MDEXT(1)%DIFFRAD)
        !rayonnement des murs connectes a L'exterieur
        IF ((OBJ%LOCIDS(1)=='EXT').AND.(TIME.NE.TETA0)) THEN
            OBJ%FSUNIN=OBJ%ABS(1)*FSUN
            OBJ%FRADIN=OBJ%FSUNIN+OBJ%EPS(1)*SIGMA*((1.D0+DCOS(OBJ%SLOPE))/2.D0*(MDEXT(1)%TSKY**4.D0-OBJ%TPIN**4.D0)+(1.D0-DCOS(OBJ%SLOPE))/2.D0*(MDEXT(1)%TEXT**4.D0-OBJ%TPIN**4.D0))
            OBJ%NETQ(1)= OBJ%NETQ(1)+OBJ%FRADIN
        ENDIF
        IF ((OBJ%LOCIDS(2)=='EXT').AND.(TIME.NE.TETA0)) THEN
            OBJ%FSUNOUT=OBJ%ABS(2)*FSUN
            OBJ%FRADOUT= OBJ%FSUNOUT+OBJ%EPS(2)*SIGMA*((1.D0+DCOS(OBJ%SLOPE))/2.D0*(MDEXT(1)%TSKY**4.D0-OBJ%TPOUT**4.D0)+(1.D0-DCOS(OBJ%SLOPE))/2.D0*(MDEXT(1)%TEXT**4.D0-OBJ%TPOUT**4.D0))
            OBJ%NETQ(2)= OBJ%NETQ(2)+OBJ%FRADOUT
        ENDIF
        !rayonnement des murs connectes a une conditions aux limites
        IF (OBJ%TAB(1)>N_LOC+MDEXT(1)%N_ZPEXT)  THEN
            OBJ%FRADIN=OBJ%EPS(1)*SIGMA*(OBJ%TIN**4-OBJ%TPIN**4.D0)
            OBJ%NETQ(1)= OBJ%NETQ(1)+OBJ%FRADIN
        ENDIF
        IF (OBJ%TAB(2)>N_LOC+MDEXT(1)%N_ZPEXT)  THEN
            OBJ%FRADOUT=OBJ%EPS(2)*SIGMA*(OBJ%TOUT**4-OBJ%TPOUT**4.D0)
            OBJ%NETQ(2)= OBJ%NETQ(2)+OBJ%FRADOUT
        ENDIF
    ENDIF
    !IF (OBJ%WALLTYPE=='HYGROTHERMAL') THEN
    IF (OBJ%enum_walltype==wall_hygrothermal) THEN
        !determination des flux de masse aux surfaces externes
        OBJ%PVIN=OBJ%HRIN/100.D0*PREF*EXP(13.7D0-5120.D0/OBJ%TIN)
        OBJ%PVOUT=OBJ%HROUT/100.D0*PREF*EXP(13.7D0-5120.D0/OBJ%TOUT)
        OBJ%NETM(1)=OBJ%NETM(1)+OBJ%HM(1)*(OBJ%PVIN-OBJ%PV(1))
        OBJ%NETM(2)=OBJ%NETM(2)+OBJ%HM(2)*(OBJ%PVOUT-OBJ%PV(OBJ%NUMNODE(1)))
        !et prise en compte de la chaleur latente associee a la vapeur d'eau dans les flux de chaleur
        !OBJ%NETQ(1)=OBJ%NETQ(1)+LV*OBJ%NETM(1)
        !OBJ%NETQ(2)=OBJ%NETQ(2)+LV*OBJ%NETM(2)
        
        OBJ%NETQ(1)=OBJ%NETQ(1)+(SPECRT(IDNH2O)%CP*(OBJ%TIN-OBJ%TPIN)+LV)*OBJ%NETM(1)
        OBJ%NETQ(2)=OBJ%NETQ(2)+(SPECRT(IDNH2O)%CP*(OBJ%TOUT-OBJ%TPOUT)+LV)*OBJ%NETM(2)
        
        
        !ajout des sources de masses d'eau definies par l'utilisateur
        MH2OCURRENT=OBJ%RHO(1)*OBJ%DX(1)*OBJ%YW(1)
        IF (SUPERSATURATION.EQV..FALSE.) THEN
            !SAM YWSAT=MINVAL(GET_MAT_VALUE(OBJ%MAT(1)%SORPTABLE,(/1.D0/),1)) 
            YWSAT=MINVAL(GET_MAT_VALUE_FAST(OBJ%MAT(1)%SORPTABLE,(/1.D0/),1))
        ELSE
            !SAM YWSAT=MINVAL(GET_MAT_VALUE(OBJ%MAT(1)%SORPTABLE,(/1.01D0/),1))
            YWSAT=MINVAL(GET_MAT_VALUE_FAST(OBJ%MAT(1)%SORPTABLE,(/1.01D0/),1))
        ENDIF
        MH2OMAX=MINVAL(OBJ%RHO(1)*OBJ%DX(1)*YWSAT)
        OBJ%NETMTOT(1)=OBJ%NETM(1)+MAX(0.D0,MIN((MH2OMAX-MH2OCURRENT)/MDEXT(1)%DTIME,OBJ%MFRACIN*OBJ%MPRIME(1)))
        OBJ%NETMTOT(2)=OBJ%NETM(2)+OBJ%MFRACOUT*OBJ%MPRIME(2)
        !et prise en compte de la chaleur enthalpique
        OBJ%NETQ(1)=OBJ%NETQ(1)+CPW*(OBJ%TIN-OBJ%TPIN)*OBJ%MFRACIN*OBJ%MPRIME(1)
        OBJ%NETQ(2)=OBJ%NETQ(2)+CPW*(OBJ%TOUT-OBJ%TPOUT)*OBJ%MFRACOUT*OBJ%MPRIME(2)        

    ENDIF

    END SUBROUTINE EVAL_MUR_BOUNDS

    SUBROUTINE EVAL_MUR_HYGROVARS(OBJ,FLAG)
    !calcul des grandeurs fonction de HR dans les differents noeus d'un mur compose de plusieurs materiaux ("slab")

    TYPE(TYPE_MUR),INTENT(INOUT)			:: OBJ
    INTEGER,INTENT(IN),OPTIONAL             :: FLAG

    INTEGER                             :: I,INODE,ISLAB,IBREAK
    OBJ%PSAT=PREF*EXP(13.7D0-5120.D0/OBJ%TEMP)
    OBJ%PV=MIN(1.D0,OBJ%HR)*OBJ%PSAT
    OBJ%DPSATDT=MH2O*LV/(RU*TREF**2.D0)*OBJ%PSAT
    INODE=0
    OBJ%MWTOT=0.D0
    DO ISLAB=1,OBJ%NSLAB
        !IF (OBJ%WALLTYPE=='THERMAL') THEN
        IF (OBJ%enum_walltype==wall_thermal) THEN
            !a priori ca ne sert a rien, a modifier
            !SAM OBJ%LAMBDA(INODE+2:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE(OBJ%MAT(ISLAB)%LAMBDATABLE,OBJ%HR(INODE+2:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            OBJ%LAMBDA(INODE+2:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE_FAST(OBJ%MAT(ISLAB)%LAMBDATABLE,OBJ%HR(INODE+2:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
        ENDIF
        !IF (OBJ%WALLTYPE=='HYGROTHERMAL') THEN
        IF (OBJ%enum_walltype==wall_hygrothermal) THEN
            !SAM OBJ%LAMBDA(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE(OBJ%MAT(ISLAB)%LAMBDATABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            OBJ%LAMBDA(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE_FAST(OBJ%MAT(ISLAB)%LAMBDATABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            !SAM OBJ%DW(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE(OBJ%MAT(ISLAB)%DWTABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            OBJ%DW(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE_FAST(OBJ%MAT(ISLAB)%DWTABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            !SAM OBJ%PERM(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE(OBJ%MAT(ISLAB)%PERMTABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            OBJ%PERM(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE_FAST(OBJ%MAT(ISLAB)%PERMTABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            ! remember: here YW in kg/m3
            IF (PRESENT(FLAG)) THEN
                !SAM OBJ%YW(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE(OBJ%MAT(ISLAB)%SORPTABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB),OBJ%SORP(INODE+1:INODE+(OBJ%NODES(ISLAB))))
                OBJ%YW(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE_FAST(OBJ%MAT(ISLAB)%SORPTABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB),&
            	 						OBJ%SORP(INODE+1:INODE+(OBJ%NODES(ISLAB))) )
            ELSE
                OBJ%YW(INODE+1:INODE+(OBJ%NODES(ISLAB)))=GET_MAT_VALUE_FAST(OBJ%MAT(ISLAB)%SORPTABLE,OBJ%HR(INODE+1:INODE+(OBJ%NODES(ISLAB))),OBJ%NODES(ISLAB))
            ENDIF
            !calcul de YW moyen par slab en kg/kg
            OBJ%YWMEAN(ISLAB)=OBJ%DX(INODE+1)/2.D0*OBJ%YW(INODE+1)&
                +SUM(OBJ%DX(INODE+2:INODE+OBJ%NODES(ISLAB)-1)*OBJ%YW(INODE+2:INODE+OBJ%NODES(ISLAB)-1))&
                +OBJ%DX(INODE+OBJ%NODES(ISLAB))/2.D0*OBJ%YW(INODE+OBJ%NODES(ISLAB))
            OBJ%YWMEAN(ISLAB)=OBJ%YWMEAN(ISLAB)/SURFRT(OBJ%ISURF)%E(ISLAB)/OBJ%MAT(ISLAB)%RHO
            !calcul de MWTOT  en kg/m2
            OBJ%MWTOT=OBJ%MWTOT+OBJ%YWMEAN(ISLAB)*OBJ%MAT(ISLAB)%RHO*SURFRT(OBJ%ISURF)%E(ISLAB)
        ENDIF
        INODE=INODE+OBJ%NODES(ISLAB)
    ENDDO
    !IF (OBJ%WALLTYPE=='HYGROTHERMAL') THEN
    !    !calcul de K (conductivite hydraulique) pour le terme de gravite
    !    OBJ%K=OBJ%SORP*OBJ%DW*OBJ%HR/(RU*TREF)*(MH2O/RHOH2O)
    !ENDIF
    END SUBROUTINE EVAL_MUR_HYGROVARS


    END MODULE PROC_MUR_MODULE
