    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************

    MODULE PROC_CTRL_MODULE

    USE DATA_CTRL_MODULE
    USE DATA_LOC_MODULE,ONLY : LOCRT,N_LOC
    USE DATA_BRANCHE_MODULE,ONLY : BRANCHERT,N_BRANCHE
    USE GLOBAL_VAR_MODULE,ONLY: LU1,LU3,LUOUT,MESSAGE,TIMESCALE,TETA0,DTETA,TIME,DTIME,HOUR,JOUR,QUANTIEME,DP,T15,IDNH2O,YK15,HUM15,RHO15,HEATLOSS
    USE DATA_SPEC_MODULE,ONLY:	SPECRT,N_SPEC
    USE DATA_EXT_MODULE,ONLY:	MDEXT
    USE DATA_BOUND_MODULE,ONLY:	BOUNDRT,N_BOUND
    USE DATA_MUR_MODULE,ONLY:	MURRT,N_MUR
    USE DATA_MOD_MODULE,ONLY:	MODRT,N_MOD
    USE GLOBAL_VAR_MODULE, ONLY:	   CHECKREAD,SHUTDOWN,YK_HUM_EVAL

    IMPLICIT NONE

    CONTAINS


    SUBROUTINE INIT_CONS_CTRL(OBJ,IDN)

    TYPE(TYPE_CTRL),INTENT(INOUT)					:: OBJ
    INTEGER,INTENT(IN)								:: IDN

    INTEGER											:: ILOC,IEXT,IBOUND,ISPEC,IBRANCHE,IMUR,IMOD
    CHARACTER(100)									:: IDEXT

    OBJ%IDN=IDN
    OBJ%IP=0
    DO ILOC=1,N_LOC
        IF (OBJ%LOCID==LOCRT(ILOC)%ID) OBJ%IP=ILOC
    ENDDO
    IF ((OBJ%LOCID=='EXT').AND.(OBJ%BRANCHID=='null').AND.(OBJ%MODID=='null')) OBJ%IP=N_LOC+1
    DO IEXT=1,MDEXT(1)%N_ZPEXT
        WRITE(IDEXT,"('',i,'')") IEXT
        IF (OBJ%LOCID=='EXT'//TRIM(ADJUSTL(IDEXT))) OBJ%IP=N_LOC+IEXT
    ENDDO
    DO IBOUND=1,N_BOUND
        IF (OBJ%LOCID==BOUNDRT(IBOUND)%ID) OBJ%IP=N_LOC+MDEXT(1)%N_ZPEXT+IBOUND
    ENDDO
    DO IBRANCHE=1,N_BRANCHE
        IF (OBJ%BRANCHID==BRANCHERT(IBRANCHE)%ID) OBJ%IP=IBRANCHE
    ENDDO
    DO IMUR=1,N_MUR
        IF (OBJ%WALLID==MURRT(IMUR)%ID) OBJ%IP=IMUR
    ENDDO
    DO IMOD=1,N_MOD
        IF (OBJ%MODID==MODRT(IMOD)%ID) OBJ%IP=IMOD
    ENDDO
    IF ((OBJ%IP==0).AND.(OBJ%BRANCHID=='null').AND.(OBJ%WALLID=='null').AND.(OBJ%MODID=='null')) THEN
        WRITE(MESSAGE,'(A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': LOCID is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ELSEIF ((OBJ%IP==0).AND.(OBJ%WALLID=='null').AND.(OBJ%MODID=='null')) THEN
        WRITE(MESSAGE,'(A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': BRANCHID is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ELSEIF ((OBJ%IP==0).AND.(OBJ%MODID=='null')) THEN
        WRITE(MESSAGE,'(A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': WALLID is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ELSEIF ((OBJ%IP==0)) THEN
        WRITE(MESSAGE,'(A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': MODID is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF

    IF (OBJ%RAMPFILE.NE.'null') THEN
        CALL READ_RAMP(OBJ)
    ELSE
        ALLOCATE(OBJ%RAMP(1000,2))
        OBJ%RAMP=OBJ%RAMPDATA
    ENDIF

    IF (OBJ%QUANTITY=='TIME')   OBJ%RAMP(:,1)=OBJ%RAMP(:,1)*TIMESCALE
    IF (OBJ%QUANTITY=='T')      OBJ%RAMP(:,1)=OBJ%RAMP(:,1)+273.15D0
    IF (OBJ%QUANTITY=='TW')     OBJ%RAMP(:,1)=OBJ%RAMP(:,1)+273.15D0
    IF (OBJ%QUANTITY=='TOPER')  OBJ%RAMP(:,1)=OBJ%RAMP(:,1)+273.15D0
    IF (OBJ%QUANTITY=='QV')     OBJ%RAMP(:,1)=OBJ%RAMP(:,1)/3600.D0
    IF (OBJ%QUANTITY=='TFLOW')  OBJ%RAMP(:,1)=OBJ%RAMP(:,1)+273.15D0
    IF (OBJ%QUANTITY=='TWNODE') OBJ%RAMP(:,1)=OBJ%RAMP(:,1)+273.15D0
    IF (OBJ%QUANTITY=='TP1')    OBJ%RAMP(:,1)=OBJ%RAMP(:,1)+273.15D0
    IF (OBJ%QUANTITY=='TP2')    OBJ%RAMP(:,1)=OBJ%RAMP(:,1)+273.15D0

    DO ISPEC=1,N_SPEC
        IF (OBJ%SPECID==SPECRT(ISPEC)%ID) OBJ%ISPEC=ISPEC
    ENDDO

    IF ((OBJ%QUANTITY=='T').OR.(OBJ%QUANTITY=='TW').OR.(OBJ%QUANTITY=='TOPER').OR.(OBJ%QUANTITY=='TFLOW').OR.(OBJ%QUANTITY=='TWNODE').OR.(OBJ%QUANTITY=='TP1').OR.(OBJ%QUANTITY=='TP2')) OBJ%SETPOINT=OBJ%SETPOINT+273.15D0

    IF (OBJ%TIMEUNIT=='S') THEN
        OBJ%TIMESCALE=1.D0
    ELSEIF (OBJ%TIMEUNIT=='M') THEN
        OBJ%TIMESCALE=60.D0
    ELSEIF (OBJ%TIMEUNIT=='H') THEN
        OBJ%TIMESCALE=3600.D0
    ELSEIF (OBJ%TIMEUNIT=='J') THEN
        OBJ%TIMESCALE=3600.D0*24.D0
    ELSEIF (OBJ%TIMEUNIT=='D') THEN
        OBJ%TIMESCALE=3600.D0*24.D0
    ENDIF
    END SUBROUTINE INIT_CONS_CTRL

    SUBROUTINE INIT_CONS_CTRLRT

    INTEGER							::	I,L,M,N,K

    N_PROBE=0
    N_FLAG0=0
    N_FLAG1=0
    N_PASSIVE=0
    DO I=1,N_CTRL
        CALL INIT_CONS_CTRL(CTRLRT(I),I)
        
        !IF (CTRLRT(I)%CTRLTYPE=='PROBE') N_PROBE=N_PROBE+1
        IF (CTRLRT(I)%enum_ctrltype==ctrl_probe) N_PROBE=N_PROBE+1

        !IF ((CTRLRT(I)%QUANTITY=='TIME').AND.(CTRLRT(I)%CTRLTYPE=='RAMP').AND.(CTRLRT(I)%CTRLID=='CTRLDEFAULT')) N_FLAG0=N_FLAG0+1
        IF ((CTRLRT(I)%QUANTITY=='TIME').AND.(CTRLRT(I)%enum_ctrltype==ctrl_ramp).AND.(CTRLRT(I)%CTRLID=='CTRLDEFAULT')) N_FLAG0=N_FLAG0+1

        !IF (CTRLRT(I)%CTRLTYPE=='PASSIVE') N_PASSIVE=N_PASSIVE+1
        IF (CTRLRT(I)%enum_ctrltype==ctrl_passive) N_PASSIVE=N_PASSIVE+1
    ENDDO
    N_FLAG1=N_CTRL-N_FLAG0
    IF (ALLOCATED(IDNPROBE)) DEALLOCATE(IDNPROBE);ALLOCATE(IDNPROBE(N_PROBE))
    IF (ALLOCATED(IDNPASSIVE)) DEALLOCATE(IDNPASSIVE);ALLOCATE(IDNPASSIVE(N_PASSIVE))
    IF (ALLOCATED(IDNFLAG0)) DEALLOCATE(IDNFLAG0);ALLOCATE(IDNFLAG0(N_FLAG0))
    IF (ALLOCATED(IDNFLAG1)) DEALLOCATE(IDNFLAG1);ALLOCATE(IDNFLAG1(N_FLAG1))
    
    L=0
    M=0
    N=0
    K=0
    DO I=1,N_CTRL
        IF (CTRLRT(I)%CTRLTYPE=='PROBE') THEN
            L=L+1
            IDNPROBE(L)=I
        ENDIF
        !IF ((CTRLRT(I)%QUANTITY=='TIME').AND.(CTRLRT(I)%CTRLTYPE=='RAMP').AND.(CTRLRT(I)%CTRLID=='CTRLDEFAULT')) THEN
        IF ((CTRLRT(I)%QUANTITY=='TIME').AND.(CTRLRT(I)%enum_ctrltype==ctrl_ramp).AND.(CTRLRT(I)%CTRLID=='CTRLDEFAULT')) THEN
            M=M+1
            IDNFLAG0(M)=I
        ELSE
            N=N+1
            IDNFLAG1(N)=I
        ENDIF
        !IF (CTRLRT(I)%CTRLTYPE=='PASSIVE') THEN
        IF (CTRLRT(I)%enum_ctrltype==ctrl_passive) THEN
            K=K+1
            IDNPASSIVE(K)=I
        ENDIF
    ENDDO

    END SUBROUTINE INIT_CONS_CTRLRT


    SUBROUTINE INIT_VAR_CTRL(ICTRL)

    INTEGER,INTENT(IN)				:: ICTRL
    TYPE(TYPE_CTRL),POINTER			:: OBJ
    INTEGER							:: I,J

    NULLIFY(OBJ);OBJ=>CTRLRT(ICTRL)

    OBJ%VALUE=OBJ%RAMP(1,2)
    OBJ%STATE=.TRUE.
    !**************************************
    !Obsolete :  supprimer lorsque l'on ne gardera plus 'DT'
    IF (OBJ%QUANTITY=='DT') THEN !Obsolete (remplace par  DTINI
        OBJ%DT=T15(OBJ%IP)-LOCRT(OBJ%IP)%TINI
    ENDIF
    IF (OBJ%QUANTITY=='DTINI') THEN
        OBJ%DT=T15(OBJ%IP)-LOCRT(OBJ%IP)%TINI
    ENDIF

    IF (OBJ%QUANTITY=='DTEXT') THEN
        OBJ%DT=T15(OBJ%IP)-MDEXT(1)%TEXT
    ENDIF
    !**************************************

    OBJ%NCTRLIDNS=0.D0
    DO J=1,NCTRLIDSMAX
        IF (OBJ%QUANTITIES(J)=='CTRLDEFAULT') EXIT
        OBJ%NCTRLIDNS=OBJ%NCTRLIDNS+1
    ENDDO
    
    IF (OBJ%FUNCTION=='TIME_AVERAGE') THEN
        IF (OBJ%CONSTANT.NE.0.D0) THEN ! obsolete
            ALLOCATE(OBJ%TIMEVALUES(MAX(1,CEILING(OBJ%CONSTANT*TIMESCALE/DTETA))))
        ELSE
            ALLOCATE(OBJ%TIMEVALUES(MAX(1,CEILING(OBJ%DURATION*OBJ%TIMESCALE/DTETA))))
        ENDIF
        OBJ%TIMEVALUES=-9999.D0
    ENDIF
    
    ALLOCATE(OBJ%CTRLIDNS(OBJ%NCTRLIDNS))
    ALLOCATE(OBJ%CTRLVALUES(OBJ%NCTRLIDNS))
    ALLOCATE(OBJ%CTRLSTATES(OBJ%NCTRLIDNS))
    DO J=1,OBJ%NCTRLIDNS
        OBJ%CTRLIDNS(J)=0
        DO I=1,N_CTRL
            IF (OBJ%QUANTITIES(J)==CTRLRT(I)%ID) THEN
                OBJ%CTRLIDNS(J)=I
                OBJ%CTRLVALUES(J)=CTRLRT(I)%VALUE
                OBJ%CTRLSTATES(J)=CTRLRT(I)%STATE
            ENDIF
        ENDDO
        IF (OBJ%CTRLIDNS(J)==0) THEN
            SELECT CASE (OBJ%QUANTITIES(J))
            CASE ('CONSTANT')
                OBJ%CTRLVALUES(J)=OBJ%CONSTANT
            CASE ('TIME')
                OBJ%CTRLVALUES(J)=TIME
            CASE ('HOUR')
                OBJ%CTRLVALUES(J)=HOUR
            CASE ('WEEKDAY')
                OBJ%CTRLVALUES(J)=JOUR
            CASE ('YEARDAY')
                OBJ%CTRLVALUES(J)=QUANTIEME
            CASE ('VREF')
                OBJ%CTRLVALUES(J)=MDEXT(1)%VREF
            CASE ('WDIR')
                OBJ%CTRLVALUES(J)=MDEXT(1)%WINC
                CASE DEFAULT
                WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': QUANTITIES(',OBJ%QUANTITIES(J),') is unknown'
                CALL SHUTDOWN(TRIM(MESSAGE),LUOUT)
            END SELECT
            IF (OBJ%CTRLVALUES(J)==0.D0) THEN
                OBJ%CTRLSTATES(J)=.FALSE.
            ELSE
                OBJ%CTRLSTATES(J)=.TRUE.
            ENDIF
        ENDIF
    ENDDO
    OBJ%PREVIOUS_VALUE=OBJ%VALUE
    OBJ%CURRENT_VALUE=OBJ%PREVIOUS_VALUE
    OBJ%INTEGRAL=0.D0
    OBJ%IOLD=1

    OBJ%ERROR=0.D0
    OBJ%PREVIOUS_ERROR=0.D0

    OBJ%VALUE_OLD=OBJ%VALUE
    OBJ%STATE_OLD=OBJ%STATE
    OBJ%PREVIOUS_VALUE_OLD=OBJ%PREVIOUS_VALUE
    OBJ%INTEGRAL_OLD=OBJ%INTEGRAL

    OBJ%TIMER=TETA0-DTIME


    END SUBROUTINE INIT_VAR_CTRL

    SUBROUTINE INIT_VAR_CTRLRT

    INTEGER										:: I

    DO I=1,SIZE(CTRLRT)
        CALL INIT_VAR_CTRL(I)
    ENDDO

    END SUBROUTINE INIT_VAR_CTRLRT


    SUBROUTINE INIT_IMAGE_CTRL(ICTRL)

    INTEGER,INTENT(IN)				:: ICTRL
    TYPE(TYPE_CTRL),POINTER			:: OBJ
    INTEGER							:: I,J
    CHARACTER(100)					:: MESSAGE,OUTPUTJ
    CHARACTER(6)                    :: JSTRING

    NULLIFY(OBJ);OBJ=>CTRLRT(ICTRL)

    NULLIFY(OBJ%FRAC)
    NULLIFY(OBJ%SETPOINTFRAC)
    DO I=1,N_CTRL
        IF (OBJ%CTRLID==CTRLRT(I)%ID) OBJ%FRAC=>CTRLRT(I)%VALUE
        IF (OBJ%SETPOINTCTRLID==CTRLRT(I)%ID) OBJ%SETPOINTFRAC=>CTRLRT(I)%VALUE
    ENDDO
    IF (.NOT.ASSOCIATED(OBJ%FRAC)) THEN
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': CTRLID(',TRIM(OBJ%CTRLID),') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF

    NULLIFY(OBJ%PNT)

    DO I=1,N_CTRL
        IF(OBJ%QUANTITY==CTRLRT(I)%ID) THEN
            OBJ%PNT=>CTRLRT(I)%VALUE
        ENDIF
    ENDDO

    IF (.NOT.ASSOCIATED(OBJ%PNT)) THEN
        SELECT CASE (OBJ%QUANTITY)
        CASE ('TIME')
            OBJ%PNT=>TIME
        CASE ('DTIME')
            OBJ%PNT=>DTIME
        CASE ('HOUR')
            OBJ%PNT=>HOUR
        CASE ('HORAIRE')
            OBJ%PNT=>HOUR
        CASE ('VREF')
            OBJ%PNT=>MDEXT(1)%VREF
        CASE ('WINC')
            OBJ%PNT=>MDEXT(1)%WINC
        CASE ('WDIR')
            OBJ%PNT=>MDEXT(1)%WINC
        CASE ('RA')
            IF (OBJ%IP.LE.N_LOC) THEN
                OBJ%PNT=>LOCRT(OBJ%IP)%RA
            ELSE
                OBJ%PNT=>MDEXT(1)%RA
            ENDIF
        CASE ('T')
            IF (OBJ%BRANCHID=='null') THEN
                IF (OBJ%IP.LE.N_LOC) THEN
                    OBJ%PNT=>T15(OBJ%IP)
                ELSEIF (OBJ%IP.LE.N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    OBJ%PNT=>MDEXT(1)%TEXT
                ELSE
                    OBJ%PNT=>BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%TBOUND
                ENDIF
            ELSE
                OBJ%PNT=>BRANCHERT(OBJ%IP)%T15AM
            ENDIF
        CASE ('TW')
            IF (OBJ%BRANCHID=='null') THEN
                IF (OBJ%IP.LE.N_LOC) THEN
                    OBJ%PNT=>LOCRT(OBJ%IP)%TWMEAN
                ELSEIF (OBJ%IP.LE.N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    OBJ%PNT=>MDEXT(1)%TEXT
                ELSE
                    OBJ%PNT=>BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%TBOUND
                ENDIF
            ELSE
                OBJ%PNT=>BRANCHERT(OBJ%IP)%T15AM
            ENDIF
        CASE ('TOPER')
            IF (OBJ%BRANCHID=='null') THEN
                IF (OBJ%IP.LE.N_LOC) THEN
                    OBJ%PNT=>LOCRT(OBJ%IP)%TOPER
                ELSEIF (OBJ%IP.LE.N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    OBJ%PNT=>MDEXT(1)%TEXT
                ELSE
                    OBJ%PNT=>BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%TBOUND
                ENDIF
            ELSE
                OBJ%PNT=>BRANCHERT(OBJ%IP)%T15AM
            ENDIF
        CASE ('HR')
            IF (OBJ%BRANCHID=='null') THEN
                IF (OBJ%IP.LE.N_LOC) THEN
                    OBJ%PNT=>HUM15(OBJ%IP)
                ELSEIF (OBJ%IP.LE.N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    OBJ%PNT=>MDEXT(1)%HUMIDITE
                ELSE
                    OBJ%PNT=>BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%HR
                ENDIF
            ENDIF
        CASE ('YK')
            IF (OBJ%BRANCHID=='null') THEN
                IF (OBJ%IP.LE.N_LOC) THEN
                    OBJ%PNT=>YK15(OBJ%IP,OBJ%ISPEC)
                ELSEIF (OBJ%IP.LE.N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    OBJ%PNT=>MDEXT(1)%YK(OBJ%ISPEC)
                ELSE
                    OBJ%PNT=>BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%YK(OBJ%ISPEC)
                ENDIF
            ELSE
                OBJ%PNT=>BRANCHERT(OBJ%IP)%YK15AM(OBJ%ISPEC)
            ENDIF
        CASE('HRinlet')
            OBJ%PNT=>CTRLRT(OBJ%IDN)%DP
        CASE('DP')
            IF (OBJ%BRANCHID=='null') THEN
                OBJ%PNT=>CTRLRT(OBJ%IDN)%DP
            ELSE
                OBJ%PNT=>BRANCHERT(OBJ%IP)%DP
            ENDIF
        CASE('RHO')
            IF (OBJ%BRANCHID=='null') THEN
                IF (OBJ%IP.LE.N_LOC) THEN
                    OBJ%PNT=>RHO15(OBJ%IP)
                ELSEIF (OBJ%IP.LE.N_LOC+MDEXT(1)%N_ZPEXT) THEN
                    OBJ%PNT=>MDEXT(1)%RHOEXT
                ELSE
                    OBJ%PNT=>BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%RHO
                ENDIF
            ELSE
                OBJ%PNT=>BRANCHERT(OBJ%IP)%RHO15AM
            ENDIF
        CASE('PRESENCE')
            IF (OBJ%IP.LE.N_LOC) THEN
                OBJ%PNT=>LOCRT(OBJ%IP)%OCC
            ENDIF
        CASE('HEATLOSS')
            IF (OBJ%IP.LE.N_LOC) THEN
                OBJ%PNT=>HEATLOSS(OBJ%IP)
            ENDIF
        CASE('CONSTANT')
            OBJ%PNT=>CTRLRT(OBJ%IDN)%CONSTANT
        CASE('WEEKDAY')
            OBJ%PNT=>JOUR
        CASE('YEARDAY')
            OBJ%PNT=>QUANTIEME
        CASE('QM')
            IF (OBJ%BRANCHID.NE.'null') OBJ%PNT=>BRANCHERT(OBJ%IP)%QM
        CASE('QV')
            IF (OBJ%BRANCHID.NE.'null') OBJ%PNT=>BRANCHERT(OBJ%IP)%QV
        CASE('DPFLOW')
            IF (OBJ%BRANCHID.NE.'null') OBJ%PNT=>BRANCHERT(OBJ%IP)%DP
        CASE('TFLOW')
            IF (OBJ%BRANCHID.NE.'null') OBJ%PNT=>BRANCHERT(OBJ%IP)%T15AM
        CASE('YKFLOW')
            IF (OBJ%BRANCHID.NE.'null') OBJ%PNT=>BRANCHERT(OBJ%IP)%YK15AM(OBJ%ISPEC)
        CASE('RHOFLOW')
            IF (OBJ%BRANCHID.NE.'null') OBJ%PNT=>BRANCHERT(OBJ%IP)%RHO15AM
        CASE('TWNODE')
            IF (OBJ%WALLID.NE.'null') OBJ%PNT=>MURRT(OBJ%IP)%TEMP(OBJ%IWNODE)
        CASE('TP1')
            IF (OBJ%WALLID.NE.'null') OBJ%PNT=>MURRT(OBJ%IP)%TPIN
        CASE('TP2')
            IF (OBJ%WALLID.NE.'null') OBJ%PNT=>MURRT(OBJ%IP)%TPOUT
        CASE('HRWNODE')
            IF (OBJ%WALLID.NE.'null') OBJ%PNT=>MURRT(OBJ%IP)%HR(OBJ%IWNODE)
            CASE DEFAULT
            IF (OBJ%MODID.NE.'null') THEN
                DO J=1,MODRT(OBJ%IP)%NOUTPUTS
                    WRITE(JSTRING,'(i6)') J
                    OUTPUTJ='OUTPUT'//TRIM(ADJUSTL(JSTRING))
                    IF (TRIM(OBJ%QUANTITY).EQ.TRIM(OUTPUTJ)) OBJ%PNT=>MODRT(OBJ%IP)%OUTPUTS(J)
                ENDDO
            ENDIF
            IF (.NOT.ASSOCIATED(OBJ%PNT)) THEN
                WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': QUANTITY (',TRIM(OBJ%QUANTITY),') is unknown'
                CALL SHUTDOWN(MESSAGE,LUOUT)
            ENDIF
        END SELECT
    ENDIF

    CALL EVAL_CTRL(OBJ,1)
    CALL EVAL_CTRL(OBJ,2)


    END SUBROUTINE INIT_IMAGE_CTRL

    SUBROUTINE INIT_IMAGE_CTRLRT

    INTEGER										:: I

    DO I=1,SIZE(CTRLRT)
        CALL INIT_IMAGE_CTRL(I)
    ENDDO

    END SUBROUTINE INIT_IMAGE_CTRLRT



    SUBROUTINE EVAL_CTRL(OBJ,FLAG)

    INTEGER, INTENT(IN)		:: FLAG
    TYPE(TYPE_CTRL), INTENT(INOUT)	:: OBJ

    INTEGER							:: I,J,JCTRL
    DOUBLE PRECISION				:: PID_VALUE,YKEA,TEA


    !*****************************
    !gestion du DP
    !IF ((OBJ%QUANTITY=='DP').AND.(OBJ%BRANCHID=='null')) THEN
    IF ((OBJ%enum_quantity==ctrl_quantity_dp).AND.(OBJ%BRANCHID=='null')) THEN
        IF (OBJ%IP.LE.N_LOC) THEN
            OBJ%DP=LOCRT(OBJ%IP)%DP+MDEXT(1)%RHOBUOY*9.81D0*LOCRT(OBJ%IP)%ALT
        ELSEIF (OBJ%IP.LE.N_LOC+MDEXT(1)%N_ZPEXT) THEN
            OBJ%DP=MDEXT(1)%DP(OBJ%IP-N_LOC)
        ELSE
            OBJ%DP=BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%DP+MDEXT(1)%RHOBUOY*9.81D0*BOUNDRT(OBJ%IP-N_LOC-MDEXT(1)%N_ZPEXT)%ALT
        ENDIF

    ENDIF
    !ici OBJ%PNT contient l'humidite relative vue par une sonde type entree hygroreglable
    !IF (OBJ%QUANTITY=='HRinlet') THEN
    IF (OBJ%enum_quantity==ctrl_quantity_hrinlet) THEN
        YKEA=YK15(OBJ%IP,IDNH2O)
        TEA=(0.7D0)*T15(OBJ%IP)+0.3D0*MDEXT(1)%TEXT
        CALL YK_HUM_EVAL(TEA,OBJ%PNT,YKEA,0)
    ENDIF

    !SELECT CASE (OBJ%CTRLTYPE)
    SELECT CASE (OBJ%enum_ctrltype)

    !CASE ('TIMER')
    CASE (ctrl_timer)
        !SELECT CASE (OBJ%FUNCTION)
        SELECT CASE (OBJ%enum_function)
        !CASE ('LAST') ! obsolete********************************
        CASE (ctrl_function_last)
            IF (FLAG==2) THEN
                IF (OBJ%PNT>=OBJ%RAMP(2,1)) THEN
                    OBJ%TIMER=TIME+(OBJ%CONSTANT)*OBJ%TIMESCALE
                ENDIF
            ENDIF
            IF (OBJ%TIMER>=TIME) THEN
                OBJ%VALUE=OBJ%RAMP(2,2)
            ELSE
                OBJ%VALUE=OBJ%RAMP(1,2)
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        !CASE ('FIRST') ! obsolete****************************************
        CASE (ctrl_function_first)
            IF (FLAG==2) THEN
                IF ((OBJ%PNT>=OBJ%RAMP(2,1)).AND.(OBJ%TIMER<=TIME)) THEN
                    OBJ%TIMER=TIME+(OBJ%CONSTANT)*OBJ%TIMESCALE
                ENDIF
            ENDIF
            IF (OBJ%TIMER>=TIME) THEN
                OBJ%VALUE=OBJ%RAMP(2,2)
            ELSE
                OBJ%VALUE=OBJ%RAMP(1,2)
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
            
        !CASE ('UPWARD_LAST') 
        CASE (ctrl_function_upward_last)
            IF (FLAG==2) THEN
                IF (OBJ%PNT>=(OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%HYSTERESIS)) THEN
                    OBJ%TIMER=TIME+(OBJ%DURATION)*OBJ%TIMESCALE
                ENDIF
            ENDIF
            IF (OBJ%TIMER>=TIME) THEN
                OBJ%VALUE=1
            ELSE
                OBJ%VALUE=0
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        !CASE ('UPWARD_FIRST')
        CASE (ctrl_function_upward_first)
            IF (FLAG==2) THEN
                IF ((OBJ%PNT>=OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%HYSTERESIS).AND.(OBJ%TIMER<=TIME)) THEN
                    OBJ%TIMER=TIME+(OBJ%DURATION)*OBJ%TIMESCALE
                ENDIF
            ENDIF
            IF (OBJ%TIMER>=TIME) THEN
                OBJ%VALUE=1
            ELSE
                OBJ%VALUE=0
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF    
        !CASE ('DOWNWARD_LAST') 
        CASE (ctrl_function_downward_last)
            IF (FLAG==2) THEN
                IF (OBJ%PNT<=(OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%HYSTERESIS)) THEN
                    OBJ%TIMER=TIME+(OBJ%DURATION)*TIMESCALE
                ENDIF
            ENDIF
            IF (OBJ%TIMER>=TIME) THEN
                OBJ%VALUE=1
            ELSE
                OBJ%VALUE=0
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        !CASE ('DOWNWARD_FIRST')
        CASE (ctrl_function_downward_first)
            IF (FLAG==2) THEN
                IF ((OBJ%PNT<=OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%HYSTERESIS).AND.(OBJ%TIMER<=TIME)) THEN
                    OBJ%TIMER=TIME+(OBJ%DURATION)*OBJ%TIMESCALE
                ENDIF
            ENDIF
            IF (OBJ%TIMER>=TIME) THEN
                OBJ%VALUE=1
            ELSE
                OBJ%VALUE=0
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF    
        CASE DEFAULT
            WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        END SELECT
            
    !CASE ('RAMP')!Controleurs reels  de type rampe
    CASE (ctrl_ramp)
        !SELECT CASE (OBJ%FUNCTION)
        SELECT CASE (OBJ%enum_function)
            !CASE ('LINEAR')
            CASE (ctrl_function_linear)
                    I=1
                    DO WHILE (OBJ%PNT>=OBJ%RAMP(I+1,1))
                        I=I+1
                        IF (I==SIZE(OBJ%RAMP(:,1))-1) EXIT
                    ENDDO
                    OBJ%VALUE=OBJ%RAMP(I,2)+(OBJ%RAMP(I+1,2)-OBJ%RAMP(I,2))*(OBJ%PNT-OBJ%RAMP(I,1))/(OBJ%RAMP(I+1,1)-OBJ%RAMP(I,1))
                    IF (OBJ%VALUE==0.D0) THEN
                        OBJ%STATE=.FALSE.
                    ELSE
                        OBJ%STATE=.TRUE.
                    ENDIF

            !CASE ('CRENEL')
            CASE (ctrl_function_crenel)
                I=1
                DO WHILE (OBJ%PNT>=OBJ%RAMP(I+1,1))
                    I=I+1
                ENDDO
                OBJ%VALUE=OBJ%RAMP(I,2)
                IF (OBJ%VALUE==0.D0) THEN
                    OBJ%STATE=.FALSE.
                ELSE
                    OBJ%STATE=.TRUE.
                ENDIF

            !CASE ('HYSTERESIS_UPWARD')
            CASE (ctrl_function_hysteresis_upward)
                I=1
                ! Identification de l'index de l'etat precedent sur la sortie (RAMP( :,2))
                DO WHILE (OBJ%PREVIOUS_VALUE>OBJ%RAMP(I,2))
                    I=I+1
                ENDDO
                ! identification si l'etat courant se situe hors de l'intervalle de l'etat precedent alors il faut changer la valeur, sinon, on change rien
                IF ((OBJ%PNT.LE.OBJ%RAMP(I,1)-OBJ%HYSTERESIS).OR.(OBJ%PNT.GE.OBJ%RAMP(I+1,1))) THEN
                    ! Identification si montee ou descente
                    IF (OBJ%PNT.GE.OBJ%RAMP(I+1,1)) THEN
                        J=1
                        DO WHILE (OBJ%PNT.GT.OBJ%RAMP(J,1))
                            J=J+1
                        ENDDO
                    ELSE
                        J=1
                        DO WHILE (OBJ%PNT.GT.OBJ%RAMP(J,1)-OBJ%HYSTERESIS)
                            J=J+1
                        ENDDO
                    ENDIF
                    IF (J>1) THEN
                        OBJ%VALUE=OBJ%RAMP(J-1,2)
                    ELSE
                        OBJ%VALUE=OBJ%RAMP(1,2)
                    ENDIF
                ELSE
                    OBJ%VALUE=OBJ%PREVIOUS_VALUE
                ENDIF
                IF (FLAG==2) OBJ%PREVIOUS_VALUE= OBJ%VALUE
                IF (OBJ%VALUE==0.D0) THEN
                    OBJ%STATE=.FALSE.
                ELSE
                    OBJ%STATE=.TRUE.
                ENDIF

            !CASE ('HYSTERESIS_DOWNWARD')
            CASE (ctrl_function_hysteresis_downward)
                I=1
                ! Identification de l'index de l'etat precedent sur la sortie (RAMP( :,2))
                DO WHILE (OBJ%PREVIOUS_VALUE<OBJ%RAMP(I,2))
                    I=I+1
                ENDDO
                ! identification si l'etat courant se situe hors de l'intervalle de l'etat precedent alors il faut changer la valeur, sinon, on change rien
                IF ((OBJ%PNT.LE.OBJ%RAMP(I,1)-OBJ%HYSTERESIS).OR.(OBJ%PNT.GE.OBJ%RAMP(I+1,1))) THEN
                    ! Identification si montee ou descente
                    IF (OBJ%PNT.GE.OBJ%RAMP(I+1,1)) THEN
                        J=1
                        DO WHILE (OBJ%PNT.GT.OBJ%RAMP(J,1))
                            J=J+1
                        ENDDO
                    ELSE
                        J=1
                        DO WHILE (OBJ%PNT.GT.OBJ%RAMP(J,1)-OBJ%HYSTERESIS)
                            J=J+1
                        ENDDO
                    ENDIF
                    IF (J>1) THEN
                        OBJ%VALUE=OBJ%RAMP(J-1,2)
                    ELSE
                        OBJ%VALUE=OBJ%RAMP(1,2)
                    ENDIF
                ELSE
                    OBJ%VALUE=OBJ%PREVIOUS_VALUE
                ENDIF
                IF (FLAG==2) OBJ%PREVIOUS_VALUE= OBJ%VALUE
                IF (OBJ%VALUE==0.D0) THEN
                    OBJ%STATE=.FALSE.
                ELSE
                    OBJ%STATE=.TRUE.
                ENDIF
                    
            CASE DEFAULT
                WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
                CALL SHUTDOWN(MESSAGE,LUOUT)
        END SELECT

    !CASE ('PROBE')!Controleurs reels  de type sonde
    CASE (ctrl_probe)
        !SELECT CASE (OBJ%FUNCTION)
        SELECT CASE (OBJ%enum_function)
        !CASE ('VALUE')
        CASE(ctrl_function_value)
            !IF ((OBJ%QUANTITY=='T').OR.(OBJ%QUANTITY=='TW').OR.(OBJ%QUANTITY=='TOPER').OR.(OBJ%QUANTITY=='TFLOW').OR.(OBJ%QUANTITY=='TWNODE').OR.(OBJ%QUANTITY=='TP1').OR.(OBJ%QUANTITY=='TP2')) THEN
            if( CtrlQuantity_Family(OBJ%enum_quantity , ctrl_quantity_groupe1)) then
                OBJ%VALUE=OBJ%PNT-273.15D0
            !ELSEIF(OBJ%QUANTITY=='QV') THEN
            else if(OBJ%enum_quantity == ctrl_quantity_qv) then
                OBJ%VALUE=OBJ%PNT*3600.D0
            !ELSEIF (OBJ%QUANTITY=='DP') THEN
            else if(OBJ%enum_quantity == ctrl_quantity_dp) then
                OBJ%VALUE=OBJ%PNT 
            ELSE
                OBJ%VALUE=OBJ%PNT
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF

        !CASE ('DERIVATIVE')
        CASE(ctrl_function_derivative)
            !IF ((OBJ%QUANTITY=='T').OR.(OBJ%QUANTITY=='TW').OR.(OBJ%QUANTITY=='TOPER').OR.(OBJ%QUANTITY=='TFLOW').OR.(OBJ%QUANTITY=='TWNODE').OR.(OBJ%QUANTITY=='TP1').OR.(OBJ%QUANTITY=='TP2')) THEN
            if( CtrlQuantity_Family(OBJ%enum_quantity , ctrl_quantity_groupe1)) then
                OBJ%CURRENT_VALUE=(OBJ%PNT-273.15D0)
            !ELSEIF(OBJ%QUANTITY=='QV') THEN
            else if(OBJ%enum_quantity == ctrl_quantity_qv) then
                OBJ%CURRENT_VALUE=OBJ%PNT*3600.D0
            ELSE
                OBJ%CURRENT_VALUE=OBJ%PNT
            ENDIF
            OBJ%VALUE=(OBJ%CURRENT_VALUE-OBJ%PREVIOUS_VALUE)/DTIME
            IF (FLAG==2) OBJ%PREVIOUS_VALUE=OBJ%CURRENT_VALUE

            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF

        !CASE ('PREVIOUS_VALUE')
        CASE(ctrl_function_previous_value)
            IF (FLAG.NE.2) THEN
                !IF ((OBJ%QUANTITY=='T').OR.(OBJ%QUANTITY=='TW').OR.(OBJ%QUANTITY=='TOPER').OR.(OBJ%QUANTITY=='TFLOW').OR.(OBJ%QUANTITY=='TWNODE').OR.(OBJ%QUANTITY=='TP1').OR.(OBJ%QUANTITY=='TP2')) THEN
                if( CtrlQuantity_Family(OBJ%enum_quantity , ctrl_quantity_groupe1)) then
                    OBJ%CURRENT_VALUE=(OBJ%PNT-273.15D0)
                !ELSEIF(OBJ%QUANTITY=='QV') THEN
                else if(OBJ%enum_quantity == ctrl_quantity_qv) then
                    OBJ%CURRENT_VALUE=OBJ%PNT*3600.D0
                ELSE
                    OBJ%CURRENT_VALUE=OBJ%PNT
                ENDIF
            ENDIF

            OBJ%VALUE=OBJ%PREVIOUS_VALUE

            IF (FLAG==2) THEN
                OBJ%PREVIOUS_VALUE=OBJ%CURRENT_VALUE
            ENDIF

            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF

        !CASE ('TIME_AVERAGE')
        CASE(ctrl_function_time_average)
            IF (FLAG.NE.2) THEN
                !IF ((OBJ%QUANTITY=='T').OR.(OBJ%QUANTITY=='TW').OR.(OBJ%QUANTITY=='TOPER').OR.(OBJ%QUANTITY=='TFLOW').OR.(OBJ%QUANTITY=='TWNODE').OR.(OBJ%QUANTITY=='TP1').OR.(OBJ%QUANTITY=='TP2')) THEN
                if( CtrlQuantity_Family(OBJ%enum_quantity , ctrl_quantity_groupe1)) then
                    OBJ%CURRENT_VALUE=(OBJ%PNT-273.15D0)
                !ELSEIF(OBJ%QUANTITY=='QV') THEN
                else if(OBJ%enum_quantity == ctrl_quantity_qv) then
                    OBJ%CURRENT_VALUE=OBJ%PNT*3600.D0
                ELSE
                    OBJ%CURRENT_VALUE=OBJ%PNT
                ENDIF
            ENDIF
            OBJ%TIMEVALUES(SIZE(OBJ%TIMEVALUES))=OBJ%CURRENT_VALUE    
            IF (FLAG==2) THEN
                IF (SIZE(OBJ%TIMEVALUES).GT.1) THEN
                    OBJ%TIMEVALUES(1:SIZE(OBJ%TIMEVALUES)-1)=OBJ%TIMEVALUES(2:SIZE(OBJ%TIMEVALUES))
                ENDIF
                IF (OBJ%TIMEVALUES(1)==-9999.D0) THEN
                    OBJ%TIMEVALUES(:)=OBJ%CURRENT_VALUE
                ENDIF  
            ENDIF
            OBJ%VALUE=SUM(OBJ%TIMEVALUES)/SIZE(OBJ%TIMEVALUES)

            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF

        CASE DEFAULT
            WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        END SELECT

    !CASE ('SETPOINT')!Controleurs booleens de type consigne
    CASE (ctrl_setpoint)
        IF (FLAG==2) THEN
            SELECT CASE (OBJ%FUNCTION)
            CASE ('DOWNWARD')
                IF (OBJ%CONSTANT.NE.0.D0) THEN !obsolete
                    IF (OBJ%PNT>=(OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%CONSTANT)) THEN
                        OBJ%STATE=.FALSE.
                    ELSEIF (OBJ%PNT<=(OBJ%SETPOINT*OBJ%SETPOINTFRAC-OBJ%CONSTANT)) THEN
                        OBJ%STATE=.TRUE.
                    ENDIF
                ELSE
                    IF (OBJ%PNT>=(OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%HYSTERESIS)) THEN
                        OBJ%STATE=.FALSE.
                    ELSEIF (OBJ%PNT<=(OBJ%SETPOINT*OBJ%SETPOINTFRAC-OBJ%HYSTERESIS)) THEN
                        OBJ%STATE=.TRUE.
                    ENDIF
                ENDIF
                IF (OBJ%STATE.EQV..TRUE.) OBJ%VALUE=1.D0
                IF (OBJ%STATE.EQV..FALSE.) OBJ%VALUE=0.D0

            CASE ('UPWARD')
                IF (OBJ%CONSTANT.NE.0.D0) THEN !obsolete
                    IF (OBJ%PNT>=(OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%CONSTANT)) THEN
                        OBJ%STATE=.TRUE.
                    ELSEIF (OBJ%PNT<=(OBJ%SETPOINT*OBJ%SETPOINTFRAC-OBJ%CONSTANT)) THEN
                        OBJ%STATE=.FALSE.
                    ENDIF
                ELSE
                    IF (OBJ%PNT>=(OBJ%SETPOINT*OBJ%SETPOINTFRAC+OBJ%HYSTERESIS)) THEN
                        OBJ%STATE=.TRUE.
                    ELSEIF (OBJ%PNT<=(OBJ%SETPOINT*OBJ%SETPOINTFRAC-OBJ%HYSTERESIS)) THEN
                        OBJ%STATE=.FALSE.
                    ENDIF
                ENDIF
                IF (OBJ%STATE.EQV..TRUE.) OBJ%VALUE=1.D0
                IF (OBJ%STATE.EQV..FALSE.) OBJ%VALUE=0.D0

            CASE DEFAULT
                WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
                CALL SHUTDOWN(MESSAGE,LUOUT)
            END SELECT
        ENDIF

    !CASE ('PID_SETPOINT')!Controleurs booleens de type consigne avec boucle de regulation PID
    CASE (ctrl_pid_setpoint)
        IF (FLAG==2) THEN
            SELECT CASE (OBJ%FUNCTION)
            CASE ('DOWNWARD')
                OBJ%ERROR=OBJ%SETPOINT*OBJ%SETPOINTFRAC-OBJ%PNT

            CASE ('UPWARD')
                OBJ%ERROR=OBJ%PNT-OBJ%SETPOINT*OBJ%SETPOINTFRAC

            CASE DEFAULT
                WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
                CALL SHUTDOWN(MESSAGE,LUOUT)
            END SELECT

            !IF (TIME.NE.TETA0) OBJ%INTEGRAL=OBJ%INTEGRAL+0.5D0*DTIME*(OBJ%ERROR+OBJ%PREVIOUS_ERROR)
            IF (TIME.NE.TETA0) OBJ%INTEGRAL=0.5D0*DTIME*(OBJ%ERROR+OBJ%PREVIOUS_ERROR)
            PID_VALUE=OBJ%PROPORTIONAL_GAIN*OBJ%ERROR+OBJ%INTEGRAL_GAIN*OBJ%INTEGRAL+OBJ%DIFFERENTIAL_GAIN*(OBJ%ERROR-OBJ%PREVIOUS_ERROR)/DTIME
            IF (FLAG==2) OBJ%PREVIOUS_ERROR=OBJ%ERROR
            !OBJ%VALUE=MIN(MAX(PID_VALUE,0.D0),1.D0)
            OBJ%VALUE=PID_VALUE
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF

        ENDIF

    !CASE ('OPERATOR')!Controleurs reels de type operateurs
    CASE (ctrl_operator)

        DO JCTRL=1,OBJ%NCTRLIDNS
            SELECT CASE (OBJ%QUANTITIES(JCTRL))
            CASE ('CONSTANT')
                OBJ%CTRLVALUES(JCTRL)=OBJ%CONSTANT
            CASE ('TIME')
                OBJ%CTRLVALUES(JCTRL)=TIME
            CASE ('HOUR')
                OBJ%CTRLVALUES(JCTRL)=HOUR
            CASE ('WEEKDAY')
                OBJ%CTRLVALUES(JCTRL)=JOUR
            CASE ('YEARDAY')
                OBJ%CTRLVALUES(JCTRL)=QUANTIEME
            CASE ('VREF')
                OBJ%CTRLVALUES(JCTRL)=MDEXT(1)%VREF
                CASE DEFAULT
                OBJ%CTRLVALUES(JCTRL)=CTRLRT(OBJ%CTRLIDNS(JCTRL))%VALUE
                OBJ%CTRLSTATES(JCTRL)=CTRLRT(OBJ%CTRLIDNS(JCTRL))%STATE
            END SELECT
            IF ((OBJ%CTRLVALUES(JCTRL)==0.D0).AND.(OBJ%CTRLIDNS(JCTRL)==0)) THEN
                OBJ%CTRLSTATES(JCTRL)=.FALSE.
            ELSEIF (OBJ%CTRLIDNS(JCTRL)==0) THEN
                OBJ%CTRLSTATES(JCTRL)=.TRUE.
            ENDIF
        ENDDO

        SELECT CASE (OBJ%FUNCTION)
        CASE ('ADD')
            OBJ%VALUE=OBJ%CTRLVALUES(1)
            DO JCTRL=2,OBJ%NCTRLIDNS
                OBJ%VALUE=OBJ%VALUE+OBJ%CTRLVALUES(JCTRL)
            ENDDO
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('SUBSTRACT')
            OBJ%VALUE=OBJ%CTRLVALUES(1)
            DO JCTRL=2,OBJ%NCTRLIDNS
                OBJ%VALUE=OBJ%VALUE-OBJ%CTRLVALUES(JCTRL)
            ENDDO
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('MULTIPLY')
            OBJ%VALUE=PRODUCT(OBJ%CTRLVALUES)
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('DIVIDE')
            OBJ%VALUE=OBJ%CTRLVALUES(1)
            DO JCTRL=2,OBJ%NCTRLIDNS
                IF (OBJ%CTRLVALUES(JCTRL)==0) THEN
                    WRITE(MESSAGE,'(A,A,A,i0,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': QUANTITIES(',JCTRL,') reached 0 during operation.'
                    CALL SHUTDOWN(MESSAGE,LUOUT)
                ENDIF
                OBJ%VALUE=OBJ%VALUE/OBJ%CTRLVALUES(JCTRL)
            ENDDO
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('POWER')
            OBJ%VALUE=OBJ%CTRLVALUES(1)
            DO JCTRL=2,OBJ%NCTRLIDNS
                IF (OBJ%VALUE<0) THEN
                    WRITE(MESSAGE,'(A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': Negative result during operation.'
                    CALL SHUTDOWN(MESSAGE,LUOUT)
                ENDIF
                OBJ%VALUE=OBJ%VALUE**OBJ%CTRLVALUES(JCTRL)
            ENDDO
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('MAX')
            OBJ%VALUE=MAXVAL(OBJ%CTRLVALUES)
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('MIN')
            OBJ%VALUE=MINVAL(OBJ%CTRLVALUES)
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('AVERAGE')
            OBJ%VALUE=SUM(OBJ%CTRLVALUES)/OBJ%NCTRLIDNS
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE DEFAULT
            WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        END SELECT

    !CASE ('COMPARISON_OPERATOR')!Controleurs logiques de type comparateurs
    CASE (ctrl_comparison_operator)

        DO JCTRL=1,OBJ%NCTRLIDNS
            SELECT CASE (OBJ%QUANTITIES(JCTRL))
            CASE ('CONSTANT')
                OBJ%CTRLVALUES(JCTRL)=OBJ%CONSTANT
            CASE ('TIME')
                OBJ%CTRLVALUES(JCTRL)=TIME
            CASE ('HOUR')
                OBJ%CTRLVALUES(JCTRL)=HOUR
            CASE ('WEEKDAY')
                OBJ%CTRLVALUES(JCTRL)=JOUR
            CASE ('YEARDAY')
                OBJ%CTRLVALUES(JCTRL)=QUANTIEME
            CASE ('VREF')
                OBJ%CTRLVALUES(JCTRL)=MDEXT(1)%VREF
                CASE DEFAULT
                OBJ%CTRLVALUES(JCTRL)=CTRLRT(OBJ%CTRLIDNS(JCTRL))%VALUE
                OBJ%CTRLSTATES(JCTRL)=CTRLRT(OBJ%CTRLIDNS(JCTRL))%STATE
            END SELECT
            IF ((OBJ%CTRLVALUES(JCTRL)==0.D0).AND.(OBJ%CTRLIDNS(JCTRL)==0)) THEN
                OBJ%CTRLSTATES(JCTRL)=.FALSE.
            ELSEIF (OBJ%CTRLIDNS(JCTRL)==0) THEN
                OBJ%CTRLSTATES(JCTRL)=.TRUE.
            ENDIF
        ENDDO

        SELECT CASE (OBJ%FUNCTION)
        CASE ('==')
            IF (OBJ%CTRLVALUES(1).EQ.OBJ%CTRLVALUES(2)) THEN
                OBJ%VALUE=1.D0
                OBJ%STATE=.TRUE.
            ELSE
                OBJ%VALUE=0.D0
                OBJ%STATE=.FALSE.
            ENDIF
        CASE ('/=')
            IF (OBJ%CTRLVALUES(1).NE.OBJ%CTRLVALUES(2)) THEN
                OBJ%VALUE=1.D0
                OBJ%STATE=.TRUE.
            ELSE
                OBJ%VALUE=0.D0
                OBJ%STATE=.FALSE.
            ENDIF
        CASE ('>=')
            IF (OBJ%CTRLVALUES(1).GE.OBJ%CTRLVALUES(2)) THEN
                OBJ%VALUE=1.D0
                OBJ%STATE=.TRUE.
            ELSE
                OBJ%VALUE=0.D0
                OBJ%STATE=.FALSE.
            ENDIF
        CASE ('>')
            IF (OBJ%CTRLVALUES(1).GT.OBJ%CTRLVALUES(2)) THEN
                OBJ%VALUE=1.D0
                OBJ%STATE=.TRUE.
            ELSE
                OBJ%VALUE=0.D0
                OBJ%STATE=.FALSE.
            ENDIF
        CASE ('<=')
            IF (OBJ%CTRLVALUES(1).LE.OBJ%CTRLVALUES(2)) THEN
                OBJ%VALUE=1.D0
                OBJ%STATE=.TRUE.
            ELSE
                OBJ%VALUE=0.D0
                OBJ%STATE=.FALSE.
            ENDIF
        CASE ('<')
            IF (OBJ%CTRLVALUES(1).LT.OBJ%CTRLVALUES(2)) THEN
                OBJ%VALUE=1.D0
                OBJ%STATE=.TRUE.
            ELSE
                OBJ%VALUE=0.D0
                OBJ%STATE=.FALSE.
            ENDIF
        CASE DEFAULT
            WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        END SELECT

    !CASE ('LOGICAL_OPERATOR')!!Controleurs logique de type operateurs
    CASE (ctrl_logical_operator)

        DO JCTRL=1,OBJ%NCTRLIDNS
            SELECT CASE (OBJ%QUANTITIES(JCTRL))
            CASE ('CONSTANT')
                OBJ%CTRLVALUES(JCTRL)=OBJ%CONSTANT
            CASE ('TIME')
                OBJ%CTRLVALUES(JCTRL)=TIME
            CASE ('HOUR')
                OBJ%CTRLVALUES(JCTRL)=HOUR
            CASE ('WEEKDAY')
                OBJ%CTRLVALUES(JCTRL)=JOUR
            CASE ('YEARDAY')
                OBJ%CTRLVALUES(JCTRL)=QUANTIEME
            CASE ('VREF')
                OBJ%CTRLVALUES(JCTRL)=MDEXT(1)%VREF
            CASE DEFAULT
                OBJ%CTRLVALUES(JCTRL)=CTRLRT(OBJ%CTRLIDNS(JCTRL))%VALUE
                OBJ%CTRLSTATES(JCTRL)=CTRLRT(OBJ%CTRLIDNS(JCTRL))%STATE
            END SELECT
            IF ((OBJ%CTRLVALUES(JCTRL)==0.D0).AND.(OBJ%CTRLIDNS(JCTRL)==0)) THEN
                OBJ%CTRLSTATES(JCTRL)=.FALSE.
            ELSEIF (OBJ%CTRLIDNS(JCTRL)==0) THEN
                OBJ%CTRLSTATES(JCTRL)=.TRUE.
            ENDIF
        ENDDO

        SELECT CASE (OBJ%FUNCTION)
        CASE ('ALL')
            OBJ%STATE=ALL(OBJ%CTRLSTATES)
            IF (OBJ%STATE.EQV..TRUE.) THEN
                OBJ%VALUE=1.D0
            ELSE
                OBJ%VALUE=0.D0
            ENDIF
        CASE ('ANY')
            OBJ%STATE=ANY(OBJ%CTRLSTATES)
            IF (OBJ%STATE.EQV..TRUE.) THEN
                OBJ%VALUE=1.D0
            ELSE
                OBJ%VALUE=0.D0
            ENDIF
        CASE ('COUNT')
            OBJ%VALUE=REAL(COUNT(OBJ%CTRLSTATES),KIND(OBJ%VALUE))
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE ('NOT')
            OBJ%VALUE=PRODUCT(OBJ%CTRLVALUES)
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%VALUE=1.D0
            ELSE
                OBJ%VALUE=0.D0
            ENDIF
            IF (OBJ%VALUE==0.D0) THEN
                OBJ%STATE=.FALSE.
            ELSE
                OBJ%STATE=.TRUE.
            ENDIF
        CASE DEFAULT
            WRITE(MESSAGE,'(A,A,A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': FUNCTION (',TRIM(OBJ%FUNCTION),') is unknown for CTRLTYPE (',TRIM(OBJ%CTRLTYPE),')'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        END SELECT

    !CASE ('PASSIVE')
    CASE (ctrl_passive)
        IF (OBJ%VALUE==0.D0) THEN
            OBJ%STATE=.FALSE.
        ELSE
            OBJ%STATE=.TRUE.
        ENDIF

    CASE DEFAULT
        WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with CTRL ',TRIM(OBJ%ID),': CTRLTYPE (',OBJ%CTRLTYPE,') is unknown'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    END SELECT
        
    OBJ%VALUE=OBJ%FRAC*OBJ%VALUE
    IF (OBJ%VALUE==0.D0) THEN
        OBJ%STATE=.FALSE.
    ELSE
        OBJ%STATE=.TRUE.
    ENDIF

    END SUBROUTINE EVAL_CTRL

    SUBROUTINE EVAL_CTRLRT(FLAG)

    INTEGER									  :: I,IDN,FLAG

    IF (FLAG==2) THEN
        DO I=1,SIZE(CTRLRT)
            CALL EVAL_CTRL(CTRLRT(I),FLAG)
        ENDDO
    ELSEIF (FLAG==1) THEN
        DO I=1,SIZE(IDNFLAG1)
            IDN=IDNFLAG1(I)
            CALL EVAL_CTRL(CTRLRT(IDN),FLAG)
        ENDDO
    ELSEIF (FLAG==0) THEN
        DO I=1,SIZE(IDNFLAG0)
            IDN=IDNFLAG0(I)
            CALL EVAL_CTRL(CTRLRT(IDN),FLAG)
        ENDDO
    ENDIF

    END SUBROUTINE EVAL_CTRLRT

    SUBROUTINE READ_RAMP(OBJ)

    TYPE(TYPE_CTRL),INTENT(INOUT)	:: OBJ
    INTEGER							:: I
    LOGICAL							:: EX

    INQUIRE(FILE=OBJ%RAMPFILE,EXIST=EX)
    IF (.NOT.EX) THEN
        WRITE(MESSAGE,'(A,A,A)') 'ERROR - Problem with CTRL (',TRIM(OBJ%ID),'): RAMPFILE is unknown.'
        CALL SHUTDOWN(MESSAGE,LUOUT)
    ENDIF
    I=0
    OPEN(UNIT=LU3,FILE=OBJ%RAMPFILE,FORM="FORMATTED",STATUS="OLD",ACCESS="sequential",ACTION="read")
    COUNTLOOP: DO
        I=I+1
        READ(LU3,*,END=10)
    ENDDO COUNTLOOP
10                              CLOSE(LU3)
    ALLOCATE(OBJ%RAMP(I+1,2))
    OBJ%RAMP(:,2)=1.D0
    OBJ%RAMP(:,1)=5.D16
    OBJ%RAMP(1,1)=0.D0
    I=0
    OPEN(UNIT=LU3,FILE=OBJ%RAMPFILE,FORM="FORMATTED",STATUS="OLD",ACCESS="sequential",ACTION="read")
    READLOOP: DO
        I=I+1
        READ(LU3,*,END=20) OBJ%RAMP(I,:)
    ENDDO READLOOP
20                              CLOSE(LU3)

    END SUBROUTINE READ_RAMP


    SUBROUTINE UPDATE_CTRL(ICTRL,FLAG)

    INTEGER,INTENT(IN)					:: ICTRL,FLAG
    TYPE(TYPE_CTRL),POINTER				:: OBJ

    OBJ=>CTRLRT(ICTRL)

    IF (FLAG==1) THEN
        OBJ%VALUE_OLD=OBJ%VALUE
        OBJ%STATE_OLD=OBJ%STATE
        OBJ%PREVIOUS_VALUE_OLD=OBJ%PREVIOUS_VALUE
        OBJ%INTEGRAL_OLD=OBJ%INTEGRAL
    ENDIF
    CALL EVAL_CTRL(OBJ,2)

    END SUBROUTINE UPDATE_CTRL

    SUBROUTINE UPDATE_CTRLRT(FLAG)

    INTEGER,INTENT(IN)					:: FLAG
    INTEGER								:: I

    DO I=1,SIZE(CTRLRT)
        CALL UPDATE_CTRL(I,FLAG)
    ENDDO

    END SUBROUTINE UPDATE_CTRLRT

    SUBROUTINE REWIND_CTRL(ICTRL,FLAG)

    INTEGER,INTENT(IN)					:: ICTRL,FLAG
    TYPE(TYPE_CTRL),POINTER				:: OBJ

    OBJ=>CTRLRT(ICTRL)
    OBJ%PREVIOUS_VALUE=OBJ%PREVIOUS_VALUE_OLD
    OBJ%INTEGRAL=OBJ%INTEGRAL_OLD

    END SUBROUTINE REWIND_CTRL

    SUBROUTINE REWIND_CTRLRT(FLAG)

    INTEGER,INTENT(IN)					:: FLAG
    INTEGER								:: I

    DO I=1,SIZE(CTRLRT)
        CALL REWIND_CTRL(I,FLAG)
    ENDDO

    END SUBROUTINE REWIND_CTRLRT

END MODULE PROC_CTRL_MODULE
