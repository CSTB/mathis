    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE READ_NML_PERSON_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    CHARACTER(100)							:: ID,LOCCTRLID,WAKECTRLID,PERSONTYPE
    CHARACTER(100),DIMENSION(NLOCMAX)		:: LOCIDS
    CHARACTER(100),DIMENSION(NSPECMAX)		:: SPECIDS
    DOUBLE PRECISION,DIMENSION(2)			:: HFLUX
    DOUBLE PRECISION,DIMENSION(NSPECMAX,2)	:: MKFLUX

    NAMELIST /PERSON/ ID,LOCIDS,LOCCTRLID,WAKECTRLID,MKFLUX,HFLUX,SPECIDS,PERSONTYPE

    CONTAINS

    SUBROUTINE DEFAULT_NML_PERSON

    TYPE(TYPE_PERSON)		:: OBJ

    ID=OBJ%ID
    LOCIDS=OBJ%LOCIDS;LOCIDS(1:2)=(/'EXT','null'/)
    SPECIDS=OBJ%SPECIDS;SPECIDS(1:2)=(/'CO2','H2O'/)
    MKFLUX=OBJ%MKFLUX;MKFLUX(1:2,1)=(/29.28453D0,55.D0/);MKFLUX(1:2,2)=(/18.30283D0,38.D0/)
    HFLUX=OBJ%HFLUX
    LOCCTRLID=OBJ%LOCCTRLID
    WAKECTRLID=OBJ%WAKECTRLID
    PERSONTYPE=OBJ%PERSONTYPE


    END SUBROUTINE DEFAULT_NML_PERSON
    
    SUBROUTINE PRINT_DEFAULT_NML_PERSON(LUOUT)
    
    INTEGER,INTENT(IN)			:: LUOUT
    
    CALL DEFAULT_NML_PERSON
    WRITE(LUOUT,NML=PERSON,DELIM='APOSTROPHE ')
    
    END SUBROUTINE PRINT_DEFAULT_NML_PERSON

    SUBROUTINE SET_NML_PERSON(OBJ)

    TYPE(TYPE_PERSON),INTENT(INOUT)	:: OBJ

    OBJ%ID=ID
    OBJ%LOCIDS=LOCIDS
    OBJ%SPECIDS=SPECIDS
    OBJ%MKFLUX=MKFLUX
    OBJ%HFLUX=HFLUX
    OBJ%LOCCTRLID=LOCCTRLID
    OBJ%WAKECTRLID=WAKECTRLID
    OBJ%PERSONTYPE=PERSONTYPE

    END SUBROUTINE SET_NML_PERSON

    SUBROUTINE COUNT_NML_PERSON(LU1,LUOUT,N_OBJ)

    INTEGER,INTENT(IN)			:: LU1,LUOUT
    INTEGER,INTENT(OUT)			:: N_OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    N_OBJ=0
    REWIND(LU1)
    COUNT_OBJ_LOOP: DO
        CALL CHECKREAD('PERSON',LU1,IOS)
        IF (IOS==0) EXIT COUNT_OBJ_LOOP
        READ(LU1,NML=PERSON,END=209,ERR=210,IOSTAT=IOS)
        N_OBJ=N_OBJ+1
210     IF (IOS>0) THEN
            WRITE(LUOUT,NML=PERSON)
            WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &PERSON line number ',N_OBJ+1,': check parameters names and values'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
    ENDDO COUNT_OBJ_LOOP
209 REWIND(LU1)

    END SUBROUTINE COUNT_NML_PERSON

    SUBROUTINE READ_NML_PERSON(LU1,LUOUT,OBJRT)

    INTEGER,INTENT(IN)								:: LU1,LUOUT
    TYPE(TYPE_PERSON),DIMENSION(:),INTENT(INOUT)	:: OBJRT
    CHARACTER(256)									:: MESSAGE
    INTEGER											:: I,IOS

    REWIND(LU1)
    SET_OBJ_LOOP: DO I=1,SIZE(OBJRT)
        CALL CHECKREAD('PERSON',LU1,IOS)
        IF (IOS==0) EXIT SET_OBJ_LOOP
        CALL DEFAULT_NML_PERSON
        READ(LU1,PERSON)
        CALL SET_NML_PERSON(OBJRT(I))
    ENDDO SET_OBJ_LOOP
    REWIND(LU1)

    END SUBROUTINE READ_NML_PERSON

    END MODULE READ_NML_PERSON_MODULE
