    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    PROGRAM MATHIS

    USE GLOBAL_VAR_MODULE,ONLY: LU2,MESSAGE,ATEC,GET_ARGUMENTS
    USE SOLVER_MODULE,ONLY: INIT_SOLVER,SOLVE_TIMESTEP
    USE POST_ATEC_MODULE,ONLY:  WRITE_ATEC_RES
    USE PROC_MOD_MODULE,ONLY: UPDATE_MODRT
    !SAM pour flush fichiers
    USE PROC_SAVE_RES_MODULE,ONLY: WRITE_BUFFERS

    IMPLICIT NONE

    DOUBLE PRECISION				            :: TDEBUT,TFIN,PASDETEMPS
    DOUBLE PRECISION							:: T,TCPU,TDEB
    INTEGER										:: LU,ISATEC,NL,NB
    INTEGER                                     :: ISTEP,NBSTEP
    CHARACTER(256)				                :: FILE

    CALL CPU_TIME(TDEB)
    CALL GET_ARGUMENTS(FILE,LU)
    CALL INIT_SOLVER(FILE,LU,ISATEC,TDEBUT,TFIN,PASDETEMPS,NL,NB)
    T=TDEBUT
    NBSTEP=NINT((TFIN-TDEBUT)/PASDETEMPS)+1
    ISTEP=1
    WRITE(LU,'(/A)') 'Initial steady state calculation...'
100 CALL SOLVE_TIMESTEP(T,PASDETEMPS)
    IF (ISTEP<NBSTEP) THEN
        T=T+PASDETEMPS
        ISTEP=ISTEP+1
        GOTO 100
    ENDIF

    !SAM : flush des derniers enregistrements contenus dans les buffers
    CALL WRITE_BUFFERS(LU2)

    IF (ISATEC==1) CALL WRITE_ATEC_RES(LU2)
    CALL UPDATE_MODRT(2)
    CALL CPU_TIME(TCPU)
    WRITE(MESSAGE,'(A,F8.1,A)') 'Calculation end. CPU time: ',TCPU-TDEB,' s'
    WRITE(0,'(/A)') TRIM(MESSAGE)
    IF (LU.NE.0) WRITE(LU,'(/A)') TRIM(MESSAGE)
    CLOSE(LU)

    CONTAINS

    END PROGRAM
