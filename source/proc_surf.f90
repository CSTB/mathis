    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE PROC_SURF_MODULE

    USE DATA_SURF_MODULE
    USE DATA_MAT_MODULE,ONLY:	MATRT,N_MAT
    USE GLOBAL_VAR_MODULE, ONLY:   LUOUT,MESSAGE,SHUTDOWN


    IMPLICIT NONE

    CONTAINS

    SUBROUTINE INIT_CONS_SURF(OBJ,IDN)

    TYPE(TYPE_SURF),INTENT(INOUT)		:: OBJ
    INTEGER,INTENT(IN)					:: IDN

    INTEGER								:: ISLAB,IMAT,MATGOOD,IM,N,NBM
    DOUBLEPRECISION                     :: DXM1

    OBJ%IDN=IDN
    ALLOCATE(OBJ%MESH(OBJ%NSLAB))
    DO ISLAB=1,OBJ%NSLAB
        MATGOOD=0
        DO IMAT=0,N_MAT
            IF (MATRT(IMAT)%ID==OBJ%MATIDS(ISLAB)) THEN
                MATGOOD=1
            ENDIF
        ENDDO
        IF (MATGOOD==0) THEN
            WRITE(MESSAGE,'(A,A,A,A,A)') 'ERROR - Problem with SURF ',TRIM(OBJ%ID),': MATID (',OBJ%MATIDS(ISLAB),') is unknown'
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
        IF (OBJ%NODES(ISLAB)<=0) THEN
            OBJ%NODES(ISLAB)=OBJ%NODEPERSLAB
        ENDIF
        NBM=OBJ%NODES(ISLAB)+1
        ALLOCATE(OBJ%MESH(ISLAB)%DXM(NBM))
        OBJ%MESH(ISLAB)%DXM=0.D0
        IF (OBJ%MESH_RATIO==1.D0) THEN
            OBJ%MESH(ISLAB)%DXM=OBJ%E(ISLAB)/(NBM)
        ELSEIF (MOD(OBJ%NODES(ISLAB),2)==1) THEN !nombre de noeuds internes impair
            N=(NBM)/2
            OBJ%MESH(ISLAB)%DXM(1)=OBJ%E(ISLAB)/2.D0*(1.D0-OBJ%MESH_RATIO)/(1.D0-OBJ%MESH_RATIO**N)
            DO IM=2,N
                OBJ%MESH(ISLAB)%DXM(IM)=OBJ%MESH(ISLAB)%DXM(IM-1)*OBJ%MESH_RATIO
                OBJ%MESH(ISLAB)%DXM(NBM-IM+1)=OBJ%MESH(ISLAB)%DXM(IM-1)*OBJ%MESH_RATIO
            ENDDO
            OBJ%MESH(ISLAB)%DXM(NBM)=OBJ%MESH(ISLAB)%DXM(1)
        ELSEIF (MOD(OBJ%NODES(ISLAB),2)==0) THEN !nombre de noeuds internes pair
            N=(NBM-1)/2
            OBJ%MESH(ISLAB)%DXM(1)=OBJ%E(ISLAB)/2.D0/((1.D0-OBJ%MESH_RATIO**N)/(1.D0-OBJ%MESH_RATIO)+0.5D0*OBJ%MESH_RATIO**(N))
            DO IM=2,N
                OBJ%MESH(ISLAB)%DXM(IM)=OBJ%MESH(ISLAB)%DXM(IM-1)*OBJ%MESH_RATIO
                OBJ%MESH(ISLAB)%DXM(NBM-IM+1)=OBJ%MESH(ISLAB)%DXM(IM-1)*OBJ%MESH_RATIO
            ENDDO
            OBJ%MESH(ISLAB)%DXM(NBM)=OBJ%MESH(ISLAB)%DXM(1)
            OBJ%MESH(ISLAB)%DXM(N+1)=OBJ%E(ISLAB)-SUM(OBJ%MESH(ISLAB)%DXM)

        ENDIF
    ENDDO




    END SUBROUTINE INIT_CONS_SURF

    SUBROUTINE INIT_CONS_SURFRT

    INTEGER								:: I

    DO I=0,SIZE(SURFRT)-1
        CALL INIT_CONS_SURF(SURFRT(I),I)
    ENDDO

    END SUBROUTINE INIT_CONS_SURFRT


    END MODULE PROC_SURF_MODULE