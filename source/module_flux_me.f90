    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE FLUX_ME_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    CONTAINS

    SUBROUTINE INIT_FLUX(F,N_LOC,N_SPEC,FLAG)
    TYPE(FLUX_ME),INTENT(INOUT) :: F
    INTEGER,INTENT(IN)          :: N_LOC,N_SPEC,FLAG

    IF (FLAG==0) THEN
        ALLOCATE(F%DE15DT(N_LOC))
        ALLOCATE(F%DMIN15DT(N_LOC))
        ALLOCATE(F%DMOUT15DT(N_LOC))
        ALLOCATE(F%DMKIN15DT(N_LOC,N_SPEC))
        ALLOCATE(F%DMKOUT15DT(N_LOC,N_SPEC))
    ENDIF
    F%DE15DT(1:N_LOC)=0.D0
    F%DMIN15DT(1:N_LOC)=0.D0
    F%DMOUT15DT(1:N_LOC)=0.D0
    F%DMKIN15DT(1:N_LOC,1:(N_SPEC))=0.D0
    F%DMKOUT15DT(1:N_LOC,1:(N_SPEC))=0.D0

    END SUBROUTINE INIT_FLUX

    SUBROUTINE DEALLOCATE_FLUX(F)
    TYPE(FLUX_ME),INTENT(INOUT) :: F

    IF (ALLOCATED(F%DE15DT)) DEALLOCATE(F%DE15DT)
    IF (ALLOCATED(F%DMIN15DT)) DEALLOCATE(F%DMIN15DT)
    IF (ALLOCATED(F%DMOUT15DT)) DEALLOCATE(F%DMOUT15DT)
    IF (ALLOCATED(F%DMKIN15DT)) DEALLOCATE(F%DMKIN15DT)
    IF (ALLOCATED(F%DMKOUT15DT)) DEALLOCATE(F%DMKOUT15DT)
    
    END SUBROUTINE DEALLOCATE_FLUX

    SUBROUTINE ADD_FLUX(X,F1,F2)

    TYPE(FLUX_ME),INTENT(IN)		:: F1,F2
    TYPE(FLUX_ME),INTENT(INOUT)	:: X

    !X%DE15DT(:)=F1%DE15DT(:)+F2%DE15DT(:)
    X%DMIN15DT(:)=F1%DMIN15DT(:)+F2%DMIN15DT(:)
    X%DMOUT15DT(:)=F1%DMOUT15DT(:)+F2%DMOUT15DT(:)

    X%DE15DT(:)=F1%DE15DT(:)+F2%DE15DT(:)

    X%DMKIN15DT(:,:)=F1%DMKIN15DT(:,:)+F2%DMKIN15DT(:,:)
    X%DMKOUT15DT(:,:)=F1%DMKOUT15DT(:,:)+F2%DMKOUT15DT(:,:)

    END SUBROUTINE ADD_FLUX


    !SAM : extension pour l'increment de flux (toujours utilisee comme ca visiblement)
    SUBROUTINE INCREMENTE_FLUX(X,F)

        TYPE(FLUX_ME),INTENT(IN)		:: F
        TYPE(FLUX_ME),INTENT(INOUT)	    :: X
    
        !X%DE15DT(:)      = X%DE15DT(:)      +F%DE15DT(:)
        X%DMIN15DT(:)    = X%DMIN15DT(:)    +F%DMIN15DT(:)
        X%DMOUT15DT(:)   = X%DMOUT15DT(:)   +F%DMOUT15DT(:)

        X%DE15DT(:)      = X%DE15DT(:)      +F%DE15DT(:)

        X%DMKIN15DT(:,:) = X%DMKIN15DT(:,:) +F%DMKIN15DT(:,:)
        X%DMKOUT15DT(:,:)= X%DMKOUT15DT(:,:)+F%DMKOUT15DT(:,:)
    
    END SUBROUTINE INCREMENTE_FLUX
    

    !SAM : pour debug
    SUBROUTINE Copie_FLUX(Fin,Fout)
        TYPE(FLUX_ME),INTENT(IN)		:: Fin
        TYPE(FLUX_ME),INTENT(INOUT)	    :: Fout
    
        Fout%DE15DT(:)      = Fin%DE15DT(:)
        Fout%DMIN15DT(:)    = Fin%DMIN15DT(:)
        Fout%DMOUT15DT(:)   = Fin%DMOUT15DT(:)
        Fout%DMKIN15DT(:,:) = Fin%DMKIN15DT(:,:)
        Fout%DMKOUT15DT(:,:)= Fin%DMKOUT15DT(:,:)  
    END SUBROUTINE Copie_FLUX

    !SAM : pour debug
    SUBROUTINE Compare_FLUX(Fref,F, N_LOC,N_SPEC, indice)
        TYPE(FLUX_ME),INTENT(IN)		:: Fref
        TYPE(FLUX_ME),INTENT(IN)	    :: F
        INTEGER,INTENT(IN)          :: N_LOC,N_SPEC, indice 
        INTEGER                     :: I, J

        integer                     :: zero_ecarts
        double precision            :: seuil_absolu, seuil_relatif
        double precision            :: le_max_absolu, difference_absolue, difference_relative

        integer                     :: cause_probleme = 0

        seuil_absolu=1.0D-9
        seuil_relatif=1.0D-6
        zero_ecarts=1

        DO I=1,N_LOC

            le_max_absolu       = max(DABS(F%DE15DT(I)), DABS(Fref%DE15DT(I)))
            difference_absolue  = DABS(F%DE15DT(I) - Fref%DE15DT(I))
            difference_relative = difference_absolue / le_max_absolu

            if( (difference_absolue > 1.0E-9 ) .OR. (zero_ecarts .AND. (difference_absolue > 0.0D0)) ) then
                print*, indice, "delta absolu DE15DT I=", I, Fref%DE15DT(I), F%DE15DT(I), difference_absolue
                if( difference_relative > 1.0E-6 .OR. zero_ecarts) then
                    cause_probleme=1
                    !call exit(-1)
                endif
            endif
            if(difference_relative > 1.0E-6) then
                print*, indice, "delta relative DE15DT I=", I, Fref%DE15DT(I), F%DE15DT(I), difference_relative
                !call exit(-1000)
            endif
        enddo

        DO I=1,N_LOC

            le_max_absolu       = max(DABS(F%DMIN15DT(I)), DABS(Fref%DMIN15DT(I)))
            difference_absolue  = DABS(F%DMIN15DT(I) - Fref%DMIN15DT(I))
            difference_relative = difference_absolue / le_max_absolu

            if( (difference_absolue > 1.0E-11) .OR. (zero_ecarts .AND. (difference_absolue > 0.0D0)) ) then
                print*, indice, "compare DMIN15DT I=", I, Fref%DMIN15DT(I), F%DMIN15DT(I), Fref%DMIN15DT(I)-F%DMIN15DT(I)
                if( difference_relative > 1.0E-6 .OR. zero_ecarts) then
                    cause_probleme=cause_probleme+2
                    !call exit(-1)
                endif
            endif
            if(difference_relative > 1.0E-6 ) then
                print*, indice, "delta relative DMIN15DT I=", I, Fref%DMIN15DT(I), F%DMIN15DT(I), difference_relative
            endif
        enddo

        DO I=1,N_LOC
 
            le_max_absolu       = max(DABS(F%DMOUT15DT(I)), DABS(Fref%DMOUT15DT(I)))
            difference_absolue  = DABS(F%DMOUT15DT(I) - Fref%DMOUT15DT(I))
            difference_relative = difference_absolue / le_max_absolu

           if( (difference_absolue > 1.0E-11 ) .OR. (zero_ecarts .AND. (difference_absolue > 0.0D0)) ) then
                print*, indice, "compare DMOUT15DT I=", I, Fref%DMOUT15DT(I), F%DMOUT15DT(I), Fref%DMOUT15DT(I)-F%DMOUT15DT(I)
                if( difference_relative > 1.0E-6 .OR. zero_ecarts) then
                    cause_probleme=cause_probleme+4
                    !call exit(-1)
                endif
            endif
            if(difference_relative > 1.0E-6 ) then
                print*, indice, "delta relative DMOUT15DT I=", I, Fref%DMOUT15DT(I), F%DMOUT15DT(I), difference_relative
            endif
        enddo

        DO I=1,N_LOC
           do J=1,N_SPEC
 
                le_max_absolu       = max(DABS(F%DMKIN15DT(I,J)), DABS(Fref%DMKIN15DT(I,J)))
                difference_absolue  = DABS(F%DMKIN15DT(I,J) - Fref%DMKIN15DT(I,J))
                difference_relative = difference_absolue / le_max_absolu

                if( ( difference_absolue > 1.0E-11 )  .OR. (zero_ecarts .AND. (difference_absolue > 0.0D0)) ) then
                    print*, indice, "compare DMKIN15DT I=", I, " J=",J, Fref%DMKIN15DT(I,J), F%DMKIN15DT(I,J), Fref%DMKIN15DT(I,J)-F%DMKIN15DT(I,J)
                    if( difference_relative > 1.0E-6 .OR. zero_ecarts) then
                        cause_probleme=cause_probleme+8
                        !call exit(-1)
                    endif
                endif
                if(difference_relative > 1.0E-6 ) then
                    print*, indice, "delta relative DMKIN15DT I=", I, Fref%DMKIN15DT(I,J), F%DMKIN15DT(I,J), difference_relative
                endif
               enddo
        enddo

        DO I=1,N_LOC
           do J=1,N_SPEC

                le_max_absolu       = max(DABS(F%DMKOUT15DT(I,J)), DABS(Fref%DMKOUT15DT(I,J)))
                difference_absolue  = DABS(F%DMKOUT15DT(I,J) - Fref%DMKOUT15DT(I,J))
                difference_relative = difference_absolue / le_max_absolu

                if( ( difference_absolue > 1.0E-11 )  .OR. (zero_ecarts .AND. (difference_absolue > 0.0D0)) ) then
                    print*, indice, "compare DMKOUT15DT I=", I, " J=",J, Fref%DMKOUT15DT(I,J), F%DMKOUT15DT(I,J), Fref%DMKOUT15DT(I,J)-F%DMKOUT15DT(I,J)
                    if( difference_relative > 1.0E-6 .OR. zero_ecarts) then
                        cause_probleme=cause_probleme+16
                        !call exit(-1)
                    endif
                endif
                if(difference_relative > 1.0E-6 ) then
                    print*, indice, "delta relative DMKOUT15DT I=", I, Fref%DMKOUT15DT(I,J), F%DMKOUT15DT(I,J), difference_relative
                endif
            enddo
        enddo

        if(cause_probleme > 0) then
            print*, "probleme code ", cause_probleme
            call exit(-1)
        endif

    END SUBROUTINE Compare_FLUX




    SUBROUTINE SUBSTRACT_FLUX(X,F1,F2)

    TYPE(FLUX_ME),INTENT(IN)		:: F1,F2
    TYPE(FLUX_ME),INTENT(INOUT)	:: X

    X%DE15DT(:)=F1%DE15DT(:)-F2%DE15DT(:)
    X%DMIN15DT(:)=F1%DMIN15DT(:)-F2%DMIN15DT(:)
    X%DMOUT15DT(:)=F1%DMOUT15DT(:)-F2%DMOUT15DT(:)
    X%DMKIN15DT(:,:)=F1%DMKIN15DT(:,:)-F2%DMKIN15DT(:,:)
    X%DMKOUT15DT(:,:)=F1%DMKOUT15DT(:,:)-F2%DMKOUT15DT(:,:)

    END SUBROUTINE SUBSTRACT_FLUX

    SUBROUTINE WRITE_FLUX(UNIT,F)
    TYPE(FLUX_ME),INTENT(IN)  :: F
    INTEGER,INTENT(IN)		:: UNIT

    WRITE(UNIT,*) 'DE15DT=',F%DE15DT
    WRITE(UNIT,*) 'DMIN15DT=',F%DMIN15DT
    WRITE(UNIT,*) 'DMOUT15DT=',F%DMOUT15DT
    WRITE(UNIT,*) 'DMKIN15DT=',F%DMKIN15DT
    WRITE(UNIT,*) 'DMKOUT15DT=',F%DMKOUT15DT

    END SUBROUTINE WRITE_FLUX

    END MODULE FLUX_ME_MODULE
