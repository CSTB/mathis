!***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE PROC_MISC_MODULE
    
    USE DATA_MISC_MODULE
    USE GLOBAL_VAR_MODULE,ONLY: TIMESCALE,DTETA_OLD,DTIME,HOUR0,JOUR0,JOUR,QUANTIEME0,QUANTIEME,TIME,VA_IDN

    IMPLICIT NONE
    
    CONTAINS
    
    SUBROUTINE INIT_CONS_MISC
    
    IF (TIMEUNIT=='S') THEN
        TIMESCALE=1.D0
    ELSEIF (TIMEUNIT=='M') THEN
        TIMESCALE=60.D0
    ELSEIF (TIMEUNIT=='H') THEN
        TIMESCALE=3600.D0
    ELSEIF (TIMEUNIT=='J') THEN
        TIMESCALE=3600.D0*24.D0
    ELSEIF (TIMEUNIT=='D') THEN
        TIMESCALE=3600.D0*24.D0
    ENDIF
    TETA0=TETA0*TIMESCALE
    TETAEND=TETAEND*TIMESCALE
    DTETA=DTETA*TIMESCALE
    DTETA_OLD=DTETA
    DTIME=DTETASTAT
    HOUR0=HOUR
    JOUR0=WEEKDAY+HOUR/24.D0
    JOUR=JOUR0
    QUANTIEME0=YEARDAY+HOUR/24.D0
    QUANTIEME=QUANTIEME0
    TIME=TETA0
    IF (ISOTHERMAL.EQV..TRUE.) THEN
        GUESS_POWER=.TRUE.
    ENDIF
    IF (ISOTHERMAL.EQV..FALSE.) THEN
        ISOTHERMALNODE=.FALSE.
    ENDIF
    IF (ISOTHERMALNODE.EQV..TRUE.) THEN
        GUESS_POWER=.TRUE.
    ENDIF
    IF (DEBUGPLUS.EQV..TRUE.) THEN
        DEBUG=.TRUE.
    ENDIF
    IF (BOUSSINESQ==.TRUE.) THEN
        QUASISTAT=.TRUE.
        !CONSTANT_SPECIFIC_HEAT=.TRUE.
        COMPRESSIBLE=.FALSE.
    ENDIF
    
    VA_IDN=0
    
    END SUBROUTINE INIT_CONS_MISC
    
    SUBROUTINE INIT_VAR_MISC
    
    END SUBROUTINE INIT_VAR_MISC
    
    SUBROUTINE INIT_IMAGE_MISC
    
    NULLIFY(MISCRT(1)%TIMESCALE);MISCRT(1)%TIMESCALE=>TIMESCALE
    
    END SUBROUTINE INIT_IMAGE_MISC
    
    END MODULE PROC_MISC_MODULE
    