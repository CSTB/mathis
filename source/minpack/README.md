# MINPACK

MATHIS uses the subroutine hybrd1 from the MinPack library developped by the University of Chicago, as Operator of Argonne National Laboratory.  

We provide here MinPack static library binaries for windows 32 bits `minpack_lib.lib` and windows 64 bits `minpack_lib64.lib`.
Full source code can be downloaded at [http://www.netlib.org/minpack/](http://www.netlib.org/minpack/).