    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE DATA_SURF_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    TYPE(TYPE_SURF),DIMENSION(:),ALLOCATABLE,TARGET:: SURFRT
    INTEGER									:: N_SURF

    END MODULE DATA_SURF_MODULE
