    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE DATA_CTRL_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    TYPE(TYPE_CTRL),DIMENSION(:),ALLOCATABLE,TARGET:: CTRLRT
    INTEGER									:: N_CTRL,N_PROBE,N_FLAG0,N_FLAG1,N_PASSIVE
    INTEGER,DIMENSION(:),ALLOCATABLE			:: IDNPROBE,IDNFLAG0,IDNFLAG1,IDNPASSIVE

    CONTAINS

    logical function CtrlQuantity_Family(type,flag_famille)
        integer, intent(in) :: type, flag_famille
        CtrlQuantity_Family = (IAND(type,flag_famille) /=0)
    end function CtrlQuantity_Family

    SUBROUTINE TRANSLATE_CTRLTYPE(OBJ)

        TYPE(TYPE_CTRL),INTENT(INOUT)	:: OBJ
    
        ! definition de l'enum TYPE
        OBJ%enum_ctrltype = ctrl_DEFAULT
        SELECT CASE (OBJ%CTRLTYPE)
            CASE ('TIMER')
                OBJ%enum_ctrltype = ctrl_timer
            CASE ('RAMP')
                OBJ%enum_ctrltype = ctrl_ramp
            CASE ('PROBE')
                OBJ%enum_ctrltype = ctrl_probe
            CASE ('SETPOINT')
                OBJ%enum_ctrltype = ctrl_setpoint
            CASE ('PID_SETPOINT')
                OBJ%enum_ctrltype = ctrl_pid_setpoint
            CASE ('OPERATOR')
                OBJ%enum_ctrltype = ctrl_operator
            CASE ('COMPARISON_OPERATOR')
                OBJ%enum_ctrltype = ctrl_comparison_operator
            CASE ('LOGICAL_OPERATOR')
                OBJ%enum_ctrltype = ctrl_logical_operator
            CASE ('PASSIVE')
                OBJ%enum_ctrltype = ctrl_passive
            CASE ('DEFAULT')
                OBJ%enum_ctrltype = ctrl_DEFAULT
            CASE DEFAULT
                OBJ%enum_ctrltype = ctrl_DEFAULT
        ENDSELECT
        !if(OBJ%enum_ctrltype == ctrl_DEFAULT) then
        !    print*, "Be carreful : Ctrl Type ", OBJ%CTRLTYPE, " is unknown !"
        !endif

        ! definition de l'enum QUANTITY
        OBJ%enum_quantity = ctrl_quantity_OTHER
        SELECT CASE (OBJ%QUANTITY)
            CASE ('TIME')
                OBJ%enum_quantity = ctrl_quantity_time

            CASE ('T')
                OBJ%enum_quantity = ctrl_quantity_t
            CASE ('TW')
                OBJ%enum_quantity = ctrl_quantity_tw
            CASE ('TOPER')
                OBJ%enum_quantity = ctrl_quantity_toper
            CASE ('TFLOW')
                OBJ%enum_quantity = ctrl_quantity_tflow
            CASE ('TWNODE')
                OBJ%enum_quantity = ctrl_quantity_twnode
            CASE ('TP1')
                OBJ%enum_quantity = ctrl_quantity_tp1
            CASE ('TP2')
                OBJ%enum_quantity = ctrl_quantity_tp2

            CASE ('QV')
                OBJ%enum_quantity = ctrl_quantity_qv
            CASE ('DT')
                OBJ%enum_quantity = ctrl_quantity_dt
            CASE ('DTINI')
                OBJ%enum_quantity = ctrl_quantity_dtini
            CASE ('DTEXT')
                OBJ%enum_quantity = ctrl_quantity_dtext
            CASE ('DP')
                OBJ%enum_quantity = ctrl_quantity_dp

            CASE ('HRWNODE')
                OBJ%enum_quantity = ctrl_quantity_hrwnode
            CASE ('HRinlet')
                OBJ%enum_quantity = ctrl_quantity_hrinlet

            CASE DEFAULT
                OBJ%enum_quantity = ctrl_quantity_OTHER
        ENDSELECT

        !if(OBJ%enum_quantity == ctrl_quantity_OTHER) then
        !    print*, "Be carreful : Ctrl Quantity ", OBJ%QUANTITY, " is unknown !"
        !endif


        ! definition de l'enum FUNCTION
        OBJ%enum_function = ctrl_function_DEFAULT
        SELECT CASE (OBJ%FUNCTION)
            CASE ('LAST')
                OBJ%enum_function = ctrl_function_last
            CASE ('FIRST')
                OBJ%enum_function = ctrl_function_first
            CASE ('UPWARD_FIRST')
                OBJ%enum_function = ctrl_function_upward_first
            CASE ('UPWARD_LAST')
                OBJ%enum_function = ctrl_function_upward_last
            CASE ('DOWNWARD_FIRST')
                OBJ%enum_function = ctrl_function_downward_first
            CASE ('DOWNWARD_LAST')
                OBJ%enum_function = ctrl_function_downward_last

            CASE ('LINEAR')
                OBJ%enum_function = ctrl_function_linear
            CASE ('CRENEL')
                OBJ%enum_function = ctrl_function_crenel
            CASE ('HYSTERESIS_UPWARD')
                OBJ%enum_function = ctrl_function_hysteresis_upward
            CASE ('HYSTERESIS_DOWNWARD')
                OBJ%enum_function = ctrl_function_hysteresis_downward

            CASE ('VALUE')
                OBJ%enum_function = ctrl_function_value
            CASE ('DERIVATIVE')
                OBJ%enum_function = ctrl_function_derivative
            CASE ('PREVIOUS_VALUE')
                OBJ%enum_function = ctrl_function_previous_value
            CASE ('TIME_AVERAGE')
                OBJ%enum_function = ctrl_function_time_average

            CASE ('DOWNWARD')
                OBJ%enum_function = ctrl_function_downward
            CASE ('UPWARD')
                OBJ%enum_function = ctrl_function_upward

            CASE ('CONSTANT')
                OBJ%enum_function = ctrl_function_constant
            CASE ('TIME')
                OBJ%enum_function = ctrl_function_time
            CASE ('HOUR')
                OBJ%enum_function = ctrl_function_hour
            CASE ('WEEKDAY')
                OBJ%enum_function = ctrl_function_weekday
            CASE ('YEARDAY')
                OBJ%enum_function = ctrl_function_yearday
            CASE ('VREF')
                OBJ%enum_function = ctrl_function_vref

            CASE ('ADD')
                OBJ%enum_function = ctrl_function_add
            CASE ('SUBSTRACT')
                OBJ%enum_function = ctrl_function_substract
            CASE ('MULTIPLY')
                OBJ%enum_function = ctrl_function_multiply
            CASE ('DIVIDE')
                OBJ%enum_function = ctrl_function_divide
            CASE ('POWER')
                OBJ%enum_function = ctrl_function_power
            CASE ('MAX')
                OBJ%enum_function = ctrl_function_max
            CASE ('MIN')
                OBJ%enum_function = ctrl_function_min
            CASE ('AVERAGE')
                OBJ%enum_function = ctrl_function_average

            CASE ('==')
                OBJ%enum_function = ctrl_function_equal
            CASE ('/=')
                OBJ%enum_function = ctrl_function_notequal
            CASE ('>=')
                OBJ%enum_function = ctrl_function_greaterequal
            CASE ('>')
                OBJ%enum_function = ctrl_function_greaterthan
            CASE ('<=')
                OBJ%enum_function = ctrl_function_smallerequal
            CASE ('<')
                OBJ%enum_function = ctrl_function_smaller

            CASE DEFAULT
                OBJ%enum_function = ctrl_function_DEFAULT

        ENDSELECT

        !if(OBJ%enum_function == ctrl_function_DEFAULT) then
        !    print*, "Be carreful : Ctrl Function ", OBJ%FUNCTION, " is unknown !"
        !endif

    END SUBROUTINE TRANSLATE_CTRLTYPE

    END MODULE DATA_CTRL_MODULE
