    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE GLOBAL_VAR_MODULE

    USE TYPE_MATHIS_MODULE
    USE DATA_MISC_MODULE
    USE DATA_SPEC_MODULE, ONLY:		IDNH2O

    IMPLICIT NONE

    INTEGER						:: LUOUT=0
    CHARACTER(256)				:: INPUT_FILE,OUTPUT_FILE
    LOGICAL						:: FLUSH_FILE_BUFFERS
    CHARACTER(1000)				:: MESSAGE

    DOUBLE PRECISION,PARAMETER	:: PI=3.14159265359D0
    DOUBLE PRECISION,PARAMETER  :: RHOREF=1.204785775D0,TREF=293.15D0,MH2O=0.018D0,RU=8.314472D0,RHOH2O=1000.D0,CPW=4180.D0
    DOUBLE PRECISION,PARAMETER  :: PREF0=101300.D0
    
    DOUBLE PRECISION            :: PREF

    DOUBLE PRECISION,TARGET     :: TIMESCALE
    DOUBLE PRECISION            :: HOUR0,JOUR0,QUANTIEME0
    DOUBLE PRECISION,TARGET		:: DTETA_OLD,JOUR,QUANTIEME
    DOUBLE PRECISION            :: CFLNUMBER
    
    INTEGER						:: STATCALL
    INTEGER												:: VA_IDN

    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE,TARGET    :: DP,T15,RHO15,RHOBUOY,HUM15,MW15,CP15,CV15,SOU15INI,TROSEE,HEATLOSS
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE,TARGET	:: YK15
    DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE         :: QINTRA

    TYPE(FLUX_ME)										:: F

    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE,TARGET    :: Y,Y0,Y0GOOD,DYDT,YTAMPON,YTAMPONMAIN,C,CTAMPON
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE			:: ATOL,RTOL
    DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE			:: RES

    INTEGER												:: NEQ,FLAG

    DOUBLE PRECISION,TARGET								:: TIME0,TIME0_OLD,TIME,DTIME

    TYPE(TYPE_BUILDING)                                  :: BUILDING

    CONTAINS

    SUBROUTINE VARDED(DP,TEMP,YK,CP,CV,RHO,RHOBUOY,HR,TROSEE)

    DOUBLE PRECISION,INTENT(IN)					:: DP,TEMP,CP,CV
    DOUBLE PRECISION,DIMENSION(:),INTENT(IN)	:: YK
    DOUBLE PRECISION,INTENT(OUT)				:: RHO,RHOBUOY,HR,TROSEE

    IF (BACKGROUND_SPECIE=='WATER') THEN
        RHO=1000.D0
        RHOBUOY=RHO
    ELSE
        IF (BOUSSINESQ==.TRUE.) THEN
            RHO=RHOREF
            !RHOBUOY=RHOREF*(1.D0-1.D0/TREF*(TEMP-TREF))
            RHOBUOY=(PREF)/(TEMP*(CP-CV))
        ELSEIF (COMPRESSIBLE.EQV..TRUE.) THEN
            RHO=(PREF+DP)/(TEMP*(CP-CV))
            RHOBUOY=RHO
        ELSE
            RHO=(PREF)/(TEMP*(CP-CV))
            RHOBUOY=RHO
        ENDIF
    ENDIF
    IF (IDNH2O.NE.0) THEN
        CALL YK_HUM_EVAL(TEMP,HR,YK(IDNH2O),0,TROSEE)
    ENDIF

    END SUBROUTINE VARDED

    ! definition de l'enum TYPE
    SUBROUTINE TRANSLATE_PSATMODEL()
        enum_PSATMODEL = psatmodel_DEFAULT
        SELECT CASE (PSATMODEL)
            CASE ('RANKINE')
                enum_PSATMODEL = psatmodel_rankine
            CASE ('JRM')
                enum_PSATMODEL = psatmodel_jrm
            CASE ('CLAPEYRON')
                enum_PSATMODEL = psatmodel_clapeyron

            CASE DEFAULT
                enum_PSATMODEL = psatmodel_DEFAULT
        ENDSELECT
    END SUBROUTINE TRANSLATE_PSATMODEL

    SUBROUTINE YK_HUM_EVAL(TEMP,HUMIDITE,YH2O,FLAG,TROSEE)

    DOUBLE PRECISION,PARAMETER			:: T0=373.15D0,MH2O=0.018D0
    DOUBLE PRECISION					:: HUMIDITE,YH2O
    DOUBLE PRECISION,INTENT(IN)			:: TEMP
    DOUBLE PRECISION					:: PSAT,P0,P,MW,PV
    DOUBLE PRECISION,OPTIONAL			:: TROSEE
    INTEGER,INTENT(IN)					:: FLAG

    P0=PREF
    MW=29.D-3
    !SELECT CASE (PSATMODEL)
    SELECT CASE (enum_PSATMODEL)
        !CASE ('RANKINE')
        CASE (psatmodel_rankine)
            PSAT=P0*EXP(13.7D0-5120.D0/TEMP) ! FORMULE DE RANKINE
        !CASE ('JRM')
        CASE (psatmodel_jrm)
            PSAT=P0*EXP(18.8161D0-4110.34D0/(TEMP+235.0D0-273.15D0))/1000.D0*MW/MH2O !FORMULE DE JRM
        !CASE ('CLAPEYRON')
        CASE (psatmodel_clapeyron)
            PSAT=P0*EXP(MH2O*LV/RU*(1.D0/T0-1.D0/TEMP)) ! FORMULE DE CLAPEYRON
        CASE DEFAULT
            PSAT=P0*EXP(13.7D0-5120.D0/TEMP) ! FORMULE DE RANKINE
    END SELECT
    IF (FLAG==1) THEN
        YH2O=MAX(0.D0,MIN(1.D0,(HUMIDITE/100.D0)*(PSAT/P0)*(MH2O/MW)))
    ELSE
        HUMIDITE=MAX(0.D0,MIN(100.D0,100.D0*YH2O/((PSAT/P0)*(MH2O/MW))))
    ENDIF
    PV=PSAT*HUMIDITE/100.D0
    IF (PRESENT(TROSEE)) THEN
        !SELECT CASE (PSATMODEL)
        SELECT CASE (enum_PSATMODEL)
            !CASE ('RANKINE')
            CASE (psatmodel_rankine)
                TROSEE=5120.D0/(13.7D0-LOG(MAX(0.0001D0,PV/P0)))
            !CASE ('JRM')
            CASE (psatmodel_jrm)
                TROSEE=4110.34D0/(18.8161D0-LOG(MAX(0.0001D0,PV/P0*(1000.D0*MH2O/MW))))-235.D0+273.15D0
            !CASE ('CLAPEYRON')
            CASE (psatmodel_clapeyron)
                TROSEE=1.D0/(1.D0/T0-LOG(MAX(0.0001D0,PV/P0))*RU/(MH2O*LV))
            CASE DEFAULT
                TROSEE=5120.D0/(13.7D0-LOG(MAX(0.0001D0,PV/P0)))
        END SELECT
    ENDIF

    END SUBROUTINE YK_HUM_EVAL

    SUBROUTINE BILINEAR_INTERP(NX,NY,X,Y,XDATA,YDATA,ZDATA,Z)

    INTEGER,INTENT(IN)							:: NX,NY
    DOUBLE PRECISION,INTENT(IN)					:: X,Y
    DOUBLE PRECISION,DIMENSION(NX),INTENT(IN)	:: XDATA
    DOUBLE PRECISION,DIMENSION(NY),INTENT(IN)	:: YDATA
    DOUBLE PRECISION,DIMENSION(NX,NY),INTENT(IN):: ZDATA
    DOUBLE PRECISION,INTENT(OUT)				:: Z
    DOUBLE PRECISION							:: X1,Y1,X2,Y2,DX,DY,DELTAX,DELTAY,DELTAFX,DELTAFY,DELTAFXY
    INTEGER										:: IX,IY,IX1,IX2,IY1,IY2

    IX1=1;X1=XDATA(IX1)
    IX2=NX;X2=XDATA(IX2)
    DO IX=1,NX
        IF (X>=XDATA(IX)) THEN
            X1=XDATA(IX)
            IX1=IX
        ENDIF
        IF (X<=XDATA(NX+1-IX)) THEN
            X2=XDATA(NX+1-IX)
            IX2=NX+1-IX
        ENDIF
    ENDDO
    IY1=1;Y1=YDATA(IY1)
    IY2=NY;Y2=YDATA(IY2)
    DO IY=1,NY
        IF (Y>=YDATA(IY)) THEN
            Y1=YDATA(IY)
            IY1=IY
        ENDIF
        IF (Y<=YDATA(NY+1-IY)) THEN
            Y2=YDATA(NY+1-IY)
            IY2=NY+1-IY
        ENDIF
    ENDDO

    DX=X-X1
    DY=Y-Y1
    DELTAX=X2-X1
    DELTAY=Y2-Y1
    DELTAFX=ZDATA(IX2,IY1)-ZDATA(IX1,IY1)
    DELTAFY=ZDATA(IX1,IY2)-ZDATA(IX1,IY1)
    DELTAFXY=ZDATA(IX1,IY1)+ZDATA(IX2,IY2)-ZDATA(IX2,IY1)-ZDATA(IX1,IY2)

    IF ((DELTAX.NE.0D0).AND.(DELTAY.NE.0D0)) THEN
        Z=ZDATA(IX1,IY1)+DELTAFX*DX/DELTAX+DELTAFY*DY/DELTAY+DELTAFXY*DX*DY/(DELTAX*DELTAY)
    ELSEIF ((DELTAX.NE.0D0).AND.(DELTAY.EQ.0D0)) THEN
        Z=ZDATA(IX1,IY1)+DELTAFX*DX/DELTAX
    ELSEIF ((DELTAX.EQ.0D0).AND.(DELTAY.NE.0D0)) THEN
        Z=ZDATA(IX1,IY1)+DELTAFY*DY/DELTAY
    ELSEIF ((DELTAX.EQ.0D0).AND.(DELTAY.EQ.0D0)) THEN
        Z=ZDATA(IX1,IY1)
    ELSE
    ENDIF

    END SUBROUTINE BILINEAR_INTERP

    SUBROUTINE WRITE_CALC_HEADER
    
    
    WRITE(LUOUT,'(/A)') '***********************************************************************'
    !DEC$ IF DEFINED(_WIN64)
    VERSION_STRING=TRIM(VERSION_STRING)//' - WIN64BITS'
    !DEC$ ELSEIF DEFINED(_WIN32)
    VERSION_STRING=TRIM(VERSION_STRING)//' - WIN32BITS'
    !DEC$ ELSEIF DEFINED(linux)
    VERSION_STRING=TRIM(VERSION_STRING)//' - LINUX'
    !DEC$ ELSE
    VERSION_STRING=TRIM(VERSION_STRING)//' - UNKNOWN OS'
    !DEC$ ENDIF
        
    !DEC$ IF DEFINED(_DEBUG)
    COMPIL_VERSION = TRIM(VERSION_STRING)//' - DEBUG VERSION'
    !DEC$ ELSE
    COMPIL_VERSION = TRIM(VERSION_STRING)//' - RELEASE VERSION'
    !DEC$ ENDIF
    WRITE(LUOUT,'(A)') TRIM(COMPIL_VERSION)
    COMPIL_DATE= __DATE__//' '//__TIME__
    WRITE(LUOUT,'(A)') 'Compilation date: '//TRIM(COMPIL_DATE)
    WRITE(LUOUT,'(/A)') 'Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)'
    WRITE(LUOUT,'(/A)') 'MATHIS is free software: you can redistribute it and/or modify'
    WRITE(LUOUT,'(A)') 'it under the terms of the GNU Lesser General Public License as '
    WRITE(LUOUT,'(A)') 'published by the Free Software Foundation, either version 3 of '
    WRITE(LUOUT,'(A)') 'the License, or (at your option) any later version.'
    WRITE(LUOUT,'(A)') 'MATHIS is distributed in the hope that it will be useful,'
    WRITE(LUOUT,'(A)') 'but WITHOUT ANY WARRANTY; without even the implied warranty of'
    WRITE(LUOUT,'(A)') 'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the'
    WRITE(LUOUT,'(A)') 'GNU Lesser General Public License for more details.'
    WRITE(LUOUT,'(A)') 'You should have received a copy of the GNU Lesser General Public'
    WRITE(LUOUT,'(A)') 'License along with MATHIS. See <https://www.gnu.org/licenses/>.'
    WRITE(LUOUT,'(/A)') 'Acknowledgment: this product includes software developed by the'
    WRITE(LUOUT,'(A)') 'University of Chicago, as Operator of Argonne National Laboratory.'
    WRITE(LUOUT,'(/A)') '***********************************************************************'
    
    END SUBROUTINE WRITE_CALC_HEADER
    
    SUBROUTINE GET_ARGUMENTS(FILE,LU) ! Read the argument after the command
    !DEC$ IF DEFINED(DLL)
    !DEC$ ATTRIBUTES DLLEXPORT,ALIAS:'GET_ARGUMENTS':: GET_ARGUMENTS
    !DEC$ ENDIF

    INTEGER(2)					:: STATUS
    CHARACTER(256)				:: BUFFER,MESSAGE
    CHARACTER(256),INTENT(OUT)	:: FILE
    INTEGER,INTENT(OUT)			:: LU
    LOGICAL						:: EX
    INTEGER                     :: ERR

    LU = 0
    OUTPUT_FILE='null'
    CALL GETARG(1,BUFFER)
    IF (BUFFER==' ') THEN
        INPUT_FILE='null'
    ELSEIF(BUFFER=='default') THEN
        INPUT_FILE='default'
        LU=LU0 
    ELSE
        INPUT_FILE=TRIM(BUFFER)
        INQUIRE(FILE=INPUT_FILE,EXIST=EX)
        IF (.NOT.EX) THEN
            WRITE(MESSAGE,'(A,A,A)') "ERROR: File ", TRIM(INPUT_FILE)," is not found."
            CALL SHUTDOWN(MESSAGE,LUOUT)
        ENDIF
        CALL GETARG(2,BUFFER)
        IF (BUFFER.NE.' ') THEN
            CALL GETARG(2,BUFFER)
            IF ((TRIM(BUFFER)=='-log').OR.(TRIM(BUFFER)=='-l')) THEN
                LU=LU0
                CALL GETARG(3,BUFFER)
                IF (BUFFER.NE.' ') OUTPUT_FILE=TRIM(BUFFER)
            ELSE
                LU=0
            ENDIF
        ENDIF
    ENDIF
    FILE=INPUT_FILE
    
    END SUBROUTINE GET_ARGUMENTS


    END MODULE GLOBAL_VAR_MODULE
