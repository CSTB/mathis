    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !Copying and distribution of this file, with or without modification,
    !are permitted in any medium without royalty provided the copyright
    !notice and this notice are preserved.  This file is offered as-is,
    !without any warranty.
    !***********************************************************************

    MODULE TYPE_MATHIS_MODULE

    CHARACTER(256)				:: VERSION_STRING="MATHIS 3.4"
    CHARACTER(256)				:: COMPIL_VERSION,COMPIL_DATE

    INTEGER,PARAMETER			:: NZPEXTMAX=20 !<max number of external pressure zones
    INTEGER,PARAMETER			:: NLOCMAX=10	!<max number of rooms accessible by a person
    INTEGER,PARAMETER			:: NSPECMAX=20 !<max number of species produced by a person or a source
    INTEGER,PARAMETER			:: NSLABMAX=10	!<max number of slabs in a surf
    INTEGER,PARAMETER			:: NCTRLIDSMAX=20	!<max number of quantities for an operator controller
    INTEGER,PARAMETER 			:: NBREPTSMAX=100
    INTEGER,PARAMETER 			:: NMOTYPEMAX=20

    INTEGER, parameter          :: SIZE_MAT_TABLE=1012 ! tabulation sur 1000 valeurs de HR entre 0.0 et 1.0 (1001 valeurs) + 10 valeurs entre 1.000 et 1.010 + 1 valeur pour eviter un test
    DOUBLE PRECISION, parameter :: MULT_MAT_TABLE = real(SIZE_MAT_TABLE - (1+10+1)) ! facteur multiplicatif a appliquer sur HR

    INTEGER,PARAMETER			:: LU0=54,LU1=55,LU2=56,LU3=57,LUERR=58

    TYPE FLUX_ME
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: DE15DT !<W - flux net de chaleur - dimension N_LOC
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: DMIN15DT  !<kg/s - flux de masse entrant - dimension N_LOC
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: DMOUT15DT !<kg/s - flux de masse sortant - dimension N_LOC
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: DMKIN15DT   !<kg/s (ou [userunit]/s si TRACE=.TRUE.) - flux de masse d'espece entrant - dimension [N_LOC,N_SPEC]
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: DMKOUT15DT  !<kg/s (ou [userunit]/s si TRACE=.TRUE.) - flux de masse d'espece sortant - dimension [N_LOC,N_SPEC]
    END TYPE FLUX_ME

    TYPE TYPE_LOC
        !PARAMETRES
        CHARACTER(100)                 :: ID='LOC'  !< - identifiant du noeud
        CHARACTER(100)                 :: LOCTYPE='ROOM'    !< - type de noeud ('ROOM' : local ou 'NODE' : noeud sans volume)
        CHARACTER(100)                 :: TCTRLID='CTRLDEFAULT' !< - identifiant du controleur de TINI
        DOUBLE PRECISION			   :: AREA=0.D0 !<m2 - surface de local si LOCTYPE='ROOM'
        DOUBLE PRECISION               :: ALT=0.D0  !<m - altitude
        DOUBLE PRECISION			   :: HEIGHT=0.D0   !<m - hauteur du local si LOCTYPE='ROOM'
        DOUBLE PRECISION               :: TINI=20.D0    !<C - temperature initiale
        DOUBLE PRECISION               :: TUP=-9999.D0  !<C - temperature de zone haute si LOCTYPE='ROOM'
        DOUBLE PRECISION               :: TDOWN=-9999.D0    !<C - temperature de zone haute si LOCTYPE='ROOM'
        DOUBLE PRECISION               :: ZINT=-9999.D0 !<m - hauteur d'interface entre zones haute et basse si LOCTYPE='ROOM'
        CHARACTER(100)                 :: PZONEID='null'!< - identifiant du noeud referent comme zone de pression
        !CONSTANTES
        DOUBLE PRECISION               :: VOLUME    !m3
        DOUBLE PRECISION               :: DTUP  !C - differentiel entre temperature de zone haute et temperature moyenne
        DOUBLE PRECISION               :: DTDOWN    !C - differentiel entre temperature de zone basse et temperature moyenne
        INTEGER                        :: IDN  ! - numero dans la liste LOC
        INTEGER                        :: IDNPZONE  ! - numero du noeud referent comme zone de pression
        !VARIABLES
        DOUBLE PRECISION			   :: T15UP,T15DOWN !K - temperature des zones haute et basse
        DOUBLE PRECISION			   :: HRUP,HRDOWN   !% - humidite relative des zones haute et basse
        DOUBLE PRECISION			   :: RHOUP,RHODOWN   !kg/m3 - masse volumique des zones haute et basse
        DOUBLE PRECISION			   :: RHOBUOYUP,RHOBUOYDOWN   !kg/m3 - masse volumique des zones haute et basse utilisées par les branches pour calculer le DP moteur
        DOUBLE PRECISION			   :: TROSEEUP,TROSEEDOWN !K - temperature de rosee des zones hautes et basses
        DOUBLE PRECISION			   :: OCC   ! - marqueur d'occupation
        DOUBLE PRECISION			   :: MH2OLIQ   !kg - quantite d'eau liquide du pas de temps courant
        DOUBLE PRECISION			   :: MH2OLIQ0  !kg - quantite d'eau liquide du pas de temps precedent
        DOUBLE PRECISION			   :: MH2OLIQ0_OLD  !kg - quantite d'eau liquide sauvegardee si l'on doit faire un "rewind" suite a mauvaise convergence
        DOUBLE PRECISION			   :: TWMEAN    !K - temperature de surface moyenne des parois connectee aux noeuds
        DOUBLE PRECISION			   :: TOPER !K - temperature operative
        DOUBLE PRECISION			   :: HRAD  !W - puissance radiative issues des sources et de la vaporisation d'eau
        DOUBLE PRECISION			   :: RA    !vol/h - renouvellment d'air
        INTEGER,DIMENSION(:),ALLOCATABLE  :: LATERALIDNS,STRAIGHTIDNS   !identifiants des conduits lateraux et rectilignes si jonction - dimension variable
        !IMAGES
        DOUBLE PRECISION,POINTER	  :: RHO15  !kg/m3 - masse volume du noeud
        DOUBLE PRECISION,POINTER	  :: T15    !K - temperature du noeud
        DOUBLE PRECISION,POINTER	  :: DP     !Pa - differentiel entre pression du noeud et pression exterieure a l'altitude de reference
        DOUBLE PRECISION,POINTER	  :: CP,CV  !J/kg/K - Chaleur specifique du noeud a pression constante (CP) et a volume constant (CV)
        DOUBLE PRECISION,DIMENSION(:),POINTER	:: YK   !kg/kg (ou [userunit]/kg si TRACE=.TRUE.) - fractions massiques d'especes - dimension N_SPEC
        DOUBLE PRECISION,POINTER	  :: FRAC   !ctrlunit - valeur de la variable VALUE du controleur sur TINI
        DOUBLE PRECISION,POINTER      :: SOU15 ! W - puissance necessaire au maintien de la temperature si calcul isotherme
        DOUBLE PRECISION,POINTER      :: HR ! % -Humidite relative du noeud
        !FLUX DE MASSE ET D'ENERGIE
        TYPE(FLUX_ME)				  :: FLUXES !flux de masse et d'energie generes par la condensation/vaporisation
    END TYPE TYPE_LOC

    
    TYPE TYPE_BRANCHE
        !PARAMETRES
        CHARACTER(100)					:: ID='BRANCH'
        integer                         :: enum_type
        CHARACTER(100)					:: BRANCHTYPE='ORIFICE'
        CHARACTER(100),DIMENSION(2)		:: LOCIDS=(/'EXT','EXT'/)
        DOUBLE PRECISION				:: Z1=0.D0
        DOUBLE PRECISION				:: Z2=0.D0
        DOUBLE PRECISION				:: LENGTH=0.D0
        DOUBLE PRECISION				:: DIAM=0.D0
        DOUBLE PRECISION				:: SECTION=0.D0
        DOUBLE PRECISION				:: HEIGHT=2.D0
        DOUBLE PRECISION				:: COEF=0.D0
        DOUBLE PRECISION				:: RUGO=0.D0
        DOUBLE PRECISION,DIMENSION(2)	:: SINGU=(/0.D0,-9999.D0/)
        DOUBLE PRECISION				:: DPV0=10.D0
        DOUBLE PRECISION				:: DPV1=20.D0
        DOUBLE PRECISION				:: DPV2=100.D0
        DOUBLE PRECISION				:: QM0=0.D0
        DOUBLE PRECISION				:: QV0=0.D0
        DOUBLE PRECISION				:: QV1=0.D0
        DOUBLE PRECISION				:: QV2=0.D0
        DOUBLE PRECISION				:: HR1=20.D0
        DOUBLE PRECISION				:: HR2=50.D0
        DOUBLE PRECISION,DIMENSION(2)	:: ALPHA=1.D0
        DOUBLE PRECISION				:: K0=1.D0
        DOUBLE PRECISION				:: K1=1.D0
        DOUBLE PRECISION				:: K2=1.D0
        DOUBLE PRECISION				:: EXPO=0.5D0
        DOUBLE PRECISION				:: DPREF=20.D0
        DOUBLE PRECISION				:: RHOREF=1.204785775D0
        DOUBLE PRECISION				:: IOR=0.D0
        INTEGER                         :: NV=1
        CHARACTER(1000)					:: CDFILE='null'
        CHARACTER(1000)					:: PQFILE='null'
        CHARACTER(100)					:: CTRLID='CTRLDEFAULT'
        !parameters for tunnel branch model
        DOUBLE PRECISION				:: FRICTION=0.025D0
        INTEGER         				:: NACC=0
        DOUBLE PRECISION				:: KACC=0.8D0
        DOUBLE PRECISION				:: FACC=1200.D0
        DOUBLE PRECISION				:: UACC=33.D0
        DOUBLE PRECISION				:: AV=0.D0 !9.D0
        DOUBLE PRECISION				:: AR=0.D0 !9.D0
        DOUBLE PRECISION				:: CX=0.5D0
        DOUBLE PRECISION				:: URAME=0.D0
        DOUBLE PRECISION				:: PCRAME=0.012D0
        DOUBLE PRECISION				:: DENSRAME=1.D0
        DOUBLE PRECISION				:: SRAME=9.D0
        CHARACTER(100)                  :: ACCCTRLID='CTRLDEFAULT'
        CHARACTER(100)                  :: RAMECTRLID='CTRLDEFAULT'
        DOUBLE PRECISION                :: LRAME0=0.D0
        DOUBLE PRECISION				:: K31BRD=-9999.D0
        DOUBLE PRECISION				:: K31BRI=-9999.D0
        DOUBLE PRECISION				:: K11BR=-9999.D0
        !CONSTANTES
        CHARACTER(2)							:: ATECTYPE
        INTEGER								    :: IDN,IMAX
        INTEGER,DIMENSION(2)					:: TAB
        DOUBLE PRECISION,DIMENSION(36,2)		:: CDASWSURU
        DOUBLE PRECISION,DIMENSION(36,2)		:: CDASWINC
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE	:: PQCOURBE
        DOUBLE PRECISION						:: EXPO0,FRICT4000
        LOGICAL                                 :: ISDUCT=.FALSE.
        !VARIABLES
        integer                                 :: QM_PREC_POSITIF=.TRUE.
        DOUBLE PRECISION						:: FRICT,QM,QV,DP,RHO15AM,T15AM,CP15AM,WSURU,WSURU1,WSURU2,DPLOSS,K,EA_MODULE,JUNCTION_SINGU,MEMOFRAC
        INTEGER								    :: MONTEE,AMONT,NQ
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE     :: YK15AM,QDEPER,ODEPER,QTRAVDEPER,VDATA,QDATA
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE	:: PDATA
        DOUBLE PRECISION,DIMENSION(2)			:: QMOUV,QVOUV
        DOUBLE PRECISION						:: DPCRIT,QCRIT
        DOUBLE PRECISION                        :: Lrame
        !IMAGES
        DOUBLE PRECISION,POINTER				:: DP1,T151,RHO151,RHOBUOY1,DP2,T152,RHO152,RHOBUOY2,ALTLOC1,ALTLOC2,CP151,CP152,WINC,VREF,FRAC
        DOUBLE PRECISION,DIMENSION(:),POINTER	:: YK151,YK152
        DOUBLE PRECISION,POINTER				:: fracacc,fracrame
        !FLUX DE MASSE ET D'ENERGIE
        TYPE(FLUX_ME)							:: FLUXES
    END TYPE TYPE_BRANCHE

    TYPE TYPE_HSRC
        !PARAMETRES
        CHARACTER(100)							:: ID='HSRC'
        CHARACTER(100)							:: LOCID='EXT'
        CHARACTER(100)						    :: ORIENTATION='WALL'
        DOUBLE PRECISION						:: MFLUX=0.D0
        DOUBLE PRECISION						:: HFLUX=0.D0
        DOUBLE PRECISION						:: T=-9999.D0
        CHARACTER(100),DIMENSION(NSPECMAX)		:: SPECIDS='null'
        CHARACTER(100),DIMENSION(NSLABMAX)      :: MATIDS='null'
        DOUBLE PRECISION,DIMENSION(NSPECMAX)	:: YKS=0.D0
        CHARACTER(100)							:: CTRLID='CTRLDEFAULT'
        CHARACTER(100)							:: HSRCTYPE='STANDARD'
        CHARACTER(100)							:: WALLID='MUR'
        DOUBLE PRECISION						:: AREA=0.D0
        DOUBLE PRECISION						:: XRAD=0.D0
        DOUBLE PRECISION						:: COEF=3.32D-3
        DOUBLE PRECISION                        :: MH2O0=0.D0
        !CONSTANTES
        INTEGER									:: IDN,IP,IM,IOR
        DOUBLE PRECISION						:: CP
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE:: KYIELD
        !VARIABLES
        DOUBLE PRECISION							:: MPDOT,QDOT,QDOTRAD,MH2O,MH2O0_OLD
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: PRODYK
        !IMAGES
        DOUBLE PRECISION,POINTER					:: TIME,T15,CP15,FRAC,TP,HUM15
        !FLUX DE MASSE ET D'ENERGIE
        TYPE(FLUX_ME)								:: FLUXES
    END TYPE TYPE_HSRC

    TYPE TYPE_PERSON
        !PARAMETRES
        CHARACTER(100)							:: ID='PERSON'
        CHARACTER(100),DIMENSION(NLOCMAX)		:: LOCIDS='null'
        CHARACTER(100),DIMENSION(NSPECMAX)		:: SPECIDS='null'
        DOUBLE PRECISION,DIMENSION(NSPECMAX,2)	:: MKFLUX=0.D0
        DOUBLE PRECISION,DIMENSION(2)			:: HFLUX=(/90.D0,70.D0/)
        CHARACTER(100)							:: LOCCTRLID='CTRLDEFAULT'
        CHARACTER(100)							:: WAKECTRLID='CTRLDEFAULT'
        CHARACTER(100)							:: PERSONTYPE='DEFAULT'
        !CONSTANTES
        INTEGER									:: IDN
        INTEGER,DIMENSION(NLOCMAX)				:: IDNLOC
        INTEGER									:: NLOC
        INTEGER,DIMENSION(NSPECMAX)				:: IDNSPEC
        INTEGER									:: NSPEC
        !VARIABLES
        INTEGER									:: IP
        DOUBLE PRECISION						:: MPDOT,QDOT,CP
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: PRODYK
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: KYIELD
        !IMAGES
        LOGICAL,POINTER							:: STATE
        DOUBLE PRECISION,POINTER				:: FRAC,OCC
        !FLUX DE MASSE ET D'ENERGIE
        TYPE(FLUX_ME)							:: FLUXES
    END TYPE TYPE_PERSON

    TYPE TYPE_SPEC
        !PARAMETRES
        CHARACTER(100)						    :: ID='AIR'
        DOUBLE PRECISION						:: MW=29.D-3
        DOUBLE PRECISION						:: CV=710.D0
        DOUBLE PRECISION						:: YKREF=1.D-2
        LOGICAL                                 :: TRACE=.TRUE.
        !CONSTANTES
        INTEGER								    :: IDN
        DOUBLE PRECISION						:: CP
    END TYPE TYPE_SPEC

    TYPE TYPE_MAT
        !PARAMETRES
        CHARACTER(100)						:: ID='DEFAULTCONCRETE'
        DOUBLE PRECISION					:: RHO=2200.D0
        DOUBLE PRECISION					:: CS=1000.D0
        DOUBLE PRECISION					:: LAMBDA=1.75D0
        DOUBLE PRECISION                    :: DW=6.D-10
        DOUBLE PRECISION                    :: PERM=1.D-15
        DOUBLE PRECISION                    :: SORP=20.D0
        CHARACTER(100)                      :: LAMBDAFILE='null'
        CHARACTER(100)                      :: DWFILE='null'
        CHARACTER(100)                      :: PERMFILE='null'
        CHARACTER(100)                      :: SORPFILE='null'
        integer                             :: EXPORT_TABLES=0
        integer                             :: USE_LINEAR_LAMBDA=0
        integer                             :: USE_LINEAR_DW=0
        integer                             :: USE_LINEAR_PERM=0
        integer                             :: USE_LINEAR_SORP=0

        !SAM : ajout de 6 colonnes afin de stocker des elements tabules sur SIZE_MAT_TABLE valeurs. a precalculer
        !DOUBLEPRECISION,DIMENSION(1001,2)    :: LAMBDATABLE=9999.D0,DWTABLE=9999.D0,PERMTABLE=9999.D0,SORPTABLE=9999.D0
        ! 8 colonnes maintenant : X val_lue A B C D (polynome A*x^3 + B*x^2 + C*x + D) + 2 colonnes pour l'equation en lineaire (apres avoir appliquer la cubique) car probleme avec la cubique
        DOUBLEPRECISION,DIMENSION(SIZE_MAT_TABLE,8)    :: LAMBDATABLE=9999.D0,DWTABLE=9999.D0,PERMTABLE=9999.D0,SORPTABLE=9999.D0
        !CONSTANTES
        INTEGER								:: IDN
    END TYPE TYPE_MAT

    TYPE TYPE_MESH
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: DXM
    END TYPE TYPE_MESH

    TYPE TYPE_SURF
        !PARAMETRES
        CHARACTER(100)						:: ID='SURFDEFAULT'
        CHARACTER(100),DIMENSION(NSLABMAX)	:: MATIDS='DEFAULTCONCRETE'
        INTEGER								:: NSLAB=1
        DOUBLE PRECISION,DIMENSION(NSLABMAX):: E=0.2D0
        DOUBLE PRECISION,DIMENSION(NSLABMAX-1):: HMCT=1.D-6
        DOUBLE PRECISION,DIMENSION(NSLABMAX-1):: HCCT=1000.D0
        INTEGER,DIMENSION(NSLABMAX)         :: NODES=0
        INTEGER                             :: NODEPERSLAB=5
        DOUBLE PRECISION                    :: MESH_RATIO=1.D0
        DOUBLE PRECISION,DIMENSION(NSLABMAX):: HRINI=9999.D0
        DOUBLE PRECISION,DIMENSION(NSLABMAX):: TINI=9999.D0
        !CONSTANTES
        INTEGER								:: IDN
        TYPE(TYPE_MESH),DIMENSION(:),ALLOCATABLE :: MESH
    END TYPE TYPE_SURF

    TYPE TYPE_MUR
        !PARAMETRES
        CHARACTER(100)					    		:: ID='MUR'
        CHARACTER(100)					    		:: SURFID='SURFDEFAULT'
        integer                                     :: enum_walltype
        CHARACTER(100)					    		:: WALLTYPE='THERMAL'
        CHARACTER(100),DIMENSION(2)		    		:: LOCIDS='EXT'
        CHARACTER(100),DIMENSION(2)		    		:: QCTRLIDS='CTRLDEFAULT'
        CHARACTER(100),DIMENSION(2)		    		:: MCTRLIDS='CTRLDEFAULT'
        CHARACTER(100)          		    		:: TRSCTRLID='CTRLDEFAULT'
        DOUBLE PRECISION							:: AREA=0.D0
        INTEGER										:: NBREPTS=0.D0
        DOUBLE PRECISION,DIMENSION(2)				:: HCONV=5.D0
        DOUBLE PRECISION,DIMENSION(2)				:: HM=20.D-9
        DOUBLE PRECISION,DIMENSION(2)				:: EPS=0.9D0
        DOUBLE PRECISION,DIMENSION(2)				:: ABS=0.4D0
        DOUBLE PRECISION,DIMENSION(2)				:: QPRIME=0.D0
        DOUBLE PRECISION,DIMENSION(2)				:: MPRIME=0.D0
        DOUBLE PRECISION							:: TRS=0.D0
        DOUBLE PRECISION							:: SLOPE=90.D0
        DOUBLE PRECISION							:: INT_THO_DIFF=1.D0
        DOUBLE PRECISION							:: ORIENTATION=180.D0
        DOUBLE PRECISION,DIMENSION(NBREPTSMAX)		:: ANGLE=0.D0
        DOUBLE PRECISION,DIMENSION(NBREPTSMAX)		:: THO=0.D0
        LOGICAL                                     :: POST_INTERNAL_VAR=.FALSE.
        !CONSTANTES
        INTEGER										:: IDN,NSLAB,ISURF
        INTEGER,DIMENSION(:),ALLOCATABLE            :: NODES
        INTEGER,DIMENSION(2)						:: TAB
        TYPE(TYPE_MAT),DIMENSION(:),ALLOCATABLE     :: MAT
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: DX
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: HCCT,HMCT
        INTEGER,DIMENSION(NSLABMAX+1)				:: NUMNODE
        DOUBLE PRECISION                             :: MRATIO
        
        !VARIABLES
        
        !SAM ajout d'une variable double pour stockage instant de stockage de la valeur OLD
        double precision                            :: TIME_OLD
        !SAM ajout d'une variable double pour stockage instant de stockage de la valeur OLD2
        double precision                            :: TIME_OLD2

        !SAM Ajout d'un autre instant precedent (HR_TEMP_OLD2)
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: TEMP,TEMP_OLD,TEMP_OLD2,YWMEAN
        DOUBLE PRECISION							:: MWTOT

        !SAM Ajout d'un autre instant precedent (HR_OLD2)
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: HR,HR_OLD, HR_OLD2
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: MSOURCE,QSOURCE
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: RHO,CP,LAMBDA,DW,PERM,SORP,PV,YW,PSAT,DPSATDT,K
        DOUBLE PRECISION,DIMENSION(2)               :: NETQ,NETM,NETMTOT
        DOUBLE PRECISION							:: TPIN,TPOUT
        DOUBLE PRECISION							:: PVIN,PVOUT,HRPIN,HRPOUT
        DOUBLE PRECISION							:: FRADIN,FRADOUT,FSUNIN,FSUNOUT,COSI
        INTEGER                                     :: COUNTSAVE=0
        !IMAGES
        DOUBLE PRECISION,POINTER					:: TIN,TOUT,HRIN,HROUT
        DOUBLE PRECISION,POINTER     	            :: QFRACIN,QFRACOUT,MFRACIN,MFRACOUT,TRSFRAC
        !FLUX DE MASSE ET D'ENERGIE
        TYPE(FLUX_ME)								:: FLUXES
    END TYPE TYPE_MUR


    TYPE TYPE_EXT
        !PARAMETRES
        DOUBLE PRECISION							:: TEXT=20.D0
        DOUBLE PRECISION							:: HR=50.D0
        DOUBLE PRECISION							:: VMETEO=0.D0
        DOUBLE PRECISION							:: WDIR=0.D0
        DOUBLE PRECISION,DIMENSION(NZPEXTMAX)   	:: CP=0.D0
        INTEGER                                     :: N_ZPEXT=1
        CHARACTER(1000)								:: CPFILE='null'
        CHARACTER(1000)								:: ATMOFILE='null'
        DOUBLE PRECISION							:: SUNRAD=0.D0
        DOUBLE PRECISION							:: DIFFRAD=0.D0
        DOUBLE PRECISION							:: TSKY=0.D0
        DOUBLE PRECISION							:: TGROUND=12.D0
        DOUBLE PRECISION							:: LATITUDE=47.306067D0
        DOUBLE PRECISION							:: LONGITUDE=-1.548849D0
        DOUBLE PRECISION							:: STDMERIDIAN=0.D0
        LOGICAL										:: INTERPOLATION=.FALSE.
        CHARACTER(100),DIMENSION(NSPECMAX)			:: SPECIDS='null' !SPECIDS(1)='CO2' in DEFAULT_NML_EXT
        DOUBLE PRECISION,DIMENSION(NSPECMAX)		:: YKS=0.D0       !YKS(1)=607.D-6 (kg/kg) in DEFAULT_NML_EXT
        CHARACTER(100),DIMENSION(NSPECMAX)			:: YKCTRLIDS='CTRLDEFAULT'
        DOUBLE PRECISION							:: HVREF=10.D0
        CHARACTER(4)								:: RUGO='II'
        DOUBLE PRECISION							:: HVMETEO=10.D0
        DOUBLE PRECISION							:: ALBEDO=0.2D0
        DOUBLE PRECISION							:: ALTREF=0.D0
        CHARACTER(1)								:: TIMEUNIT='Y'
        LOGICAL										:: MOVING_SUN=.TRUE.
        !constantes et variables (liste a reprendre)
        DOUBLE PRECISION							:: VREF,RHOEXT,RHOBUOY,ZD,ALT,HUMIDITE,MWEXT,CPEXT,CVEXT,WINC,TROSEE,PREF,RA
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: YK
        INTEGER										:: NKGIVEN
        DOUBLE PRECISION,DIMENSION(NZPEXTMAX)		:: DP
        DOUBLE PRECISION							:: H_ANGLE,AZ_ANGLE
        INTEGER,DIMENSION(NSPECMAX)					:: IDNKGIVEN
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE	:: CPASWINC
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: ATMOASTIME
        INTEGER										:: NINC,ITOLD   
        DOUBLE PRECISION                            :: TIMESCALE
        DOUBLE PRECISION                            :: TIME,DTIME,YEARDAY,WEEKDAY,HOUR
    END TYPE TYPE_EXT

    TYPE TYPE_BOUND
        !PARAMETRES
        CHARACTER(100)								:: ID='BOUND'
        DOUBLE PRECISION							:: TBOUND=20.D0
        DOUBLE PRECISION							:: HR=50.D0
        DOUBLE PRECISION							:: DP=0.D0
        DOUBLE PRECISION							:: CP=0.D0
        CHARACTER(100),DIMENSION(NSPECMAX)			:: SPECIDS='null'
        DOUBLE PRECISION,DIMENSION(NSPECMAX)		:: YKS=0.D0
        CHARACTER(1000)								:: BOUNDFILE='null'
        CHARACTER(1000)								:: CPFILE='null'
        CHARACTER(100)								:: CTRLID='CTRLDEFAULT'
        DOUBLE PRECISION							:: ALT=0.D0
        LOGICAL										:: INTERPOLATION=.FALSE.
        CHARACTER(100)								:: TCTRLID='CTRLDEFAULT'
        CHARACTER(100)								:: HRCTRLID='CTRLDEFAULT'
        CHARACTER(100)								:: DPCTRLID='CTRLDEFAULT'
        CHARACTER(100),DIMENSION(NSPECMAX)			:: YKCTRLIDS='CTRLDEFAULT'
        !CONSTANTES
        DOUBLE PRECISION							:: ZD
        INTEGER										:: IDN,NINC,NKGIVEN,ITOLD
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: CPASWINC
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: CONDASTIME
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: YK
        !VARIABLES
        !les grandeurs TBOUND,HR,DP,CP et YK sont variables si on utilise un fichier de donnees ou des controleurs
        DOUBLE PRECISION							:: RHO,RHOBUOY,CPBOUND,CVBOUND,TROSEE
        !IMAGES
        DOUBLE PRECISION,POINTER					:: TIME,WINC,VREF
        DOUBLE PRECISION,POINTER					:: HRFRAC,TFRAC,DPFRAC
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: YKFRAC !ceci n'est pas un pointeur, on y stocke le numero des controleurs
    END TYPE TYPE_BOUND

    TYPE TYPE_CTRL
        !PARAMETRES
        CHARACTER(100)						:: ID='CTRLDEFAULT'
        CHARACTER(100)						:: LOCID='EXT'
        CHARACTER(100)						:: BRANCHID='null'
        CHARACTER(100)						:: WALLID='null'
        CHARACTER(100)						:: MODID='null'
        INTEGER								:: IWNODE=1
        integer                             :: enum_quantity
        CHARACTER(100)						:: QUANTITY='TIME'
        CHARACTER(1000)						:: RAMPFILE='null'
        DOUBLE PRECISION,DIMENSION(1000,2)	:: RAMPDATA=1.D0
        CHARACTER(100)						:: SPECID='null'
        integer                             :: enum_ctrltype
        CHARACTER(100)						:: CTRLTYPE='RAMP'
        integer                             :: enum_function
        CHARACTER(100)						:: FUNCTION='LINEAR'
        DOUBLE PRECISION					:: CONSTANT=0.D0
        DOUBLE PRECISION					:: DURATION=0.D0
        CHARACTER(1)                        :: TIMEUNIT='M'
        DOUBLE PRECISION					:: HYSTERESIS=0.D0
        DOUBLE PRECISION					:: SETPOINT=0.D0
        CHARACTER(100)						:: CTRLID='CTRLDEFAULT'
        CHARACTER(100)						:: SETPOINTCTRLID='CTRLDEFAULT'
        CHARACTER(100),DIMENSION(NCTRLIDSMAX):: QUANTITIES='CTRLDEFAULT'
        DOUBLE PRECISION					:: PROPORTIONAL_GAIN=1.D0
        DOUBLE PRECISION					:: INTEGRAL_GAIN=0.D0
        DOUBLE PRECISION					:: DIFFERENTIAL_GAIN=0.D0
        !CONSTANTES
        INTEGER								:: IDN,IP,ISPEC
        INTEGER,DIMENSION(2)				:: TAB
        DOUBLE PRECISION                    :: TIMESCALE
        !VARIABLES
        DOUBLE PRECISION					:: VALUE,VALUE_OLD,DT,DP,CURRENT_VALUE,PREVIOUS_VALUE,PREVIOUS_VALUE_OLD,INTEGRAL,INTEGRAL_OLD,ERROR,PREVIOUS_ERROR,TIMER
        LOGICAL								:: STATE,STATE_OLD
        INTEGER,DIMENSION(:),ALLOCATABLE	:: CTRLIDNS
        INTEGER								:: NCTRLIDNS,IOLD
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE:: CTRLVALUES,TIMEVALUES
        LOGICAL,DIMENSION(:),ALLOCATABLE	:: CTRLSTATES
        DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE:: RAMP
        !IMAGES
        DOUBLE PRECISION,POINTER			:: PNT
        DOUBLE PRECISION,POINTER			:: FRAC,SETPOINTFRAC
    END TYPE TYPE_CTRL
        
    TYPE TYPE_MISC
        !PARAMETRES
        CHARACTER(256)				:: JOBNAME='null' !set to INPUT_FILE at init
        CHARACTER(1)                :: TIMEUNIT='SECONDE'
        DOUBLE PRECISION		    :: TETA0=0.D0
        DOUBLE PRECISION		    :: TETAEND=0.D0
        DOUBLE PRECISION		    :: DTETA=900.D0
        DOUBLE PRECISION		    :: DTETASTAT=3600.D0 ! la donnee utilisateur doit toujours etre en seconde
        DOUBLE PRECISION		    :: HOUR=0.D0
        DOUBLE PRECISION		    :: WEEKDAY=1.D0
        DOUBLE PRECISION		    :: YEARDAY=1.D0
        LOGICAL						:: SUMMERTIME=.FALSE.
        DOUBLE PRECISION		    :: HYBRIDTOL=1.D-16
        DOUBLE PRECISION		    :: PTOL=1.D-7
        DOUBLE PRECISION		    :: TTOL=1.D-3
        DOUBLE PRECISION		    :: YKTOL=1.D-3
        DOUBLE PRECISION		    :: QMTOL=1.D-5
        DOUBLE PRECISION		    :: QMRTOL=1.D-3
        DOUBLE PRECISION		    :: STATTOL=1.D-32 !Obsolete
        INTEGER						:: NSTATMAX=1000
        INTEGER						:: NSAVE=1
        LOGICAL						:: GUESS_POWER=.TRUE.
        LOGICAL						:: QUASISTAT=.TRUE.
        CHARACTER(100)              :: BACKGROUND_SPECIE='AIR'
        LOGICAL						:: RADIATION=.FALSE.
        LOGICAL						:: BOUSSINESQ=.FALSE.
        LOGICAL						:: COMPRESSIBLE=.FALSE.
        LOGICAL						:: UNCOUPLED=.TRUE.
        LOGICAL						:: UNCOUPLED_THERMAL_WALLS=.FALSE.
        LOGICAL						:: GROUPED_RESULTS=.TRUE.
        LOGICAL						:: ATEC=.FALSE.
        DOUBLE PRECISION		    :: ATEC_BEGIN=274.D0
        DOUBLE PRECISION		    :: ATEC_END=140.D0
        LOGICAL						:: EXPLICIT=.FALSE. !obsolete
        LOGICAL						:: DEBUG=.FALSE.
        LOGICAL						:: DEBUGPLUS=.FALSE.
        LOGICAL						:: ISOTHERMAL=.TRUE.
        DOUBLE PRECISION		    :: QCRIT=2.D-3
        DOUBLE PRECISION		    :: DPCRIT=1.D-2
        DOUBLE PRECISION		    :: RECRIT=500.D0
        CHARACTER(100)              :: PSATMODEL='RANKINE'
        DOUBLE PRECISION		    :: ROTATION=0.D0
        CHARACTER(100)              :: OUTPUT='ALL'
        LOGICAL						:: CONSTANT_SPECIFIC_HEAT=.TRUE.
        CHARACTER(100)              :: VA_LOCID='null'
        INTEGER						:: CONVCRIT=0
        LOGICAL						:: ISOTHERMALNODE=.FALSE.
        DOUBLE PRECISION		    :: PRELAX=1.D0
        DOUBLE PRECISION		    :: LV=2.257D6
        DOUBLE PRECISION		    :: HRWRELAX=1.D0
        DOUBLE PRECISION		    :: HRWTOL=1.D-3
        DOUBLE PRECISION		    :: TWTOL=1.D-3
        INTEGER						:: NHTWSTATMAX=20
        LOGICAL						:: OUTPUT_MESSAGE_ONLY=.FALSE.
        LOGICAL						:: MOISTURE_NODES=.FALSE.
        LOGICAL						:: SUPERSATURATION=.FALSE.
        LOGICAL                     :: OUTPUT_HSRC_SPECPROD=.FALSE.
        LOGICAL                     :: ALL_SPEC_ARE_TRACE=.FALSE.
        INTEGER                     :: BUFFERSIZE=8760.
        !IMAGE
        DOUBLE PRECISION,POINTER    :: TIMESCALE
    END TYPE TYPE_MISC

    TYPE TYPE_MOD
        !PARAMETRES
        CHARACTER(100)						        :: ID='USR'
        CHARACTER(256)						        :: MODTYPE='null'
        INTEGER                                     :: NOUTPUTS=1000
        !CONSTANTES
        INTEGER								        :: IDN
        INTEGER								        :: IDNUSR
        !VARIABLES
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE	:: OUTPUTS
        !FLUX DE MASSE ET D'ENERGIE
        TYPE(FLUX_ME)						        :: FLUXES
    END TYPE TYPE_MOD

    TYPE TYPE_RADIANT_SURFACE
        DOUBLE PRECISION								:: EPS,TRSINT,TRSEXT,AREA,ABS,INT_THO_DIFF
        DOUBLE PRECISION,POINTER						:: TP,FRAD,FSUN,COSI,SLOPE,TRSFRAC
        DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE		:: F,H
        INTEGER											:: NBREPTS
        DOUBLE PRECISION,DIMENSION(NBREPTSMAX)			:: ANGLE,THO
    END TYPE TYPE_RADIANT_SURFACE

    TYPE TYPE_RADIANT_CAVITY
        INTEGER												:: NM
        TYPE(TYPE_RADIANT_SURFACE),DIMENSION(:),ALLOCATABLE	:: SURF
        DOUBLE PRECISION									:: STOT,STOTSQUARED,TRS_MOY,ABS_MOY
        DOUBLE PRECISION,POINTER							:: TWMEAN,HRAD
    END TYPE	TYPE_RADIANT_CAVITY


    TYPE TYPE_BUILDING
        TYPE(TYPE_MISC),POINTER    				:: MISC
        TYPE(TYPE_EXT),POINTER    				:: EXT
        TYPE(TYPE_BOUND),DIMENSION(:),POINTER   :: BOUND
        TYPE(TYPE_LOC),DIMENSION(:),POINTER    	:: LOC
        TYPE(TYPE_BRANCHE),DIMENSION(:),POINTER :: BRANCH
        TYPE(TYPE_HSRC),DIMENSION(:),POINTER   	:: HSRC
        TYPE(TYPE_PERSON),DIMENSION(:),POINTER  :: PERSON
        TYPE(TYPE_CTRL),DIMENSION(:),POINTER   	:: CTRL
        TYPE(TYPE_MUR),DIMENSION(:),POINTER   	:: WALL
        TYPE(TYPE_SURF),DIMENSION(:),POINTER   	:: SURF
        TYPE(TYPE_MAT),DIMENSION(:),POINTER   	:: MAT
        TYPE(TYPE_MOD),DIMENSION(:),POINTER   	:: MOD
        TYPE(TYPE_SPEC),DIMENSION(:),POINTER    :: SPEC
        TYPE(TYPE_RADIANT_CAVITY),DIMENSION(:),POINTER    :: RADCAV
    END TYPE TYPE_BUILDING

   !ENUMERATIONS
    !PSATMODEL
    enum, bind(C)
        enumerator :: psatmodel_rankine
        enumerator :: psatmodel_jrm
        enumerator :: psatmodel_clapeyron
        enumerator :: psatmodel_DEFAULT
    endenum


    enum, bind(C)
        enumerator :: branch_singularite            = ISHFT(1,8)
        enumerator :: branch_branche_kn             = ISHFT(1,9)
        enumerator :: branch_orifice                = ISHFT(1,10)

        enumerator :: branch_ouverture              = ISHFT(1,11)
        enumerator :: branch_ouverture_verticale

        enumerator :: branch_permeabilite           = ISHFT(1,12)

        enumerator :: branch_entree_family          = ISHFT(1,13)
        enumerator :: branch_entree_fixe
        enumerator :: branch_entree_auto
        enumerator :: branch_entree_auto_antiretour 
        enumerator :: branch_entree_hygro 

        enumerator :: branch_grille_family          = ISHFT(1,14)
        enumerator :: branch_grille_fixe 
        enumerator :: branch_grille_hygro
        enumerator :: branch_grille_hygro_gd
        enumerator :: branch_grille_auto

        enumerator :: branch_debit_family           = ISHFT(1,15)
        enumerator :: branch_DEBIT_CONSTANT
        enumerator :: branch_DEBIT_MASSIQUE_CONSTANT

        enumerator :: branch_bouche_family          = ISHFT(1,16)
        enumerator :: branch_bouche_hygro

        enumerator :: branch_ventilateur_family    = ISHFT(1,17)
        enumerator :: branch_ventilateur           
        enumerator :: branch_ventilateur_lin

        enumerator :: branch_TUYAU_family           = ISHFT(1,18)
        enumerator :: branch_TUYAU
        enumerator :: branch_TUYAU_DTU
        enumerator :: branch_TUYAU_NFE51766
        enumerator :: branch_TUYAU_MIXTE

        enumerator :: branch_EXTRACTEUR_family      = ISHFT(1,19)
        enumerator :: branch_EXTRACTEUR_MECANIQUE
        enumerator :: branch_EXTRACTEUR_STATIQUE
        enumerator :: branch_EXTRACTEUR_STATO_MECANIQUE

        enumerator :: branch_TUNNEL                 = ISHFT(1,20)

        enumerator :: branch_DIVERS_family          = ISHFT(1,30)
        enumerator :: branch_GROUPE_PQ
        enumerator :: branch_PASSIVE
        enumerator :: branch_PERMEABILITE_SIREN
        enumerator :: branch_DETALONNAGE_SIREN

        enumerator :: branch_DEFAULT                = ISHFT(1,31)
    end enum

    enum, bind(C)
        enumerator :: wall_thermal            = ISHFT(1,8)
        enumerator :: wall_hygrothermal       = ISHFT(1,9)

        enumerator :: wall_DEFAULT            = ISHFT(1,31)
    end enum

 
    enum, bind(C)
        enumerator :: ctrl_timer                = ISHFT(1,8)
        enumerator :: ctrl_ramp                 = ISHFT(1,9)
        enumerator :: ctrl_probe                = ISHFT(1,10)
        enumerator :: ctrl_setpoint             = ISHFT(1,11)
        enumerator :: ctrl_pid_setpoint         = ISHFT(1,12)
        enumerator :: ctrl_operator             = ISHFT(1,13)
        enumerator :: ctrl_comparison_operator  = ISHFT(1,14)
        enumerator :: ctrl_logical_operator     = ISHFT(1,15)
        enumerator :: ctrl_passive              = ISHFT(1,16)

        enumerator :: ctrl_DEFAULT              = ISHFT(1,31)
    endenum

    enum, bind(C)
        enumerator  :: ctrl_quantity_time        = ISHFT(1,8)

        enumerator  :: ctrl_quantity_t           = ISHFT(1,9)
        enumerator  :: ctrl_quantity_tw          = ISHFT(1,10)
        enumerator  :: ctrl_quantity_toper       = ISHFT(1,11)
        enumerator  :: ctrl_quantity_qv          = ISHFT(1,12)
        enumerator  :: ctrl_quantity_tflow       = ISHFT(1,13)
        enumerator  :: ctrl_quantity_twnode      = ISHFT(1,14)
        enumerator  :: ctrl_quantity_tp1         = ISHFT(1,15)
        enumerator  :: ctrl_quantity_tp2         = ISHFT(1,16)

        enumerator  :: ctrl_quantity_dt          = ISHFT(1,17)
        enumerator  :: ctrl_quantity_dtini       = ISHFT(1,18)
        enumerator  :: ctrl_quantity_dtext       = ISHFT(1,19)
        enumerator  :: ctrl_quantity_dp          = ISHFT(1,20)

        enumerator  :: ctrl_quantity_hrwnode     = ISHFT(1,21)
        enumerator  :: ctrl_quantity_hrinlet     = ISHFT(1,22)

        enumerator  :: ctrl_quantity_groupe1    =   ctrl_quantity_t         + ctrl_quantity_tw      +&
                                                    ctrl_quantity_toper     + ctrl_quantity_tflow   +&
                                                    ctrl_quantity_twnode    + ctrl_quantity_tp1     +  ctrl_quantity_tp2

        enumerator  :: ctrl_quantity_OTHER       = ISHFT(1,30)  ! ATTENTION : limite a 30 afin d'eviter un eventuel probleme avec les entiers signes
    endenum

    enum, bind(C)
        !enumerator  :: ctrl_function_time_average       = ISHFT(1,8)

        enumerator  :: ctrl_function_last               = ISHFT(1,9)
        enumerator  :: ctrl_function_first
        enumerator  :: ctrl_function_upward_last
        enumerator  :: ctrl_function_upward_first
        enumerator  :: ctrl_function_downward_last
        enumerator  :: ctrl_function_downward_first

        enumerator  :: ctrl_function_linear             = ISHFT(1,10)
        enumerator  :: ctrl_function_crenel
        enumerator  :: ctrl_function_hysteresis_upward
        enumerator  :: ctrl_function_hysteresis_downward

        enumerator  :: ctrl_function_value              = ISHFT(1,11)
        enumerator  :: ctrl_function_derivative
        enumerator  :: ctrl_function_previous_value
        enumerator  :: ctrl_function_time_average

        enumerator  :: ctrl_function_downward           = ISHFT(1,12)
        enumerator  :: ctrl_function_upward

        enumerator  :: ctrl_function_constant           = ISHFT(1,13)
        enumerator  :: ctrl_function_time
        enumerator  :: ctrl_function_hour
        enumerator  :: ctrl_function_weekday
        enumerator  :: ctrl_function_yearday
        enumerator  :: ctrl_function_vref

        enumerator  :: ctrl_function_add                = ISHFT(1,14)
        enumerator  :: ctrl_function_substract
        enumerator  :: ctrl_function_multiply
        enumerator  :: ctrl_function_divide
        enumerator  :: ctrl_function_power
        enumerator  :: ctrl_function_max
        enumerator  :: ctrl_function_min
        enumerator  :: ctrl_function_average

        enumerator  :: ctrl_function_equal              = ISHFT(1,15)
        enumerator  :: ctrl_function_notequal
        enumerator  :: ctrl_function_greaterequal
        enumerator  :: ctrl_function_greaterthan
        enumerator  :: ctrl_function_smallerequal
        enumerator  :: ctrl_function_smaller

        enumerator  :: ctrl_function_all                = ISHFT(1,16)
        enumerator  :: ctrl_function_any
        enumerator  :: ctrl_function_count
        enumerator  :: ctrl_function_not


        enumerator  :: ctrl_function_DEFAULT            = ISHFT(1,31)
    endenum


    CONTAINS

    SUBROUTINE CHECKREAD(NAME,LU,IOS)

    INTEGER :: II
    INTEGER, INTENT(OUT) ::IOS
    INTEGER, INTENT(IN)  :: LU
    CHARACTER(*), INTENT(IN) :: NAME
    CHARACTER(80) TEXT

    IOS = 0
    READLOOP: DO
        READ(LU,'(A)',END=10) TEXT
        TLOOP: DO II=1,72
            IF (TEXT(II:II)/='&' .AND. TEXT(II:II)/=' ') EXIT TLOOP
            IF (TEXT(II:II)=='&') THEN
                IF (TEXT(II+1:II+LEN_TRIM(NAME))==NAME) THEN
                    BACKSPACE(LU)
                    IOS = 1
                    EXIT READLOOP
                ELSE
                    CYCLE READLOOP
                ENDIF
            ENDIF
        ENDDO TLOOP
    ENDDO READLOOP
10  RETURN

    END SUBROUTINE CHECKREAD

    SUBROUTINE SHUTDOWN(MESSAGE,LUOUT)  ! Arrete le calcul apres ecriture d'un message
    CHARACTER(*) MESSAGE
    INTEGER		::LUOUT

    IF (LUOUT.NE.0) THEN
         WRITE(*,'(/A)') TRIM(MESSAGE)
    ENDIF
    WRITE(LUOUT,'(/A)') TRIM(MESSAGE)
    STOP

    END SUBROUTINE SHUTDOWN


 




    END MODULE TYPE_MATHIS_MODULE
