    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE READ_NML_MOD_MODULE

    USE TYPE_MATHIS_MODULE
    USE GLOBAL_VAR_MODULE,ONLY: LUOUT
    USE DATA_MUR_MODULE,ONLY: MURRT,N_MUR
    USE DATA_BOUND_MODULE,ONLY: BOUNDRT,N_BOUND
    USE DATA_LOC_MODULE,ONLY: LOCRT,N_LOC
    USE DATA_MAT_MODULE,ONLY: MATRT,N_MAT
    USE DATA_SURF_MODULE,ONLY: SURFRT,N_SURF
    USE DATA_BRANCHE_MODULE,ONLY: BRANCHERT,N_BRANCHE
    USE DATA_HSRC_MODULE,ONLY: HSRCRT,N_HSRC
    USE DATA_PERSON_MODULE,ONLY: PERSONRT,N_PERSON
    USE DATA_CTRL_MODULE,ONLY: CTRLRT,N_CTRL
    USE DATA_SPEC_MODULE,ONLY: SPECRT,N_SPEC


    USE DYNAMICLOAD_MODULE

    IMPLICIT NONE

    CHARACTER(100)			:: ID
    CHARACTER(256)			:: MODTYPE
    INTEGER                 :: NOUTPUTS

    ABSTRACT INTERFACE
    SUBROUTINE COUNT_NML_USR_INT(FILE,I)
    CHARACTER(256) FILE
    INTEGER I
    END SUBROUTINE
    END INTERFACE

    ABSTRACT INTERFACE
    SUBROUTINE READ_NML_USR_INT(FILE,I,J)
    CHARACTER(256) FILE
    INTEGER I,J
    END SUBROUTINE
    END INTERFACE

    PROCEDURE(COUNT_NML_USR_INT), POINTER :: COUNT_NML_USR
    PROCEDURE(READ_NML_USR_INT), POINTER :: READ_NML_USR
    
    NAMELIST /MOD/ ID,MODTYPE,NOUTPUTS

    CONTAINS

    SUBROUTINE DEFAULT_NML_MOD

    TYPE(TYPE_MOD)		:: OBJ

    ID=OBJ%ID
    MODTYPE=OBJ%MODTYPE
    NOUTPUTS=OBJ%NOUTPUTS

    END SUBROUTINE DEFAULT_NML_MOD
    
    SUBROUTINE PRINT_DEFAULT_NML_MOD(LUOUT)
    
    INTEGER,INTENT(IN)			:: LUOUT
    
    CALL DEFAULT_NML_MOD
    WRITE(LUOUT,NML=MOD,DELIM='APOSTROPHE ')
    
    END SUBROUTINE PRINT_DEFAULT_NML_MOD

    SUBROUTINE SET_NML_MOD(OBJ)

    TYPE(TYPE_MOD),INTENT(INOUT)	:: OBJ

    OBJ%ID=ID
    OBJ%MODTYPE=MODTYPE
    OBJ%NOUTPUTS=NOUTPUTS

    END SUBROUTINE SET_NML_MOD

    SUBROUTINE COUNT_NML_MOD(INPUT_FILE,LU1,LUOUT,N_OBJ)

    CHARACTER(256),INTENT(IN)	:: INPUT_FILE
    INTEGER,INTENT(IN)			:: LU1,LUOUT
    INTEGER,INTENT(OUT)			:: N_OBJ
    CHARACTER(256)				:: MESSAGE

    INTEGER							:: I,IOS

    OPEN(LU1,FILE=INPUT_FILE,ACTION='READ')

    N_OBJ=0
    REWIND(LU1)
    COUNT_OBJ_LOOP: DO
        CALL CHECKREAD('MOD',LU1,IOS)
        IF (IOS==0) EXIT COUNT_OBJ_LOOP
        CALL DEFAULT_NML_MOD
        READ(LU1,NML=MOD,END=209,ERR=210,IOSTAT=IOS)
210     IF (IOS>0) THEN
            IF (MODTYPE.NE.'null') THEN
                N_OBJ=N_OBJ+1
                !DEC$ IF .NOT.DEFINED(linux)
                PLIB = loadlibrary(TRIM(MODTYPE)//".dll"C)
                IF (PLIB==NULL) THEN
                    WRITE(MESSAGE,'(A,I3,A,A,A)') 'ERROR : Problem with &MOD line number ',N_OBJ,': library ',TRIM(MODTYPE)//".dll" ,' not found'
                    CALL SHUTDOWN(MESSAGE,LUOUT)
                ENDIF
                !DEC$ ELSE
                PLIB = dlopen(TRIM(MODTYPE)//".so"C,rtld_lazy)
                IF (PLIB==0) THEN
                    WRITE(*,*) "Error in dlopen: ", C_F_STRING(DLError())
                    WRITE(MESSAGE,'(A,I3,A,A,A)') 'ERROR : Problem with &MOD line number ',N_OBJ,': library ',TRIM(MODTYPE)//".so" ,' not found'
                    CALL SHUTDOWN(MESSAGE,LUOUT)
                ENDIF
                !DEC$ ENDIF

                !DEC$ IF .NOT.DEFINED(linux)
                QCOUNT = getprocaddress(PLIB, "COUNT_NML_USR"C)
                !DEC$ ELSE
                QCOUNT = dlsym(PLIB, "COUNT_NML_USR"C)
                !DEC$ ENDIF
                call C_F_PROCPOINTER (TRANSFER(QCOUNT, C_NULL_FUNPTR), COUNT_NML_USR)
                CALL COUNT_NML_USR(INPUT_FILE,N_OBJ)
            ELSE
                WRITE(LUOUT,NML=MOD)
                WRITE(MESSAGE,'(A,I3,A)') 'ERROR : Problem with &MOD line number ',N_OBJ+1,': check parameters names and values'
                CALL SHUTDOWN(MESSAGE,LUOUT)
            ENDIF
        ENDIF
    ENDDO COUNT_OBJ_LOOP
209 CLOSE(LU1)

    END SUBROUTINE COUNT_NML_MOD

    SUBROUTINE READ_NML_MOD(INPUT_FILE,LU1,LUOUT,OBJRT)

    CHARACTER(256),INTENT(IN)						:: INPUT_FILE
    INTEGER,INTENT(IN)								:: LU1,LUOUT
    TYPE(TYPE_MOD),DIMENSION(:),INTENT(INOUT)		:: OBJRT
    CHARACTER(256)									:: MESSAGE
    INTEGER											:: I,IMOD,IOS

    OPEN(LU1,FILE=INPUT_FILE,ACTION='READ')

    REWIND(LU1)
    IMOD=0
    SET_OBJ_LOOP: DO I=1,SIZE(OBJRT)
        CALL CHECKREAD('MOD',LU1,IOS)
        IF (IOS==0) EXIT SET_OBJ_LOOP
        CALL DEFAULT_NML_MOD
        !DEC$ IF .NOT.DEFINED(linux)
        READ(LU1,NML=MOD,END=209,ERR=210,IOSTAT=IOS)
210     PLIB = loadlibrary(TRIM(MODTYPE)//".dll"C)
        QREAD = getprocaddress(PLIB, "READ_NML_USR"C)
        !DEC$ ELSE
        READ(LU1,NML=MOD,END=209,ERR=211,IOSTAT=IOS)
211     PLIB = dlopen(TRIM(MODTYPE)//".so"C,rtld_lazy)
        QREAD = dlsym(PLIB, "READ_NML_USR"C)
        !DEC$ ENDIF
        call C_F_PROCPOINTER (TRANSFER(QREAD, C_NULL_FUNPTR), READ_NML_USR)
        IMOD=IMOD+1
        CALL READ_NML_USR(INPUT_FILE,IMOD,OBJRT(I)%IDNUSR)
        CALL SET_NML_MOD(OBJRT(I))
        !DEC$ IF .NOT.DEFINED(linux)
        FREE_STATUS=FreeLibrary(hLibModule=PLIB)
        !DEC$ ELSE
        FREE_STATUS=dlclose(PLIB)
        !DEC$ ENDIF
    ENDDO SET_OBJ_LOOP
209 CLOSE(LU1)

    END SUBROUTINE READ_NML_MOD


    END MODULE READ_NML_MOD_MODULE
