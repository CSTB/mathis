    !***********************************************************************
    !Copyright 2019, CSTB (Centre Scientifique et Technique du Batiment)
    !
    !This file is part of MATHIS.
    !
    !MATHIS is free software: you can redistribute it and/or modify
    !it under the terms of the GNU Lesser General Public License as published by
    !the Free Software Foundation, either version 3 of the License, or
    !(at your option) any later version.
    !
    !MATHIS is distributed in the hope that it will be useful,
    !but WITHOUT ANY WARRANTY; without even the implied warranty of
    !MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    !GNU Lesser General Public License for more details.
    !
    !You should have received a copy of the GNU Lesser General Public License
    !along with MATHIS.  If not, see <https://www.gnu.org/licenses/>.
    !***********************************************************************
    MODULE DATA_BRANCHE_MODULE

    USE TYPE_MATHIS_MODULE

    IMPLICIT NONE

    DOUBLE PRECISION, PARAMETER				:: MU=17.1D-6,GAMMA=0.868589D0

    TYPE(TYPE_BRANCHE),DIMENSION(:),ALLOCATABLE,TARGET	:: BRANCHERT
    INTEGER										:: N_BRANCHE,N_DUCT,N_OUV
    INTEGER,DIMENSION(:),ALLOCATABLE			:: IDNDUCT,IDNOUV





    CONTAINS

    

    ! Fonction pour comparer deux branche type
    !logical function BranchEqual(type1,type2)
    !    type(branchetype), intent(in) :: type1, type2
    !    BranchEqual = (type1%value == type2%value)
    !end function BranchEqual

    !logical function BranchNotEqual(type1,type2)
    !    type(branchetype), intent(in) :: type1, type2
    !    BranchNotEqual = (type1%value .NE. type2%value)
    !end function BranchNotEqual

    !logical function BranchFamily(type,famille)
    !    type(branchetype), intent(in) :: type, famille
    !    BranchFamily = IAND(type%value,famille%value)
    !end function BranchFamily

    !logical function BranchNotFamily(type,famille)
    !    type(branchetype), intent(in) :: type, famille
    !    BranchNotFamily = (IAND(type%value,famille%value) == 0)
    !end function BranchNotFamily


    !logical function BranchEqual(type1,enumeration)
    !    type(branchetype), intent(in) :: type1
    !    integer                       :: enumeration
    !    BranchEqual = (type1%value == enumeration)
    !end function BranchEqual

    !logical function BranchNotEqual(type1,enumeration)
    !    type(branchetype), intent(in) :: type1
    !    integer                       :: enumeration
    !    BranchNotEqual = (type1%value .NE. enumeration)
    !end function BranchNotEqual

    logical function BranchFamily(type,flag_famille)
        integer, intent(in) :: type, flag_famille
        BranchFamily = (IAND(type,flag_famille) /= 0)
    end function BranchFamily

    logical function BranchNotFamily(type,flag_famille)
    integer, intent(in) :: type, flag_famille
        BranchNotFamily = (IAND(type,flag_famille) == 0)
    end function BranchNotFamily


    SUBROUTINE TRANSLATE_BRANCHTYPE(OBJ)

    TYPE(TYPE_BRANCHE),INTENT(INOUT)	:: OBJ

    OBJ%enum_type = branch_DEFAULT

    SELECT CASE (OBJ%BRANCHTYPE)
    CASE ('SINGULARITY')
        OBJ%BRANCHTYPE='SINGULARITE'
    CASE ('BRANCH_KN')
        OBJ%BRANCHTYPE='BRANCHE_KN'
    CASE ('ORIFICE')
        OBJ%BRANCHTYPE='ORIFICE'
    CASE ('VERTICAL_OPENING')
        OBJ%BRANCHTYPE='OUVERTURE_VERTICALE'
    CASE ('LEAKAGE')
        OBJ%BRANCHTYPE='PERMEABILITE'
    CASE ('INLET_FIXED')
        OBJ%BRANCHTYPE='ENTREE_FIXE'
    CASE ('INLET_AUTO')
        OBJ%BRANCHTYPE='ENTREE_AUTO'
    CASE ('INLET_AUTO_CHECK_VALVE')
        OBJ%BRANCHTYPE='ENTREE_AUTO_ANTIRETOUR'
    CASE ('INLET_HYGRO')
        OBJ%BRANCHTYPE='ENTREE_HYGRO'
    CASE ('GRILLE_FIXED')
        OBJ%BRANCHTYPE='GRILLE_FIXE'
    CASE ('GRILLE_AUTO')
        OBJ%BRANCHTYPE='GRILLE_AUTO'
    CASE ('GRILLE_HYGRO')
        OBJ%BRANCHTYPE='GRILLE_HYGRO'
    CASE ('VENT_VOLUMEFLOW')
        OBJ%BRANCHTYPE='DEBIT_CONSTANT'
    CASE ('VENT_MASSFLOW')
        OBJ%BRANCHTYPE='DEBIT_MASSIQUE_CONSTANT'
    CASE ('VENT_HYGRO')
        OBJ%BRANCHTYPE='BOUCHE_HYGRO'
    CASE ('VENT_LINEAR') !deprecated
        OBJ%BRANCHTYPE='VENTILATEUR_LIN'
    CASE ('FAN_LINEAR')
        OBJ%BRANCHTYPE='VENTILATEUR_LIN'
    CASE ('FAN_QCONTROL')
        OBJ%BRANCHTYPE='VENTILATEUR'
    CASE ('DUCT')
        OBJ%BRANCHTYPE='TUYAU'
    CASE ('DUCT_DTU')
        OBJ%BRANCHTYPE='TUYAU_DTU'
    CASE ('DUCT_NFE51766')
        OBJ%BRANCHTYPE='TUYAU_NFE51766'
    CASE ('DUCT_MIXTE')
        OBJ%BRANCHTYPE='TUYAU_MIXTE'
    CASE ('FAN_MECHANICAL')
        OBJ%BRANCHTYPE='EXTRACTEUR_MECANIQUE'
    CASE ('FAN_STATIC')
        OBJ%BRANCHTYPE='EXTRACTEUR_STATIQUE'
    CASE ('FAN_STATO_MECHANICAL')
        OBJ%BRANCHTYPE='EXTRACTEUR_STATO_MECANIQUE'
    CASE ('TUNNEL')
        OBJ%BRANCHTYPE='TUNNEL'
    END SELECT

    ! definition du type enum

    SELECT CASE (OBJ%BRANCHTYPE)
    CASE ('SINGULARITE')
        OBJ%enum_type = branch_singularite

    CASE ('BRANCHE_KN')
        OBJ%enum_type = branch_branche_kn

    CASE ('ORIFICE')
        OBJ%enum_type = branch_orifice

    CASE ('OUVERTURE_VERTICALE')
        OBJ%enum_type = branch_OUVERTURE_VERTICALE

    CASE ('PERMEABILITE')
        OBJ%enum_type = branch_PERMEABILITE

    CASE ('ENTREE_FIXE')
        OBJ%enum_type = branch_ENTREE_FIXE
    CASE ('ENTREE_AUTO')
        OBJ%enum_type = branch_ENTREE_AUTO
    CASE ('ENTREE_AUTO_ANTIRETOUR')
        OBJ%enum_type = branch_ENTREE_AUTO_ANTIRETOUR
    CASE ('ENTREE_HYGRO')
        OBJ%enum_type = branch_ENTREE_HYGRO

    CASE ('GRILLE_FIXE')
        OBJ%enum_type = branch_GRILLE_FIXE
    CASE ('GRILLE_AUTO')
        OBJ%enum_type = branch_GRILLE_AUTO
    CASE ('GRILLE_HYGRO')
        OBJ%enum_type = branch_GRILLE_HYGRO
    CASE ('GRILLE_HYGRO_GD')
        OBJ%enum_type = branch_GRILLE_HYGRO_GD

    CASE ('DEBIT_CONSTANT')
        OBJ%enum_type = branch_DEBIT_CONSTANT
    CASE ('DEBIT_MASSIQUE_CONSTANT')
        OBJ%enum_type = branch_DEBIT_MASSIQUE_CONSTANT

    CASE ('BOUCHE_HYGRO')
        OBJ%enum_type = branch_BOUCHE_HYGRO

    CASE ('VENTILATEUR_LIN') !deprecated
        OBJ%enum_type = branch_VENTILATEUR_LIN
    CASE ('VENTILATEUR')
        OBJ%enum_type = branch_VENTILATEUR

    CASE ('TUYAU')
        OBJ%enum_type = branch_TUYAU
    CASE ('TUYAU_DTU')
        OBJ%enum_type = branch_TUYAU_DTU
    CASE ('TUYAU_NFE51766')
        OBJ%enum_type = branch_TUYAU_NFE51766
    CASE ('TUYAU_MIXTE')
        OBJ%enum_type = branch_TUYAU_MIXTE

    CASE ('EXTRACTEUR_MECANIQUE')
        OBJ%enum_type = branch_EXTRACTEUR_MECANIQUE
    CASE ('EXTRACTEUR_STATIQUE')
        OBJ%enum_type = branch_EXTRACTEUR_STATIQUE
    CASE ('EXTRACTEUR_STATO_MECANIQUE')
        OBJ%enum_type =branch_EXTRACTEUR_STATO_MECANIQUE

    CASE ('TUNNEL')
        OBJ%enum_type = branch_TUNNEL
    CASE ('PERMEABILITE_SIREN')
        OBJ%enum_type = branch_PERMEABILITE_SIREN
    END SELECT

    if(OBJ%enum_type == branch_DEFAULT) then
        print*, "Be carreful : Branch Type ", OBJ%BRANCHTYPE, " is unknown !"
    endif

    END SUBROUTINE TRANSLATE_BRANCHTYPE



    END MODULE DATA_BRANCHE_MODULE
